#include <string>
#include <cassert>
#include <znspin/utils/parameters.h>

int main(int argc, char** argv)
{
  znspin::parameters params(argc, argv);
  assert(params.name.size() > 0);
  assert(params.ppointname_Lx_kT_lm(1,1,1).size() > 0);
  assert(params.ppackname_Lx_kT_lm(1,1,1).size() > 0);
  return 0;
}
