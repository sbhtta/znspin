#include <cassert>
#include <iostream>
#include <array>
#include <znspin/model/spinconfig.h>

void test_accessors()
{
  const int  N      = 7;
  const auto LatNm  = qtools::lattice::name::square;
        auto psi    = znspin::spinconfig<N,LatNm>(std::array<int,2>{{10,10}});

  psi(1,1) = 5;
  psi(9,9) = psi(1,1) + 1;
  assert(psi.wrap(-1,-1) == 6);
  assert(psi.num_vertices() == 100);
  assert(psi.num_edges() == 200);
  assert(psi.num_edge_types() == 2);
  return;
}

void test_domain_wall_components()
{
  const int  N      = 3;
  const auto LatNm  = qtools::lattice::name::square;
        auto s      = znspin::spinconfig<N,LatNm>(std::array<int,2>{{4,4}});

  s.fill(std::vector<int>{
    0,1,0,2,
    1,0,2,0,
    0,1,0,2,
    1,0,0,0
  });

  s.label_domain_walls<false,false>([](const int si, const int sj, const int b) { return (si != sj); });
  assert(s.largest_domain_wall_size() == 28);

  s.label_domain_walls<false,false>(
    [](const int si, const int sj, const int b){ return ((si == 0) && (sj == 1)) || ((si == 1) && (sj == 0)); });
  assert(s.largest_domain_wall_size() == 16);

  s.label_domain_walls<true,false>(
    [](const int si, const int sj, const int b){ return ((si == 0) && (sj == 1)) || ((si == 1) && (sj == 0)); });
  assert(s.largest_domain_wall_size() == 14);

  s.label_domain_walls<true,true>(
    [](const int si, const int sj, const int b){ return ((si == 0) && (sj == 1)) || ((si == 1) && (sj == 0)); });
  assert(s.largest_domain_wall_size() == 14);
  assert(s.domain_walls_are_spanning() == true);

  s.label_domain_walls<true,true>(
    [](const int si, const int sj, const int b){ return ((si == 1) && (sj == 2)) || ((si == 2) && (sj == 1)); });
  assert(s.largest_domain_wall_size() == 1);
  assert(s.domain_walls_are_spanning() == false);

  s.label_domain_walls<true,true>(
    [](const int si, const int sj, const int b){ return ((si == 2) && (sj == 0)) || ((si == 0) && (sj == 2)); });
  assert(s.largest_domain_wall_size() == 11);
  assert(s.domain_walls_are_spanning() == false);

  s.label_vortex_strings<true,true>();
  return;
}

void
test_fill_from_sublattice_states_of()
{
  const int  N      = 3;
  const auto LatNm  = qtools::lattice::name::square;
        auto s      = znspin::spinconfig<N,LatNm>(std::array<int,2>{{4,4}});
        auto snew   = znspin::spinconfig<(N&1 ? 2*N : N),LatNm>(s.length());

  s.fill(std::vector<int>{
    0,1,0,2,
    1,0,2,0,
    0,1,0,2,
    1,0,0,0
  });
  
  s.copy_sublattice_states_to(snew);

  assert(snew(0,0) == znspin::sublattice_state<N>(s(0,0),0,0));
  assert(snew(1,0) == znspin::sublattice_state<N>(s(1,0),1,0));
  assert(snew(0,1) == znspin::sublattice_state<N>(s(0,1),0,1));
  assert(snew(1,1) == znspin::sublattice_state<N>(s(1,1),1,1));

  return;
}

int main()
{
  test_accessors();
  test_domain_wall_components();
  test_fill_from_sublattice_states_of();
  return 0;
}
