#include <cassert>
#include <array>
#include <znspin/update.h>

int main()
{
  const int  N        = 3;
  const auto LatNm    = qtools::lattice::name::square;
  const auto ExtFld   = znspin::external_field::on;
  const auto NoVxSupp = znspin::vortex_suppression::off;
  const auto VxSupp   = znspin::vortex_suppression::on;
  const auto length   = std::array<int,2>{{100,100}};

        auto s          = znspin::spinconfig<N,LatNm>(length);
  const auto Hnovxsupp  = znspin::hamiltonian<N,LatNm,ExtFld,NoVxSupp>();
  const auto Hvxsupp    = znspin::hamiltonian<N,LatNm,ExtFld,VxSupp>();
        auto r          = qtools::rng_vector(1);

  znspin::metropolis_update(Hvxsupp, s, r);
  znspin::effectivez3_state_metropolis_update(Hvxsupp, s, r);
  znspin::sublattice_state_metropolis_update(Hvxsupp, s, r);
  znspin::sublattice_effectivez3_state_metropolis_update(Hvxsupp, s, r);
  znspin::ucmsw_multicluster_update(Hnovxsupp, s, r);
  znspin::wolff_multicluster_update(Hnovxsupp, s, r);

  return 0;
}
