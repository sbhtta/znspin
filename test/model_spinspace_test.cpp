#include <cassert>
#include <znspin/model/spinspace.h>
#include <iostream>

void test_state_properties()
{
  assert(std::fabs(znspin::state_angle<4>(2) - M_PI) < 1e-4);

  assert(znspin::increment_state<4>(1) == 2);
  assert(znspin::increment_state<4>(3) == 0);
  assert(znspin::decrement_state<4>(2) == 1);
  assert(znspin::decrement_state<4>(0) == 3);
  return;
}

void test_state_difference()
{
  const int state_diff_N4[4][4] =  {{ 0,-1,-2,+1},
                                    {+1, 0,-1,-2},
                                    {+2,+1, 0,-1},
                                    {-1,+2,+1, 0}};
  for (int si = 0; si < 4; si++) {
    for (int sj = 0; sj < 4; sj++) {
      assert((znspin::state_difference<4>(si,sj) == state_diff_N4[si][sj]));
      assert((znspin::state_difference<4>(si-sj) == state_diff_N4[si][sj]));
    }
  }
  const int wrong_state_diff_N4[4][4] =  {{ 0,-1,+2,+1},
                                          {+1, 0,-1,+2},
                                          {+2,+1, 0,-1},
                                          {-1,+2,+1, 0}};
  for (int si = 0; si < 4; si++) {
    for (int sj = 0; sj < 4; sj++) {
      assert((znspin::wrong_state_difference<4>(si,sj) == wrong_state_diff_N4[si][sj]));
      assert((znspin::wrong_state_difference<4>(si-sj) == wrong_state_diff_N4[si][sj]));
    }
  }
  const int state_diff_N5[5][5] =  {{ 0,-1,-2,+2,+1},
                                    {+1, 0,-1,-2,+2},
                                    {+2,+1, 0,-1,-2},
                                    {-2,+2,+1, 0,-1},
                                    {-1,-2,+2,+1, 0}};
  for (int si = 0; si < 5; si++) {
    for (int sj = 0; sj < 5; sj++) {
      assert((znspin::state_difference<5>(si,sj) == state_diff_N5[si][sj]));
      assert((znspin::state_difference<5>(si-sj) == state_diff_N5[si][sj]));
    }
  }

  return;
}

void test_winding_number()
{
  constexpr const int N1 = 4;
  // test Z(N) symmetry of winding number:
  for (int s4 = 0; s4 < N1; s4++)
  for (int s3 = 0; s3 < N1; s3++)
  for (int s2 = 0; s2 < N1; s2++)
  for (int s1 = 0; s1 < N1; s1++) {
    const int w  = znspin::winding_number<N1>(s1+0,s2+0,s3+0,s4+0);
       assert((w == znspin::winding_number<N1>(s1+1,s2+1,s3+1,s4+1)));
       assert((w == znspin::winding_number<N1>(s1+2,s2+2,s3+2,s4+2)));
       assert((w == znspin::winding_number<N1>(s1+3,s2+3,s3+3,s4+3)));
  }

  assert((znspin::winding_number<4>(0,1,2,2)  ==  0));
  assert((znspin::winding_number<4>(2,2,1,0)  ==  0));
  return;
}

int main()
{
  test_state_properties();
  test_state_difference();
  test_winding_number();
  return 0;
}
