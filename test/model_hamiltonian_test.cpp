#include <cassert>
#include <znspin/model/hamiltonian.h>

int main()
{
  constexpr const int   N       = 3;
  constexpr const auto  LatNm   = qtools::lattice::name::square;
  constexpr const auto  ExtFld  = znspin::external_field::on;
  constexpr const auto  VxSupp  = znspin::vortex_suppression::on;
  constexpr const auto  Z3VxSupp  = znspin::vortex_suppression::on;

  const auto Esisj  = [&](const int si, const int sj, const int edg)
                      { return ((edg + 1) * (si != sj)); };
  const auto Esi    = [&](const int si)
                      { return (si == 0 ? -1.0 : 0.0); };

  constexpr const double kT     = 1.5;
  constexpr const double h      = 1.0;
  constexpr const double lambda = 2.0;
  constexpr const double lambda3 = 0.0;

  auto H = znspin::hamiltonian<N, LatNm, ExtFld, VxSupp, Z3VxSupp>();
  H.set_Esisj(Esisj);
  H.set_Esi(Esi);
  H.set_kT(kT);
  H.set_h(h);
  H.set_lambda(lambda);
  H.set_lambda3(lambda3);

  assert(H.num_states()               == N);
  assert(H.num_edge_types()           == qtools::lattice::info<LatNm>::max_num_outgoing_edges_per_vertex);
  assert(H.external_field_present()   == (ExtFld == znspin::external_field::on));
  assert(H.vortex_suppressed()        == ((VxSupp == znspin::vortex_suppression::on) || (VxSupp == znspin::vortex_suppression::full)));
  assert(H.vortex_fully_suppressed()  == (VxSupp == znspin::vortex_suppression::full));
  assert(H.z3vortex_suppressed()        == ((Z3VxSupp == znspin::vortex_suppression::on) || (Z3VxSupp == znspin::vortex_suppression::full)));
  assert(H.z3vortex_fully_suppressed()  == (Z3VxSupp == znspin::vortex_suppression::full));

  assert(std::fabs(H.kT()     - kT       ) < 1e-4);
  assert(std::fabs(H.beta()   - (1./kT)  ) < 1e-4);
  assert(std::fabs(H.h()      - h        ) < 1e-4);
  assert(std::fabs(H.lambda() - lambda   ) < 1e-4);
  assert(std::fabs(H.lambda3() - lambda3   ) < 1e-4);

  assert(std::fabs(H.Esisj(1,2,0) - 1.) < 1e-4);
  assert(std::fabs(H.Esisj(1,2,1) - 2.) < 1e-4);
  assert(std::fabs(H.Esi(0)       + 1.) < 1e-4);
  assert(std::fabs(H.Esi(1)       - 0.) < 1e-4);

  assert(std::fabs(H.exp_minus_beta_Esisj(1,2,0)                - 0.513417) < 1e-4);
  assert(std::fabs(H.exp_plus_beta_Esisj(1,2,0)                 - 1.947734) < 1e-4);
  assert(std::fabs(H.exp_minus_beta_h_Esf_minus_Esi(0,1)        - 1.947734) < 1e-4);
  assert(std::fabs(H.exp_minus_beta_lambda_wnf_minus_wni(+1,0)  - 0.263597) < 1e-4);

  return 0;
}
