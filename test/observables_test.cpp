#include <cassert>
#include <cmath>
#include <array>
#include <znspin/model.h>
#include <znspin/observables.h>

int main()
{
  constexpr const int  N      = 3;
  constexpr const auto LatNm  = qtools::lattice::name::square;
  constexpr const auto ExtFld = znspin::external_field::on;
  constexpr const auto VxSupp = znspin::vortex_suppression::on;
  constexpr const auto length = std::array<int,2>{{10,10}};

  auto psi = znspin::spinconfig<N,LatNm>(length);
  auto H = znspin::hamiltonian<N,LatNm,ExtFld,VxSupp>();
  H.set_Esisj([](const int si, const int sj, const int b){ return (b + 1) * (si != sj); });

  psi.fill(1);
  psi(1,1) = 0;
  psi(2,1) = 2;
  auto sfrac = znspin::state_fraction(psi);
  auto large = znspin::largest_states(sfrac);
  assert(large[0] == 1);
  assert(((large[1] == 0) && (large[2] == 2)) || ((large[1] == 0) && (large[2] == 2)));
  auto m_Zn  = znspin::order_parameter_Zn(sfrac);
  assert(std::fabs(std::arg(m_Zn) - (2. * M_PI / 3.)) < 1e-4);
  auto m_Sn  = znspin::order_parameter_Sn(sfrac);
  assert(m_Sn > 0.5);

  auto bfrac = znspin::edge_fraction(psi);
  auto espin = znspin::Esisj_per_spin(H, bfrac);
  assert(std::fabs(espin - (3. + 8.)/100.) < 1e-4);
  assert(std::fabs(znspin::domain_wall_fraction(bfrac,[](int si, int sj, int b){ return si != sj; }) - 7./200.) < 1e-4);
  
  auto vxfrac = znspin::vortex_fraction(psi);
  assert(std::fabs(vxfrac - (2./100.)) < 1e-4);
  auto svxfrac = znspin::sublattice_vortex_fraction(psi);
  assert(std::fabs(svxfrac[0] - (2./100.)) < 1e-4);
  assert(std::fabs(svxfrac[1] - (0./100.)) < 1e-4);

  auto C = znspin::correlator<LatNm>(length);
  C.add_data(psi, [](const int si, const int sj){ return (si != sj); });
  for (int i = 0; i < C.size(); i++) {
    assert(C.distance(i) >= 0);
    assert(C(i) >= 0);
  }

  return 0;
}
