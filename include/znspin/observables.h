#ifndef ZNSPIN_OBSERVABLES_H
#define ZNSPIN_OBSERVABLES_H

#include <znspin/observables/state.h>
#include <znspin/observables/edge.h>
#include <znspin/observables/vortex.h>
#include <znspin/observables/wrong_vortex.h>
#include <znspin/observables/corr.h>

#endif // ZNSPIN_OBSERVABLES_H
