#ifndef ZNSPIN_UPDATE_UCMSW_H
#define ZNSPIN_UPDATE_UCMSW_H

#include <vector>
#include <cmath>
#include <cassert>
#include <qtools/rng.h>
#include <znspin/model.h>

namespace znspin {

template<int ox0, int ox1, int ox2, int ox3,
         std::size_t N,
         qtools::lattice::name LatNm,
         external_field ExtFld,
         vortex_suppression VxSupp,
         vortex_suppression Z3VxSupp>
void
label_ucmsw_stochastic_clusters(const hamiltonian<N,LatNm,ExtFld,VxSupp,Z3VxSupp>& H,
	  			  			              spinconfig<N,LatNm>& s,
		 			 	  	 	              qtools::rng_vector& rng_states)
{
  // make local copy of random number generator state for this thread
  // for optimal OpenMP performance :
  auto local_rng_state = rng_states[0];

  s.misc_vector_resize(s.max_num_outgoing_edges());  
  s.misc_vector_clear();

  if (LatNm == qtools::lattice::name::square) {
    qtools::lattice::info<qtools::lattice::name::square>::for_each_vertex(s.length(),
      [&](const int x, const int y) {
        // copy nearest and next nearest neighbor spins of site (x,y):
        // i7 i8 i9
        // i4 i0 i6
        // i1 i2 i3
        const auto i0  = s.flatten_wrap_coords(x+0,y+0);
        const auto i6  = s.flatten_wrap_coords(x+1,y+0);
        const auto i8  = s.flatten_wrap_coords(x+0,y+1);
        const auto s0  = s[i0 ];
        const auto s6  = s[i6 ];
        const auto s8  = s[i8 ];

        if (1-H.exp_minus_beta_abs_Esisj_minus_Emax(s0,s6) > qtools::rng_uniform(local_rng_state)) {
          s.misc_vector(s.edge_index(0,i0)) = 1;
        }
        if (1-H.exp_minus_beta_abs_Esisj_minus_Emax(s0,s8) > qtools::rng_uniform(local_rng_state)) {
          s.misc_vector(s.edge_index(1,i0)) = 1;
        }
      }
    );
  } else if (LatNm == qtools::lattice::name::cube) {
    qtools::lattice::info<qtools::lattice::name::cube>::for_each_vertex(s.length(),
      [&](const int x, const int y, const int z) {
        // i7 i8 i9   i16 i17 i18   i25 i26 i27
        // i4 i5 i6   i13 i0  i15   i22 i23 i24
        // i1 i2 i3   i10 i11 i12   i19 i20 i21
        const auto i0  = s.flatten_wrap_coords(x+0,y+0,z+0);
        const auto i15 = s.flatten_wrap_coords(x+1,y+0,z+0);
        const auto i17 = s.flatten_wrap_coords(x+0,y+1,z+0);
        const auto i23 = s.flatten_wrap_coords(x+0,y+0,z+1);
        // copy spin values:
        const auto s0  = s[i0 ];
        const auto s15 = s[i15];
        const auto s17 = s[i17];
        const auto s23 = s[i23];

        if (1-H.exp_minus_beta_abs_Esisj_minus_Emax(s0,s15) > qtools::rng_uniform(local_rng_state)) {
          s.misc_vector(s.edge_index(0,i0)) = 1;
        }
        if (1-H.exp_minus_beta_abs_Esisj_minus_Emax(s0,s17) > qtools::rng_uniform(local_rng_state)) {
          s.misc_vector(s.edge_index(1,i0)) = 1;
        }
        if (1-H.exp_minus_beta_abs_Esisj_minus_Emax(s0,s23) > qtools::rng_uniform(local_rng_state)) {
          s.misc_vector(s.edge_index(2,i0)) = 1;
        }
      }
    );	
  } else if (LatNm == qtools::lattice::name::hypercube) {
    qtools::lattice::info<qtools::lattice::name::hypercube>::for_each_vertex(s.length(),
      [&](const int x, const int y, const int z, const int t) {
        // vertex neighbor map:
        // i61 i62 i63  i70 i71 i72  i79 i80 i81
        // i58 i59 i60  i67 i68 i69  i76 i77 i78
        // i55 i56 i57  i64 i65 i66  i73 i74 i75
        //
        // i34 i35 i36  i43 i44 i45  i52 i53 i54
        // i31 i32 i33  i40 i0  i42  i49 i50 i51
        // i28 i29 i30  i37 i38 i39  i46 i47 i48
        //
        // i7  i8  i9   i16 i17 i18  i25 i26 i27
        // i4  i5  i6   i13 i14 i15  i22 i23 i24
        // i1  i2  i3   i10 i11 i12  i19 i20 i21
        const auto i0  = s.flatten_wrap_coords(x+0,y+0,z+0,t+0);
        const auto i42 = s.flatten_wrap_coords(x+1,y+0,z+0,t+0);
        const auto i44 = s.flatten_wrap_coords(x+0,y+1,z+0,t+0);
        const auto i50 = s.flatten_wrap_coords(x+0,y+0,z+1,t+0);
        const auto i68 = s.flatten_wrap_coords(x+0,y+0,z+0,t+1);
        // copy spin values:
        const auto s0  = s[i0 ];
        const auto s42 = s[i42];
        const auto s44 = s[i44];
        const auto s50 = s[i50];
        const auto s68 = s[i68];
  
        if (1-H.exp_minus_beta_abs_Esisj_minus_Emax(s0,s42) > qtools::rng_uniform(local_rng_state)) {
          s.misc_vector(s.edge_index(0,i0)) = 1;
        }
        if (1-H.exp_minus_beta_abs_Esisj_minus_Emax(s0,s44) > qtools::rng_uniform(local_rng_state)) {
          s.misc_vector(s.edge_index(1,i0)) = 1;
        }
        if (1-H.exp_minus_beta_abs_Esisj_minus_Emax(s0,s50) > qtools::rng_uniform(local_rng_state)) {
          s.misc_vector(s.edge_index(2,i0)) = 1;
        }
        if (1-H.exp_minus_beta_abs_Esisj_minus_Emax(s0,s68) > qtools::rng_uniform(local_rng_state)) {
          s.misc_vector(s.edge_index(3,i0)) = 1;
        }
      }
    );	
  } // lattice check

  s.template label_stochastic_clusters<ox0,ox1,ox2,ox3>();

  // copy local random number generator state back to global state array:
  rng_states[0] = local_rng_state;
}
  
template<std::size_t N,
         qtools::lattice::name LatNm,
         external_field ExtFld,
         vortex_suppression VxSupp,
         vortex_suppression Z3VxSupp>
void
ucmsw_multicluster_update(const hamiltonian<N,LatNm,ExtFld,VxSupp,Z3VxSupp>& H,
	    						        spinconfig<N,LatNm>& s,
		 	  		 		 	        qtools::rng_vector& rng_states)
{

  assert(VxSupp == vortex_suppression::off);
  assert(Z3VxSupp == vortex_suppression::off);
  label_ucmsw_stochastic_clusters<false,false,false,false>(H, s, rng_states);

  // make local copy of random number generator state for this thread
  // for optimal OpenMP performance :
  auto local_rng_state = rng_states[0];

  // rotate each cluster by an arbitrary state:
  s.misc_vector_resize(s.num_vertices());
  for (int i = 0; i < s.num_vertices(); i++) {
    s.misc_vector(i) = (int)(N * qtools::rng_uniform(local_rng_state));
  }
  for (int i = 0; i < s.num_vertices(); i++) {
    s[i] = state_pullback<N>(s[i] + s.misc_vector(s.ccl().label(i)));
  }

  // copy local random number generator state back to global state array:
  rng_states[0] = local_rng_state;

  return;
}

} // namespace znspin

#endif // ZNSPIN_UPDATE_UCMSW_H
