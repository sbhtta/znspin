#ifndef ZNSPIN_UPDATE_SUBLATTICE_STATE_METROPOLIS_H
#define ZNSPIN_UPDATE_SUBLATTICE_STATE_METROPOLIS_H

#include <vector>
#include <cmath>
#include <cassert>
#include <qtools/rng.h>
#include <znspin/model.h>

namespace znspin {

template<std::size_t N, qtools::lattice::name LatNm, external_field ExtFld,
         vortex_suppression VxSupp, vortex_suppression Z3VxSupp>
void
sublattice_state_metropolis_update(const hamiltonian<N,LatNm,ExtFld,VxSupp,Z3VxSupp>& H,
								     	             spinconfig<N,LatNm>& s,
			 			 		 	                 qtools::rng_vector& rng_states,
                                   const bool flip_random = false)
{
    const int NEff = (N & 1 ? 2 * N : N);   
		// make local copy of random number generator state for this thread
		// for optimal OpenMP performance :
		auto local_rng_state = rng_states[0];

		if (LatNm == qtools::lattice::name::square) {
      qtools::lattice::info<qtools::lattice::name::square>::for_each_vertex(s.length(),
        [&](const int x, const int y) {
          // copy nearest and next nearest neighbor spins of site (x,y):
          // s7 s8 s9
          // s4 s0 s6
          // s1 s2 s3
          const auto s0 = s.wrap(x+0,y+0);
          const auto s1 = s.wrap(x-1,y-1);
          const auto s2 = s.wrap(x+0,y-1);
          const auto s3 = s.wrap(x+1,y-1);
          const auto s4 = s.wrap(x-1,y+0);
          //const auto s5 = s.wrap(x+0,y+0);
          const auto s6 = s.wrap(x+1,y+0);
          const auto s7 = s.wrap(x-1,y+1);
          const auto s8 = s.wrap(x+0,y+1);
          const auto s9 = s.wrap(x+1,y+1);

          const auto subs0 = sublattice_state<N>(s0,x+0,y+0);
          const auto subs1 = sublattice_state<N>(s1,x-1,y-1);
          const auto subs2 = sublattice_state<N>(s2,x+0,y-1);
          const auto subs3 = sublattice_state<N>(s3,x+1,y-1);
          const auto subs4 = sublattice_state<N>(s4,x-1,y+0);
          const auto subs6 = sublattice_state<N>(s6,x+1,y+0);
          const auto subs7 = sublattice_state<N>(s7,x-1,y+1);
          const auto subs8 = sublattice_state<N>(s8,x+0,y+1);
          const auto subs9 = sublattice_state<N>(s9,x+1,y+1);

          // new candidate spin (s0 + 1) mod N or (s0 - 1) mod N 
          // with equal probability:
          const auto sn =  flip_random
                           ? qtools::rng_uniform(local_rng_state) * N
                           : ((qtools::rng_uniform(local_rng_state) > 0.5) ?
                             increment_state<N>(s0) : decrement_state<N>(s0));
          const auto subsn = sublattice_state<N>(sn,x,y);

          // calculate probability of flipping s0 -> sn:
          double p = 1.0;

          // spin-spin interaction:
          p *= H.exp_minus_beta_Esisj(sn, s6, 0);
          p *= H.exp_minus_beta_Esisj(s4, sn, 0);
          p *= H.exp_minus_beta_Esisj(sn, s8, 1);
          p *= H.exp_minus_beta_Esisj(s2, sn, 1);
          p *= H.exp_plus_beta_Esisj( s0, s6, 0);
          p *= H.exp_plus_beta_Esisj( s4, s0, 0);
          p *= H.exp_plus_beta_Esisj( s0, s8, 1);
          p *= H.exp_plus_beta_Esisj( s2, s0, 1);

          // spin-external field interaction:
          if (H.external_field_present() == true) {
            p *= H.exp_minus_beta_h_Esf_minus_Esi(sn, s0);
          }

          // vortex suppression:
          if (H.vortex_suppressed() == true) {
            // s7 s8 s9
            // s4 s0 s6
            // s1 s2 s3
            const auto wn1f = winding_number_is_nonzero<NEff>(subsn,subs2,subs3,subs6);
            const auto wn1i = winding_number_is_nonzero<NEff>(subs0,subs2,subs3,subs6);
            const auto wn2f = winding_number_is_nonzero<NEff>(subsn,subs4,subs1,subs2);
            const auto wn2i = winding_number_is_nonzero<NEff>(subs0,subs4,subs1,subs2);
            const auto wn3f = winding_number_is_nonzero<NEff>(subsn,subs8,subs7,subs4);
            const auto wn3i = winding_number_is_nonzero<NEff>(subs0,subs8,subs7,subs4);
            const auto wn4f = winding_number_is_nonzero<NEff>(subsn,subs6,subs9,subs8);
            const auto wn4i = winding_number_is_nonzero<NEff>(subs0,subs6,subs9,subs8);
            if (H.vortex_fully_suppressed() == true) {
              p *= !(wn1f + wn2f + wn3f + wn4f);
            } else {
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn1f, wn1i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn2f, wn2i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn3f, wn3i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn4f, wn4i);
            }
          }

          // attempt spin flip:
          if (p > qtools::rng_uniform(local_rng_state)) {
            s.wrap(x, y) = sn;
          }
        }
			);
		} else if (LatNm == qtools::lattice::name::cube) {
      qtools::lattice::info<qtools::lattice::name::cube>::for_each_vertex(s.length(),
        [&](const int x, const int y, const int z) {
          // copy nearest and next nearest neighbor spins of site (x,y,z):
          // s7 s8 s9  s16 s17 s18  s25 s26 s27
          // s4 s5 s6  s13 s0  s15  s22 s23 s24
          // s1 s2 s3  s10 s11 s12  s19 s20 s21
          const auto s0  = s.wrap(x+0,y+0,z+0);
          const auto s2  = s.wrap(x+0,y-1,z-1);
          const auto s4  = s.wrap(x-1,y+0,z-1);
          const auto s5  = s.wrap(x+0,y+0,z-1);
          const auto s6  = s.wrap(x+1,y+0,z-1);
          const auto s8  = s.wrap(x+0,y+1,z-1);
          const auto s10 = s.wrap(x-1,y-1,z+0);
          const auto s11 = s.wrap(x+0,y-1,z+0);
          const auto s12 = s.wrap(x+1,y-1,z+0);
          const auto s13 = s.wrap(x-1,y+0,z+0);
          //const auto s14 = s.wrap(x+0,y+0,z+0);
          const auto s15 = s.wrap(x+1,y+0,z+0);
          const auto s16 = s.wrap(x-1,y+1,z+0);
          const auto s17 = s.wrap(x+0,y+1,z+0);
          const auto s18 = s.wrap(x+1,y+1,z+0);
          const auto s20 = s.wrap(x+0,y-1,z+1);
          const auto s22 = s.wrap(x-1,y+0,z+1);
          const auto s23 = s.wrap(x+0,y+0,z+1);
          const auto s24 = s.wrap(x+1,y+0,z+1);
          const auto s26 = s.wrap(x+0,y+1,z+1);

          const auto subs0  = sublattice_state<N>(s0 ,x+0,y+0,z+0);
          const auto subs2  = sublattice_state<N>(s2 ,x+0,y-1,z-1);
          const auto subs4  = sublattice_state<N>(s4 ,x-1,y+0,z-1);
          const auto subs5  = sublattice_state<N>(s5 ,x+0,y+0,z-1);
          const auto subs6  = sublattice_state<N>(s6 ,x+1,y+0,z-1);
          const auto subs8  = sublattice_state<N>(s8 ,x+0,y+1,z-1);
          const auto subs10 = sublattice_state<N>(s10,x-1,y-1,z+0);
          const auto subs11 = sublattice_state<N>(s11,x+0,y-1,z+0);
          const auto subs12 = sublattice_state<N>(s12,x+1,y-1,z+0);
          const auto subs13 = sublattice_state<N>(s13,x-1,y+0,z+0);
          //const auto subs14 = sublattice_state<N>(s14,x+0,y+0,z+0);
          const auto subs15 = sublattice_state<N>(s15,x+1,y+0,z+0);
          const auto subs16 = sublattice_state<N>(s16,x-1,y+1,z+0);
          const auto subs17 = sublattice_state<N>(s17,x+0,y+1,z+0);
          const auto subs18 = sublattice_state<N>(s18,x+1,y+1,z+0);
          const auto subs20 = sublattice_state<N>(s20,x+0,y-1,z+1);
          const auto subs22 = sublattice_state<N>(s22,x-1,y+0,z+1);
          const auto subs23 = sublattice_state<N>(s23,x+0,y+0,z+1);
          const auto subs24 = sublattice_state<N>(s24,x+1,y+0,z+1);
          const auto subs26 = sublattice_state<N>(s26,x+0,y+1,z+1);
          // new candidate spin (s0 + 1) mod N or (s0 - 1) mod N 
          // with equal probability:
          const auto sn =  flip_random
                           ? qtools::rng_uniform(local_rng_state) * N
                           : ((qtools::rng_uniform(local_rng_state) > 0.5) ?
                             increment_state<N>(s0) : decrement_state<N>(s0));
          const auto subsn = sublattice_state<N>(sn,x,y,z);

          // calculate probability of flipping s0 -> sn:
          double p = 1.0;

          // spin-spin interaction:
          p *= H.exp_minus_beta_Esisj(sn,  s15, 0);
          p *= H.exp_minus_beta_Esisj(s13, sn,  0);
          p *= H.exp_minus_beta_Esisj(sn,  s17, 1);
          p *= H.exp_minus_beta_Esisj(s11, sn,  1);
          p *= H.exp_minus_beta_Esisj(sn,  s23, 2);
          p *= H.exp_minus_beta_Esisj(s5,  sn,  2);
          p *= H.exp_plus_beta_Esisj(s0,  s15, 0);
          p *= H.exp_plus_beta_Esisj(s13, s0,  0);
          p *= H.exp_plus_beta_Esisj(s0,  s17, 1);
          p *= H.exp_plus_beta_Esisj(s11, s0,  1);
          p *= H.exp_plus_beta_Esisj(s0,  s23, 2);
          p *= H.exp_plus_beta_Esisj(s5,  s0,  2);
    
          // spin-external field interaction:
          if (H.external_field_present() == true) {
            p *= H.exp_minus_beta_h_Esf_minus_Esi(sn, s0);
          }

          // vortex suppression:
          if (H.vortex_suppressed() == true) {
            const auto wn1f  = winding_number_is_nonzero<NEff>(subsn,subs11,subs12,subs15);
            const auto wn1i  = winding_number_is_nonzero<NEff>(subs0,subs11,subs12,subs15);
            const auto wn2f  = winding_number_is_nonzero<NEff>(subsn,subs13,subs10,subs11);
            const auto wn2i  = winding_number_is_nonzero<NEff>(subs0,subs13,subs10,subs11);
            const auto wn3f  = winding_number_is_nonzero<NEff>(subsn,subs17,subs16,subs13);
            const auto wn3i  = winding_number_is_nonzero<NEff>(subs0,subs17,subs16,subs13);
            const auto wn4f  = winding_number_is_nonzero<NEff>(subsn,subs15,subs18,subs17);
            const auto wn4i  = winding_number_is_nonzero<NEff>(subs0,subs15,subs18,subs17);
            const auto wn5f  = winding_number_is_nonzero<NEff>(subsn,subs5,subs4,subs13);
            const auto wn5i  = winding_number_is_nonzero<NEff>(subs0,subs5,subs4,subs13);
            const auto wn6f  = winding_number_is_nonzero<NEff>(subsn,subs15,subs6,subs5);
            const auto wn6i  = winding_number_is_nonzero<NEff>(subs0,subs15,subs6,subs5);
            const auto wn7f  = winding_number_is_nonzero<NEff>(subsn,subs13,subs22,subs23);
            const auto wn7i  = winding_number_is_nonzero<NEff>(subs0,subs13,subs22,subs23);
            const auto wn8f  = winding_number_is_nonzero<NEff>(subsn,subs23,subs24,subs15);
            const auto wn8i  = winding_number_is_nonzero<NEff>(subs0,subs23,subs24,subs15);
            const auto wn9f  = winding_number_is_nonzero<NEff>(subsn,subs23,subs20,subs11);
            const auto wn9i  = winding_number_is_nonzero<NEff>(subs0,subs23,subs20,subs11);
            const auto wn10f = winding_number_is_nonzero<NEff>(subsn,subs17,subs26,subs23);
            const auto wn10i = winding_number_is_nonzero<NEff>(subs0,subs17,subs26,subs23);
            const auto wn11f = winding_number_is_nonzero<NEff>(subsn,subs5,subs8,subs17);
            const auto wn11i = winding_number_is_nonzero<NEff>(subs0,subs5,subs8,subs17);
            const auto wn12f = winding_number_is_nonzero<NEff>(subsn,subs11,subs2,subs5);
            const auto wn12i = winding_number_is_nonzero<NEff>(subs0,subs11,subs2,subs5);
            if (H.vortex_fully_suppressed() == true) {
              p *= !( wn1f + wn2f + wn3f + wn4f + wn5f + wn6f + wn7f
                    + wn8f + wn9f + wn10f + wn11f + wn12f);
            } else {
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn1f,  wn1i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn2f,  wn2i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn3f,  wn3i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn4f,  wn4i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn5f,  wn5i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn6f,  wn6i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn7f,  wn7i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn8f,  wn8i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn9f,  wn9i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn10f, wn10i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn11f, wn11i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn12f, wn12i);
            }
          }

          // attempt spin flip:
          if (p > qtools::rng_uniform(local_rng_state)) {
            s.wrap(x,y,z) = sn;
          }
        }
			);	
		} else if (LatNm == qtools::lattice::name::hypercube) {
      qtools::lattice::info<qtools::lattice::name::hypercube>::for_each_vertex(s.length(),
        [&](const int x, const int y, const int z, const int t) {
          // copy nearest and next nearest neighbor spins of site (x,y,z,t):
          // s61 s62 s63  s70 s71 s72  s79 s80 s81
          // s58 s59 s60  s67 s68 s69  s76 s77 s78
          // s55 s56 s57  s64 s65 s66  s73 s74 s75
          //
          // s34 s35 s36  s43 s44 s45  s52 s53 s54
          // s31 s32 s33  s40 s0  s42  s49 s50 s51
          // s28 s29 s30  s37 s38 s39  s46 s47 s48
          //
          // s7  s8  s9   s16 s17 s18  s25 s26 s27
          // s4  s5  s6   s13 s14 s15  s22 s23 s24
          // s1  s2  s3   s10 s11 s12  s19 s20 s21
          const auto s0  = s.wrap(x+0,y+0,z+0,t+0);
          const auto s5  = s.wrap(x+0,y+0,z-1,t-1);
          const auto s11 = s.wrap(x+0,y-1,z+0,t-1);
          const auto s13 = s.wrap(x-1,y+0,z+0,t-1);
          const auto s14 = s.wrap(x+0,y+0,z+0,t-1);
          const auto s15 = s.wrap(x+1,y+0,z+0,t-1);
          const auto s17 = s.wrap(x+0,y+1,z+0,t-1);
          const auto s23 = s.wrap(x+0,y+0,z+1,t-1);
          const auto s29 = s.wrap(x+0,y-1,z-1,t+0);
          const auto s31 = s.wrap(x-1,y+0,z-1,t+0);
          const auto s32 = s.wrap(x+0,y+0,z-1,t+0);
          const auto s33 = s.wrap(x+1,y+0,z-1,t+0);
          const auto s35 = s.wrap(x-1,y+1,z-1,t+0);
          const auto s37 = s.wrap(x-1,y-1,z+0,t+0);
          const auto s38 = s.wrap(x+0,y-1,z+0,t+0);
          const auto s39 = s.wrap(x+1,y-1,z+0,t+0);
          const auto s40 = s.wrap(x-1,y+0,z+0,t+0);
          const auto s42 = s.wrap(x+1,y+0,z+0,t+0);
          const auto s43 = s.wrap(x-1,y+1,z+0,t+0);
          const auto s44 = s.wrap(x+0,y+1,z+0,t+0);
          const auto s45 = s.wrap(x+1,y+1,z+0,t+0);
          const auto s47 = s.wrap(x+0,y-1,z+1,t+0);
          const auto s49 = s.wrap(x-1,y+0,z+1,t+0);
          const auto s50 = s.wrap(x+0,y+0,z+1,t+0);
          const auto s51 = s.wrap(x+1,y+0,z+1,t+0);
          const auto s53 = s.wrap(x+0,y+1,z+1,t+0);
          const auto s59 = s.wrap(x+0,y+0,z-1,t+1);
          const auto s65 = s.wrap(x+0,y-1,z+0,t+1);
          const auto s67 = s.wrap(x-1,y+0,z+0,t+1);
          const auto s68 = s.wrap(x+0,y+0,z+0,t+1);
          const auto s69 = s.wrap(x+1,y+0,z+0,t+1);
          const auto s71 = s.wrap(x+0,y+1,z+0,t+1);
          const auto s77 = s.wrap(x+0,y+0,z+1,t+1);

          const auto subs0  = sublattice_state<N>(s0 ,x+0,y+0,z+0,t+0);
          const auto subs5  = sublattice_state<N>(s5 ,x+0,y+0,z-1,t-1);
          const auto subs11 = sublattice_state<N>(s11,x+0,y-1,z+0,t-1);
          const auto subs13 = sublattice_state<N>(s13,x-1,y+0,z+0,t-1);
          const auto subs14 = sublattice_state<N>(s14,x+0,y+0,z+0,t-1);
          const auto subs15 = sublattice_state<N>(s15,x+1,y+0,z+0,t-1);
          const auto subs17 = sublattice_state<N>(s17,x+0,y+1,z+0,t-1);
          const auto subs23 = sublattice_state<N>(s23,x+0,y+0,z+1,t-1);
          const auto subs29 = sublattice_state<N>(s29,x+0,y-1,z-1,t+0);
          const auto subs31 = sublattice_state<N>(s31,x-1,y+0,z-1,t+0);
          const auto subs32 = sublattice_state<N>(s32,x+0,y+0,z-1,t+0);
          const auto subs33 = sublattice_state<N>(s33,x+1,y+0,z-1,t+0);
          const auto subs35 = sublattice_state<N>(s35,x-1,y+1,z-1,t+0);
          const auto subs37 = sublattice_state<N>(s37,x-1,y-1,z+0,t+0);
          const auto subs38 = sublattice_state<N>(s38,x+0,y-1,z+0,t+0);
          const auto subs39 = sublattice_state<N>(s39,x+1,y-1,z+0,t+0);
          const auto subs40 = sublattice_state<N>(s40,x-1,y+0,z+0,t+0);
          const auto subs42 = sublattice_state<N>(s42,x+1,y+0,z+0,t+0);
          const auto subs43 = sublattice_state<N>(s43,x-1,y+1,z+0,t+0);
          const auto subs44 = sublattice_state<N>(s44,x+0,y+1,z+0,t+0);
          const auto subs45 = sublattice_state<N>(s45,x+1,y+1,z+0,t+0);
          const auto subs47 = sublattice_state<N>(s47,x+0,y-1,z+1,t+0);
          const auto subs49 = sublattice_state<N>(s49,x-1,y+0,z+1,t+0);
          const auto subs50 = sublattice_state<N>(s50,x+0,y+0,z+1,t+0);
          const auto subs51 = sublattice_state<N>(s51,x+1,y+0,z+1,t+0);
          const auto subs53 = sublattice_state<N>(s53,x+0,y+1,z+1,t+0);
          const auto subs59 = sublattice_state<N>(s59,x+0,y+0,z-1,t+1);
          const auto subs65 = sublattice_state<N>(s65,x+0,y-1,z+0,t+1);
          const auto subs67 = sublattice_state<N>(s67,x-1,y+0,z+0,t+1);
          const auto subs68 = sublattice_state<N>(s68,x+0,y+0,z+0,t+1);
          const auto subs69 = sublattice_state<N>(s69,x+1,y+0,z+0,t+1);
          const auto subs71 = sublattice_state<N>(s71,x+0,y+1,z+0,t+1);
          const auto subs77 = sublattice_state<N>(s77,x+0,y+0,z+1,t+1);

          // new candidate spin (s0 + 1) mod N or (s0 - 1) mod N 
          // with equal probability:
          const auto sn =  flip_random
                           ? qtools::rng_uniform(local_rng_state) * N
                           : ((qtools::rng_uniform(local_rng_state) > 0.5) ?
                             increment_state<N>(s0) : decrement_state<N>(s0));
          const auto subsn = sublattice_state<N>(sn,x,y,z,t);

          // calculate probability of flipping s0 -> sn:
          double p = 1.0;

          // spin-spin interaction:
          p *= H.exp_minus_beta_Esisj(sn,  s42, 0);
          p *= H.exp_minus_beta_Esisj(s40, sn,  0);
          p *= H.exp_minus_beta_Esisj(sn,  s44, 1);
          p *= H.exp_minus_beta_Esisj(s38, sn,  1);
          p *= H.exp_minus_beta_Esisj(sn,  s50, 2);
          p *= H.exp_minus_beta_Esisj(s32, sn,  2);
          p *= H.exp_minus_beta_Esisj(sn,  s68, 3);
          p *= H.exp_minus_beta_Esisj(s14, sn,  3);
          p *= H.exp_plus_beta_Esisj(s0,  s42, 0);
          p *= H.exp_plus_beta_Esisj(s40, s0,  0);
          p *= H.exp_plus_beta_Esisj(s0,  s44, 1);
          p *= H.exp_plus_beta_Esisj(s38, s0,  1);
          p *= H.exp_plus_beta_Esisj(s0,  s50, 2);
          p *= H.exp_plus_beta_Esisj(s32, s0,  2);
          p *= H.exp_plus_beta_Esisj(s0,  s68, 3);
          p *= H.exp_plus_beta_Esisj(s14, s0,  3);

          // spin-external field interaction:
          if (H.external_field_present() == true) {
            p *= H.exp_minus_beta_h_Esf_minus_Esi(sn, s0);
          }

          // vortex suppression:
          if (H.vortex_suppressed() == true) {
            // x,y plane:
            const auto wn1f  = winding_number_is_nonzero<NEff>(subsn,subs42,subs45,subs44);
            const auto wn1i  = winding_number_is_nonzero<NEff>(subs0,subs42,subs45,subs44);
            const auto wn2f  = winding_number_is_nonzero<NEff>(subsn,subs44,subs43,subs40);
            const auto wn2i  = winding_number_is_nonzero<NEff>(subs0,subs44,subs43,subs40);
            const auto wn3f  = winding_number_is_nonzero<NEff>(subsn,subs40,subs37,subs38);
            const auto wn3i  = winding_number_is_nonzero<NEff>(subs0,subs40,subs37,subs38);
            const auto wn4f  = winding_number_is_nonzero<NEff>(subsn,subs38,subs39,subs42);
            const auto wn4i  = winding_number_is_nonzero<NEff>(subs0,subs38,subs39,subs42);
            // x,z plane:
            const auto wn5f  = winding_number_is_nonzero<NEff>(subsn,subs42,subs51,subs50);
            const auto wn5i  = winding_number_is_nonzero<NEff>(subs0,subs42,subs51,subs50);
            const auto wn6f  = winding_number_is_nonzero<NEff>(subsn,subs50,subs49,subs40);
            const auto wn6i  = winding_number_is_nonzero<NEff>(subs0,subs50,subs49,subs40);
            const auto wn7f  = winding_number_is_nonzero<NEff>(subsn,subs40,subs31,subs32);
            const auto wn7i  = winding_number_is_nonzero<NEff>(subs0,subs40,subs31,subs32);
            const auto wn8f  = winding_number_is_nonzero<NEff>(subsn,subs32,subs33,subs42);
            const auto wn8i  = winding_number_is_nonzero<NEff>(subs0,subs32,subs33,subs42);
            // x,t plane:
            const auto wn9f  = winding_number_is_nonzero<NEff>(subsn,subs42,subs69,subs68);
            const auto wn9i  = winding_number_is_nonzero<NEff>(subs0,subs42,subs69,subs68);
            const auto wn10f = winding_number_is_nonzero<NEff>(subsn,subs68,subs67,subs40);
            const auto wn10i = winding_number_is_nonzero<NEff>(subs0,subs68,subs67,subs40);
            const auto wn11f = winding_number_is_nonzero<NEff>(subsn,subs40,subs13,subs14);
            const auto wn11i = winding_number_is_nonzero<NEff>(subs0,subs40,subs13,subs14);
            const auto wn12f = winding_number_is_nonzero<NEff>(subsn,subs14,subs15,subs42);
            const auto wn12i = winding_number_is_nonzero<NEff>(subs0,subs14,subs15,subs42);
            // y,z plane:
            const auto wn13f = winding_number_is_nonzero<NEff>(subsn,subs44,subs53,subs50);
            const auto wn13i = winding_number_is_nonzero<NEff>(subs0,subs44,subs53,subs50);
            const auto wn14f = winding_number_is_nonzero<NEff>(subsn,subs50,subs47,subs38);
            const auto wn14i = winding_number_is_nonzero<NEff>(subs0,subs50,subs47,subs38);
            const auto wn15f = winding_number_is_nonzero<NEff>(subsn,subs38,subs29,subs32);
            const auto wn15i = winding_number_is_nonzero<NEff>(subs0,subs38,subs29,subs32);
            const auto wn16f = winding_number_is_nonzero<NEff>(subsn,subs32,subs35,subs44);
            const auto wn16i = winding_number_is_nonzero<NEff>(subs0,subs32,subs35,subs44);
            // y,t plane:
            const auto wn17f = winding_number_is_nonzero<NEff>(subsn,subs44,subs71,subs68);
            const auto wn17i = winding_number_is_nonzero<NEff>(subs0,subs44,subs71,subs68);
            const auto wn18f = winding_number_is_nonzero<NEff>(subsn,subs68,subs65,subs38);
            const auto wn18i = winding_number_is_nonzero<NEff>(subs0,subs68,subs65,subs38);
            const auto wn19f = winding_number_is_nonzero<NEff>(subsn,subs38,subs11,subs14);
            const auto wn19i = winding_number_is_nonzero<NEff>(subs0,subs38,subs11,subs14);
            const auto wn20f = winding_number_is_nonzero<NEff>(subsn,subs14,subs17,subs44);
            const auto wn20i = winding_number_is_nonzero<NEff>(subs0,subs14,subs17,subs44);
            // z,t plane:
            const auto wn21f = winding_number_is_nonzero<NEff>(subsn,subs50,subs77,subs68);
            const auto wn21i = winding_number_is_nonzero<NEff>(subs0,subs50,subs77,subs68);
            const auto wn22f = winding_number_is_nonzero<NEff>(subsn,subs68,subs59,subs32);
            const auto wn22i = winding_number_is_nonzero<NEff>(subs0,subs68,subs59,subs32);
            const auto wn23f = winding_number_is_nonzero<NEff>(subsn,subs32,subs5 ,subs14);
            const auto wn23i = winding_number_is_nonzero<NEff>(subs0,subs32,subs5 ,subs14);
            const auto wn24f = winding_number_is_nonzero<NEff>(subsn,subs14,subs23,subs50);
            const auto wn24i = winding_number_is_nonzero<NEff>(subs0,subs14,subs23,subs50);
            if (H.vortex_fully_suppressed() == true) {
              p *= !( wn1f  + wn2f  + wn3f  + wn4f  + wn5f  + wn6f  + wn7f
                    + wn8f  + wn9f  + wn10f + wn11f + wn12f + wn13f + wn14f
                    + wn15f + wn16f + wn17f + wn18f + wn19f + wn20f + wn21f
                    + wn22f + wn23f + wn24f);
            } else {
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn1f,  wn1i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn2f,  wn2i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn3f,  wn3i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn4f,  wn4i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn5f,  wn5i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn6f,  wn6i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn7f,  wn7i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn8f,  wn8i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn9f,  wn9i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn10f, wn10i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn11f, wn11i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn12f, wn12i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn13f, wn13i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn14f, wn14i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn15f, wn15i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn16f, wn16i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn17f, wn17i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn18f, wn18i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn19f, wn19i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn20f, wn20i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn21f, wn21i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn22f, wn22i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn23f, wn23i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn24f, wn24i);
            }
          }

          // attempt spin flip:
          if (p > qtools::rng_uniform(local_rng_state)) {
            s.wrap(x,y,z,t) = sn;
          }
        }
			);	
		} // lattice check
		
		// copy local random number generator state back to global state array:
		rng_states[0] = local_rng_state;

	return;
}

} // namespace znspin

#endif // ZNSPIN_UPDATE_SUBLATTICE_STATE_METROPOLIS_H
