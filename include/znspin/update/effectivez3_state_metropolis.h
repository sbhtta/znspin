#ifndef ZNSPIN_UPDATE_EFFECTIVEZ3_STATE_METROPOLIS_H
#define ZNSPIN_UPDATE_EFFECTIVEZ3_STATE_METROPOLIS_H

#include <vector>
#include <cmath>
#include <cassert>
#include <qtools/rng.h>
#include <znspin/model.h>

namespace znspin {

template<std::size_t N, qtools::lattice::name LatNm, external_field ExtFld,
         vortex_suppression VxSupp,vortex_suppression Z3VxSupp>
void
effectivez3_state_metropolis_update(const hamiltonian<N,LatNm,ExtFld,VxSupp,Z3VxSupp>& H,
					      	                  spinconfig<N,LatNm>& s,
			 		  	                      qtools::rng_vector& rng_states,
                                    const bool flip_random = false)
{
		// make local copy of random number generator state for this thread
		// for optimal OpenMP performance :
		auto local_rng_state = rng_states[0];

		if (LatNm == qtools::lattice::name::square) {
      qtools::lattice::info<qtools::lattice::name::square>::for_each_vertex(s.length(),
        [&](const int x, const int y) {
          // copy nearest and next nearest neighbor spins of site (x,y):
          // s7 s8 s9
          // s4 s0 s6
          // s1 s2 s3
          const auto s0 = s.wrap(x+0,y+0);
          const auto s1 = s.wrap(x-1,y-1);
          const auto s2 = s.wrap(x+0,y-1);
          const auto s3 = s.wrap(x+1,y-1);
          const auto s4 = s.wrap(x-1,y+0);
          //const auto s5 = s.wrap(x+0,y+0);
          const auto s6 = s.wrap(x+1,y+0);
          const auto s7 = s.wrap(x-1,y+1);
          const auto s8 = s.wrap(x+0,y+1);
          const auto s9 = s.wrap(x+1,y+1);

          // new candidate spin (s0 + 1) mod N or (s0 - 1) mod N 
          // with equal probability:
          const auto sn =  flip_random
                           ? qtools::rng_uniform(local_rng_state) * N
                           : ((qtools::rng_uniform(local_rng_state) > 0.5) ?
                             increment_state<N>(s0) : decrement_state<N>(s0));

          // calculate probability of flipping s0 -> sn:
          double p = 1.0;

          // spin-spin interaction:
          p *= H.exp_minus_beta_Esisj(sn, s6, 0);
          p *= H.exp_minus_beta_Esisj(s4, sn, 0);
          p *= H.exp_minus_beta_Esisj(sn, s8, 1);
          p *= H.exp_minus_beta_Esisj(s2, sn, 1);
          p *= H.exp_plus_beta_Esisj( s0, s6, 0);
          p *= H.exp_plus_beta_Esisj( s4, s0, 0);
          p *= H.exp_plus_beta_Esisj( s0, s8, 1);
          p *= H.exp_plus_beta_Esisj( s2, s0, 1);

          // spin-external field interaction:
          if (H.external_field_present() == true) {
            p *= H.exp_minus_beta_h_Esf_minus_Esi(sn, s0);
          }

          // vortex suppression:
          if (H.vortex_suppressed() == true) {
            // s7 s8 s9
            // s4 s0 s6
            // s1 s2 s3
            const auto wn1f = winding_number_is_nonzero<N>(sn,s2,s3,s6);
            const auto wn1i = winding_number_is_nonzero<N>(s0,s2,s3,s6);
            const auto wn2f = winding_number_is_nonzero<N>(sn,s4,s1,s2);
            const auto wn2i = winding_number_is_nonzero<N>(s0,s4,s1,s2);
            const auto wn3f = winding_number_is_nonzero<N>(sn,s8,s7,s4);
            const auto wn3i = winding_number_is_nonzero<N>(s0,s8,s7,s4);
            const auto wn4f = winding_number_is_nonzero<N>(sn,s6,s9,s8);
            const auto wn4i = winding_number_is_nonzero<N>(s0,s6,s9,s8);
            if (H.vortex_fully_suppressed() == true) {
              p *= !(wn1f + wn2f + wn3f + wn4f);
            } else {
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn1f, wn1i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn2f, wn2i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn3f, wn3i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn4f, wn4i);
            }
          }
          if (H.z3vortex_suppressed() == true) {
            const auto z3s0 = effectivez3_state(s0);
            const auto z3s1 = effectivez3_state(s1);
            const auto z3s2 = effectivez3_state(s2);
            const auto z3s3 = effectivez3_state(s3);
            const auto z3s4 = effectivez3_state(s4);
            const auto z3s6 = effectivez3_state(s6);
            const auto z3s7 = effectivez3_state(s7);
            const auto z3s8 = effectivez3_state(s8);
            const auto z3s9 = effectivez3_state(s9);

            const auto z3sn = effectivez3_state(sn);

            // s7 s8 s9
            // s4 s0 s6
            // s1 s2 s3
            const auto z3wn1f = winding_number_is_nonzero<3>(z3sn,z3s2,z3s3,z3s6);
            const auto z3wn1i = winding_number_is_nonzero<3>(z3s0,z3s2,z3s3,z3s6);
            const auto z3wn2f = winding_number_is_nonzero<3>(z3sn,z3s4,z3s1,z3s2);
            const auto z3wn2i = winding_number_is_nonzero<3>(z3s0,z3s4,z3s1,z3s2);
            const auto z3wn3f = winding_number_is_nonzero<3>(z3sn,z3s8,z3s7,z3s4);
            const auto z3wn3i = winding_number_is_nonzero<3>(z3s0,z3s8,z3s7,z3s4);
            const auto z3wn4f = winding_number_is_nonzero<3>(z3sn,z3s6,z3s9,z3s8);
            const auto z3wn4i = winding_number_is_nonzero<3>(z3s0,z3s6,z3s9,z3s8);
            if (H.z3vortex_fully_suppressed() == true) {
              p *= !(z3wn1f + z3wn2f + z3wn3f + z3wn4f);
            } else {
              p *= H.exp_minus_beta_lambda3_z3wnf_minus_z3wni(z3wn1f, z3wn1i);
              p *= H.exp_minus_beta_lambda3_z3wnf_minus_z3wni(z3wn2f, z3wn2i);
              p *= H.exp_minus_beta_lambda3_z3wnf_minus_z3wni(z3wn3f, z3wn3i);
              p *= H.exp_minus_beta_lambda3_z3wnf_minus_z3wni(z3wn4f, z3wn4i);
            }
          }

          // attempt spin flip:
          if (p > qtools::rng_uniform(local_rng_state)) {
            s.wrap(x, y) = sn;
          }
        }
			);
		} else if (LatNm == qtools::lattice::name::cube) {
      qtools::lattice::info<qtools::lattice::name::cube>::for_each_vertex(s.length(),
        [&](const int x, const int y, const int z) {
          // copy nearest and next nearest neighbor spins of site (x,y,z):
          // s7 s8 s9  s16 s17 s18  s25 s26 s27
          // s4 s5 s6  s13 s0  s15  s22 s23 s24
          // s1 s2 s3  s10 s11 s12  s19 s20 s21
          const auto s0  = s.wrap(x+0,y+0,z+0);
          const auto s2  = s.wrap(x+0,y-1,z-1);
          const auto s4  = s.wrap(x-1,y+0,z-1);
          const auto s5  = s.wrap(x+0,y+0,z-1);
          const auto s6  = s.wrap(x+1,y+0,z-1);
          const auto s8  = s.wrap(x+0,y+1,z-1);
          const auto s10 = s.wrap(x-1,y-1,z+0);
          const auto s11 = s.wrap(x+0,y-1,z+0);
          const auto s12 = s.wrap(x+1,y-1,z+0);
          const auto s13 = s.wrap(x-1,y+0,z+0);
          //const auto s14 = s.wrap(x+0,y+0,z+0);
          const auto s15 = s.wrap(x+1,y+0,z+0);
          const auto s16 = s.wrap(x-1,y+1,z+0);
          const auto s17 = s.wrap(x+0,y+1,z+0);
          const auto s18 = s.wrap(x+1,y+1,z+0);
          const auto s20 = s.wrap(x+0,y-1,z+1);
          const auto s22 = s.wrap(x-1,y+0,z+1);
          const auto s23 = s.wrap(x+0,y+0,z+1);
          const auto s24 = s.wrap(x+1,y+0,z+1);
          const auto s26 = s.wrap(x+0,y+1,z+1);

          // new candidate spin (s0 + 1) mod N or (s0 - 1) mod N 
          // with equal probability:
          const auto sn =  flip_random
                           ? qtools::rng_uniform(local_rng_state) * N
                           : ((qtools::rng_uniform(local_rng_state) > 0.5) ?
                             increment_state<N>(s0) : decrement_state<N>(s0));

          // calculate probability of flipping s0 -> sn:
          double p = 1.0;

          // spin-spin interaction:
          p *= H.exp_minus_beta_Esisj(sn,  s15, 0);
          p *= H.exp_minus_beta_Esisj(s13, sn,  0);
          p *= H.exp_minus_beta_Esisj(sn,  s17, 1);
          p *= H.exp_minus_beta_Esisj(s11, sn,  1);
          p *= H.exp_minus_beta_Esisj(sn,  s23, 2);
          p *= H.exp_minus_beta_Esisj(s5,  sn,  2);
          p *= H.exp_plus_beta_Esisj(s0,  s15, 0);
          p *= H.exp_plus_beta_Esisj(s13, s0,  0);
          p *= H.exp_plus_beta_Esisj(s0,  s17, 1);
          p *= H.exp_plus_beta_Esisj(s11, s0,  1);
          p *= H.exp_plus_beta_Esisj(s0,  s23, 2);
          p *= H.exp_plus_beta_Esisj(s5,  s0,  2);
    
          // spin-external field interaction:
          if (H.external_field_present() == true) {
            p *= H.exp_minus_beta_h_Esf_minus_Esi(sn, s0);
          }

          // vortex suppression:
          if (H.vortex_suppressed() == true) {
            const auto wn1f  = winding_number_is_nonzero<N>(sn,s11,s12,s15);
            const auto wn1i  = winding_number_is_nonzero<N>(s0,s11,s12,s15);
            const auto wn2f  = winding_number_is_nonzero<N>(sn,s13,s10,s11);
            const auto wn2i  = winding_number_is_nonzero<N>(s0,s13,s10,s11);
            const auto wn3f  = winding_number_is_nonzero<N>(sn,s17,s16,s13);
            const auto wn3i  = winding_number_is_nonzero<N>(s0,s17,s16,s13);
            const auto wn4f  = winding_number_is_nonzero<N>(sn,s15,s18,s17);
            const auto wn4i  = winding_number_is_nonzero<N>(s0,s15,s18,s17);
            const auto wn5f  = winding_number_is_nonzero<N>(sn,s5,s4,s13);
            const auto wn5i  = winding_number_is_nonzero<N>(s0,s5,s4,s13);
            const auto wn6f  = winding_number_is_nonzero<N>(sn,s15,s6,s5);
            const auto wn6i  = winding_number_is_nonzero<N>(s0,s15,s6,s5);
            const auto wn7f  = winding_number_is_nonzero<N>(sn,s13,s22,s23);
            const auto wn7i  = winding_number_is_nonzero<N>(s0,s13,s22,s23);
            const auto wn8f  = winding_number_is_nonzero<N>(sn,s23,s24,s15);
            const auto wn8i  = winding_number_is_nonzero<N>(s0,s23,s24,s15);
            const auto wn9f  = winding_number_is_nonzero<N>(sn,s23,s20,s11);
            const auto wn9i  = winding_number_is_nonzero<N>(s0,s23,s20,s11);
            const auto wn10f = winding_number_is_nonzero<N>(sn,s17,s26,s23);
            const auto wn10i = winding_number_is_nonzero<N>(s0,s17,s26,s23);
            const auto wn11f = winding_number_is_nonzero<N>(sn,s5,s8,s17);
            const auto wn11i = winding_number_is_nonzero<N>(s0,s5,s8,s17);
            const auto wn12f = winding_number_is_nonzero<N>(sn,s11,s2,s5);
            const auto wn12i = winding_number_is_nonzero<N>(s0,s11,s2,s5);
            if (H.vortex_fully_suppressed() == true) {
              p *= !( wn1f + wn2f + wn3f + wn4f + wn5f + wn6f + wn7f
                    + wn8f + wn9f + wn10f + wn11f + wn12f);
            } else {
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn1f,  wn1i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn2f,  wn2i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn3f,  wn3i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn4f,  wn4i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn5f,  wn5i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn6f,  wn6i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn7f,  wn7i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn8f,  wn8i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn9f,  wn9i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn10f, wn10i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn11f, wn11i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn12f, wn12i);
            }
          }

          // z3vortex suppression:
          if (H.z3vortex_suppressed() == true) {
            const auto z3s0  = effectivez3_state(s0);
            const auto z3s2  = effectivez3_state(s2);
            const auto z3s4  = effectivez3_state(s4);
            const auto z3s5  = effectivez3_state(s5);
            const auto z3s6  = effectivez3_state(s6);
            const auto z3s8  = effectivez3_state(s8);
            const auto z3s10 = effectivez3_state(s10);
            const auto z3s11 = effectivez3_state(s11);
            const auto z3s12 = effectivez3_state(s12);
            const auto z3s13 = effectivez3_state(s13);
            const auto z3s15 = effectivez3_state(s15);
            const auto z3s16 = effectivez3_state(s16);
            const auto z3s17 = effectivez3_state(s17);
            const auto z3s18 = effectivez3_state(s18);
            const auto z3s20 = effectivez3_state(s20);
            const auto z3s22 = effectivez3_state(s22);
            const auto z3s23 = effectivez3_state(s23);
            const auto z3s24 = effectivez3_state(s24);
            const auto z3s26 = effectivez3_state(s26);
            const auto z3sn = effectivez3_state(sn);

            const auto z3wn1f  = winding_number_is_nonzero<3>(z3sn,z3s11,z3s12,z3s15);
            const auto z3wn1i  = winding_number_is_nonzero<3>(z3s0,z3s11,z3s12,z3s15);
            const auto z3wn2f  = winding_number_is_nonzero<3>(z3sn,z3s13,z3s10,z3s11);
            const auto z3wn2i  = winding_number_is_nonzero<3>(z3s0,z3s13,z3s10,z3s11);
            const auto z3wn3f  = winding_number_is_nonzero<3>(z3sn,z3s17,z3s16,z3s13);
            const auto z3wn3i  = winding_number_is_nonzero<3>(z3s0,z3s17,z3s16,z3s13);
            const auto z3wn4f  = winding_number_is_nonzero<3>(z3sn,z3s15,z3s18,z3s17);
            const auto z3wn4i  = winding_number_is_nonzero<3>(z3s0,z3s15,z3s18,z3s17);
            const auto z3wn5f  = winding_number_is_nonzero<3>(z3sn,z3s5,z3s4,z3s13);
            const auto z3wn5i  = winding_number_is_nonzero<3>(z3s0,z3s5,z3s4,z3s13);
            const auto z3wn6f  = winding_number_is_nonzero<3>(z3sn,z3s15,z3s6,z3s5);
            const auto z3wn6i  = winding_number_is_nonzero<3>(z3s0,z3s15,z3s6,z3s5);
            const auto z3wn7f  = winding_number_is_nonzero<3>(z3sn,z3s13,z3s22,z3s23);
            const auto z3wn7i  = winding_number_is_nonzero<3>(z3s0,z3s13,z3s22,z3s23);
            const auto z3wn8f  = winding_number_is_nonzero<3>(z3sn,z3s23,z3s24,z3s15);
            const auto z3wn8i  = winding_number_is_nonzero<3>(z3s0,z3s23,z3s24,z3s15);
            const auto z3wn9f  = winding_number_is_nonzero<3>(z3sn,z3s23,z3s20,z3s11);
            const auto z3wn9i  = winding_number_is_nonzero<3>(z3s0,z3s23,z3s20,z3s11);
            const auto z3wn10f = winding_number_is_nonzero<3>(z3sn,z3s17,z3s26,z3s23);
            const auto z3wn10i = winding_number_is_nonzero<3>(z3s0,z3s17,z3s26,z3s23);
            const auto z3wn11f = winding_number_is_nonzero<3>(z3sn,z3s5,z3s8,z3s17);
            const auto z3wn11i = winding_number_is_nonzero<3>(z3s0,z3s5,z3s8,z3s17);
            const auto z3wn12f = winding_number_is_nonzero<3>(z3sn,z3s11,z3s2,z3s5);
            const auto z3wn12i = winding_number_is_nonzero<3>(z3s0,z3s11,z3s2,z3s5);
            if (H.z3vortex_fully_suppressed() == true) {
              p *= !( z3wn1f + z3wn2f + z3wn3f + z3wn4f + z3wn5f + z3wn6f + z3wn7f
                    + z3wn8f + z3wn9f + z3wn10f + z3wn11f + z3wn12f);
            } else {
              p *= H.exp_minus_beta_lambda3_z3wnf_minus_z3wni(z3wn1f,  z3wn1i);
              p *= H.exp_minus_beta_lambda3_z3wnf_minus_z3wni(z3wn2f,  z3wn2i);
              p *= H.exp_minus_beta_lambda3_z3wnf_minus_z3wni(z3wn3f,  z3wn3i);
              p *= H.exp_minus_beta_lambda3_z3wnf_minus_z3wni(z3wn4f,  z3wn4i);
              p *= H.exp_minus_beta_lambda3_z3wnf_minus_z3wni(z3wn5f,  z3wn5i);
              p *= H.exp_minus_beta_lambda3_z3wnf_minus_z3wni(z3wn6f,  z3wn6i);
              p *= H.exp_minus_beta_lambda3_z3wnf_minus_z3wni(z3wn7f,  z3wn7i);
              p *= H.exp_minus_beta_lambda3_z3wnf_minus_z3wni(z3wn8f,  z3wn8i);
              p *= H.exp_minus_beta_lambda3_z3wnf_minus_z3wni(z3wn9f,  z3wn9i);
              p *= H.exp_minus_beta_lambda3_z3wnf_minus_z3wni(z3wn10f, z3wn10i);
              p *= H.exp_minus_beta_lambda3_z3wnf_minus_z3wni(z3wn11f, z3wn11i);
              p *= H.exp_minus_beta_lambda3_z3wnf_minus_z3wni(z3wn12f, z3wn12i);
            }
          }

          // attempt spin flip:
          if (p > qtools::rng_uniform(local_rng_state)) {
            s.wrap(x,y,z) = sn;
          }
        }
			);	
		} else if (LatNm == qtools::lattice::name::hypercube) {
      qtools::lattice::info<qtools::lattice::name::hypercube>::for_each_vertex(s.length(),
        [&](const int x, const int y, const int z, const int t) {
          // copy nearest and next nearest neighbor spins of site (x,y,z,t):
          // s61 s62 s63  s70 s71 s72  s79 s80 s81
          // s58 s59 s60  s67 s68 s69  s76 s77 s78
          // s55 s56 s57  s64 s65 s66  s73 s74 s75
          //
          // s34 s35 s36  s43 s44 s45  s52 s53 s54
          // s31 s32 s33  s40 s0  s42  s49 s50 s51
          // s28 s29 s30  s37 s38 s39  s46 s47 s48
          //
          // s7  s8  s9   s16 s17 s18  s25 s26 s27
          // s4  s5  s6   s13 s14 s15  s22 s23 s24
          // s1  s2  s3   s10 s11 s12  s19 s20 s21
          const auto s0  = s.wrap(x+0,y+0,z+0,t+0);
          const auto s5  = s.wrap(x+0,y+0,z-1,t-1);
          const auto s11 = s.wrap(x+0,y-1,z+0,t-1);
          const auto s13 = s.wrap(x-1,y+0,z+0,t-1);
          const auto s14 = s.wrap(x+0,y+0,z+0,t-1);
          const auto s15 = s.wrap(x+1,y+0,z+0,t-1);
          const auto s17 = s.wrap(x+0,y+1,z+0,t-1);
          const auto s23 = s.wrap(x+0,y+0,z+1,t-1);
          const auto s29 = s.wrap(x+0,y-1,z-1,t+0);
          const auto s31 = s.wrap(x-1,y+0,z-1,t+0);
          const auto s32 = s.wrap(x+0,y+0,z-1,t+0);
          const auto s33 = s.wrap(x+1,y+0,z-1,t+0);
          const auto s35 = s.wrap(x-1,y+1,z-1,t+0);
          const auto s37 = s.wrap(x-1,y-1,z+0,t+0);
          const auto s38 = s.wrap(x+0,y-1,z+0,t+0);
          const auto s39 = s.wrap(x+1,y-1,z+0,t+0);
          const auto s40 = s.wrap(x-1,y+0,z+0,t+0);
          const auto s42 = s.wrap(x+1,y+0,z+0,t+0);
          const auto s43 = s.wrap(x-1,y+1,z+0,t+0);
          const auto s44 = s.wrap(x+0,y+1,z+0,t+0);
          const auto s45 = s.wrap(x+1,y+1,z+0,t+0);
          const auto s47 = s.wrap(x+0,y-1,z+1,t+0);
          const auto s49 = s.wrap(x-1,y+0,z+1,t+0);
          const auto s50 = s.wrap(x+0,y+0,z+1,t+0);
          const auto s51 = s.wrap(x+1,y+0,z+1,t+0);
          const auto s53 = s.wrap(x+0,y+1,z+1,t+0);
          const auto s59 = s.wrap(x+0,y+0,z-1,t+1);
          const auto s65 = s.wrap(x+0,y-1,z+0,t+1);
          const auto s67 = s.wrap(x-1,y+0,z+0,t+1);
          const auto s68 = s.wrap(x+0,y+0,z+0,t+1);
          const auto s69 = s.wrap(x+1,y+0,z+0,t+1);
          const auto s71 = s.wrap(x+0,y+1,z+0,t+1);
          const auto s77 = s.wrap(x+0,y+0,z+1,t+1);

          // new candidate spin (s0 + 1) mod N or (s0 - 1) mod N 
          // with equal probability:
          const auto sn =  flip_random
                           ? qtools::rng_uniform(local_rng_state) * N
                           : ((qtools::rng_uniform(local_rng_state) > 0.5) ?
                             increment_state<N>(s0) : decrement_state<N>(s0));

          // calculate probability of flipping s0 -> sn:
          double p = 1.0;

          // spin-spin interaction:
          p *= H.exp_minus_beta_Esisj(sn,  s42, 0);
          p *= H.exp_minus_beta_Esisj(s40, sn,  0);
          p *= H.exp_minus_beta_Esisj(sn,  s44, 1);
          p *= H.exp_minus_beta_Esisj(s38, sn,  1);
          p *= H.exp_minus_beta_Esisj(sn,  s50, 2);
          p *= H.exp_minus_beta_Esisj(s32, sn,  2);
          p *= H.exp_minus_beta_Esisj(sn,  s68, 3);
          p *= H.exp_minus_beta_Esisj(s14, sn,  3);
          p *= H.exp_plus_beta_Esisj(s0,  s42, 0);
          p *= H.exp_plus_beta_Esisj(s40, s0,  0);
          p *= H.exp_plus_beta_Esisj(s0,  s44, 1);
          p *= H.exp_plus_beta_Esisj(s38, s0,  1);
          p *= H.exp_plus_beta_Esisj(s0,  s50, 2);
          p *= H.exp_plus_beta_Esisj(s32, s0,  2);
          p *= H.exp_plus_beta_Esisj(s0,  s68, 3);
          p *= H.exp_plus_beta_Esisj(s14, s0,  3);

          // spin-external field interaction:
          if (H.external_field_present() == true) {
            p *= H.exp_minus_beta_h_Esf_minus_Esi(sn, s0);
          }

          // vortex suppression:
          if (H.vortex_suppressed() == true) {
            // x,y plane:
            const auto wn1f  = winding_number_is_nonzero<N>(sn,s42,s45,s44);
            const auto wn1i  = winding_number_is_nonzero<N>(s0,s42,s45,s44);
            const auto wn2f  = winding_number_is_nonzero<N>(sn,s44,s43,s40);
            const auto wn2i  = winding_number_is_nonzero<N>(s0,s44,s43,s40);
            const auto wn3f  = winding_number_is_nonzero<N>(sn,s40,s37,s38);
            const auto wn3i  = winding_number_is_nonzero<N>(s0,s40,s37,s38);
            const auto wn4f  = winding_number_is_nonzero<N>(sn,s38,s39,s42);
            const auto wn4i  = winding_number_is_nonzero<N>(s0,s38,s39,s42);
            // x,z plane:
            const auto wn5f  = winding_number_is_nonzero<N>(sn,s42,s51,s50);
            const auto wn5i  = winding_number_is_nonzero<N>(s0,s42,s51,s50);
            const auto wn6f  = winding_number_is_nonzero<N>(sn,s50,s49,s40);
            const auto wn6i  = winding_number_is_nonzero<N>(s0,s50,s49,s40);
            const auto wn7f  = winding_number_is_nonzero<N>(sn,s40,s31,s32);
            const auto wn7i  = winding_number_is_nonzero<N>(s0,s40,s31,s32);
            const auto wn8f  = winding_number_is_nonzero<N>(sn,s32,s33,s42);
            const auto wn8i  = winding_number_is_nonzero<N>(s0,s32,s33,s42);
            // x,t plane:
            const auto wn9f  = winding_number_is_nonzero<N>(sn,s42,s69,s68);
            const auto wn9i  = winding_number_is_nonzero<N>(s0,s42,s69,s68);
            const auto wn10f = winding_number_is_nonzero<N>(sn,s68,s67,s40);
            const auto wn10i = winding_number_is_nonzero<N>(s0,s68,s67,s40);
            const auto wn11f = winding_number_is_nonzero<N>(sn,s40,s13,s14);
            const auto wn11i = winding_number_is_nonzero<N>(s0,s40,s13,s14);
            const auto wn12f = winding_number_is_nonzero<N>(sn,s14,s15,s42);
            const auto wn12i = winding_number_is_nonzero<N>(s0,s14,s15,s42);
            // y,z plane:
            const auto wn13f = winding_number_is_nonzero<N>(sn,s44,s53,s50);
            const auto wn13i = winding_number_is_nonzero<N>(s0,s44,s53,s50);
            const auto wn14f = winding_number_is_nonzero<N>(sn,s50,s47,s38);
            const auto wn14i = winding_number_is_nonzero<N>(s0,s50,s47,s38);
            const auto wn15f = winding_number_is_nonzero<N>(sn,s38,s29,s32);
            const auto wn15i = winding_number_is_nonzero<N>(s0,s38,s29,s32);
            const auto wn16f = winding_number_is_nonzero<N>(sn,s32,s35,s44);
            const auto wn16i = winding_number_is_nonzero<N>(s0,s32,s35,s44);
            // y,t plane:
            const auto wn17f = winding_number_is_nonzero<N>(sn,s44,s71,s68);
            const auto wn17i = winding_number_is_nonzero<N>(s0,s44,s71,s68);
            const auto wn18f = winding_number_is_nonzero<N>(sn,s68,s65,s38);
            const auto wn18i = winding_number_is_nonzero<N>(s0,s68,s65,s38);
            const auto wn19f = winding_number_is_nonzero<N>(sn,s38,s11,s14);
            const auto wn19i = winding_number_is_nonzero<N>(s0,s38,s11,s14);
            const auto wn20f = winding_number_is_nonzero<N>(sn,s14,s17,s44);
            const auto wn20i = winding_number_is_nonzero<N>(s0,s14,s17,s44);
            // z,t plane:
            const auto wn21f = winding_number_is_nonzero<N>(sn,s50,s77,s68);
            const auto wn21i = winding_number_is_nonzero<N>(s0,s50,s77,s68);
            const auto wn22f = winding_number_is_nonzero<N>(sn,s68,s59,s32);
            const auto wn22i = winding_number_is_nonzero<N>(s0,s68,s59,s32);
            const auto wn23f = winding_number_is_nonzero<N>(sn,s32,s5 ,s14);
            const auto wn23i = winding_number_is_nonzero<N>(s0,s32,s5 ,s14);
            const auto wn24f = winding_number_is_nonzero<N>(sn,s14,s23,s50);
            const auto wn24i = winding_number_is_nonzero<N>(s0,s14,s23,s50);
            if (H.vortex_fully_suppressed() == true) {
              p *= !( wn1f  + wn2f  + wn3f  + wn4f  + wn5f  + wn6f  + wn7f
                    + wn8f  + wn9f  + wn10f + wn11f + wn12f + wn13f + wn14f
                    + wn15f + wn16f + wn17f + wn18f + wn19f + wn20f + wn21f
                    + wn22f + wn23f + wn24f);
            } else {
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn1f,  wn1i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn2f,  wn2i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn3f,  wn3i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn4f,  wn4i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn5f,  wn5i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn6f,  wn6i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn7f,  wn7i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn8f,  wn8i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn9f,  wn9i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn10f, wn10i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn11f, wn11i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn12f, wn12i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn13f, wn13i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn14f, wn14i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn15f, wn15i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn16f, wn16i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn17f, wn17i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn18f, wn18i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn19f, wn19i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn20f, wn20i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn21f, wn21i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn22f, wn22i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn23f, wn23i);
              p *= H.exp_minus_beta_lambda_wnf_minus_wni(wn24f, wn24i);
            }
          }

          // z3vortex suppression:
          if (H.z3vortex_suppressed() == true) {
            const auto z3s0  = effectivez3_state(s0);
            const auto z3s5  = effectivez3_state(s5);
            const auto z3s11 = effectivez3_state(s11);
            const auto z3s13 = effectivez3_state(s13);
            const auto z3s14 = effectivez3_state(s14);
            const auto z3s15 = effectivez3_state(s15);
            const auto z3s17 = effectivez3_state(s17);
            const auto z3s23 = effectivez3_state(s23);
            const auto z3s29 = effectivez3_state(s29);
            const auto z3s31 = effectivez3_state(s31);
            const auto z3s32 = effectivez3_state(s32);
            const auto z3s33 = effectivez3_state(s33);
            const auto z3s35 = effectivez3_state(s35);
            const auto z3s37 = effectivez3_state(s37);
            const auto z3s38 = effectivez3_state(s38);
            const auto z3s39 = effectivez3_state(s39);
            const auto z3s40 = effectivez3_state(s40);
            const auto z3s42 = effectivez3_state(s42);
            const auto z3s43 = effectivez3_state(s43);
            const auto z3s44 = effectivez3_state(s44);
            const auto z3s45 = effectivez3_state(s45);
            const auto z3s47 = effectivez3_state(s47);
            const auto z3s49 = effectivez3_state(s49);
            const auto z3s50 = effectivez3_state(s50);
            const auto z3s51 = effectivez3_state(s51);
            const auto z3s53 = effectivez3_state(s53);
            const auto z3s59 = effectivez3_state(s59);
            const auto z3s65 = effectivez3_state(s65);
            const auto z3s67 = effectivez3_state(s67);
            const auto z3s68 = effectivez3_state(s68);
            const auto z3s69 = effectivez3_state(s69);
            const auto z3s71 = effectivez3_state(s71);
            const auto z3s77 = effectivez3_state(s77);

            const auto z3sn = effectivez3_state(sn);

            // x,y plane:
            const auto z3wn1f  = winding_number_is_nonzero<3>(z3sn,z3s42,z3s45,z3s44);
            const auto z3wn1i  = winding_number_is_nonzero<3>(z3s0,z3s42,z3s45,z3s44);
            const auto z3wn2f  = winding_number_is_nonzero<3>(z3sn,z3s44,z3s43,z3s40);
            const auto z3wn2i  = winding_number_is_nonzero<3>(z3s0,z3s44,z3s43,z3s40);
            const auto z3wn3f  = winding_number_is_nonzero<3>(z3sn,z3s40,z3s37,z3s38);
            const auto z3wn3i  = winding_number_is_nonzero<3>(z3s0,z3s40,z3s37,z3s38);
            const auto z3wn4f  = winding_number_is_nonzero<3>(z3sn,z3s38,z3s39,z3s42);
            const auto z3wn4i  = winding_number_is_nonzero<3>(z3s0,z3s38,z3s39,z3s42);
            // x,z plane:
            const auto z3wn5f  = winding_number_is_nonzero<3>(z3sn,z3s42,z3s51,z3s50);
            const auto z3wn5i  = winding_number_is_nonzero<3>(z3s0,z3s42,z3s51,z3s50);
            const auto z3wn6f  = winding_number_is_nonzero<3>(z3sn,z3s50,z3s49,z3s40);
            const auto z3wn6i  = winding_number_is_nonzero<3>(z3s0,z3s50,z3s49,z3s40);
            const auto z3wn7f  = winding_number_is_nonzero<3>(z3sn,z3s40,z3s31,z3s32);
            const auto z3wn7i  = winding_number_is_nonzero<3>(z3s0,z3s40,z3s31,z3s32);
            const auto z3wn8f  = winding_number_is_nonzero<3>(z3sn,z3s32,z3s33,z3s42);
            const auto z3wn8i  = winding_number_is_nonzero<3>(z3s0,z3s32,z3s33,z3s42);
            // x,t plane:
            const auto z3wn9f  = winding_number_is_nonzero<3>(z3sn,z3s42,z3s69,z3s68);
            const auto z3wn9i  = winding_number_is_nonzero<3>(z3s0,z3s42,z3s69,z3s68);
            const auto z3wn10f = winding_number_is_nonzero<3>(z3sn,z3s68,z3s67,z3s40);
            const auto z3wn10i = winding_number_is_nonzero<3>(z3s0,z3s68,z3s67,z3s40);
            const auto z3wn11f = winding_number_is_nonzero<3>(z3sn,z3s40,z3s13,z3s14);
            const auto z3wn11i = winding_number_is_nonzero<3>(z3s0,z3s40,z3s13,z3s14);
            const auto z3wn12f = winding_number_is_nonzero<3>(z3sn,z3s14,z3s15,z3s42);
            const auto z3wn12i = winding_number_is_nonzero<3>(z3s0,z3s14,z3s15,z3s42);
            // y,z plane:
            const auto z3wn13f = winding_number_is_nonzero<3>(z3sn,z3s44,z3s53,z3s50);
            const auto z3wn13i = winding_number_is_nonzero<3>(z3s0,z3s44,z3s53,z3s50);
            const auto z3wn14f = winding_number_is_nonzero<3>(z3sn,z3s50,z3s47,z3s38);
            const auto z3wn14i = winding_number_is_nonzero<3>(z3s0,z3s50,z3s47,z3s38);
            const auto z3wn15f = winding_number_is_nonzero<3>(z3sn,z3s38,z3s29,z3s32);
            const auto z3wn15i = winding_number_is_nonzero<3>(z3s0,z3s38,z3s29,z3s32);
            const auto z3wn16f = winding_number_is_nonzero<3>(z3sn,z3s32,z3s35,z3s44);
            const auto z3wn16i = winding_number_is_nonzero<3>(z3s0,z3s32,z3s35,z3s44);
            // y,t plane:
            const auto z3wn17f = winding_number_is_nonzero<3>(z3sn,z3s44,z3s71,z3s68);
            const auto z3wn17i = winding_number_is_nonzero<3>(z3s0,z3s44,z3s71,z3s68);
            const auto z3wn18f = winding_number_is_nonzero<3>(z3sn,z3s68,z3s65,z3s38);
            const auto z3wn18i = winding_number_is_nonzero<3>(z3s0,z3s68,z3s65,z3s38);
            const auto z3wn19f = winding_number_is_nonzero<3>(z3sn,z3s38,z3s11,z3s14);
            const auto z3wn19i = winding_number_is_nonzero<3>(z3s0,z3s38,z3s11,z3s14);
            const auto z3wn20f = winding_number_is_nonzero<3>(z3sn,z3s14,z3s17,z3s44);
            const auto z3wn20i = winding_number_is_nonzero<3>(z3s0,z3s14,z3s17,z3s44);
            // z,t plane:
            const auto z3wn21f = winding_number_is_nonzero<3>(z3sn,z3s50,z3s77,z3s68);
            const auto z3wn21i = winding_number_is_nonzero<3>(z3s0,z3s50,z3s77,z3s68);
            const auto z3wn22f = winding_number_is_nonzero<3>(z3sn,z3s68,z3s59,z3s32);
            const auto z3wn22i = winding_number_is_nonzero<3>(z3s0,z3s68,z3s59,z3s32);
            const auto z3wn23f = winding_number_is_nonzero<3>(z3sn,z3s32,z3s5 ,z3s14);
            const auto z3wn23i = winding_number_is_nonzero<3>(z3s0,z3s32,z3s5 ,z3s14);
            const auto z3wn24f = winding_number_is_nonzero<3>(z3sn,z3s14,z3s23,z3s50);
            const auto z3wn24i = winding_number_is_nonzero<3>(z3s0,z3s14,z3s23,z3s50);
            if (H.z3vortex_fully_suppressed() == true) {
              p *= !( z3wn1f  + z3wn2f  + z3wn3f  + z3wn4f  + z3wn5f  + z3wn6f  + z3wn7f
                    + z3wn8f  + z3wn9f  + z3wn10f + z3wn11f + z3wn12f + z3wn13f + z3wn14f
                    + z3wn15f + z3wn16f + z3wn17f + z3wn18f + z3wn19f + z3wn20f + z3wn21f
                    + z3wn22f + z3wn23f + z3wn24f);
            } else {
              p *= H.exp_minus_beta_lambda3_z3wnf_minus_z3wni(z3wn1f,  z3wn1i);
              p *= H.exp_minus_beta_lambda3_z3wnf_minus_z3wni(z3wn2f,  z3wn2i);
              p *= H.exp_minus_beta_lambda3_z3wnf_minus_z3wni(z3wn3f,  z3wn3i);
              p *= H.exp_minus_beta_lambda3_z3wnf_minus_z3wni(z3wn4f,  z3wn4i);
              p *= H.exp_minus_beta_lambda3_z3wnf_minus_z3wni(z3wn5f,  z3wn5i);
              p *= H.exp_minus_beta_lambda3_z3wnf_minus_z3wni(z3wn6f,  z3wn6i);
              p *= H.exp_minus_beta_lambda3_z3wnf_minus_z3wni(z3wn7f,  z3wn7i);
              p *= H.exp_minus_beta_lambda3_z3wnf_minus_z3wni(z3wn8f,  z3wn8i);
              p *= H.exp_minus_beta_lambda3_z3wnf_minus_z3wni(z3wn9f,  z3wn9i);
              p *= H.exp_minus_beta_lambda3_z3wnf_minus_z3wni(z3wn10f, z3wn10i);
              p *= H.exp_minus_beta_lambda3_z3wnf_minus_z3wni(z3wn11f, z3wn11i);
              p *= H.exp_minus_beta_lambda3_z3wnf_minus_z3wni(z3wn12f, z3wn12i);
              p *= H.exp_minus_beta_lambda3_z3wnf_minus_z3wni(z3wn13f, z3wn13i);
              p *= H.exp_minus_beta_lambda3_z3wnf_minus_z3wni(z3wn14f, z3wn14i);
              p *= H.exp_minus_beta_lambda3_z3wnf_minus_z3wni(z3wn15f, z3wn15i);
              p *= H.exp_minus_beta_lambda3_z3wnf_minus_z3wni(z3wn16f, z3wn16i);
              p *= H.exp_minus_beta_lambda3_z3wnf_minus_z3wni(z3wn17f, z3wn17i);
              p *= H.exp_minus_beta_lambda3_z3wnf_minus_z3wni(z3wn18f, z3wn18i);
              p *= H.exp_minus_beta_lambda3_z3wnf_minus_z3wni(z3wn19f, z3wn19i);
              p *= H.exp_minus_beta_lambda3_z3wnf_minus_z3wni(z3wn20f, z3wn20i);
              p *= H.exp_minus_beta_lambda3_z3wnf_minus_z3wni(z3wn21f, z3wn21i);
              p *= H.exp_minus_beta_lambda3_z3wnf_minus_z3wni(z3wn22f, z3wn22i);
              p *= H.exp_minus_beta_lambda3_z3wnf_minus_z3wni(z3wn23f, z3wn23i);
              p *= H.exp_minus_beta_lambda3_z3wnf_minus_z3wni(z3wn24f, z3wn24i);
            }
          }

          // attempt spin flip:
          if (p > qtools::rng_uniform(local_rng_state)) {
            s.wrap(x,y,z,t) = sn;
          }
        }
			);	
		} // lattice check
		
		// copy local random number generator state back to global state array:
		rng_states[0] = local_rng_state;

	return;
}

} // namespace znspin

#endif // ZNSPIN_UPDATE_EFFECTIVEZ3_STATE_METROPOLIS_H
