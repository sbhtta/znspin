#ifndef ZNSPIN_MODEL_SPINSPACE_H
#define ZNSPIN_MODEL_SPINSPACE_H

#include <cmath>
#include <cassert>

namespace znspin {

template <std::size_t N>
inline double
state_angle(const int s)
{
  assert((0 <= s) && (s < static_cast<int>(N)));
  return 2. * M_PI * s / (1. * N);
}

template <std::size_t N>
inline int 
increment_state(const int s)
{
  assert((0 <= s) && (s < static_cast<int>(N)));
  return (s == (N - 1) ? 0 : s + 1);
}

template <std::size_t N>
inline int 
decrement_state(const int s)
{
  assert((0 <= s) && (s < static_cast<int>(N)));
  return (s == 0 ? (N - 1) : s - 1);
}

template <std::size_t N>
inline int 
state_pullback(const int s)
{
  assert((-static_cast<int>(N) <= s) && (s < 2 * static_cast<int>(N)));
  return (s >= static_cast<int>(N) ? s - static_cast<int>(N) : s < 0 ? s + static_cast<int>(N) : s);
}

template <std::size_t N>
inline int 
state_difference(const int delta)
{
  assert((-static_cast<int>(N) <= delta ) && (delta <= +static_cast<int>(N)));
  if      (delta >  +(static_cast<int>(N) / 2))  return delta - static_cast<int>(N);
  else if (delta <  -(static_cast<int>(N) / 2))  return delta + static_cast<int>(N);
  else                                           return delta;
}

template <std::size_t N>
inline int 
state_difference(const int s1, const int s2)
{
  assert((0 <= s1) && (s1 < static_cast<int>(N)));
  assert((0 <= s2) && (s2 < static_cast<int>(N)));
  return state_difference<N>(s1 - s2);
}

template <std::size_t N>
inline int
winding_number(const int sa, const int sb, 
    			     const int sc, const int sd)
{
    const auto Dba = state_difference<N>(sb - sa);
    const auto Dcb = state_difference<N>(sc - sb);
    const auto Ddc = state_difference<N>(sd - sc);
    const auto Dad = state_difference<N>(sa - sd);
    return (Dba + Dcb + Ddc + Dad) / static_cast<int>(N);
}

template <std::size_t N>
inline int
winding_number_is_nonzero(const int sa, const int sb, 
				                  const int sc, const int sd)
{
  if (N == 3) {
    return (((sa != sb) + (sb != sc) + (sc != sd) + (sd != sa)) == 3);
  } else {
    return (winding_number<N>(sa,sb,sc,sd) != 0);
  }
}

template <std::size_t N>
inline int
sublattice_state(const int s,
                 const int x,
                 const int y = 0,
                 const int z = 0,
                 const int t = 0)
{
  if (N & 1) {
    return state_pullback<2 * N>( s * 2 + ((x + y + z + t)&1) * static_cast<int>(N) );
  } else {
    return state_pullback<N>( s + ((x + y + z + t)&1) * static_cast<int>(N) / 2 );
  }
}

inline int
effectivez3_state(const int s)
{
  return (s < 3 ? s : s - 3);
}

template <std::size_t N>
inline int 
wrong_state_difference(const int delta)
{
  assert((-static_cast<int>(N) <= delta ) && (delta <= +static_cast<int>(N)));
  if (N&1) {
    if      (delta >  +(static_cast<int>(N) / 2)) return delta - static_cast<int>(N);
    else if (delta <  -(static_cast<int>(N) / 2)) return delta + static_cast<int>(N);
    else                                          return delta;
  } else {
    if      (delta >  +(static_cast<int>(N) / 2)) return delta - static_cast<int>(N);
    else if (delta <= -(static_cast<int>(N) / 2)) return delta + static_cast<int>(N);
    else                                          return delta;
  }
}

template <std::size_t N>
inline int 
wrong_state_difference(const int s1, const int s2)
{
  assert((0 <= s1) && (s1 < static_cast<int>(N)));
  assert((0 <= s2) && (s2 < static_cast<int>(N)));
  return wrong_state_difference<N>(s1 - s2);
}

template <std::size_t N>
inline int
wrong_winding_number(const int sa, const int sb, 
    	       		     const int sc, const int sd)
{
    const auto Dba = wrong_state_difference<N>(sb - sa);
    const auto Dcb = wrong_state_difference<N>(sc - sb);
    const auto Ddc = wrong_state_difference<N>(sd - sc);
    const auto Dad = wrong_state_difference<N>(sa - sd);
    return (Dba + Dcb + Ddc + Dad) / static_cast<int>(N);
}

template <std::size_t N>
inline int
wrong_winding_number_is_nonzero(const int sa, const int sb, 
			       	                  const int sc, const int sd)
{
  if (N == 3) {
    return (((sa != sb) + (sb != sc) + (sc != sd) + (sd != sa)) == 3);
  } else {
    return (wrong_winding_number<N>(sa,sb,sc,sd) != 0);
  }
}

} // namespace znspin

#endif // ZNSPIN_MODEL_SPINSPACE_H
