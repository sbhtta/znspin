#ifndef ZNSPIN_MODEL_HAMILTONIAN_H
#define ZNSPIN_MODEL_HAMILTONIAN_H

#include <array>
#include <cmath>
#include <cassert>
#include <vector>
#include <utility>
#include <functional>

#include <qtools/multidim.h>
#include <qtools/lattice.h>
#include <znspin/model/spinspace.h>

namespace znspin {

enum class vortex_suppression {
  off,
  on,
  full,
};

enum class external_field {
  off,
  on,
};

template<std::size_t            N,
				 qtools::lattice::name 	LatNm,  
				 external_field 		    ExtFld,
				 vortex_suppression     VxSupp = vortex_suppression::off,
				 vortex_suppression     Z3VxSupp = vortex_suppression::off>
class hamiltonian
{
	public:
  // types and constants:
  using LatInfo = qtools::lattice::info<LatNm>;
  static constexpr const int MaxWn       = 2;
  static constexpr const int NumWn       = 2 * MaxWn + 1;
  static constexpr const int D           = LatInfo::dimension;
	static constexpr const int NumOutBonds = LatInfo::max_num_outgoing_edges_per_vertex;

  // constructor:
  hamiltonian(){;}

  template <typename EsisjFn, typename EsiFn>
  hamiltonian(const EsisjFn&  init_Esisj,
              const EsiFn&    init_Esi,
              const double    init_kT = 0,
              const double    init_h = 0,
              const double    init_lambda = 0,
              const double    init_lambda3 = 0)
  {
    set_Esisj(init_Esisj);
    set_Esi(init_Esi);
    set_kT(init_kT);
    set_h(init_h);
    set_lambda(init_lambda);
    set_lambda3(init_lambda3);
  }

  // access model properties:
	inline constexpr int
	num_states() const noexcept
	{ return N; }	

	inline constexpr int
	num_edge_types() const noexcept
	{ return NumOutBonds; }

	inline constexpr bool
	external_field_present() const noexcept
	{ return (ExtFld == external_field::on); }

	inline constexpr bool
	vortex_suppressed() const noexcept
	{ return ((VxSupp == vortex_suppression::on)
				 || (VxSupp == vortex_suppression::full)); }

	inline constexpr bool
	vortex_fully_suppressed() const noexcept
	{ return (VxSupp == vortex_suppression::full); }

	inline constexpr bool
	z3vortex_suppressed() const noexcept
	{ return ((Z3VxSupp == vortex_suppression::on)
				 || (Z3VxSupp == vortex_suppression::full)); }

	inline constexpr bool
	z3vortex_fully_suppressed() const noexcept
	{ return (Z3VxSupp == vortex_suppression::full); }

  // access couplings:
	inline constexpr double
	kT() const noexcept
	{ return 1. / beta(); }	

	void
  set_kT(const double init_kT)
	{ beta_ = 1./init_kT; set_boltzmann_weights(); return; }

	inline constexpr double
	beta() const noexcept
	{ return beta_; }	

	void
  set_beta(const double init_beta)
	{ beta_ = init_beta; set_boltzmann_weights(); return; }

	inline constexpr double
	h() const noexcept
	{ return h_; }	

	void
  set_h(const double init_h)
	{
    if (external_field_present() == true) {
      h_ = init_h; 
    } else {
      h_ = 0.;
    }
    set_boltzmann_weights();
    return;
  }
 
	inline constexpr double
	lambda() const noexcept
	{ return lambda_; }	

	void set_lambda(const double init_lambda)
	{
    if (vortex_suppressed() == true) {
      lambda_ = init_lambda; 
    } else {
      lambda_ = 0.;
    }
    set_boltzmann_weights();
    return;
  }
 
	inline constexpr double
	lambda3() const noexcept
	{ return lambda3_; }	

	void set_lambda3(const double init_lambda3)
	{
    if (z3vortex_suppressed() == true) {
      lambda3_ = init_lambda3; 
    } else {
      lambda3_ = 0.;
    }
    set_boltzmann_weights();
    return;
  }
 
  // access energy levels:
	inline double&
	Esisj(const int si, const int sj, const int b) noexcept
	{ return Esisj_[qtools::flatten_coords<3>({{N,N,NumOutBonds}}, si, sj, b)]; }
	inline constexpr const double&
	Esisj(const int si, const int sj, const int b) const noexcept
	{ return Esisj_[qtools::flatten_coords<3>({{N,N,NumOutBonds}}, si, sj, b)]; }

	template<typename EsisjFn>
	void set_Esisj(const EsisjFn& init_Esisj)
	{
		for (int b = 0; b < num_edge_types(); b++)
			for (int si = 0; si < (int)N; si++)
				for (int sj = 0; sj < (int)N; sj++)
					Esisj(si, sj, b) = init_Esisj(si, sj, b);
		set_boltzmann_weights();
		return;
	}

	inline double&
	Esi(const int si) noexcept
	{ return Esi_[qtools::flatten_coords<1>({{N}}, si)]; }
	inline constexpr const double&
	Esi(const int si) const noexcept
	{ return Esi_[qtools::flatten_coords<1>({{N}}, si)]; }

	template<typename EsiFn>
	void set_Esi(const EsiFn& init_Esi)
	{
		for (int si = 0; si < (int)N; si++) {
			Esi(si) = (external_field_present() ? init_Esi(si) : 0.0);
    }
		set_boltzmann_weights();
		return;
	}

  // access boltzmann weights:
	inline double&
	exp_minus_beta_Esisj(const int si, const int sj, const int b) noexcept
	{ return exp_minus_beta_Esisj_[qtools::flatten_coords<3>({{N,N,NumOutBonds}}, si, sj, b)]; }
	inline constexpr const double&
	exp_minus_beta_Esisj(const int si, const int sj, const int b) const noexcept
	{ return exp_minus_beta_Esisj_[qtools::flatten_coords<3>({{N,N,NumOutBonds}}, si, sj, b)]; }

	inline double&
	exp_plus_beta_Esisj(const int si, const int sj, const int b) noexcept
	{ return exp_plus_beta_Esisj_[qtools::flatten_coords<3>({{N,N,NumOutBonds}}, si, sj, b)]; }
	inline constexpr const double&
	exp_plus_beta_Esisj(const int si, const int sj, const int b) const noexcept
	{ return exp_plus_beta_Esisj_[qtools::flatten_coords<3>({{N,N,NumOutBonds}}, si, sj, b)]; }

	inline double&
	exp_minus_beta_h_Esf_minus_Esi(const int sf, const int si) noexcept
	{ return exp_minus_beta_h_Esf_minus_Esi_[qtools::flatten_coords<2>({{N,N}}, sf, si)]; }
	inline constexpr const double&
	exp_minus_beta_h_Esf_minus_Esi(const int sf, const int si) const noexcept
	{ return exp_minus_beta_h_Esf_minus_Esi_[qtools::flatten_coords<2>({{N,N}}, sf, si)]; }

	inline double&
	exp_minus_beta_abs_Esisj_minus_Emax(const int sf, const int si) noexcept
	{ return exp_minus_beta_abs_Esisj_minus_Emax_[qtools::flatten_coords<2>({{N,N}}, sf, si)]; }
	inline constexpr const double&
	exp_minus_beta_abs_Esisj_minus_Emax(const int sf, const int si) const noexcept
	{ return exp_minus_beta_abs_Esisj_minus_Emax_[qtools::flatten_coords<2>({{N,N}}, sf, si)]; }

	inline double&
	exp_minus_beta_lambda_wnf_minus_wni(const int wnf, const int wni) noexcept
	{ return exp_minus_beta_lambda_wnf_minus_wni_[qtools::flatten_coords<2>({{NumWn,NumWn}}, wnf+MaxWn, wni+MaxWn)]; }
	inline constexpr const double&
	exp_minus_beta_lambda_wnf_minus_wni(const int wnf, const int wni) const noexcept
	{ return exp_minus_beta_lambda_wnf_minus_wni_[qtools::flatten_coords<2>({{NumWn,NumWn}}, wnf+MaxWn, wni+MaxWn)]; }

	inline double&
	exp_minus_beta_lambda3_z3wnf_minus_z3wni(const int z3wnf, const int z3wni) noexcept
	{ return exp_minus_beta_lambda3_z3wnf_minus_z3wni_[qtools::flatten_coords<2>({{NumWn,NumWn}}, z3wnf+MaxWn, z3wni+MaxWn)]; }
	inline constexpr const double&
	exp_minus_beta_lambda3_z3wnf_minus_z3wni(const int z3wnf, const int z3wni) const noexcept
	{ return exp_minus_beta_lambda3_z3wnf_minus_z3wni_[qtools::flatten_coords<2>({{NumWn,NumWn}}, z3wnf+MaxWn, z3wni+MaxWn)]; }

  private:
	void set_boltzmann_weights()
	{
    double Emax = Esisj(0,0,0);
		for (int b = 0; b < num_edge_types(); b++) {
			for (int si = 0; si < (int)N; si++) {
				for (int sj = 0; sj < (int)N; sj++) {
          if (Esisj(si, sj, b) > Emax) {
            Emax = Esisj(si, sj, b);
          }
				}
			}
		}
		for (int b = 0; b < num_edge_types(); b++) {
			for (int si = 0; si < (int)N; si++) {
				for (int sj = 0; sj < (int)N; sj++) {
					exp_minus_beta_Esisj(si, sj, b)  
						= std::exp(-beta() * Esisj(si, sj, b));
					exp_plus_beta_Esisj(si, sj, b)  
						= std::exp(+beta() * Esisj(si, sj, b));
					exp_minus_beta_h_Esf_minus_Esi(si, sj)
					  = std::exp(-beta() * h() * (Esi(si) - Esi(sj)));
					exp_minus_beta_abs_Esisj_minus_Emax(si, sj)
					  = std::exp(-beta() * std::fabs(Esisj(si, sj, b) - Emax));
				}
			}
		}
    for (int wnf = -MaxWn; wnf <= +MaxWn; wnf++) {
      for (int wni = -MaxWn; wni <= +MaxWn; wni++) {
        const int wnfval = (wnf == 0 ? 0 : wnf > 0 ? +1 : -1);
        const int wnival = (wni == 0 ? 0 : wni > 0 ? +1 : -1);
		    exp_minus_beta_lambda_wnf_minus_wni(wnf, wni) 
			    = std::exp(-beta() * lambda() * (wnfval - wnival));
		    exp_minus_beta_lambda3_z3wnf_minus_z3wni(wnf, wni) 
			    = std::exp(-beta() * lambda3() * (wnfval - wnival));
      }
    }
		return;
	}

  // couplings:
	double                                  beta_   = 0; 
  double                                  h_      = 0;
	double                                  lambda_ = 0;
	double                                  lambda3_ = 0;

  // energy levels:
	std::array<double, N * N * NumOutBonds> Esisj_{{0}};
  std::array<double, N>                   Esi_  {{0}};

  // boltzmann weights:
	std::array<double, N * N * NumOutBonds> exp_minus_beta_Esisj_               {{0}};
  std::array<double, N * N * NumOutBonds> exp_plus_beta_Esisj_                {{0}};
	std::array<double, N * N>               exp_minus_beta_h_Esf_minus_Esi_     {{0}};
	std::array<double, N * N>               exp_minus_beta_abs_Esisj_minus_Emax_{{0}};
	std::array<double, NumWn * NumWn>       exp_minus_beta_lambda_wnf_minus_wni_{{0}};
	std::array<double, NumWn * NumWn>       exp_minus_beta_lambda3_z3wnf_minus_z3wni_{{0}};

};

} // namespace znspin

#endif // ZNSPIN_MODEL_HAMILTONIAN_H
