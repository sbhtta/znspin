#ifndef ZNSPIN_MODEL_SPINCONFIG_H
#define ZNSPIN_MODEL_SPINCONFIG_H

#include <array>
#include <vector>
#include <cassert>
#include <iostream>
#include <qtools/lattice.h>
#include <qtools/multidim.h>
#include <qtools/connected_components.h>
#include <znspin/model/spinspace.h>

namespace znspin {

template <std::size_t N, qtools::lattice::name LatNm>
class spinconfig_spins;

template <std::size_t N, qtools::lattice::name LatNm>
class spinconfig_stochastic_clusters;

template <std::size_t N, qtools::lattice::name LatNm>
class spinconfig_domain_walls;

template <std::size_t N, qtools::lattice::name LatNm>
class spinconfig_vortices;

template <std::size_t N, qtools::lattice::name LatNm>
class spinconfig_wrong_vortices;

template <std::size_t N, qtools::lattice::name LatNm>
class spinconfig_z3vortices;

template <std::size_t N, qtools::lattice::name LatNm>
class spinconfig
{
  public:
  // types and constants:
  using LatInfo = qtools::lattice::info<LatNm>;
  static constexpr const int D = LatInfo::dimension;

  // constructor:
  spinconfig(const std::array<int,D> Lx)
  : length_(Lx)
  {
    elems_.resize(num_vertices());
    fill(0);
  }

  // fill:
  void
  fill(const int n)
  { std::fill(elems_.begin(),elems_.end(),n); }

  void
  fill(const std::vector<int> n)
  {
    fill(0);
    for (int i = 0; i < (int)n.size(); i++) {
      elems_[i] = n[i];
    }
    return;
  }

  void
  initialize_checkerboard()
  {
    if (LatNm == qtools::lattice::name::square) {
      qtools::lattice::info<qtools::lattice::name::square>::for_each_vertex(length_,
        [&](const int x, const int y) {
          operator()(x,y) = 0 + ((x+y)&1) * N / 2;
        }
      );
    } else if (LatNm == qtools::lattice::name::cube) {
      qtools::lattice::info<qtools::lattice::name::cube>::for_each_vertex(length_,
        [&](const int x, const int y, const int z) {
          operator()(x,y,z) = 0 + ((x+y+z)&1) * N / 2;
        }
      );
    } else if (LatNm == qtools::lattice::name::hypercube) {
      qtools::lattice::info<qtools::lattice::name::hypercube>::for_each_vertex(length_,
        [&](const int x, const int y, const int z, const int t) {
          operator()(x,y,z,t) = 0 + ((x+y+z+t)&1) * N / 2;
        }
      );
    } 
  }

  void
  initialize_vxavxlattice()
  {
    if (LatNm == qtools::lattice::name::square) {
      qtools::lattice::info<qtools::lattice::name::square>::for_each_vertex(length_,
        [&](const int x, const int y) {
          if (((x & 1) == 0) && ((y & 1) == 0)) {
            operator()(x,y) = (0 * N)/4 + (N&1);
          } else if (((x & 1) == 1) && ((y & 1) == 0)) {
            operator()(x,y) = (1 * N)/4 + (N&1);
          } else if (((x & 1) == 1) && ((y & 1) == 1)) {
            operator()(x,y) = (2 * N)/4 + (N&1);
          } else if (((x & 1) == 0) && ((y & 1) == 1)) {
            operator()(x,y) = state_pullback<N>((3 * N)/4 + (N&1));
          } else {
            operator()(x,y) = 0;
          }
        }
      );
    } else if (LatNm == qtools::lattice::name::cube) {
      qtools::lattice::info<qtools::lattice::name::cube>::for_each_vertex(length_,
        [&](const int x, const int y, const int z) {
          if (((x & 1) == 0) && ((y & 1) == 0)) {
            operator()(x,y,z) = (0 * N)/4 + (N&1);
          } else if (((x & 1) == 1) && ((y & 1) == 0)) {
            operator()(x,y,z) = (1 * N)/4 + (N&1);
          } else if (((x & 1) == 1) && ((y & 1) == 1)) {
            operator()(x,y,z) = (2 * N)/4 + (N&1);
          } else if (((x & 1) == 0) && ((y & 1) == 1)) {
            operator()(x,y,z) = state_pullback<N>((3 * N)/4 + (N&1));
          } else {
            operator()(x,y,z) = 0;
          }
        }
      );
    } else if (LatNm == qtools::lattice::name::hypercube) {
      qtools::lattice::info<qtools::lattice::name::hypercube>::for_each_vertex(length_,
        [&](const int x, const int y, const int z, const int t) {
          if (((x & 1) == 0) && ((y & 1) == 0)) {
            operator()(x,y,z,t) = (0 * N)/4 + (N&1);
          } else if (((x & 1) == 1) && ((y & 1) == 0)) {
            operator()(x,y,z,t) = (1 * N)/4 + (N&1);
          } else if (((x & 1) == 1) && ((y & 1) == 1)) {
            operator()(x,y,z,t) = (2 * N)/4 + (N&1);
          } else if (((x & 1) == 0) && ((y & 1) == 1)) {
            operator()(x,y,z,t) = state_pullback<N>((3 * N)/4 + (N&1));
          } else {
            operator()(x,y,z,t) = 0;
          }
        }
      );
    } 
  }

  // access spins:
  inline int
  operator[](const int i) const
  {
    assert((0 <= i) && (i < num_vertices()));
    return elems_[i];
  }

  inline int&
  operator[](const int i)
  {
    assert((0 <= i) && (i < num_vertices()));
    return elems_[i];
  }

  inline int
  operator()(const int x, const int y = 0, const int z = 0, const int t = 0) const
  { return elems_[flatten_coords(x,y,z,t)]; }

  inline int&
  operator()(const int x, const int y = 0, const int z = 0, const int t = 0)
  { return elems_[flatten_coords(x,y,z,t)]; }

  inline int
  wrap(const int x, const int y = 0, const int z = 0, const int t = 0) const
  { return elems_[flatten_wrap_coords(x,y,z,t)]; }

  inline int&
  wrap(const int x, const int y = 0, const int z = 0, const int t = 0)
  { return elems_[flatten_wrap_coords(x,y,z,t)]; }

  // flat index calculation:
  inline int
  flatten_coords(const int x, const int y = 0, const int z = 0, const int t = 0) const
  {
    if (D > 0) assert((0 <= x) && (x < length_[0]));
    if (D > 1) assert((0 <= y) && (y < length_[1]));
    if (D > 2) assert((0 <= z) && (z < length_[2]));
    if (D > 3) assert((0 <= t) && (t < length_[3]));
    switch(D) {
    case 1 : return qtools::flatten_coords<1>(length_, x);
    case 2 : return qtools::flatten_coords<2>(length_, x, y);
    case 3 : return qtools::flatten_coords<3>(length_, x, y, z);
    case 4 : return qtools::flatten_coords<4>(length_, x, y, z, t);
    default: return -1;
    }
  }

  inline int
  flatten_wrap_coords(const int x, const int y = 0, const int z = 0, const int t = 0) const
  {
    switch(D) {
    case 1 : return qtools::flatten_wrap_coords<1>(length_, x);
    case 2 : return qtools::flatten_wrap_coords<2>(length_, x, y);
    case 3 : return qtools::flatten_wrap_coords<3>(length_, x, y, z);
    case 4 : return qtools::flatten_wrap_coords<4>(length_, x, y, z, t);
    default: return -1;
    }
  }

  template <int dx0, int dx1 = 0, int dx2 = 0, int dx3 = 0>
  inline bool
  disp_crosses_face(const int x, const int y = 0, const int z = 0, const int t = 0) const
  {
    switch(D) {
    case 1 : return qtools::disp_crosses_face<1,dx0>(length_, x);
    case 2 : return qtools::disp_crosses_face<2,dx0,dx1>(length_, x, y);
    case 3 : return qtools::disp_crosses_face<3,dx0,dx1,dx2>(length_, x, y, z);
    case 4 : return qtools::disp_crosses_face<4,dx0,dx1,dx2,dx3>(length_, x, y, z, t);
    default: return true;
    }
  }

  inline int
  edge_coords(const int b, const int x, const int y = 0, const int z = 0, const int t = 0) const
  { return b + num_edge_types() * flatten_coords(x,y,z,t); }

  inline int
  edge_index(const int b, const int i) const
  { return b + num_edge_types() * i; }

  // lattice properties:
  int
  num_states() const noexcept
  { return N; }

	int
	length(const int i) const noexcept
	{
    assert((0 <= i) && (i < (int)D));
    return length_[i];
  }

	const std::array<int,D>&
	length() const noexcept
	{ return length_; }

	int
	num_vertices() const noexcept
	{ return LatInfo::num_vertices(length_); }

	int
	num_edges() const noexcept
	{ return LatInfo::num_edges(length_); }
	 
	int
	num_edge_types() const noexcept
	{ return LatInfo::max_num_outgoing_edges_per_vertex; }
	 
	int
	max_num_outgoing_edges() const noexcept
	{ return num_edge_types() * num_vertices(); }

  // miscellenous vector:
  int misc_vector(const int i) const
  {
    assert((0 <= i) && (i < misc_vector_size()));
    return misc_vector_[i];
  }

  int& misc_vector(const int i)
  {
    assert((0 <= i) && (i < misc_vector_size()));
    return misc_vector_[i];
  }

  int misc_vector_size() const
  { return misc_vector_size_; }

  void misc_vector_resize(const int n)
  {
    misc_vector_size_ = n;
    if ((int)misc_vector_.size() < n) {
      misc_vector_.resize(n);
    }
    return;
  }

  void misc_vector_clear()
  {
    for (int i = 0; i < (int)misc_vector_.size(); i++) {
      misc_vector_[i] = 0;
    }
    return;
  }

  // component labelling:
  enum class component_type
  {
    none,
    stochastic_clusters,
    domain_walls,
    vortex_strings,
    all,
  };
  
  qtools::connected_components&
  ccl()
  { return ccl_; }  

  const qtools::connected_components&
  ccl() const
  { return ccl_; }  

  // stochastic cluster components:
  int largest_stochastic_cluster_size()
  {
    assert(last_labelled_component_type_ == component_type::stochastic_clusters);
    return ccl_.largest_component_size();
  }

  double largest_stochastic_cluster_fraction()
  {
    assert(last_labelled_component_type_ == component_type::stochastic_clusters);
    return ccl_.largest_component_size() / (1. * num_edges());
  }

  int stochastic_clusters_are_spanning()
  {
    assert(last_labelled_component_type_ == component_type::stochastic_clusters);
    return vertex_components_are_spanning();
  }

  void bin_all_stochastic_cluster_sizes_to_distribution(std::vector<long long int>& dist)
  {
    assert(last_labelled_component_type_ == component_type::stochastic_clusters);
    ccl_.bin_all_component_sizes_to_distribution(dist);
    return;
  }
  
  template<int ox0, int ox1, int ox2 = 0, int ox3 = 0>
  void
  label_stochastic_clusters()
  {
    // set boundary condition along each axis:
    if (D > 0) openx_[0] = ox0;
    if (D > 1) openx_[1] = ox1;
    if (D > 2) openx_[2] = ox2;
    if (D > 3) openx_[3] = ox3;

    last_labelled_component_type_ = component_type::stochastic_clusters;

    assert(misc_vector_size() >= max_num_outgoing_edges());
    ccl_.resize(num_vertices());
    ccl_.initialize();
    if (LatNm == qtools::lattice::name::square) {
      qtools::lattice::info<qtools::lattice::name::square>::for_each_vertex(length(),
        [&](const int x, const int y) {
          // vertex neighbor map:
          // i7 i8 i9
          // i4 i0 i6
          // i1 i2 i3
          const auto i2  = flatten_wrap_coords(x+0,y-1);
          const auto i4  = flatten_wrap_coords(x-1,y+0);
          const auto i0  = flatten_wrap_coords(x+0,y+0);
          const auto i6  = flatten_wrap_coords(x+1,y+0);
          const auto i8  = flatten_wrap_coords(x+0,y+1);
          if (!disp_crosses_face<-ox0,  +0>(x,y)) if (misc_vector(edge_index(0, i4))) ccl_.merge_roots(i0, i4);
          if (!disp_crosses_face<+ox0,  +0>(x,y)) if (misc_vector(edge_index(0, i0))) ccl_.merge_roots(i0, i6);
          if (!disp_crosses_face<  +0,-ox1>(x,y)) if (misc_vector(edge_index(1, i2))) ccl_.merge_roots(i0, i2);
          if (!disp_crosses_face<  +0,+ox1>(x,y)) if (misc_vector(edge_index(1, i0))) ccl_.merge_roots(i0, i8);
        }
      );
    } else if (LatNm == qtools::lattice::name::cube) {
      qtools::lattice::info<qtools::lattice::name::cube>::for_each_vertex(length(),
        [&](const int x, const int y, const int z) {
          // vertex neighbor map:
          // i7 i8 i9   i16 i17 i18   i25 i26 i27
          // i4 i5 i6   i13 i0  i15   i22 i23 i24
          // i1 i2 i3   i10 i11 i12   i19 i20 i21
          const auto i5  = flatten_wrap_coords(x+0,y+0,z-1);
          const auto i11 = flatten_wrap_coords(x+0,y-1,z+0);
          const auto i13 = flatten_wrap_coords(x-1,y+0,z+0);
          const auto i0  = flatten_wrap_coords(x+0,y+0,z+0);
          const auto i15 = flatten_wrap_coords(x+1,y+0,z+0);
          const auto i17 = flatten_wrap_coords(x+0,y+1,z+0);
          const auto i23 = flatten_wrap_coords(x+0,y+0,z+1);
          if (!disp_crosses_face<-ox0,  +0,  +0>(x,y,z)) if (misc_vector(edge_index(0,i13))) ccl_.merge_roots(i0,i13);
          if (!disp_crosses_face<+ox0,  +0,  +0>(x,y,z)) if (misc_vector(edge_index(0, i0))) ccl_.merge_roots(i0,i15);
          if (!disp_crosses_face<  +0,-ox1,  +0>(x,y,z)) if (misc_vector(edge_index(1,i11))) ccl_.merge_roots(i0,i11);
          if (!disp_crosses_face<  +0,+ox1,  +0>(x,y,z)) if (misc_vector(edge_index(1, i0))) ccl_.merge_roots(i0,i17);
          if (!disp_crosses_face<  +0,  +0,-ox2>(x,y,z)) if (misc_vector(edge_index(2, i5))) ccl_.merge_roots(i0, i5);
          if (!disp_crosses_face<  +0,  +0,+ox2>(x,y,z)) if (misc_vector(edge_index(2, i0))) ccl_.merge_roots(i0,i23);
        }
      );
    } else if (LatNm == qtools::lattice::name::hypercube) {
      qtools::lattice::info<qtools::lattice::name::hypercube>::for_each_vertex(length(),
        [&](const int x, const int y, const int z, const int t) {
          // i61 i62 i63  i70 i71 i72  i79 i80 i81
          // i58 i59 i60  i67 i68 i69  i76 i77 i78
          // i55 i56 i57  i64 i65 i66  i73 i74 i75
          //
          // i34 i35 i36  i43 i44 i45  i52 i53 i54
          // i31 i32 i33  i40 i0  i42  i49 i50 i51
          // i28 i29 i30  i37 i38 i39  i46 i47 i48
          //
          // i7  i8  i9   i16 i17 i18  i25 i26 i27
          // i4  i5  i6   i13 i14 i15  i22 i23 i24
          // i1  i2  i3   i10 i11 i12  i19 i20 i21
          const auto i14 = flatten_wrap_coords(x+0,y+0,z+0,t-1);
          const auto i32 = flatten_wrap_coords(x+0,y+0,z-1,t+0);
          const auto i38 = flatten_wrap_coords(x+0,y-1,z+0,t+0);
          const auto i40 = flatten_wrap_coords(x-1,y+0,z+0,t+0);
          const auto i0  = flatten_wrap_coords(x+0,y+0,z+0,t+0);
          const auto i42 = flatten_wrap_coords(x+1,y+0,z+0,t+0);
          const auto i44 = flatten_wrap_coords(x+0,y+1,z+0,t+0);
          const auto i50 = flatten_wrap_coords(x+0,y+0,z+1,t+0);
          const auto i68 = flatten_wrap_coords(x+0,y+0,z+0,t+1);
          if (!disp_crosses_face<-ox0,  +0,  +0,  +0>(x,y,z,t)) if (misc_vector(edge_index(0,i40))) ccl_.merge_roots(i0,i40);
          if (!disp_crosses_face<+ox0,  +0,  +0,  +0>(x,y,z,t)) if (misc_vector(edge_index(0, i0))) ccl_.merge_roots(i0,i42);
          if (!disp_crosses_face<  +0,-ox1,  +0,  +0>(x,y,z,t)) if (misc_vector(edge_index(1,i38))) ccl_.merge_roots(i0,i38);
          if (!disp_crosses_face<  +0,+ox1,  +0,  +0>(x,y,z,t)) if (misc_vector(edge_index(1, i0))) ccl_.merge_roots(i0,i44);
          if (!disp_crosses_face<  +0,  +0,-ox2,  +0>(x,y,z,t)) if (misc_vector(edge_index(2,i32))) ccl_.merge_roots(i0,i32);
          if (!disp_crosses_face<  +0,  +0,+ox2,  +0>(x,y,z,t)) if (misc_vector(edge_index(2, i0))) ccl_.merge_roots(i0,i50);
          if (!disp_crosses_face<  +0,  +0,  +0,-ox3>(x,y,z,t)) if (misc_vector(edge_index(3,i14))) ccl_.merge_roots(i0,i14);
          if (!disp_crosses_face<  +0,  +0,  +0,+ox3>(x,y,z,t)) if (misc_vector(edge_index(3, i0))) ccl_.merge_roots(i0,i68);
        }
      );
    } else {
      assert(false);
    } // lattice choice
    ccl_.finalize();
    return;
  }

  // domain wall components:
  int largest_domain_wall_size()
  {
    assert(last_labelled_component_type_ == component_type::domain_walls);
    return ccl_.largest_component_size();
  }

  double largest_domain_wall_fraction()
  {
    assert(last_labelled_component_type_ == component_type::domain_walls);
    return ccl_.largest_component_size() / (1. * num_edges());
  }

  int domain_walls_are_spanning()
  {
    assert(last_labelled_component_type_ == component_type::domain_walls);
    return edge_components_are_spanning();
  }

  void bin_all_domain_wall_sizes_to_distribution(std::vector<long long int>& dist)
  {
    assert(last_labelled_component_type_ == component_type::domain_walls);
    ccl_.bin_all_component_sizes_to_distribution(dist);
    return;
  }
  
  template<int ox0, int ox1, int ox2 = 0, int ox3 = 0, typename DefDWFn = int>
  void
  label_domain_walls(const DefDWFn& defdw)
  {
    // set boundary condition along each axis:
    if (D > 0) openx_[0] = ox0;
    if (D > 1) openx_[1] = ox1;
    if (D > 2) openx_[2] = ox2;
    if (D > 3) openx_[3] = ox3;

    last_labelled_component_type_ = component_type::domain_walls;

    ccl_.resize(max_num_outgoing_edges());
    ccl_.initialize();
    if (LatNm == qtools::lattice::name::square) {
      qtools::lattice::info<qtools::lattice::name::square>::for_each_vertex(length(),
        [&](const int x, const int y) {
          // vertex neighbor map:
          // i7 i8 i9
          // i4 i0 i6
          // i1 i2 i3
          const auto i2  = flatten_wrap_coords(x+0,y-1);
          const auto i3  = flatten_wrap_coords(x+1,y-1);
          const auto i4  = flatten_wrap_coords(x-1,y+0);
          const auto i0  = flatten_wrap_coords(x+0,y+0);
          const auto i6  = flatten_wrap_coords(x+1,y+0);
          const auto i7  = flatten_wrap_coords(x-1,y+1);
          const auto i8  = flatten_wrap_coords(x+0,y+1);
          const auto i9  = flatten_wrap_coords(x+1,y+1);
          // copy spin states: 
          const auto s2  = elems_[i2 ];
          const auto s3  = elems_[i3 ];
          const auto s4  = elems_[i4 ];
          const auto s0  = elems_[i0 ];
          const auto s6  = elems_[i6 ];
          const auto s7  = elems_[i7 ];
          const auto s8  = elems_[i8 ];
          const auto s9  = elems_[i9 ];
          // label +x bond :
          if (defdw(s0, s6, 0)) {
            if (!disp_crosses_face<+ox0,+ox1>(x,y)) if (defdw(s6, s9, 1)) ccl_.merge_roots(edge_index(0, i0), edge_index(1, i6));
            if (!disp_crosses_face<+ox0,-ox1>(x,y)) if (defdw(s3, s6, 1)) ccl_.merge_roots(edge_index(0, i0), edge_index(1, i3));
            if (!disp_crosses_face<  +0,+ox1>(x,y)) if (defdw(s8, s9, 0)) ccl_.merge_roots(edge_index(0, i0), edge_index(0, i8));
            if (!disp_crosses_face<  +0,-ox1>(x,y)) if (defdw(s2, s3, 0)) ccl_.merge_roots(edge_index(0, i0), edge_index(0, i2));
            if (!disp_crosses_face<  +0,+ox1>(x,y)) if (defdw(s0, s8, 1)) ccl_.merge_roots(edge_index(0, i0), edge_index(1, i0));
            if (!disp_crosses_face<  +0,-ox1>(x,y)) if (defdw(s2, s0, 1)) ccl_.merge_roots(edge_index(0, i0), edge_index(1, i2));
          }
          // label +y bond :
          if (defdw(s0, s8, 1)) {
            if (!disp_crosses_face<+ox0,+ox1>(x,y)) if (defdw(s8, s9, 0)) ccl_.merge_roots(edge_index(1, i0), edge_index(0, i8));
            if (!disp_crosses_face<-ox0,+ox1>(x,y)) if (defdw(s7, s8, 0)) ccl_.merge_roots(edge_index(1, i0), edge_index(0, i7));
            if (!disp_crosses_face<+ox0,  +0>(x,y)) if (defdw(s6, s9, 1)) ccl_.merge_roots(edge_index(1, i0), edge_index(1, i6));
            if (!disp_crosses_face<-ox0,  +0>(x,y)) if (defdw(s4, s7, 1)) ccl_.merge_roots(edge_index(1, i0), edge_index(1, i4));
            if (!disp_crosses_face<+ox0,  +0>(x,y)) if (defdw(s0, s6, 0)) ccl_.merge_roots(edge_index(1, i0), edge_index(0, i0));
            if (!disp_crosses_face<-ox0,  +0>(x,y)) if (defdw(s4, s0, 0)) ccl_.merge_roots(edge_index(1, i0), edge_index(0, i4));
          }
        }
      );
    } else if (LatNm == qtools::lattice::name::cube) {
      qtools::lattice::info<qtools::lattice::name::cube>::for_each_vertex(length(),
        [&](const int x, const int y, const int z) {
          // vertex neighbor map:
          // i7 i8 i9   i16 i17 i18   i25 i26 i27
          // i4 i5 i6   i13 i0  i15   i22 i23 i24
          // i1 i2 i3   i10 i11 i12   i19 i20 i21
          const auto i5  = flatten_wrap_coords(x+0,y+0,z-1);
          const auto i6  = flatten_wrap_coords(x+1,y+0,z-1);
          const auto i8  = flatten_wrap_coords(x+0,y+1,z-1);
          const auto i11 = flatten_wrap_coords(x+0,y-1,z+0);
          const auto i12 = flatten_wrap_coords(x+1,y-1,z+0);
          const auto i13 = flatten_wrap_coords(x-1,y+0,z+0);
          const auto i0  = flatten_wrap_coords(x+0,y+0,z+0);
          const auto i15 = flatten_wrap_coords(x+1,y+0,z+0);
          const auto i16 = flatten_wrap_coords(x-1,y+1,z+0);
          const auto i17 = flatten_wrap_coords(x+0,y+1,z+0);
          const auto i18 = flatten_wrap_coords(x+1,y+1,z+0);
          const auto i20 = flatten_wrap_coords(x+0,y-1,z+1);
          const auto i22 = flatten_wrap_coords(x-1,y+0,z+1);
          const auto i23 = flatten_wrap_coords(x+0,y+0,z+1);
          const auto i24 = flatten_wrap_coords(x+1,y+0,z+1);
          const auto i26 = flatten_wrap_coords(x+0,y+1,z+1);
          // copy spin values:
          const auto s5  = elems_[i5 ];
          const auto s6  = elems_[i6 ];
          const auto s8  = elems_[i8 ];
          const auto s11 = elems_[i11];
          const auto s12 = elems_[i12];
          const auto s13 = elems_[i13];
          const auto s0  = elems_[i0 ];
          const auto s15 = elems_[i15];
          const auto s16 = elems_[i16];
          const auto s17 = elems_[i17];
          const auto s18 = elems_[i18];
          const auto s20 = elems_[i20];
          const auto s22 = elems_[i22];
          const auto s23 = elems_[i23];
          const auto s24 = elems_[i24];
          const auto s26 = elems_[i26];
          // label +x bond :
          if (defdw(s0, s15, 0)) {
            if (!disp_crosses_face<+ox0,+ox1,  +0>(x,y,z)) if (defdw(s15,s18, 1)) ccl_.merge_roots(edge_index(0, i0), edge_index(1, i15));
            if (!disp_crosses_face<+ox0,-ox1,  +0>(x,y,z)) if (defdw(s12,s15, 1)) ccl_.merge_roots(edge_index(0, i0), edge_index(1, i12));
            if (!disp_crosses_face<+ox0,  +0,+ox2>(x,y,z)) if (defdw(s15,s24, 2)) ccl_.merge_roots(edge_index(0, i0), edge_index(2, i15));
            if (!disp_crosses_face<+ox0,  +0,-ox2>(x,y,z)) if (defdw(s6 ,s15, 2)) ccl_.merge_roots(edge_index(0, i0), edge_index(2, i6 ));
            if (!disp_crosses_face<  +0,+ox1,  +0>(x,y,z)) if (defdw(s17,s18, 0)) ccl_.merge_roots(edge_index(0, i0), edge_index(0, i17)); 
            if (!disp_crosses_face<  +0,-ox1,  +0>(x,y,z)) if (defdw(s11,s12, 0)) ccl_.merge_roots(edge_index(0, i0), edge_index(0, i11));
            if (!disp_crosses_face<  +0,  +0,+ox2>(x,y,z)) if (defdw(s23,s24, 0)) ccl_.merge_roots(edge_index(0, i0), edge_index(0, i23));
            if (!disp_crosses_face<  +0,  +0,-ox2>(x,y,z)) if (defdw(s5, s6 , 0)) ccl_.merge_roots(edge_index(0, i0), edge_index(0, i5 ));
            if (!disp_crosses_face<  +0,+ox1,  +0>(x,y,z)) if (defdw(s0, s17, 1)) ccl_.merge_roots(edge_index(0, i0), edge_index(1, i0 ));
            if (!disp_crosses_face<  +0,-ox1,  +0>(x,y,z)) if (defdw(s11,s0 , 1)) ccl_.merge_roots(edge_index(0, i0), edge_index(1, i11));
            if (!disp_crosses_face<  +0,  +0,+ox2>(x,y,z)) if (defdw(s0, s23, 2)) ccl_.merge_roots(edge_index(0, i0), edge_index(2, i0 ));
            if (!disp_crosses_face<  +0,  +0,-ox2>(x,y,z)) if (defdw(s5, s0 , 2)) ccl_.merge_roots(edge_index(0, i0), edge_index(2, i5 ));
          }
          // label +y bond :
          if (defdw(s0, s17, 1)) {
            if (!disp_crosses_face<+ox0,+ox1,  +0>(x,y,z)) if (defdw(s17,s18, 0)) ccl_.merge_roots(edge_index(1, i0), edge_index(0, i17));
            if (!disp_crosses_face<-ox0,+ox1,  +0>(x,y,z)) if (defdw(s16,s17, 0)) ccl_.merge_roots(edge_index(1, i0), edge_index(0, i16));
            if (!disp_crosses_face<  +0,+ox1,+ox2>(x,y,z)) if (defdw(s17,s26, 2)) ccl_.merge_roots(edge_index(1, i0), edge_index(2, i17));
            if (!disp_crosses_face<  +0,+ox1,-ox2>(x,y,z)) if (defdw(s8 ,s17, 2)) ccl_.merge_roots(edge_index(1, i0), edge_index(2, i8 ));
            if (!disp_crosses_face<+ox0,  +0,  +0>(x,y,z)) if (defdw(s15,s18, 1)) ccl_.merge_roots(edge_index(1, i0), edge_index(1, i15));
            if (!disp_crosses_face<-ox0,  +0,  +0>(x,y,z)) if (defdw(s13,s16, 1)) ccl_.merge_roots(edge_index(1, i0), edge_index(1, i13));
            if (!disp_crosses_face<  +0,  +0,+ox2>(x,y,z)) if (defdw(s23,s26, 1)) ccl_.merge_roots(edge_index(1, i0), edge_index(1, i23));
            if (!disp_crosses_face<  +0,  +0,-ox2>(x,y,z)) if (defdw(s5 ,s8 , 1)) ccl_.merge_roots(edge_index(1, i0), edge_index(1, i5 ));
            if (!disp_crosses_face<+ox0,  +0,  +0>(x,y,z)) if (defdw(s0, s15, 0)) ccl_.merge_roots(edge_index(1, i0), edge_index(0, i0 ));
            if (!disp_crosses_face<-ox0,  +0,  +0>(x,y,z)) if (defdw(s13, s0, 0)) ccl_.merge_roots(edge_index(1, i0), edge_index(0, i13));
            if (!disp_crosses_face<  +0,  +0,+ox2>(x,y,z)) if (defdw(s0, s23, 2)) ccl_.merge_roots(edge_index(1, i0), edge_index(2, i0 ));
            if (!disp_crosses_face<  +0,  +0,-ox2>(x,y,z)) if (defdw(s5, s0 , 2)) ccl_.merge_roots(edge_index(1, i0), edge_index(2, i5 ));
          }
          if (defdw(s0 , s23, 2)) {
            if (!disp_crosses_face<+ox0,  +0,+ox2>(x,y,z)) if (defdw(s23, s24, 0)) ccl_.merge_roots(edge_index(2, i0), edge_index(0, i23));
            if (!disp_crosses_face<-ox0,  +0,+ox2>(x,y,z)) if (defdw(s22, s23, 0)) ccl_.merge_roots(edge_index(2, i0), edge_index(0, i22));
            if (!disp_crosses_face<  +0,+ox1,+ox2>(x,y,z)) if (defdw(s23, s26, 1)) ccl_.merge_roots(edge_index(2, i0), edge_index(1, i23));
            if (!disp_crosses_face<  +0,-ox1,+ox2>(x,y,z)) if (defdw(s20, s23, 1)) ccl_.merge_roots(edge_index(2, i0), edge_index(1, i20));
            if (!disp_crosses_face<+ox0,  +0,  +0>(x,y,z)) if (defdw(s15, s24, 2)) ccl_.merge_roots(edge_index(2, i0), edge_index(2, i15));
            if (!disp_crosses_face<-ox0,  +0,  +0>(x,y,z)) if (defdw(s13, s22, 2)) ccl_.merge_roots(edge_index(2, i0), edge_index(2, i13));
            if (!disp_crosses_face<  +0,+ox1,  +0>(x,y,z)) if (defdw(s17, s26, 2)) ccl_.merge_roots(edge_index(2, i0), edge_index(2, i17));
            if (!disp_crosses_face<  +0,-ox1,  +0>(x,y,z)) if (defdw(s11, s20, 2)) ccl_.merge_roots(edge_index(2, i0), edge_index(2, i11));
            if (!disp_crosses_face<+ox0,  +0,  +0>(x,y,z)) if (defdw(s0 , s15, 0)) ccl_.merge_roots(edge_index(2, i0), edge_index(0, i0 ));
            if (!disp_crosses_face<-ox0,  +0,  +0>(x,y,z)) if (defdw(s13, s0 , 0)) ccl_.merge_roots(edge_index(2, i0), edge_index(0, i13));
            if (!disp_crosses_face<  +0,+ox1,  +0>(x,y,z)) if (defdw(s0 , s17, 1)) ccl_.merge_roots(edge_index(2, i0), edge_index(1, i0 ));
            if (!disp_crosses_face<  +0,-ox1,  +0>(x,y,z)) if (defdw(s11, s0 , 1)) ccl_.merge_roots(edge_index(2, i0), edge_index(1, i11));
          }
        }
      );
    } else if (LatNm == qtools::lattice::name::hypercube) {
      qtools::lattice::info<qtools::lattice::name::hypercube>::for_each_vertex(length(),
        [&](const int x, const int y, const int z, const int t) {
          // vertex neighbor map:
          // i61 i62 i63  i70 i71 i72  i79 i80 i81
          // i58 i59 i60  i67 i68 i69  i76 i77 i78
          // i55 i56 i57  i64 i65 i66  i73 i74 i75
          //
          // i34 i35 i36  i43 i44 i45  i52 i53 i54
          // i31 i32 i33  i40 i0  i42  i49 i50 i51
          // i28 i29 i30  i37 i38 i39  i46 i47 i48
          //
          // i7  i8  i9   i16 i17 i18  i25 i26 i27
          // i4  i5  i6   i13 i14 i15  i22 i23 i24
          // i1  i2  i3   i10 i11 i12  i19 i20 i21
          const auto i14 = flatten_wrap_coords(x+0,y+0,z+0,t-1);
          const auto i15 = flatten_wrap_coords(x+1,y+0,z+0,t-1);
          const auto i17 = flatten_wrap_coords(x+0,y+1,z+0,t-1);
          const auto i23 = flatten_wrap_coords(x+0,y+0,z+1,t-1);
          const auto i32 = flatten_wrap_coords(x+0,y+0,z-1,t+0);
          const auto i33 = flatten_wrap_coords(x+1,y+0,z-1,t+0);
          const auto i35 = flatten_wrap_coords(x+0,y+1,z-1,t+0);
          const auto i38 = flatten_wrap_coords(x+0,y-1,z+0,t+0);
          const auto i39 = flatten_wrap_coords(x+1,y-1,z+0,t+0);
          const auto i40 = flatten_wrap_coords(x-1,y+0,z+0,t+0);
          const auto i0  = flatten_wrap_coords(x+0,y+0,z+0,t+0);
          const auto i42 = flatten_wrap_coords(x+1,y+0,z+0,t+0);
          const auto i43 = flatten_wrap_coords(x-1,y+1,z+0,t+0);
          const auto i44 = flatten_wrap_coords(x+0,y+1,z+0,t+0);
          const auto i45 = flatten_wrap_coords(x+1,y+1,z+0,t+0);
          const auto i47 = flatten_wrap_coords(x+0,y-1,z+1,t+0);
          const auto i49 = flatten_wrap_coords(x-1,y+0,z+1,t+0);
          const auto i50 = flatten_wrap_coords(x+0,y+0,z+1,t+0);
          const auto i51 = flatten_wrap_coords(x+1,y+0,z+1,t+0);
          const auto i53 = flatten_wrap_coords(x+0,y+1,z+1,t+0);
          const auto i59 = flatten_wrap_coords(x+0,y+0,z-1,t+1);
          const auto i65 = flatten_wrap_coords(x+0,y-1,z+0,t+1);
          const auto i67 = flatten_wrap_coords(x-1,y+0,z+0,t+1);
          const auto i68 = flatten_wrap_coords(x+0,y+0,z+0,t+1);
          const auto i69 = flatten_wrap_coords(x+1,y+0,z+0,t+1);
          const auto i71 = flatten_wrap_coords(x+0,y+1,z+0,t+1);
          const auto i77 = flatten_wrap_coords(x+0,y+0,z+1,t+1);
          // copy spin states:
          const auto s14 = elems_[i14];
          const auto s15 = elems_[i15];
          const auto s17 = elems_[i17];
          const auto s23 = elems_[i23];
          const auto s32 = elems_[i32];
          const auto s33 = elems_[i33];
          const auto s35 = elems_[i35];
          const auto s38 = elems_[i38];
          const auto s39 = elems_[i39];
          const auto s40 = elems_[i40];
          const auto s0  = elems_[i0 ];
          const auto s42 = elems_[i42];
          const auto s43 = elems_[i43];
          const auto s44 = elems_[i44];
          const auto s45 = elems_[i45];
          const auto s47 = elems_[i47];
          const auto s49 = elems_[i49];
          const auto s50 = elems_[i50];
          const auto s51 = elems_[i51];
          const auto s53 = elems_[i53];
          const auto s59 = elems_[i59];
          const auto s65 = elems_[i65];
          const auto s67 = elems_[i67];
          const auto s68 = elems_[i68];
          const auto s69 = elems_[i69];
          const auto s71 = elems_[i71];
          const auto s77 = elems_[i77];
          // label +x bond :
          if (defdw(s0, s42, 0)) {
            if (!disp_crosses_face<+ox0,+ox1,  +0,  +0>(x,y,z,t)) if (defdw(s42,s45, 1)) ccl_.merge_roots(edge_index(0, i0), edge_index(1, i42));
            if (!disp_crosses_face<+ox0,-ox1,  +0,  +0>(x,y,z,t)) if (defdw(s39,s42, 1)) ccl_.merge_roots(edge_index(0, i0), edge_index(1, i39));
            if (!disp_crosses_face<+ox0,  +0,+ox2,  +0>(x,y,z,t)) if (defdw(s42,s51, 2)) ccl_.merge_roots(edge_index(0, i0), edge_index(2, i42));
            if (!disp_crosses_face<+ox0,  +0,-ox2,  +0>(x,y,z,t)) if (defdw(s33,s42, 2)) ccl_.merge_roots(edge_index(0, i0), edge_index(2, i33));
            if (!disp_crosses_face<+ox0,  +0,  +0,+ox3>(x,y,z,t)) if (defdw(s42,s69, 3)) ccl_.merge_roots(edge_index(0, i0), edge_index(3, i42));
            if (!disp_crosses_face<+ox0,  +0,  +0,-ox3>(x,y,z,t)) if (defdw(s15,s42, 3)) ccl_.merge_roots(edge_index(0, i0), edge_index(3, i15));
            if (!disp_crosses_face<  +0,+ox1,  +0,  +0>(x,y,z,t)) if (defdw(s44,s45, 0)) ccl_.merge_roots(edge_index(0, i0), edge_index(0, i44));
            if (!disp_crosses_face<  +0,-ox1,  +0,  +0>(x,y,z,t)) if (defdw(s38,s39, 0)) ccl_.merge_roots(edge_index(0, i0), edge_index(0, i38));
            if (!disp_crosses_face<  +0,  +0,+ox2,  +0>(x,y,z,t)) if (defdw(s50,s51, 0)) ccl_.merge_roots(edge_index(0, i0), edge_index(0, i50));
            if (!disp_crosses_face<  +0,  +0,-ox2,  +0>(x,y,z,t)) if (defdw(s32,s33, 0)) ccl_.merge_roots(edge_index(0, i0), edge_index(0, i32));
            if (!disp_crosses_face<  +0,  +0,  +0,+ox3>(x,y,z,t)) if (defdw(s68,s69, 0)) ccl_.merge_roots(edge_index(0, i0), edge_index(0, i68));
            if (!disp_crosses_face<  +0,  +0,  +0,-ox3>(x,y,z,t)) if (defdw(s14,s15, 0)) ccl_.merge_roots(edge_index(0, i0), edge_index(0, i14));
            if (!disp_crosses_face<  +0,+ox1,  +0,  +0>(x,y,z,t)) if (defdw(s0 ,s44, 1)) ccl_.merge_roots(edge_index(0, i0), edge_index(1, i0 ));
            if (!disp_crosses_face<  +0,-ox1,  +0,  +0>(x,y,z,t)) if (defdw(s38,s0 , 1)) ccl_.merge_roots(edge_index(0, i0), edge_index(1, i38));
            if (!disp_crosses_face<  +0,  +0,+ox2,  +0>(x,y,z,t)) if (defdw(s0 ,s50, 2)) ccl_.merge_roots(edge_index(0, i0), edge_index(2, i0 ));
            if (!disp_crosses_face<  +0,  +0,-ox2,  +0>(x,y,z,t)) if (defdw(s32,s0 , 2)) ccl_.merge_roots(edge_index(0, i0), edge_index(2, i32));
            if (!disp_crosses_face<  +0,  +0,  +0,+ox3>(x,y,z,t)) if (defdw(s0 ,s68, 3)) ccl_.merge_roots(edge_index(0, i0), edge_index(3, i0 ));
            if (!disp_crosses_face<  +0,  +0,  +0,-ox3>(x,y,z,t)) if (defdw(s14,s0 , 3)) ccl_.merge_roots(edge_index(0, i0), edge_index(3, i14));
          }
          // label +y bond :
          if (defdw(s0, s44, 1)) {
            if (!disp_crosses_face<+ox0,+ox1,  +0,  +0>(x,y,z,t)) if (defdw(s44,s45, 0)) ccl_.merge_roots(edge_index(1, i0), edge_index(0, i44));
            if (!disp_crosses_face<-ox0,+ox1,  +0,  +0>(x,y,z,t)) if (defdw(s43,s44, 0)) ccl_.merge_roots(edge_index(1, i0), edge_index(0, i43));
            if (!disp_crosses_face<  +0,+ox1,+ox2,  +0>(x,y,z,t)) if (defdw(s44,s53, 2)) ccl_.merge_roots(edge_index(1, i0), edge_index(2, i44));
            if (!disp_crosses_face<  +0,+ox1,-ox2,  +0>(x,y,z,t)) if (defdw(s35,s44, 2)) ccl_.merge_roots(edge_index(1, i0), edge_index(2, i35));
            if (!disp_crosses_face<  +0,+ox1,  +0,+ox3>(x,y,z,t)) if (defdw(s44,s71, 3)) ccl_.merge_roots(edge_index(1, i0), edge_index(3, i44));
            if (!disp_crosses_face<  +0,+ox1,  +0,-ox3>(x,y,z,t)) if (defdw(s17,s44, 3)) ccl_.merge_roots(edge_index(1, i0), edge_index(3, i17));
            if (!disp_crosses_face<+ox0,  +0,  +0,  +0>(x,y,z,t)) if (defdw(s42,s45, 1)) ccl_.merge_roots(edge_index(1, i0), edge_index(1, i42));
            if (!disp_crosses_face<-ox0,  -0,  +0,  +0>(x,y,z,t)) if (defdw(s40,s43, 1)) ccl_.merge_roots(edge_index(1, i0), edge_index(1, i40));
            if (!disp_crosses_face<  +0,  +0,+ox2,  +0>(x,y,z,t)) if (defdw(s50,s53, 1)) ccl_.merge_roots(edge_index(1, i0), edge_index(1, i50));
            if (!disp_crosses_face<  +0,  +0,-ox2,  +0>(x,y,z,t)) if (defdw(s32,s35, 1)) ccl_.merge_roots(edge_index(1, i0), edge_index(1, i32));
            if (!disp_crosses_face<  +0,  +0,  +0,+ox3>(x,y,z,t)) if (defdw(s68,s71, 1)) ccl_.merge_roots(edge_index(1, i0), edge_index(1, i68));
            if (!disp_crosses_face<  +0,  +0,  +0,-ox3>(x,y,z,t)) if (defdw(s14,s17, 1)) ccl_.merge_roots(edge_index(1, i0), edge_index(1, i14));
            if (!disp_crosses_face<+ox0,  +0,  +0,  +0>(x,y,z,t)) if (defdw(s0 ,s42, 0)) ccl_.merge_roots(edge_index(1, i0), edge_index(0, i0 ));
            if (!disp_crosses_face<-ox0,  +0,  +0,  +0>(x,y,z,t)) if (defdw(s40,s0 , 0)) ccl_.merge_roots(edge_index(1, i0), edge_index(0, i40));
            if (!disp_crosses_face<  +0,  +0,+ox2,  +0>(x,y,z,t)) if (defdw(s0 ,s50, 2)) ccl_.merge_roots(edge_index(1, i0), edge_index(2, i0 ));
            if (!disp_crosses_face<  +0,  +0,-ox2,  +0>(x,y,z,t)) if (defdw(s32,s0 , 2)) ccl_.merge_roots(edge_index(1, i0), edge_index(2, i32));
            if (!disp_crosses_face<  +0,  +0,  +0,+ox3>(x,y,z,t)) if (defdw(s0 ,s68, 3)) ccl_.merge_roots(edge_index(1, i0), edge_index(3, i0 ));
            if (!disp_crosses_face<  +0,  +0,  +0,-ox3>(x,y,z,t)) if (defdw(s14,s0 , 3)) ccl_.merge_roots(edge_index(1, i0), edge_index(3, i14));
          }
          // label +z bond :
          if (defdw(s0 , s50, 2)) {
            if (!disp_crosses_face<+ox0,  +0,+ox2,  +0>(x,y,z,t)) if (defdw(s50,s51, 0)) ccl_.merge_roots(edge_index(2, i0), edge_index(0, i50));
            if (!disp_crosses_face<-ox0,  +0,+ox2,  +0>(x,y,z,t)) if (defdw(s49,s50, 0)) ccl_.merge_roots(edge_index(2, i0), edge_index(0, i49));
            if (!disp_crosses_face<  +0,+ox1,+ox2,  +0>(x,y,z,t)) if (defdw(s50,s53, 1)) ccl_.merge_roots(edge_index(2, i0), edge_index(1, i50));
            if (!disp_crosses_face<  +0,-ox1,+ox2,  +0>(x,y,z,t)) if (defdw(s47,s50, 1)) ccl_.merge_roots(edge_index(2, i0), edge_index(1, i47));
            if (!disp_crosses_face<  +0,  +0,+ox2,+ox3>(x,y,z,t)) if (defdw(s50,s77, 3)) ccl_.merge_roots(edge_index(2, i0), edge_index(3, i50));
            if (!disp_crosses_face<  +0,  +0,+ox2,-ox3>(x,y,z,t)) if (defdw(s23,s50, 3)) ccl_.merge_roots(edge_index(2, i0), edge_index(3, i23));
            if (!disp_crosses_face<+ox0,  +0,  +0,  +0>(x,y,z,t)) if (defdw(s42,s51, 2)) ccl_.merge_roots(edge_index(2, i0), edge_index(2, i42));
            if (!disp_crosses_face<-ox0,  -0,  +0,  +0>(x,y,z,t)) if (defdw(s40,s49, 2)) ccl_.merge_roots(edge_index(2, i0), edge_index(2, i40));
            if (!disp_crosses_face<  +0,+ox1,  +0,  +0>(x,y,z,t)) if (defdw(s44,s53, 2)) ccl_.merge_roots(edge_index(2, i0), edge_index(2, i44));
            if (!disp_crosses_face<  +0,-ox1,  +0,  +0>(x,y,z,t)) if (defdw(s38,s47, 2)) ccl_.merge_roots(edge_index(2, i0), edge_index(2, i38));
            if (!disp_crosses_face<  +0,  +0,  +0,+ox3>(x,y,z,t)) if (defdw(s68,s77, 2)) ccl_.merge_roots(edge_index(2, i0), edge_index(2, i68));
            if (!disp_crosses_face<  +0,  +0,  +0,-ox3>(x,y,z,t)) if (defdw(s14,s23, 2)) ccl_.merge_roots(edge_index(2, i0), edge_index(2, i14));
            if (!disp_crosses_face<+ox0,  +0,  +0,  +0>(x,y,z,t)) if (defdw(s0 ,s42, 0)) ccl_.merge_roots(edge_index(2, i0), edge_index(0, i0 ));
            if (!disp_crosses_face<-ox0,  +0,  +0,  +0>(x,y,z,t)) if (defdw(s40,s0 , 0)) ccl_.merge_roots(edge_index(2, i0), edge_index(0, i40));
            if (!disp_crosses_face<  +0,+ox1,  +0,  +0>(x,y,z,t)) if (defdw(s0 ,s44, 1)) ccl_.merge_roots(edge_index(2, i0), edge_index(1, i0 ));
            if (!disp_crosses_face<  +0,-ox1,  +0,  +0>(x,y,z,t)) if (defdw(s38,s0 , 1)) ccl_.merge_roots(edge_index(2, i0), edge_index(1, i38));
            if (!disp_crosses_face<  +0,  +0,  +0,+ox3>(x,y,z,t)) if (defdw(s0 ,s68, 3)) ccl_.merge_roots(edge_index(2, i0), edge_index(3, i0 ));
            if (!disp_crosses_face<  +0,  +0,  +0,-ox3>(x,y,z,t)) if (defdw(s14,s0 , 3)) ccl_.merge_roots(edge_index(2, i0), edge_index(3, i14));
          }
          // label +t bond :
          if (defdw(s0 , s68, 3)) {
            if (!disp_crosses_face<+ox0,  +0,  +0,+ox3>(x,y,z,t)) if (defdw(s68,s69, 0)) ccl_.merge_roots(edge_index(3, i0), edge_index(0, i68));
            if (!disp_crosses_face<-ox0,  +0,  +0,+ox3>(x,y,z,t)) if (defdw(s67,s68, 0)) ccl_.merge_roots(edge_index(3, i0), edge_index(0, i67));
            if (!disp_crosses_face<  +0,+ox1,  +0,+ox3>(x,y,z,t)) if (defdw(s68,s71, 1)) ccl_.merge_roots(edge_index(3, i0), edge_index(1, i68));
            if (!disp_crosses_face<  +0,-ox1,  +0,+ox3>(x,y,z,t)) if (defdw(s65,s68, 1)) ccl_.merge_roots(edge_index(3, i0), edge_index(1, i65));
            if (!disp_crosses_face<  +0,  +0,+ox2,+ox3>(x,y,z,t)) if (defdw(s68,s77, 2)) ccl_.merge_roots(edge_index(3, i0), edge_index(2, i68));
            if (!disp_crosses_face<  +0,  +0,-ox2,+ox3>(x,y,z,t)) if (defdw(s59,s68, 2)) ccl_.merge_roots(edge_index(3, i0), edge_index(2, i59));
            if (!disp_crosses_face<+ox0,  +0,  +0,  +0>(x,y,z,t)) if (defdw(s42,s69, 3)) ccl_.merge_roots(edge_index(3, i0), edge_index(3, i42));
            if (!disp_crosses_face<-ox0,  -0,  +0,  +0>(x,y,z,t)) if (defdw(s40,s67, 3)) ccl_.merge_roots(edge_index(3, i0), edge_index(3, i40));
            if (!disp_crosses_face<  +0,+ox1,  +0,  +0>(x,y,z,t)) if (defdw(s44,s71, 3)) ccl_.merge_roots(edge_index(3, i0), edge_index(3, i44));
            if (!disp_crosses_face<  +0,-ox1,  +0,  +0>(x,y,z,t)) if (defdw(s38,s65, 3)) ccl_.merge_roots(edge_index(3, i0), edge_index(3, i38));
            if (!disp_crosses_face<  +0,  +0,+ox2,  +0>(x,y,z,t)) if (defdw(s50,s77, 3)) ccl_.merge_roots(edge_index(3, i0), edge_index(3, i50));
            if (!disp_crosses_face<  +0,  +0,-ox2,  +0>(x,y,z,t)) if (defdw(s32,s59, 3)) ccl_.merge_roots(edge_index(3, i0), edge_index(3, i32));
            if (!disp_crosses_face<+ox0,  +0,  +0,  +0>(x,y,z,t)) if (defdw(s0 ,s42, 0)) ccl_.merge_roots(edge_index(3, i0), edge_index(0, i0 ));
            if (!disp_crosses_face<-ox0,  +0,  +0,  +0>(x,y,z,t)) if (defdw(s40,s0 , 0)) ccl_.merge_roots(edge_index(3, i0), edge_index(0, i40));
            if (!disp_crosses_face<  +0,+ox1,  +0,  +0>(x,y,z,t)) if (defdw(s0 ,s44, 1)) ccl_.merge_roots(edge_index(3, i0), edge_index(1, i0 ));
            if (!disp_crosses_face<  +0,-ox1,  +0,  +0>(x,y,z,t)) if (defdw(s38,s0 , 1)) ccl_.merge_roots(edge_index(3, i0), edge_index(1, i38));
            if (!disp_crosses_face<  +0,  +0,+ox2,  +0>(x,y,z,t)) if (defdw(s0 ,s50, 2)) ccl_.merge_roots(edge_index(3, i0), edge_index(2, i0 ));
            if (!disp_crosses_face<  +0,  +0,-ox2,  +0>(x,y,z,t)) if (defdw(s32,s0 , 2)) ccl_.merge_roots(edge_index(3, i0), edge_index(2, i32));
          }
        }
      );
    } else {
      assert(false);
    } // lattice choice
    ccl_.finalize();
    return;
  }

  // vortex string components:
  int largest_vortex_string_size()
  {
    if ((LatInfo::dimension == 3) && (N > 2)) {
      assert(last_labelled_component_type_ == component_type::vortex_strings);
      return ccl_.largest_component_size();
    } else {
      return 0;
    }
  }

  double largest_vortex_string_fraction()
  {
    if ((LatInfo::dimension == 3) && (N > 2)) {
      assert(last_labelled_component_type_ == component_type::vortex_strings);
      return ccl_.largest_component_size() / (1. * num_edges());
    } else {
      return 0;
    }
  }

  bool vortex_strings_are_spanning()
  {
    if ((LatInfo::dimension == 3) && (N > 2)) {
      assert(last_labelled_component_type_ == component_type::vortex_strings);
      return edge_components_are_spanning();
    } else {
      return 0;
    }
  }

  void bin_all_vortex_string_sizes_to_distribution(std::vector<long long int>& dist)
  {
    if ((LatInfo::dimension == 3) && (N > 2)) {
      assert(last_labelled_component_type_ == component_type::vortex_strings);
      ccl_.bin_all_component_sizes_to_distribution(dist);
    }
    return;
  }
  
  template <int ox0, int ox1, int ox2 = 0, int ox3 = 0>
  void
  label_vortex_strings()
  {
    if ((LatInfo::dimension != 3) || (N <= 2)) return;

    // set boundary condition along each axis:
    if (D > 0) openx_[0] = ox0;
    if (D > 1) openx_[1] = ox1;
    if (D > 2) openx_[2] = ox2;
    if (D > 3) openx_[3] = ox3;

    last_labelled_component_type_ = component_type::vortex_strings;

    ccl_.resize(max_num_outgoing_edges());
    ccl_.initialize();
    qtools::lattice::info<qtools::lattice::name::cube>::for_each_vertex(length(),
      [&](const int x, const int y, const int z) {
        // vertex neighbor map:
        // i7 i8 i9   i16 i17 i18   i25 i26 i27
        // i4 i5 i6   i13 i0  i15   i22 i23 i24
        // i1 i2 i3   i10 i11 i12   i19 i20 i21
        const auto sa = wrap(x+0,y+0,z+0);
        const auto sb = wrap(x+1,y+0,z+0);
        const auto sc = wrap(x+0,y+1,z+0);
        const auto sd = wrap(x+1,y+1,z+0);
        const auto se = wrap(x+0,y+0,z+1);
        const auto sf = wrap(x+1,y+0,z+1);
        const auto sg = wrap(x+0,y+1,z+1);
        const auto sh = wrap(x+1,y+1,z+1);
        const auto wn015 = winding_number<N>(sb,sd,sh,sf);
        const auto wn013 = winding_number<N>(sa,sc,sg,se);
        const auto wn017 = winding_number<N>(sh,sd,sc,sg);
        const auto wn011 = winding_number<N>(se,sa,sb,sf);
        const auto wn023 = winding_number<N>(se,sf,sh,sg);
        const auto wn05  = winding_number<N>(sb,sd,sc,sa);
        const auto i0  = flatten_wrap_coords(x+0,y+0,z+0);
        const auto i5  = flatten_wrap_coords(x+0,y+0,z-1);
        const auto i11 = flatten_wrap_coords(x+0,y-1,z+0);
        const auto i13 = flatten_wrap_coords(x-1,y+0,z+0);
        // label +x edge :
        if (!disp_crosses_face<(ox0!=0)*'b',+0,+0>(x,y,z) && (wn015 != 0) && (wn013 != 0)) ccl_.merge_roots(edge_index(0,i0 ), edge_index(0,i13));
        if (!disp_crosses_face<+ox0,+ox1,  +0>(x,y,z) && (wn015 != 0) && (wn017 != 0)) ccl_.merge_roots(edge_index(0,i0 ), edge_index(1,i0 ));
        if (!disp_crosses_face<+ox0,-ox1,  +0>(x,y,z) && (wn015 != 0) && (wn011 != 0)) ccl_.merge_roots(edge_index(0,i0 ), edge_index(1,i11));
        if (!disp_crosses_face<+ox0,  +0,+ox2>(x,y,z) && (wn015 != 0) && (wn023 != 0)) ccl_.merge_roots(edge_index(0,i0 ), edge_index(2,i0 ));
        if (!disp_crosses_face<+ox0,  +0,-ox2>(x,y,z) && (wn015 != 0) && (wn05  != 0)) ccl_.merge_roots(edge_index(0,i0 ), edge_index(2,i5 ));
        // label -x edge :
        if (!disp_crosses_face<-ox0,+ox1,  +0>(x,y,z) && (wn013 != 0) && (wn017 != 0)) ccl_.merge_roots(edge_index(0,i13), edge_index(1,i0 ));
        if (!disp_crosses_face<-ox0,-ox1,  +0>(x,y,z) && (wn013 != 0) && (wn011 != 0)) ccl_.merge_roots(edge_index(0,i13), edge_index(1,i11));
        if (!disp_crosses_face<-ox0,  +0,+ox2>(x,y,z) && (wn013 != 0) && (wn023 != 0)) ccl_.merge_roots(edge_index(0,i13), edge_index(2,i0 ));
        if (!disp_crosses_face<-ox0,  +0,-ox2>(x,y,z) && (wn013 != 0) && (wn05  != 0)) ccl_.merge_roots(edge_index(0,i13), edge_index(2,i5 ));
        // label +y edge :
        if (!disp_crosses_face<+0,(ox1!=0)*'b',+0>(x,y,z) && (wn017 != 0) && (wn011 != 0)) ccl_.merge_roots(edge_index(1,i0 ), edge_index(1,i11));
        if (!disp_crosses_face<  +0,+ox1,+ox2>(x,y,z) && (wn017 != 0) && (wn023 != 0)) ccl_.merge_roots(edge_index(1,i0 ), edge_index(2,i0 ));
        if (!disp_crosses_face<  +0,+ox1,-ox2>(x,y,z) && (wn017 != 0) && (wn05  != 0)) ccl_.merge_roots(edge_index(1,i0 ), edge_index(2,i5 ));
        // label -y edge :
        if (!disp_crosses_face<  +0,-ox1,+ox2>(x,y,z) && (wn011 != 0) && (wn023 != 0)) ccl_.merge_roots(edge_index(1,i11), edge_index(2,i0 ));
        if (!disp_crosses_face<  +0,-ox1,-ox2>(x,y,z) && (wn011 != 0) && (wn05  != 0)) ccl_.merge_roots(edge_index(1,i11), edge_index(2,i5 ));
        // label +z edge :
        if (!disp_crosses_face<+0,+0,(ox2!=0)*'b'>(x,y,z) && (wn023 != 0) && (wn05  != 0)) ccl_.merge_roots(edge_index(2,i0 ), edge_index(2,i5 ));
      }
    );
    ccl_.finalize();
    return;
  }

  template <int ox0, int ox1, int ox2 = 0, int ox3 = 0>
  void
  label_wrong_vortex_strings()
  {
    if ((LatInfo::dimension != 3) || (N <= 2)) return;

    // set boundary condition along each axis:
    if (D > 0) openx_[0] = ox0;
    if (D > 1) openx_[1] = ox1;
    if (D > 2) openx_[2] = ox2;
    if (D > 3) openx_[3] = ox3;

    last_labelled_component_type_ = component_type::vortex_strings;

    ccl_.resize(max_num_outgoing_edges());
    ccl_.initialize();
    qtools::lattice::info<qtools::lattice::name::cube>::for_each_vertex(length(),
      [&](const int x, const int y, const int z) {
        // vertex neighbor map:
        // i7 i8 i9   i16 i17 i18   i25 i26 i27
        // i4 i5 i6   i13 i0  i15   i22 i23 i24
        // i1 i2 i3   i10 i11 i12   i19 i20 i21
        const auto sa = wrap(x+0,y+0,z+0);
        const auto sb = wrap(x+1,y+0,z+0);
        const auto sc = wrap(x+0,y+1,z+0);
        const auto sd = wrap(x+1,y+1,z+0);
        const auto se = wrap(x+0,y+0,z+1);
        const auto sf = wrap(x+1,y+0,z+1);
        const auto sg = wrap(x+0,y+1,z+1);
        const auto sh = wrap(x+1,y+1,z+1);
        const auto wn015 = wrong_winding_number<N>(sb,sd,sh,sf);
        const auto wn013 = wrong_winding_number<N>(sa,sc,sg,se);
        const auto wn017 = wrong_winding_number<N>(sh,sd,sc,sg);
        const auto wn011 = wrong_winding_number<N>(se,sa,sb,sf);
        const auto wn023 = wrong_winding_number<N>(se,sf,sh,sg);
        const auto wn05  = wrong_winding_number<N>(sb,sd,sc,sa);
        const auto i0  = flatten_wrap_coords(x+0,y+0,z+0);
        const auto i5  = flatten_wrap_coords(x+0,y+0,z-1);
        const auto i11 = flatten_wrap_coords(x+0,y-1,z+0);
        const auto i13 = flatten_wrap_coords(x-1,y+0,z+0);
        // label +x edge :
        if (!disp_crosses_face<(ox0!=0)*'b',+0,+0>(x,y,z) && (wn015 != 0) && (wn013 != 0)) ccl_.merge_roots(edge_index(0,i0 ), edge_index(0,i13));
        if (!disp_crosses_face<+ox0,+ox1,  +0>(x,y,z) && (wn015 != 0) && (wn017 != 0)) ccl_.merge_roots(edge_index(0,i0 ), edge_index(1,i0 ));
        if (!disp_crosses_face<+ox0,-ox1,  +0>(x,y,z) && (wn015 != 0) && (wn011 != 0)) ccl_.merge_roots(edge_index(0,i0 ), edge_index(1,i11));
        if (!disp_crosses_face<+ox0,  +0,+ox2>(x,y,z) && (wn015 != 0) && (wn023 != 0)) ccl_.merge_roots(edge_index(0,i0 ), edge_index(2,i0 ));
        if (!disp_crosses_face<+ox0,  +0,-ox2>(x,y,z) && (wn015 != 0) && (wn05  != 0)) ccl_.merge_roots(edge_index(0,i0 ), edge_index(2,i5 ));
        // label -x edge :
        if (!disp_crosses_face<-ox0,+ox1,  +0>(x,y,z) && (wn013 != 0) && (wn017 != 0)) ccl_.merge_roots(edge_index(0,i13), edge_index(1,i0 ));
        if (!disp_crosses_face<-ox0,-ox1,  +0>(x,y,z) && (wn013 != 0) && (wn011 != 0)) ccl_.merge_roots(edge_index(0,i13), edge_index(1,i11));
        if (!disp_crosses_face<-ox0,  +0,+ox2>(x,y,z) && (wn013 != 0) && (wn023 != 0)) ccl_.merge_roots(edge_index(0,i13), edge_index(2,i0 ));
        if (!disp_crosses_face<-ox0,  +0,-ox2>(x,y,z) && (wn013 != 0) && (wn05  != 0)) ccl_.merge_roots(edge_index(0,i13), edge_index(2,i5 ));
        // label +y edge :
        if (!disp_crosses_face<+0,(ox1!=0)*'b',+0>(x,y,z) && (wn017 != 0) && (wn011 != 0)) ccl_.merge_roots(edge_index(1,i0 ), edge_index(1,i11));
        if (!disp_crosses_face<  +0,+ox1,+ox2>(x,y,z) && (wn017 != 0) && (wn023 != 0)) ccl_.merge_roots(edge_index(1,i0 ), edge_index(2,i0 ));
        if (!disp_crosses_face<  +0,+ox1,-ox2>(x,y,z) && (wn017 != 0) && (wn05  != 0)) ccl_.merge_roots(edge_index(1,i0 ), edge_index(2,i5 ));
        // label -y edge :
        if (!disp_crosses_face<  +0,-ox1,+ox2>(x,y,z) && (wn011 != 0) && (wn023 != 0)) ccl_.merge_roots(edge_index(1,i11), edge_index(2,i0 ));
        if (!disp_crosses_face<  +0,-ox1,-ox2>(x,y,z) && (wn011 != 0) && (wn05  != 0)) ccl_.merge_roots(edge_index(1,i11), edge_index(2,i5 ));
        // label +z edge :
        if (!disp_crosses_face<+0,+0,(ox2!=0)*'b'>(x,y,z) && (wn023 != 0) && (wn05  != 0)) ccl_.merge_roots(edge_index(2,i0 ), edge_index(2,i5 ));
      }
    );
    ccl_.finalize();
    return;
  }
  
  
  template <int ox0, int ox1, int ox2 = 0, int ox3 = 0>
  void
  label_z3vortex_strings()
  {
    if ((LatInfo::dimension != 3) || (N <= 2)) return;

    // set boundary condition along each axis:
    if (D > 0) openx_[0] = ox0;
    if (D > 1) openx_[1] = ox1;
    if (D > 2) openx_[2] = ox2;
    if (D > 3) openx_[3] = ox3;

    last_labelled_component_type_ = component_type::vortex_strings;

    ccl_.resize(max_num_outgoing_edges());
    ccl_.initialize();
    qtools::lattice::info<qtools::lattice::name::cube>::for_each_vertex(length(),
      [&](const int x, const int y, const int z) {
        // vertex neighbor map:
        // i7 i8 i9   i16 i17 i18   i25 i26 i27
        // i4 i5 i6   i13 i0  i15   i22 i23 i24
        // i1 i2 i3   i10 i11 i12   i19 i20 i21
        const auto z3sa = effectivez3_state(wrap(x+0,y+0,z+0));
        const auto z3sb = effectivez3_state(wrap(x+1,y+0,z+0));
        const auto z3sc = effectivez3_state(wrap(x+0,y+1,z+0));
        const auto z3sd = effectivez3_state(wrap(x+1,y+1,z+0));
        const auto z3se = effectivez3_state(wrap(x+0,y+0,z+1));
        const auto z3sf = effectivez3_state(wrap(x+1,y+0,z+1));
        const auto z3sg = effectivez3_state(wrap(x+0,y+1,z+1));
        const auto z3sh = effectivez3_state(wrap(x+1,y+1,z+1));
        const auto z3wn015 = winding_number<3>(z3sb,z3sd,z3sh,z3sf);
        const auto z3wn013 = winding_number<3>(z3sa,z3sc,z3sg,z3se);
        const auto z3wn017 = winding_number<3>(z3sh,z3sd,z3sc,z3sg);
        const auto z3wn011 = winding_number<3>(z3se,z3sa,z3sb,z3sf);
        const auto z3wn023 = winding_number<3>(z3se,z3sf,z3sh,z3sg);
        const auto z3wn05  = winding_number<3>(z3sb,z3sd,z3sc,z3sa);
        const auto i0  = flatten_wrap_coords(x+0,y+0,z+0);
        const auto i5  = flatten_wrap_coords(x+0,y+0,z-1);
        const auto i11 = flatten_wrap_coords(x+0,y-1,z+0);
        const auto i13 = flatten_wrap_coords(x-1,y+0,z+0);
        // label +x edge :
        if (!disp_crosses_face<(ox0!=0)*'b',+0,+0>(x,y,z) && (z3wn015 != 0) && (z3wn013 != 0)) ccl_.merge_roots(edge_index(0,i0 ), edge_index(0,i13));
        if (!disp_crosses_face<+ox0,+ox1,  +0>(x,y,z) && (z3wn015 != 0) && (z3wn017 != 0)) ccl_.merge_roots(edge_index(0,i0 ), edge_index(1,i0 ));
        if (!disp_crosses_face<+ox0,-ox1,  +0>(x,y,z) && (z3wn015 != 0) && (z3wn011 != 0)) ccl_.merge_roots(edge_index(0,i0 ), edge_index(1,i11));
        if (!disp_crosses_face<+ox0,  +0,+ox2>(x,y,z) && (z3wn015 != 0) && (z3wn023 != 0)) ccl_.merge_roots(edge_index(0,i0 ), edge_index(2,i0 ));
        if (!disp_crosses_face<+ox0,  +0,-ox2>(x,y,z) && (z3wn015 != 0) && (z3wn05  != 0)) ccl_.merge_roots(edge_index(0,i0 ), edge_index(2,i5 ));
        // label -x edge :
        if (!disp_crosses_face<-ox0,+ox1,  +0>(x,y,z) && (z3wn013 != 0) && (z3wn017 != 0)) ccl_.merge_roots(edge_index(0,i13), edge_index(1,i0 ));
        if (!disp_crosses_face<-ox0,-ox1,  +0>(x,y,z) && (z3wn013 != 0) && (z3wn011 != 0)) ccl_.merge_roots(edge_index(0,i13), edge_index(1,i11));
        if (!disp_crosses_face<-ox0,  +0,+ox2>(x,y,z) && (z3wn013 != 0) && (z3wn023 != 0)) ccl_.merge_roots(edge_index(0,i13), edge_index(2,i0 ));
        if (!disp_crosses_face<-ox0,  +0,-ox2>(x,y,z) && (z3wn013 != 0) && (z3wn05  != 0)) ccl_.merge_roots(edge_index(0,i13), edge_index(2,i5 ));
        // label +y edge :
        if (!disp_crosses_face<+0,(ox1!=0)*'b',+0>(x,y,z) && (z3wn017 != 0) && (z3wn011 != 0)) ccl_.merge_roots(edge_index(1,i0 ), edge_index(1,i11));
        if (!disp_crosses_face<  +0,+ox1,+ox2>(x,y,z) && (z3wn017 != 0) && (z3wn023 != 0)) ccl_.merge_roots(edge_index(1,i0 ), edge_index(2,i0 ));
        if (!disp_crosses_face<  +0,+ox1,-ox2>(x,y,z) && (z3wn017 != 0) && (z3wn05  != 0)) ccl_.merge_roots(edge_index(1,i0 ), edge_index(2,i5 ));
        // label -y edge :
        if (!disp_crosses_face<  +0,-ox1,+ox2>(x,y,z) && (z3wn011 != 0) && (z3wn023 != 0)) ccl_.merge_roots(edge_index(1,i11), edge_index(2,i0 ));
        if (!disp_crosses_face<  +0,-ox1,-ox2>(x,y,z) && (z3wn011 != 0) && (z3wn05  != 0)) ccl_.merge_roots(edge_index(1,i11), edge_index(2,i5 ));
        // label +z edge :
        if (!disp_crosses_face<+0,+0,(ox2!=0)*'b'>(x,y,z) && (z3wn023 != 0) && (z3wn05  != 0)) ccl_.merge_roots(edge_index(2,i0 ), edge_index(2,i5 ));
      }
    );
    ccl_.finalize();
    return;
  }
  
  bool vertex_components_are_spanning()
  {
    misc_vector_resize(2 * D * num_vertices());
    misc_vector_clear();
    if (D == 2) {
      for (int y = 0; y < length_[1]; y++) {
        misc_vector_[0 + 2 * 0 + 2 * D * ccl_.label(flatten_coords(           0,y))] = 1;
        misc_vector_[1 + 2 * 0 + 2 * D * ccl_.label(flatten_coords(length_[0]-1,y))] = 1;
      }
      for (int x = 0; x < length_[0]; x++) {
        misc_vector_[0 + 2 * 1 + 2 * D * ccl_.label(flatten_coords(x,           0))] = 1;
        misc_vector_[1 + 2 * 1 + 2 * D * ccl_.label(flatten_coords(x,length_[1]-1))] = 1;
      }
    } else if (D == 3) {
      for (int z = 0; z < length_[2]; z++) {
      for (int y = 0; y < length_[1]; y++) {
        misc_vector_[0 + 2 * 0 + 2 * D * ccl_.label(flatten_coords(           0,y,z))] = 1;
        misc_vector_[1 + 2 * 0 + 2 * D * ccl_.label(flatten_coords(length_[0]-1,y,z))] = 1;
      }}
      for (int z = 0; z < length_[2]; z++) {
      for (int x = 0; x < length_[0]; x++) {
        misc_vector_[0 + 2 * 1 + 2 * D * ccl_.label(flatten_coords(x,           0,z))] = 1;
        misc_vector_[1 + 2 * 1 + 2 * D * ccl_.label(flatten_coords(x,length_[1]-1,z))] = 1;
      }}
      for (int y = 0; y < length_[1]; y++) {
      for (int x = 0; x < length_[0]; x++) {
        misc_vector_[0 + 2 * 2 + 2 * D * ccl_.label(flatten_coords(x,y,           0))] = 1;
        misc_vector_[1 + 2 * 2 + 2 * D * ccl_.label(flatten_coords(x,y,length_[2]-1))] = 1;
      }}
    } else if (D == 4) {
      for (int t = 0; t < length_[3]; t++) {
      for (int z = 0; z < length_[2]; z++) {
      for (int y = 0; y < length_[1]; y++) {
        misc_vector_[0 + 2 * 0 + 2 * D * ccl_.label(flatten_coords(           0,y,z,t))] = 1;
        misc_vector_[1 + 2 * 0 + 2 * D * ccl_.label(flatten_coords(length_[0]-1,y,z,t))] = 1;
      }}}
      for (int t = 0; t < length_[3]; t++) {
      for (int z = 0; z < length_[2]; z++) {
      for (int x = 0; x < length_[0]; x++) {
        misc_vector_[0 + 2 * 1 + 2 * D * ccl_.label(flatten_coords(x,           0,z,t))] = 1;
        misc_vector_[1 + 2 * 1 + 2 * D * ccl_.label(flatten_coords(x,length_[1]-1,z,t))] = 1;
      }}}
      for (int t = 0; t < length_[3]; t++) {
      for (int y = 0; y < length_[1]; y++) {
      for (int x = 0; x < length_[0]; x++) {
        misc_vector_[0 + 2 * 2 + 2 * D * ccl_.label(flatten_coords(x,y,           0,t))] = 1;
        misc_vector_[1 + 2 * 2 + 2 * D * ccl_.label(flatten_coords(x,y,length_[2]-1,t))] = 1;
      }}}
      for (int z = 0; z < length_[2]; z++) {
      for (int y = 0; y < length_[1]; y++) {
      for (int x = 0; x < length_[0]; x++) {
        misc_vector_[0 + 2 * 3 + 2 * D * ccl_.label(flatten_coords(x,y,z,           0))] = 1;
        misc_vector_[1 + 2 * 3 + 2 * D * ccl_.label(flatten_coords(x,y,z,length_[3]-1))] = 1;
      }}}
    }
    bool spanning = false;
    for (int label = 0; label < num_vertices(); label++) {
      for (int axis = 0; axis < D; axis++) {
        if (openx_[axis] == true) {
          if ((misc_vector_[0 + 2 * axis + 2 * D * label] == 1) &&
              (misc_vector_[1 + 2 * axis + 2 * D * label] == 1)) {
            assert(ccl_.component_size_of_label(label) >= length_[axis]/2);
            spanning = true;
          }
        }
      }
    }
    return spanning;
  }

  bool edge_components_are_spanning()
  {
    misc_vector_resize(2 * D * max_num_outgoing_edges());
    misc_vector_clear();
    if (D == 2) {
      for (int edg = 0; edg < num_edge_types(); edg++) {
        for (int y = 0; y < length_[1]; y++) {
          misc_vector_[0 + 2 * 0 + 2 * D * ccl_.label(edge_coords(edg,            0,y))] = 1;
          misc_vector_[1 + 2 * 0 + 2 * D * ccl_.label(edge_coords(edg, length_[0]-1,y))] = 1;
        }
        for (int x = 0; x < length_[0]; x++) {
          misc_vector_[0 + 2 * 1 + 2 * D * ccl_.label(edge_coords(edg, x,           0))] = 1;
          misc_vector_[1 + 2 * 1 + 2 * D * ccl_.label(edge_coords(edg, x,length_[1]-1))] = 1;
        }
      }
    } else if (D == 3) {
      for (int edg = 0; edg < num_edge_types(); edg++) {
        for (int z = 0; z < length_[2]; z++) {
        for (int y = 0; y < length_[1]; y++) {
          misc_vector_[0 + 2 * 0 + 2 * D * ccl_.label(edge_coords(edg,            0,y,z))] = 1;
          misc_vector_[1 + 2 * 0 + 2 * D * ccl_.label(edge_coords(edg, length_[0]-1,y,z))] = 1;
        }}
        for (int z = 0; z < length_[2]; z++) {
        for (int x = 0; x < length_[0]; x++) {
          misc_vector_[0 + 2 * 1 + 2 * D * ccl_.label(edge_coords(edg, x,           0,z))] = 1;
          misc_vector_[1 + 2 * 1 + 2 * D * ccl_.label(edge_coords(edg, x,length_[1]-1,z))] = 1;
        }}
        for (int y = 0; y < length_[1]; y++) {
        for (int x = 0; x < length_[0]; x++) {
          misc_vector_[0 + 2 * 2 + 2 * D * ccl_.label(edge_coords(edg, x,y,           0))] = 1;
          misc_vector_[1 + 2 * 2 + 2 * D * ccl_.label(edge_coords(edg, x,y,length_[2]-1))] = 1;
        }}
      }
    } else if (D == 4) {
      for (int edg = 0; edg < num_edge_types(); edg++) {
        for (int t = 0; t < length_[3]; t++) {
        for (int z = 0; z < length_[2]; z++) {
        for (int y = 0; y < length_[1]; y++) {
          misc_vector_[0 + 2 * 0 + 2 * D * ccl_.label(edge_coords(edg,            0,y,z,t))] = 1;
          misc_vector_[1 + 2 * 0 + 2 * D * ccl_.label(edge_coords(edg, length_[0]-1,y,z,t))] = 1;
        }}}
        for (int t = 0; t < length_[3]; t++) {
        for (int z = 0; z < length_[2]; z++) {
        for (int x = 0; x < length_[0]; x++) {
          misc_vector_[0 + 2 * 1 + 2 * D * ccl_.label(edge_coords(edg, x,           0,z,t))] = 1;
          misc_vector_[1 + 2 * 1 + 2 * D * ccl_.label(edge_coords(edg, x,length_[1]-1,z,t))] = 1;
        }}}
        for (int t = 0; t < length_[3]; t++) {
        for (int y = 0; y < length_[1]; y++) {
        for (int x = 0; x < length_[0]; x++) {
          misc_vector_[0 + 2 * 2 + 2 * D * ccl_.label(edge_coords(edg, x,y,           0,t))] = 1;
          misc_vector_[1 + 2 * 2 + 2 * D * ccl_.label(edge_coords(edg, x,y,length_[2]-1,t))] = 1;
        }}}
        for (int z = 0; z < length_[2]; z++) {
        for (int y = 0; y < length_[1]; y++) {
        for (int x = 0; x < length_[0]; x++) {
          misc_vector_[0 + 2 * 3 + 2 * D * ccl_.label(edge_coords(edg, x,y,z,           0))] = 1;
          misc_vector_[1 + 2 * 3 + 2 * D * ccl_.label(edge_coords(edg, x,y,z,length_[3]-1))] = 1;
        }}}
      }
    }
    bool spanning = false;
    for (int label = 0; label < max_num_outgoing_edges(); label++) {
      for (int axis = 0; axis < D; axis++) {
        if (openx_[axis] == true) {
          if ((misc_vector_[0 + 2 * axis + 2 * D * label] == 1) &&
              (misc_vector_[1 + 2 * axis + 2 * D * label] == 1)) {
            assert(ccl_.component_size_of_label(label) >= length_[axis]/2);
            spanning = true;
          }
        }
      }
    }
    return spanning;
  }

  // print:
  spinconfig_spins<N,LatNm>
  spins()
  { return spinconfig_spins<N,LatNm>(*this); }

  spinconfig_domain_walls<N,LatNm>
  stochastic_clusters(const int threshold = 2)
  {
    label_stochastic_clusters<false,false,false,false>();
    ccl_.update_label_histogram();
    return spinconfig_stochastic_clusters<N,LatNm>(*this,threshold);
  }

  template <typename DefDWFn>
  spinconfig_domain_walls<N,LatNm>
  domain_walls(const DefDWFn& defdw, const int threshold = 2)
  {
    label_domain_walls<false,false,false,false>(defdw);
    ccl_.update_label_histogram();
    return spinconfig_domain_walls<N,LatNm>(*this,threshold);
  }

  spinconfig_vortices<N,LatNm>
  vortices(const int threshold = 2)
  {
    label_vortex_strings<false,false,false,false>();
    ccl_.update_label_histogram();
    return spinconfig_vortices<N,LatNm>(*this, threshold);
  }

  spinconfig_wrong_vortices<N,LatNm>
  wrong_vortices(const int threshold = 2)
  {
    label_wrong_vortex_strings<false,false,false,false>();
    ccl_.update_label_histogram();
    return spinconfig_wrong_vortices<N,LatNm>(*this, threshold);
  }

  spinconfig_z3vortices<N,LatNm>
  z3vortices(const int threshold = 2)
  {
    label_z3vortex_strings<false,false,false,false>();
    ccl_.update_label_histogram();
    return spinconfig_z3vortices<N,LatNm>(*this, threshold);
  }

  void
  copy_sublattice_states_to(spinconfig<((N&1) ? 2*N : N), LatNm>& snew) const
  {
    assert(snew.num_vertices() == num_vertices());
    if (LatNm == qtools::lattice::name::square) {
      qtools::lattice::info<qtools::lattice::name::square>::for_each_vertex(length_,
        [&](const int x, const int y) {
          snew(x,y) = sublattice_state<N>(operator()(x,y),x,y);
        }
      );
    } else if (LatNm == qtools::lattice::name::cube) {
      qtools::lattice::info<qtools::lattice::name::cube>::for_each_vertex(length_,
        [&](const int x, const int y, const int z) {
          snew(x,y,z) = sublattice_state<N>(operator()(x,y,z),x,y,z);
        }
      );
    } else if (LatNm == qtools::lattice::name::hypercube) {
      qtools::lattice::info<qtools::lattice::name::hypercube>::for_each_vertex(length_,
        [&](const int x, const int y, const int z, const int t) {
          snew(x,y,z,t) = sublattice_state<N>(operator()(x,y,z,t),x,y,z,t);
        }
      );
    }
    return;
  }

  void
  copy_effectivez3_states_to(spinconfig<3, LatNm>& snew) const
  {
    assert(snew.num_vertices() == num_vertices());
    for (int i = 0; i < num_vertices(); i++) {
      snew[i] = effectivez3_state(operator[](i));
    }
    return;
  }

  private:
  const std::array<int,D>   length_{{0}};
        std::array<int,D>   openx_{{0}};
        std::vector<int>    elems_{{0}};
        std::vector<int>    misc_vector_{{0}};
        int                 misc_vector_size_ = 0;
        component_type      last_labelled_component_type_ = component_type::none;
        qtools::connected_components  ccl_;
};

// prints spin configuration:
template <std::size_t N, qtools::lattice::name LatNm>
class spinconfig_spins
{
  public:
  const spinconfig<N,LatNm>& s_;

  spinconfig_spins(const spinconfig<N,LatNm>& s)
  : s_(s)
  {;}

  friend std::ostream&
  operator<<(std::ostream& output, const spinconfig_spins<N,LatNm> sp)
  {
    if (LatNm == qtools::lattice::name::square) {
      qtools::lattice::info<qtools::lattice::name::square>::for_each_vertex(sp.s_.length(),
        [&](const int x, const int y) {
          output << x << "\t" << y << "\t" << sp.s_.wrap(x,y) << std::endl;
        }
      );
    } else if (LatNm == qtools::lattice::name::cube) {
      qtools::lattice::info<qtools::lattice::name::cube>::for_each_vertex(sp.s_.length(),
        [&](const int x, const int y, const int z) {
          output << x << "\t" << y << "\t" << z << "\t" << sp.s_.wrap(x,y,z) << std::endl;
        }
      );
    }            
    return output;
  }
};

// prints stochastic cluster configuration:
template <std::size_t N, qtools::lattice::name LatNm>
class spinconfig_stochastic_clusters
{
  public:
  const spinconfig<N,LatNm>& s_;

  spinconfig_stochastic_clusters(const spinconfig<N,LatNm>& s)
  : s_(s)
  {;}

  friend std::ostream&
  operator<<(std::ostream& output, const spinconfig_stochastic_clusters<N,LatNm> sp)
  {
    if (LatNm == qtools::lattice::name::square) {
      qtools::lattice::info<qtools::lattice::name::square>::for_each_vertex(sp.s_.length(),
        [&](const int x, const int y) {
          int component_size = sp.s_.ccl().nocheck_component_size_of_label(sp.s_.ccl().label(sp.s_.flatten_coords(x,y)));
          output << x << "\t" << y << "\t" << sp.s_.wrap(x,y) << "\t" << component_size << std::endl;
        }
      );
    } else if (LatNm == qtools::lattice::name::cube) {
      qtools::lattice::info<qtools::lattice::name::cube>::for_each_vertex(sp.s_.length(),
        [&](const int x, const int y, const int z) {
          int component_size = sp.s_.ccl().nocheck_component_size_of_label(sp.s_.ccl().label(sp.s_.flatten_coords(x,y,z)));
          output << x << "\t" << y << "\t" << z << "\t" << sp.s_.wrap(x,y,z) << "\t" << component_size << std::endl;
        }
      );
    }            
    return output;
  }
};

// prints domain wall loops (2D) or domain wall bubbles (3D):
template <std::size_t N, qtools::lattice::name LatNm>
class spinconfig_domain_walls
{
  public:
  const spinconfig<N, LatNm>&  s_;
  const int                 threshold_;

  spinconfig_domain_walls(const spinconfig<N, LatNm>& s, const int threshold)
  : s_(s), threshold_(threshold)
  {;}

  friend std::ostream&
  operator<<(std::ostream& output, const spinconfig_domain_walls<N,LatNm> sp)
  {
    if (LatNm == qtools::lattice::name::square) {
      qtools::lattice::info<qtools::lattice::name::square>::for_each_vertex(sp.s_.length(),
        [&](const int x, const int y) {
          int component_size;
          component_size = sp.s_.ccl().nocheck_component_size_of_label(sp.s_.ccl().label(sp.s_.edge_coords(0,x,y)));
          if (component_size >= sp.threshold_) {
            output << x+0.5 << "\t" << y-0.5 << "\t";
            output << x+0.5 << "\t" << y+0.5 << "\t";
            output << component_size << std::endl;
          }
          component_size = sp.s_.ccl().nocheck_component_size_of_label(sp.s_.ccl().label(sp.s_.edge_coords(1,x,y)));
          if (component_size >= sp.threshold_) {
            output << x-0.5 << "\t" << y+0.5 << "\t";
            output << x+0.5 << "\t" << y+0.5 << "\t";
            output << component_size << std::endl;
          }
        }
      );
    } else if (LatNm == qtools::lattice::name::cube) {
      qtools::lattice::info<qtools::lattice::name::cube>::for_each_vertex(sp.s_.length(),
        [&](const int x, const int y, const int z) {
          int component_size;
          component_size = sp.s_.ccl().nocheck_component_size_of_label(sp.s_.ccl().label(sp.s_.edge_coords(0,x,y,z)));
          if (component_size >= sp.threshold_) {
            output << x+0.5 << "\t" << y-0.5 << "\t" << z-0.5 << "\t";
            output << x+0.5 << "\t" << y+0.5 << "\t" << z-0.5 << "\t";
            output << x+0.5 << "\t" << y+0.5 << "\t" << z+0.5 << "\t";
            output << x+0.5 << "\t" << y-0.5 << "\t" << z+0.5 << "\t";
            output << component_size << std::endl;
          }
          component_size = sp.s_.ccl().nocheck_component_size_of_label(sp.s_.ccl().label(sp.s_.edge_coords(1,x,y,z)));
          if (component_size >= sp.threshold_) {
            output << x-0.5 << "\t" << y+0.5 << "\t" << z-0.5 << "\t";
            output << x+0.5 << "\t" << y+0.5 << "\t" << z-0.5 << "\t";
            output << x+0.5 << "\t" << y+0.5 << "\t" << z+0.5 << "\t";
            output << x-0.5 << "\t" << y+0.5 << "\t" << z+0.5 << "\t";
            output << component_size << std::endl;
          }
          component_size = sp.s_.ccl().nocheck_component_size_of_label(sp.s_.ccl().label(sp.s_.edge_coords(2,x,y,z)));
          if (component_size >= sp.threshold_) {
            output << x-0.5 << "\t" << y-0.5 << "\t" << z+0.5 << "\t";
            output << x+0.5 << "\t" << y-0.5 << "\t" << z+0.5 << "\t";
            output << x+0.5 << "\t" << y+0.5 << "\t" << z+0.5 << "\t";
            output << x-0.5 << "\t" << y+0.5 << "\t" << z+0.5 << "\t";
            output << component_size << std::endl;
          }
        }
      );
    } 
    return output;
  }
};

// prints vortices (2D) or vortex strings (3D):
template <std::size_t N, qtools::lattice::name LatNm>
class spinconfig_vortices
{
  public:
  const spinconfig<N,LatNm>& s_;
  const int                threshold_;

  spinconfig_vortices(const spinconfig<N,LatNm>& s, const int threshold)
  : s_(s), threshold_(threshold)
  {;}

  friend std::ostream&
  operator<<(std::ostream& output, const spinconfig_vortices<N,LatNm> sp)
  {
    if (LatNm == qtools::lattice::name::square) {
      qtools::lattice::info<qtools::lattice::name::square>::for_each_vertex(sp.s_.length(),
        [&](const int x, const int y) {
          const auto s0 = sp.s_.wrap(x+0,y+0);
          const auto s1 = sp.s_.wrap(x+1,y+0);
          const auto s2 = sp.s_.wrap(x+1,y+1);
          const auto s3 = sp.s_.wrap(x+0,y+1);
          const auto wn = winding_number<N>(s0,s1,s2,s3);
          if (wn != 0) {
            output << x+0.5 << "\t" << y+0.5 << "\t" << wn << std::endl;
          }
        }
      );
    } else if (LatNm == qtools::lattice::name::cube) {
      qtools::lattice::info<qtools::lattice::name::cube>::for_each_vertex(sp.s_.length(),
        [&](const int x, const int y, const int z) {
          int component_size;
          // i7 i8 i9   i16 i17 i18   i25 i26 i27
          // i4 i5 i6   i13 i0  i15   i22 i23 i24
          // i1 i2 i3   i10 i11 i12   i19 i20 i21
          const auto sb = sp.s_.wrap(x+1,y+0,z+0);
          const auto sc = sp.s_.wrap(x+0,y+1,z+0);
          const auto sd = sp.s_.wrap(x+1,y+1,z+0);
          const auto se = sp.s_.wrap(x+0,y+0,z+1);
          const auto sf = sp.s_.wrap(x+1,y+0,z+1);
          const auto sg = sp.s_.wrap(x+0,y+1,z+1);
          const auto sh = sp.s_.wrap(x+1,y+1,z+1);
          const auto wn015 = winding_number<N>(sb,sd,sh,sf);
          const auto wn017 = winding_number<N>(sh,sd,sc,sg);
          const auto wn023 = winding_number<N>(se,sf,sh,sg);
          component_size = sp.s_.ccl().nocheck_component_size_of_label(sp.s_.ccl().label(sp.s_.edge_coords(0,x,y,z)));
          if (component_size >= sp.threshold_) {
            output << x+0.5 << "\t" << y+0.5 << "\t" << z+0.5 << "\t";
            output << x+1.5 << "\t" << y+0.5 << "\t" << z+0.5 << "\t";
            output << wn015 << "\t" << component_size << std::endl;
          }
          component_size = sp.s_.ccl().nocheck_component_size_of_label(sp.s_.ccl().label(sp.s_.edge_coords(1,x,y,z)));
          if (component_size >= sp.threshold_) {
            output << x+0.5 << "\t" << y+0.5 << "\t" << z+0.5 << "\t";
            output << x+0.5 << "\t" << y+1.5 << "\t" << z+0.5 << "\t";
            output << wn017 << "\t" << component_size << std::endl;
          }
          component_size = sp.s_.ccl().nocheck_component_size_of_label(sp.s_.ccl().label(sp.s_.edge_coords(2,x,y,z)));
          if (component_size >= sp.threshold_) {
            output << x+0.5 << "\t" << y+0.5 << "\t" << z+0.5 << "\t";
            output << x+0.5 << "\t" << y+0.5 << "\t" << z+1.5 << "\t";
            output << wn023 << "\t" << component_size << std::endl;
          }
        }
      );
    } 
    return output;
  }
};

// prints wrong vortices (2D) or wrong vortex strings (3D):
template <std::size_t N, qtools::lattice::name LatNm>
class spinconfig_wrong_vortices
{
  public:
  const spinconfig<N,LatNm>& s_;
  const int                threshold_;

  spinconfig_wrong_vortices(const spinconfig<N,LatNm>& s, const int threshold)
  : s_(s), threshold_(threshold)
  {;}

  friend std::ostream&
  operator<<(std::ostream& output, const spinconfig_wrong_vortices<N,LatNm> sp)
  {
    if (LatNm == qtools::lattice::name::square) {
      qtools::lattice::info<qtools::lattice::name::square>::for_each_vertex(sp.s_.length(),
        [&](const int x, const int y) {
          const auto s0 = sp.s_.wrap(x+0,y+0);
          const auto s1 = sp.s_.wrap(x+1,y+0);
          const auto s2 = sp.s_.wrap(x+1,y+1);
          const auto s3 = sp.s_.wrap(x+0,y+1);
          const auto wn = wrong_winding_number<N>(s0,s1,s2,s3);
          if (wn != 0) {
            output << x+0.5 << "\t" << y+0.5 << "\t" << wn << std::endl;
          }
        }
      );
    } else if (LatNm == qtools::lattice::name::cube) {
      qtools::lattice::info<qtools::lattice::name::cube>::for_each_vertex(sp.s_.length(),
        [&](const int x, const int y, const int z) {
          int component_size;
          // i7 i8 i9   i16 i17 i18   i25 i26 i27
          // i4 i5 i6   i13 i0  i15   i22 i23 i24
          // i1 i2 i3   i10 i11 i12   i19 i20 i21
          const auto sb = sp.s_.wrap(x+1,y+0,z+0);
          const auto sc = sp.s_.wrap(x+0,y+1,z+0);
          const auto sd = sp.s_.wrap(x+1,y+1,z+0);
          const auto se = sp.s_.wrap(x+0,y+0,z+1);
          const auto sf = sp.s_.wrap(x+1,y+0,z+1);
          const auto sg = sp.s_.wrap(x+0,y+1,z+1);
          const auto sh = sp.s_.wrap(x+1,y+1,z+1);
          const auto wn015 = wrong_winding_number<N>(sb,sd,sh,sf);
          const auto wn017 = wrong_winding_number<N>(sh,sd,sc,sg);
          const auto wn023 = wrong_winding_number<N>(se,sf,sh,sg);
          component_size = sp.s_.ccl().nocheck_component_size_of_label(sp.s_.ccl().label(sp.s_.edge_coords(0,x,y,z)));
          if (component_size >= sp.threshold_) {
            output << x+0.5 << "\t" << y+0.5 << "\t" << z+0.5 << "\t";
            output << x+1.5 << "\t" << y+0.5 << "\t" << z+0.5 << "\t";
            output << wn015 << "\t" << component_size << std::endl;
          }
          component_size = sp.s_.ccl().nocheck_component_size_of_label(sp.s_.ccl().label(sp.s_.edge_coords(1,x,y,z)));
          if (component_size >= sp.threshold_) {
            output << x+0.5 << "\t" << y+0.5 << "\t" << z+0.5 << "\t";
            output << x+0.5 << "\t" << y+1.5 << "\t" << z+0.5 << "\t";
            output << wn017 << "\t" << component_size << std::endl;
          }
          component_size = sp.s_.ccl().nocheck_component_size_of_label(sp.s_.ccl().label(sp.s_.edge_coords(2,x,y,z)));
          if (component_size >= sp.threshold_) {
            output << x+0.5 << "\t" << y+0.5 << "\t" << z+0.5 << "\t";
            output << x+0.5 << "\t" << y+0.5 << "\t" << z+1.5 << "\t";
            output << wn023 << "\t" << component_size << std::endl;
          }
        }
      );
    } 
    return output;
  }
};

// prints Z3 vortices (2D) or Z3 vortex strings (3D):
template <std::size_t N, qtools::lattice::name LatNm>
class spinconfig_z3vortices
{
  public:
  const spinconfig<N,LatNm>&  s_;
  const int                   threshold_;

  spinconfig_z3vortices(const spinconfig<N,LatNm>& s, const int threshold)
  : s_(s), threshold_(threshold)
  {;}

  friend std::ostream&
  operator<<(std::ostream& output, const spinconfig_z3vortices<N,LatNm> sp)
  {
    if (LatNm == qtools::lattice::name::square) {
      qtools::lattice::info<qtools::lattice::name::square>::for_each_vertex(sp.s_.length(),
        [&](const int x, const int y) {
          const auto z3s0 = effectivez3_state(sp.s_.wrap(x+0,y+0));
          const auto z3s1 = effectivez3_state(sp.s_.wrap(x+1,y+0));
          const auto z3s2 = effectivez3_state(sp.s_.wrap(x+1,y+1));
          const auto z3s3 = effectivez3_state(sp.s_.wrap(x+0,y+1));
          const auto z3wn = winding_number<3>(z3s0,z3s1,z3s2,z3s3);
          if (z3wn != 0) {
            output << x+0.5 << "\t" << y+0.5 << "\t" << z3wn << std::endl;
          }
        }
      );
    } else if (LatNm == qtools::lattice::name::cube) {
      qtools::lattice::info<qtools::lattice::name::cube>::for_each_vertex(sp.s_.length(),
        [&](const int x, const int y, const int z) {
          int component_size;
          // i7 i8 i9   i16 i17 i18   i25 i26 i27
          // i4 i5 i6   i13 i0  i15   i22 i23 i24
          // i1 i2 i3   i10 i11 i12   i19 i20 i21
          const auto z3sb = effectivez3_state(sp.s_.wrap(x+1,y+0,z+0));
          const auto z3sc = effectivez3_state(sp.s_.wrap(x+0,y+1,z+0));
          const auto z3sd = effectivez3_state(sp.s_.wrap(x+1,y+1,z+0));
          const auto z3se = effectivez3_state(sp.s_.wrap(x+0,y+0,z+1));
          const auto z3sf = effectivez3_state(sp.s_.wrap(x+1,y+0,z+1));
          const auto z3sg = effectivez3_state(sp.s_.wrap(x+0,y+1,z+1));
          const auto z3sh = effectivez3_state(sp.s_.wrap(x+1,y+1,z+1));
          const auto z3wn015 = winding_number<3>(z3sb,z3sd,z3sh,z3sf);
          const auto z3wn017 = winding_number<3>(z3sh,z3sd,z3sc,z3sg);
          const auto z3wn023 = winding_number<3>(z3se,z3sf,z3sh,z3sg);
          component_size = sp.s_.ccl().nocheck_component_size_of_label(sp.s_.ccl().label(sp.s_.edge_coords(0,x,y,z)));
          if (component_size >= sp.threshold_) {
            output << x+0.5 << "\t" << y+0.5 << "\t" << z+0.5 << "\t";
            output << x+1.5 << "\t" << y+0.5 << "\t" << z+0.5 << "\t";
            output << z3wn015 << "\t" << component_size << std::endl;
          }
          component_size = sp.s_.ccl().nocheck_component_size_of_label(sp.s_.ccl().label(sp.s_.edge_coords(1,x,y,z)));
          if (component_size >= sp.threshold_) {
            output << x+0.5 << "\t" << y+0.5 << "\t" << z+0.5 << "\t";
            output << x+0.5 << "\t" << y+1.5 << "\t" << z+0.5 << "\t";
            output << z3wn017 << "\t" << component_size << std::endl;
          }
          component_size = sp.s_.ccl().nocheck_component_size_of_label(sp.s_.ccl().label(sp.s_.edge_coords(2,x,y,z)));
          if (component_size >= sp.threshold_) {
            output << x+0.5 << "\t" << y+0.5 << "\t" << z+0.5 << "\t";
            output << x+0.5 << "\t" << y+0.5 << "\t" << z+1.5 << "\t";
            output << z3wn023 << "\t" << component_size << std::endl;
          }
        }
      );
    } 
    return output;
  }
};

} // namespace znspin

#endif // ZNSPIN_MODEL_SPINCONFIG_H
