#ifndef ZNSPIN_MODEL_H
#define ZNSPIN_MODEL_H

#include <znspin/model/spinspace.h>
#include <znspin/model/spinconfig.h>
#include <znspin/model/hamiltonian.h>

#endif // ZNSPIN_MODEL_H
