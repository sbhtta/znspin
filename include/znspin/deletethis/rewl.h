/*
  Replica Exchange Wang Landau algorithm for estimation of
  joint density of states with integer or real variables.
  Parallelized using OpenMP.

  Author: Soumyadeep Bhattacharya
  email: sbhtta@gmail.com
  Last Updated: 2015/07/22
*/

#ifndef STATPHY_REWL_H
#define STATPHY_REWL_H

#include <stdlib.h>
#include <math.h>
#include <assert.h>

//#define USE_RNG_GSL
//#define VERBOSE
//#define statphy_rewl_num_vars 2
//#define statphy_rewl_var_t    int
//#define statphy_rewl_max_bins 1e6

// wrappers for random number generators:
#ifdef USE_RNG_GSL

  #include <gsl/gsl_rng.h>
  #define statphy_rewl_rng_t gsl_rng

  gsl_rng*
  statphy_rewl_rng_alloc(void)
  {
    gsl_rng_env_setup();
    const gsl_rng_type* T = gsl_rng_default;
    gsl_rng* rng = gsl_rng_alloc(T);
    return rng;
  }

  void
  statphy_rewl_rng_free(gsl_rng* rng)
  {
    gsl_rng_free(rng);
    return;
  }

  void
  statphy_rewl_rng_set(gsl_rng* rng, const unsigned int value)
  { 
    gsl_rng_set(rng, value);
    return;
  }

  inline double
  statphy_rewl_rng_uniform(statphy_rewl_rng_t* rng)
  { return gsl_rng_uniform(rng); }

#else

  #define statphy_rewl_rng_t unsigned int

  unsigned int*
  statphy_rewl_rng_alloc(void)
  {
    unsigned int* rng = (unsigned int*)malloc(sizeof(unsigned int));
    return rng;
  }

  void
  statphy_rewl_rng_free(unsigned int* rng)
  {
    free(rng);
    return;
  }

  void
  statphy_rewl_rng_set(unsigned int* rng, const unsigned int value)
  { 
    *rng = value;
    return;
  }

  inline double
  statphy_rewl_rng_uniform(statphy_rewl_rng_t* rng)
  { 
    *rng = 1664525 * (*rng) + 1013904223;
    return *rng * 2.32830643708079e-10;
  }

#endif



// alias for array of variables:
typedef statphy_rewl_var_t statphy_rewl_vars_t[statphy_rewl_num_vars];

// holds information about a window:
typedef struct
{
  statphy_rewl_vars_t min;
  statphy_rewl_vars_t max;
  statphy_rewl_vars_t dx;
  statphy_rewl_vars_t length;
  statphy_rewl_vars_t lhs_var;
  statphy_rewl_vars_t rhs_var;
  statphy_rewl_vars_t initial;
  int                 in_range;
  int                 use_runtime_partition;
  double              overlap[statphy_rewl_num_vars];
  int                 num_bins[statphy_rewl_num_vars];
  int                 num_allbins;
  int                 update;
  double              lnf;
  double              raise;
  double*             lng;
  int*                H;
  double              cutoff_fraction;
}
statphy_rewl_win_t;

// holds information about the current state of a walker:
typedef struct
{
  statphy_rewl_vars_t var;
  statphy_rewl_rng_t* rng;
}
statphy_rewl_wlk_t;

// holds global information about windows and walkers:
typedef struct
{
  int                  num_walkers;
  int                  all_H_are_flat;
  statphy_rewl_win_t** win;
  statphy_rewl_wlk_t** wlk;
  statphy_rewl_win_t   gwin;
  double*              lng;
}
statphy_rewl;

// returns the ID of a walker:
inline int
statphy_rewl_wlk_id(void)
{ return omp_get_thread_num(); }

statphy_rewl*
statphy_rewl_alloc(const statphy_rewl_win_t win)
{
  int i;
  assert(omp_get_num_threads() == 1); // w should be public, not private
  statphy_rewl* w     = (statphy_rewl*)malloc(sizeof(statphy_rewl));
  w->num_walkers      = 0;
  w->gwin.num_allbins = 1;
  for (i = 0; i < statphy_rewl_num_vars; i++) {
    w->gwin.min[i]      = win.min[i];
    w->gwin.max[i]      = (win.dx[i] > 0 ? win.max[i] : win.min[i]);
    w->gwin.dx[i]       = (win.dx[i] > 0 ? win.dx[i] : 1);
    w->gwin.length[i]   = win.max[i] - win.min[i];
    w->gwin.lhs_var[i]  = w->gwin.min[i];
    w->gwin.rhs_var[i]  = w->gwin.max[i];
    w->gwin.initial[i]  = win.initial[i];
    w->gwin.overlap[i]  = (i == 0 ? win.overlap[0] : 1.);
    w->gwin.num_bins[i] = 1 + (int)(w->gwin.length[i] / w->gwin.dx[i]);
    w->gwin.num_allbins *= w->gwin.num_bins[i];
  }
  w->gwin.cutoff_fraction = (win.cutoff_fraction > 0 ? 
                             win.cutoff_fraction : 0);
  w->gwin.use_runtime_partition = win.use_runtime_partition;
  assert(w->gwin.num_allbins < statphy_rewl_max_bins);
  w->lng = NULL;
  w->win = NULL;
  w->wlk = NULL;
  return w;
}

void
statphy_rewl_free(statphy_rewl* w)
{
  assert(omp_get_num_threads() == 1); // w is public, not private
  if (w->lng != NULL) free(w->lng);
  if (w->win != NULL) free(w->win);
  if (w->win != NULL) free(w->wlk);
  return;
}

statphy_rewl_win_t*
statphy_rewl_win_alloc(const statphy_rewl* w)
{
  int i;
  const int win_id        = statphy_rewl_wlk_id();
  const size_t size_win   = sizeof(statphy_rewl_win_t);
  statphy_rewl_win_t* win = (statphy_rewl_win_t*)malloc(size_win);
  win->num_allbins        = 1;

  for (i = 0; i < statphy_rewl_num_vars; i++) {
    assert(!(fabs(w->gwin.overlap[i]) > 1.));
    if ((w->num_walkers > 1) && (i == 0)) {
      if (w->gwin.use_runtime_partition) {
        double frac     = w->gwin.overlap[0];
        double x0       = w->gwin.length[0] / (w->num_walkers * 1.);
        double delta    = pow(frac * w->num_walkers, 1./(w->num_walkers-1.));
        double gap      = (1. - frac) * win->length[0] / (w->num_walkers-1.);
        win->length[0]  = x0 * pow(delta, win_id);
        win->min[0]     = w->gwin.min[0] + gap * win_id;
        if (win_id == (w->num_walkers - 1)) {
          win->min[0] = w->gwin.max[0] - win->length[0];
        }
      } else {
        const double gap  = 1. - fabs(w->gwin.overlap[0]);
        const double frac = (1. + gap * (w->num_walkers - 1));
        win->length[0]    = (w->gwin.max[0] - w->gwin.min[0]) / frac;
        win->min[0]       = w->gwin.min[0] + win_id * gap * win->length[0];
      }
    } else {
      win->length[i]  = w->gwin.max[i] - w->gwin.min[i];
      win->min[i]     = w->gwin.min[i];
    }
    win->max[i]       = win->min[i] + win->length[i];
    win->dx[i]        = w->gwin.dx[i];
    win->lhs_var[i]   = win->min[i];
    win->rhs_var[i]   = win->max[i];
    win->initial[i]   = w->gwin.initial[i];
    win->overlap[i]   = (i == 0 ? w->gwin.overlap[i] : 1);
    win->num_bins[i]  = 1 + (int)(win->length[i] / win->dx[i]);
    win->num_allbins *= win->num_bins[i];
  }
  assert(win->num_allbins < statphy_rewl_max_bins);
  win->raise  = 0.;
  win->update = 0;
  win->lnf    = 1.;
  win->cutoff_fraction = w->gwin.cutoff_fraction;
  win->H      = (int*)malloc(win->num_allbins * sizeof(int));
  win->lng    = (double*)malloc(win->num_allbins * sizeof(double));
  return win;
}

void
statphy_rewl_win_free(statphy_rewl_win_t* win)
{
  free(win->H);
  free(win->lng);
  free(win);
  return;
}

statphy_rewl_wlk_t*
statphy_rewl_wlk_alloc(const statphy_rewl* w)
{
  int i;
  const int wlk_id        = statphy_rewl_wlk_id();
  const size_t size_wlk   = sizeof(statphy_rewl_wlk_t);
  statphy_rewl_wlk_t* wlk = (statphy_rewl_wlk_t*)malloc(size_wlk);
  wlk->rng = statphy_rewl_rng_alloc();
  statphy_rewl_rng_set(wlk->rng, wlk_id * 123 + 456);
  for (i = 0; i < statphy_rewl_num_vars; i++) {
    wlk->var[i] = w->gwin.initial[i];
  }
  return wlk;
}

void
statphy_rewl_wlk_free(statphy_rewl_wlk_t* wlk)
{
  statphy_rewl_rng_free(wlk->rng);
  free(wlk);
  return;
}

// checks if a set of variables lies within the range of a window:
inline int
statphy_rewl_in_range(const statphy_rewl_win_t* win,
                      const statphy_rewl_var_t* var)
{
  int i;
  for (i = 0; i < statphy_rewl_num_vars; i++) {
    if (var[i] < win->min[i]) return 0;
    if (var[i] > win->max[i]) return 0;
  }
  return 1;
}

// returns the flat index of the bin corresponding to a set
// of variables within range of a given window:
inline int
statphy_rewl_bin(const statphy_rewl_win_t* win,
                 const statphy_rewl_var_t* var)
{
  int i, index = 0, fac = 1;
  for (i = 0; i < statphy_rewl_num_vars; i++) {
    const int bin = ((var[i] - win->min[i]) / win->dx[i]);
    index += fac * bin;
    fac   *= win->num_bins[i];
  }
  return index;
}

// calculates values of the set of variables corresponding to
// the given flat index of a bin for a given window:
inline void
statphy_rewl_vars_of_bin(const statphy_rewl_win_t* win,
                         const int bin,
                         statphy_rewl_var_t* var)
{
  int i, fac = 1;
  for (i = 0; i < statphy_rewl_num_vars; i++) {
    var[i] = ((bin / fac) % (win->num_bins[i])) * win->dx[i]
             + win->min[i];
    fac   *= win->num_bins[i];
  }
  return;
}

// decides whether a change in the system (e.g. spin flip) should be accepted:
int
statphy_rewl_accept(statphy_rewl_win_t* win,
                    statphy_rewl_wlk_t* wlk,
                    const statphy_rewl_var_t* new_var)
{
  int i;
  int accept  = 0;
  if (0 == win->update) {
    accept = 1;
    for (i = 0; i < statphy_rewl_num_vars; i++) {
      wlk->var[i] = new_var[i];
    }
    if (statphy_rewl_in_range(win, new_var)) {
      win->in_range = 1;
      accept = 2;
    }
  } else {
    if (statphy_rewl_in_range(win, new_var)) {
      const int old_bin = statphy_rewl_bin(win, wlk->var);
      const int new_bin = statphy_rewl_bin(win, new_var);
      const double p    = exp(win->lng[old_bin] - win->lng[new_bin]);
      int current_bin   = old_bin;
      if (p > statphy_rewl_rng_uniform(wlk->rng)) {
        accept = 1;
        current_bin = new_bin;
        for (i = 0; i < statphy_rewl_num_vars; i++) {
          wlk->var[i] = new_var[i];
        }        
      }
      win->H[current_bin] += 1;
      win->lng[current_bin] += win->lnf;
    }
  }
  return accept;
}

// checks if the histogram for a given window is flat according to either of
// two criteria:
// (a) below average: H is flat if none of the non-zero bins count below
//     a certain fraction of the average.
// (b) below count + new bins: H is flat if none of the non-zero bins count
//     below a certain number and no new bin has become non-zero since the
//     last flatness check.
// Criterion (b) is chosen if flatness_threshold > 1, (a) is chosen otherwise.
int
statphy_rewl_flat_H(statphy_rewl_win_t* win,
                    const double flatness_threshold)
{
  int i;
  int count = 0;
  int default_count = 0;
  double average = 0.;
  for (i = 0; i < win->num_allbins; i++) {
    const int Hi = win->H[i];
    if (Hi > 0) {
      if (flatness_threshold > 1.) {
        if (Hi < flatness_threshold) {
          default_count++;
        }
      }
      average += (Hi - average) / (count + 1.);
      count++;
    }
  }
  if (flatness_threshold < 1.) {
    count = 0;
    const double threshold = average * flatness_threshold;
    for (i = 0; i < win->num_allbins; i++) {
      const int Hi = win->H[i];
      if (Hi > 0) {
        if (Hi < threshold) {
          default_count++;
        }
      }
    }
  }
  const int cutoff_count = win->cutoff_fraction * win->num_allbins;
  #ifdef VERBOSE
  fprintf(stderr, "wlk[%d]: default %d/%d\n", 
    omp_get_thread_num(), default_count, cutoff_count);
  #endif
  if (default_count > cutoff_count) {
    return 0;
  } else {
    return 1;
  }
}

// returns the lng value for a set of variables averaged over windows
// which contain that variable, value is normalized to lng(min_var) = 0.
double
statphy_rewl_average_dos(const statphy_rewl* w,
                         const statphy_rewl_var_t* var)
{
  int i;
  int count = 0;
  double average = 0.;
  for (i = 0; i < w->num_walkers; i++) {
    if (statphy_rewl_in_range(w->win[i], var)) {
      const int bin = statphy_rewl_bin(w->win[i], var);
      average += (w->win[i]->lng[bin] - average) / (count + 1.);
      count++;
    }
  }
  statphy_rewl_var_t min[statphy_rewl_num_vars];
  for (i = 0; i < statphy_rewl_num_vars; i++) {
    min[i] = w->gwin.min[i];
  }
  double lng0 = 0;
  count = 0;
  for (i = 0; i < w->num_walkers; i++) {
    if (statphy_rewl_in_range(w->win[i], min)) {
      const int bin = statphy_rewl_bin(w->win[i], min);
      lng0 += (w->win[i]->lng[bin] - lng0) / (count + 1.);
      count++;
    }
  }
  return average - lng0;
}

// calculates and prints the density of states:
void
statphy_rewl_calculate_dos(statphy_rewl* w,
                           void (*func)(statphy_rewl_win_t*,
                                        statphy_rewl_wlk_t*,
                                        const statphy_rewl_var_t*,
                                        void*),
                           void* params,
                           const double modification_factor,
                           const int steps_before_exchange,
                           const int exchanges_before_flatness_check,
                           const double flatness_threshold,
                           const double min_modification,
                           const char* dos_file_name)
{
  // count the number of walkers:
  const int wlk_id = statphy_rewl_wlk_id();
  #pragma omp critical
  {
    w->num_walkers += 1;
  }
  #pragma omp barrier
  #pragma omp single
  {
    assert(w->num_walkers == omp_get_num_threads());
    const size_t size_win_ptr = sizeof(statphy_rewl_win_t*);
    const size_t size_wlk_ptr = sizeof(statphy_rewl_wlk_t*);
    w->win = (statphy_rewl_win_t**)malloc(w->num_walkers * size_win_ptr);
    w->wlk = (statphy_rewl_wlk_t**)malloc(w->num_walkers * size_wlk_ptr);
    w->all_H_are_flat = 0;
  }
  #pragma omp barrier

  // allocate memory for window and walker:
  statphy_rewl_win_t* win = statphy_rewl_win_alloc(w);
  statphy_rewl_wlk_t* wlk = statphy_rewl_wlk_alloc(w);

  // bring walker to within a valid range of variables:
  #ifdef VERBOSE
  fprintf(stderr,"win[%d] var[0]: (%e,%e)\n", omp_get_thread_num(),
    (double)win->min[0], (double)win->max[0]);
  #endif
  win->update = 0;
  win->in_range = 0;
  do {
    func(win, wlk, wlk->var, params);
  } while(!win->in_range);
  win->update = 1;
  #ifdef VERBOSE
  fprintf(stderr,"wlk[%d]: in range...\n",omp_get_thread_num());
  #endif

  // start iteration:
  double start_time = omp_get_wtime();
  int step_count = 0;
  do {
    int i, nx, ns;
    // reset histogram:
    for (i = 0; i < win->num_allbins; i++) { win->H[i] = 0; }
    int flat_count = 0;
    do {
      const int nxsteps = (exchanges_before_flatness_check > 0 
                          ?exchanges_before_flatness_check : 1);
      const int nssteps = (steps_before_exchange > 0 
                          ?steps_before_exchange : 1);
      for (nx = 0; nx < nxsteps; nx++) { 
        // perform random walks:
        for (ns = 0; ns < nssteps; ns++) { 
          func(win, wlk, wlk->var, params);
          step_count++;
        }
        w->win[wlk_id] = win;
        w->wlk[wlk_id] = wlk;
        #pragma omp barrier
        // attempt to exchange windows:
        const int shift = ((nx & 1) ? +1 : -1);
        int valid_wlk = 0;
        if (w->num_walkers & 1) {
          if (shift == +1) {
            if (wlk_id != (w->num_walkers - 1)) {
              valid_wlk = 1;
            }
          } else {
            if (wlk_id != (w->num_walkers - 2)) {
              valid_wlk = 1;
            }
          }
        }
        if ((wlk_id & 1) && (valid_wlk == 1)) {
          const int pwlk_id = (wlk_id+shift+w->num_walkers)%w->num_walkers;
          statphy_rewl_win_t* pwin = w->win[pwlk_id];
          statphy_rewl_wlk_t* pwlk = w->wlk[pwlk_id];
          const int ranges_match = statphy_rewl_in_range(win, wlk->var)
                                && statphy_rewl_in_range(pwin, pwlk->var);
          if (ranges_match) {
            const double old_lng  =win->lng[statphy_rewl_bin(win, wlk->var)];
            const double new_lng  =pwin->lng[statphy_rewl_bin(pwin, wlk->var)];
            const double pold_lng =pwin->lng[statphy_rewl_bin(pwin, pwlk->var)];
            const double pnew_lng =win->lng[statphy_rewl_bin(win, pwlk->var)];
            const double p = exp(old_lng - new_lng + pold_lng - pnew_lng);
            if ((p > statphy_rewl_rng_uniform(wlk->rng))) {
              const  int bin = statphy_rewl_bin(pwin,  wlk->var);
              const int pbin = statphy_rewl_bin( win, pwlk->var);
              win->H[pbin]   += 1;
              win->lng[pbin] += win->lnf;
              pwin->H[bin]   += 1;
              pwin->lng[bin] += pwin->lnf;
              w->win[pwlk_id] = win;
              w->win[wlk_id]  = pwin;
            }
          }
        }
        #pragma omp barrier
        win = w->win[wlk_id];
        #pragma omp barrier
      } // ends nx exchange attempts

      // check flatness of H across all windows:
      {
        int flat = statphy_rewl_flat_H(win, flatness_threshold);
        #pragma omp single
        {
          w->all_H_are_flat = 1;
        }
        #pragma omp barrier
        #pragma omp critical
        {
          if (!flat) { w->all_H_are_flat = 0; }
        }
        #pragma omp barrier
        #ifdef VERBOSE
        #pragma omp master
        {
          fprintf(stderr,"verified flatness for: lnf %e, round %d, time %e s...\n\n",
            win->lnf, flat_count++, omp_get_wtime() - start_time);
        }
        #pragma omp barrier
        #endif
      }
    } while (!w->all_H_are_flat);

    // if windows overlap, average and redistribute their lng values:
    const int valid_cond = (w->num_walkers > 1) 
                        && (win->overlap[0] > 0.9999)
                        && (!w->gwin.use_runtime_partition);
    if (valid_cond) {
      const int num = win->num_allbins;
      #pragma omp barrier
      #pragma omp for
      for (i = 0; i < num; i++) {
        int j;
        double average = 0.;
        for (j = 0; j < w->num_walkers; j++) {
          average += (w->win[j]->lng[i] - average) / (1. + j);
        }
        for (j = 0; j < w->num_walkers; j++) {
          w->win[j]->lng[i] = average;
        }
      }
      #pragma omp barrier
    }
    win->lnf /= (1. * modification_factor);
  } while (win->lnf > min_modification);
  // ends iteration

  #ifdef VERBOSE
  #pragma omp single
  {
    fprintf(stderr, "completed. steps taken = %d, time = %e s\n",
      step_count, omp_get_wtime() - start_time);
  }
  #endif

  // start stitching:
  double min_diff = 1e9;
  #pragma omp barrier
  if (wlk_id > 0) {
    int i, j;
    statphy_rewl_win_t* pwin = w->win[wlk_id - 1];
    for (i = 0; i < win->num_allbins; i++) {
      statphy_rewl_vars_t var;
      statphy_rewl_vars_t pvar;
      statphy_rewl_vars_of_bin(win, i, var);
      statphy_rewl_vars_of_bin(win, i, pvar);
      pvar[0] += w->gwin.dx[0];
      const int rivar   = statphy_rewl_in_range( win,  var);
      const int ripvar  = statphy_rewl_in_range( win, pvar);
      const int rpivar  = statphy_rewl_in_range(pwin,  var);
      const int rpipvar = statphy_rewl_in_range(pwin, pvar);
      if (rivar && ripvar && rpivar && rpipvar) {
        const double lngivar   =  win->lng[statphy_rewl_bin( win,  var)];
        const double lngipvar  =  win->lng[statphy_rewl_bin( win, pvar)];
        const double lngpivar  = pwin->lng[statphy_rewl_bin(pwin,  var)];
        const double lngpipvar = pwin->lng[statphy_rewl_bin(pwin, pvar)];
        const int valid_cond   = (lngivar > 0) && (lngipvar > 0) 
                              && (lngpivar > 0) && (lngpipvar > 0);
        if (valid_cond) {
          const double dlngi_dvar0  = lngipvar  - lngivar;
          const double dlngpi_dvar0 = lngpipvar - lngpivar;
          const double diff         = fabs(dlngi_dvar0 - dlngpi_dvar0);
          if (diff < min_diff) {
            min_diff = diff;
            for (j = 0; j < statphy_rewl_num_vars; j++) {
              win->lhs_var[j] = var[j];
              pwin->rhs_var[j] = var[j];
            }
          }
        }
      }
    }
  }
  #pragma omp barrier
  #pragma omp single
  {
    int i, j;
    statphy_rewl_vars_t var;
    double* lhs_lng = (double*)malloc(w->num_walkers * sizeof(double));
    double* rhs_lng = (double*)malloc(w->num_walkers * sizeof(double));
    for (i = 0; i < w->num_walkers; i++) {
      for (j = 0; j < statphy_rewl_num_vars; j++) {
        var[j] = w->win[i]->lhs_var[j];
      }
      lhs_lng[i] = w->win[i]->lng[statphy_rewl_bin(w->win[i], var)];
      for (j = 0; j < statphy_rewl_num_vars; j++) {
        var[j] = w->win[i]->rhs_var[j];
      }
      rhs_lng[i] = w->win[i]->lng[statphy_rewl_bin(w->win[i], var)];
      w->win[i]->raise = lhs_lng[i];
    }
    for (i = 1; i < w->num_walkers; i++) {
      const double raise = rhs_lng[i - 1] - lhs_lng[i];
      lhs_lng[i] += raise;
      rhs_lng[i] += raise;
    }
    for (i = 0; i < w->num_walkers; i++) {
      w->win[i]->raise = lhs_lng[i] - w->win[i]->raise;
    }
    free(lhs_lng);
    free(rhs_lng);
  }
  #pragma omp barrier
  if (wlk_id > 0) {
    int i;
    for (i = 0; i < w->win[wlk_id]->num_allbins; i++) {
      win->lng[i] += win->raise;
    }
  }
  #pragma omp barrier
  // end stitching

  // compile and print dos:
  #pragma omp single
  {
    FILE* fp;
    if (dos_file_name == NULL) {
      fp = fopen("dos.tmp","w");
    } else {
      fp = fopen(dos_file_name, "w");
    }
    statphy_rewl_vars_t vars;
    int i;
    for (i = 0; i < w->gwin.num_allbins; i++) { 
      statphy_rewl_vars_of_bin(&(w->gwin), i, vars);
      fprintf(fp, "%e ", statphy_rewl_average_dos(w, vars));
    }
    fclose(fp);
  }
  #pragma omp barrier

  statphy_rewl_win_free(win);
  statphy_rewl_wlk_free(wlk);  

  return;  
}

// reads lng from a file into the global structure:
void
statphy_rewl_read_dos(statphy_rewl* w, const char* dos_file_name)
{
  #pragma omp barrier
  #pragma omp single
  {
    FILE* fp;
    if (dos_file_name == NULL) {
      fp = fopen("dos.tmp","r");
    } else {
      fp = fopen(dos_file_name, "r");
    }
    w->lng = (double*)malloc(w->gwin.num_allbins * sizeof(double));
    int i;
    for (i = 0; i < w->gwin.num_allbins; i++) { 
      assert(EOF != fscanf(fp,"%lf",&(w->lng[i])));
    }
    fclose(fp);
  }
  #pragma omp barrier
  return;
}

#endif // STATPHY_REWL_H
