/*
  Density of states for 3 state Potts model with vortex suppression.
  g++ -std=c++11 -Wall -O3 -fopenmp rewl_dw_vx.cpp -o rewl_dw_vx
  
  write dos:
  ./rewl_dw_vx -write -L 8 -runtime -overlap 0.75 -mulf 4 -minf 1e-6 -t 1000 -x 100 -h 0.8
  read dos:
  ./rewl_dw_vx -read -L 8 -minl -6.0 -maxl 6.0 -incl 0.2 -minkT 0.0 -maxkT 3.0 -inckT 0.1  

*/

#include <iostream>
#include <fstream>
#include <string>
#include <omp.h>

#define USE_RNG_GSL
#define VERBOSE
#define statphy_rewl_num_vars   2
#define statphy_rewl_var_t      int
#define statphy_rewl_max_bins   1e6
#include "rewl.h"

// macro for iteration over a square grid with
// four sublattice decomposition:
#define decompose_four(x, y, L, stuff) do { \
  for (y = 0; y < L; y+= 2) {               \
    for (x = 0; x < L; x+=2) {              \
      {stuff;}                              \
    }                                       \
  }                                         \
  for (y = 0; y < L; y+= 2) {               \
    for (x = 1; x < L; x+=2) {              \
      {stuff;}                              \
    }                                       \
  }                                         \
  for (y = 1; y < L; y+= 2) {               \
    for (x = 0; x < L; x+=2) {              \
      {stuff;}                              \
    }                                       \
  }                                         \
  for (y = 1; y < L; y+= 2) {               \
    for (x = 1; x < L; x+=2) {              \
      {stuff;}                              \
    }                                       \
  }                                         \
} while(0)

// winding number for three state spins:
constexpr inline int
wn3(const int s0, const int s1,
    const int s2, const int s3)
{ return (((s0 != s1) + (s1 != s2) 
         + (s2 != s3) + (s3 != s0)) == 3); }

// holds system parameters:
typedef struct
{
  int       L;
  int*      s;
}
sys_params;

void
func(statphy_rewl_win_t* win, statphy_rewl_wlk_t* wlk, 
     const statphy_rewl_var_t* var, void* params)
{
  const int L   = ((sys_params*)params)->L;
  int*      s   = ((sys_params*)params)->s;
  statphy_rewl_rng_t* rng = wlk->rng;
  int x, y;
  statphy_rewl_vars_t cur_var;
  cur_var[0] = var[0];
  cur_var[1] = var[1];
  statphy_rewl_vars_t new_var;
  decompose_four(x, y, L,
    // s7 s8 s9
    // s4 s0 s6
    // s1 s2 s3
    const int s1 = s[(x==0?L-1:x-1) + L * (y==0?L-1:y-1)];
    const int s2 = s[ x             + L * (y==0?L-1:y-1)];
    const int s3 = s[(x==L-1?0:x+1) + L * (y==0?L-1:y-1)];
    const int s4 = s[(x==0?L-1:x-1) + L *  y            ];
    const int s0 = s[ x             + L *  y            ];
    const int s6 = s[(x==L-1?0:x+1) + L *  y            ];
    const int s7 = s[(x==0?L-1:x-1) + L * (y==L-1?0:y+1)];
    const int s8 = s[ x             + L * (y==L-1?0:y+1)];
    const int s9 = s[(x==L-1?0:x+1) + L * (y==L-1?0:y+1)];
    const int flip_right = (statphy_rewl_rng_uniform(rng) > 0.5);
    const int sn = (s0 == 0 ? (flip_right ? 1 : 2) :
                    s0 == 1 ? (flip_right ? 2 : 0) :
                              (flip_right ? 0 : 1));
    const int dNdw =  ((sn != s6) + (sn != s8) + (sn != s4) + (sn != s2))
                    - ((s0 != s6) + (s0 != s8) + (s0 != s4) + (s0 != s2));
    const int dNvx =  ( wn3(sn, s6, s9, s8) + wn3(sn, s8, s7, s4)
                      + wn3(sn, s4, s1, s2) + wn3(sn, s2, s3, s6))
                    - ( wn3(s0, s6, s9, s8) + wn3(s0, s8, s7, s4)
                      + wn3(s0, s4, s1, s2) + wn3(s0, s2, s3, s6));
    new_var[0] = cur_var[0] + dNdw;
    new_var[1] = cur_var[1] + dNvx;
    const int accept_flip = statphy_rewl_accept(win, wlk, new_var);
    if (accept_flip) {
      s[x + L * y]  = sn;
      cur_var[0] = new_var[0];
      cur_var[1] = new_var[1];
    }
    if (accept_flip == 2) {
      return;  
    }
  ); 
  return;
}

double
action(const statphy_rewl_var_t* vars, const double* params)
{
  // -beta*Ndw -beta*l*Nvx
  return -params[0] * vars[0] - params[0] * params[1] * vars[1];
}

std::string
dos_filename(std::string& fprex, const int L)
{
  std::string filename = fprex
                         + "_L_" + std::to_string(L)
                         + "_dos.dat";
  return filename;
}

std::string
data_filename(std::string& fprex, const int L, 
              const int oneparam, const double param)
{
  std::string filename;
  if (oneparam == -1) {
    filename = fprex
             + "_L_" + std::to_string(L)
             + "_data.dat";
  } else if (oneparam == 0) {
    filename = fprex
             + "_l_" + std::to_string(param)
             + "_L_" + std::to_string(L)
             + "_data.dat";
  } else if (oneparam == 1) {
    filename = fprex
             + "_kT_" + std::to_string(param)
             + "_L_" + std::to_string(L)
             + "_data.dat";
  }
  return filename;
}

void
write(statphy_rewl* w, int argc, char** argv)
{
  std::string fprex = argv[0];
  int     L    = 1;
  double  mulf = 4;
  double  minf = 0.9;
  int     t    = 1;
  int     x    = 1;
  double  h    = 0.0;
  for (int i = 1; i < argc; i++) {
    const std::string p = argv[i];
    if (p == std::string("-L"))     L     = std::stoi(argv[i+1]);
    if (p == std::string("-mulf"))  mulf  = std::stod(argv[i+1]);
    if (p == std::string("-minf"))  minf  = std::stod(argv[i+1]);
    if (p == std::string("-t"))     t     = std::stoi(argv[i+1]);
    if (p == std::string("-x"))     x     = std::stoi(argv[i+1]);
    if (p == std::string("-h"))     h     = std::stod(argv[i+1]);
  }
  double start_time = omp_get_wtime();
  #pragma omp parallel
  {
    int* s = (int*)malloc(L * L * sizeof(int));
    sys_params params;
    params.L    = L;
    params.s    = s;
    auto filename = dos_filename(fprex, L);
    statphy_rewl_calculate_dos(w, func, &params, mulf, t, x, h, 
                               minf, filename.c_str());
    free(s);
  }
  double end_time = omp_get_wtime();
  fprintf(stderr, "time taken = %e s\n", end_time - start_time);
  return;
}

inline bool
file_exists(const std::string& name) {
    if (FILE *file = fopen(name.c_str(), "r")) {
        fclose(file);
        return true;
    } else {
        return false;
    }
}

void
read(statphy_rewl* w, int argc, char** argv)
{
  std::string fprex = argv[0];
  int     L         = 1;
  double  minl      = 0.0;
  double  maxl      = 1000.0;
  double  incl      = 10000.0;
  double  minkT     = 0.0;
  double  maxkT     = 1000.0;
  double  inckT     = 10000.0;
  int     oneparam  = -1;
  for (int i = 1; i < argc; i++) {
    const std::string p = argv[i];
    if (p == std::string("-L"))     L     = std::stoi(argv[i+1]);
    if (p == std::string("-minl"))  minl  = std::stod(argv[i+1]);
    if (p == std::string("-maxl"))  maxl  = std::stod(argv[i+1]);
    if (p == std::string("-incl"))  incl  = std::stod(argv[i+1]);
    if (p == std::string("-minkT")) minkT = std::stod(argv[i+1]);
    if (p == std::string("-maxkT")) maxkT = std::stod(argv[i+1]);
    if (p == std::string("-inckT")) inckT = std::stod(argv[i+1]);
    if (p == std::string("-onel"))  oneparam = 0;
    if (p == std::string("-onekT")) oneparam = 1;
  }
  if (oneparam == 0) {
    maxl = 1000;
    incl = 10000;
  }
  if (oneparam == 1) {
    maxkT = 1000;
    inckT = 10000;
  }
  
  {
    auto filename = dos_filename(fprex, L);
    if (file_exists(filename)) {
      statphy_rewl_read_dos(w, filename.c_str());
    }
  }

  std::string datafile = data_filename(fprex, L, oneparam, 
                         (oneparam == 0 ? minl : minkT));
  FILE* fp;
  fp = fopen(datafile.c_str(),"w");
  fclose(fp);
  for (double l = minl; l < maxl; l += incl) {
  for (double kT = minkT; kT < maxkT; kT += inckT) {
    const double beta = 1./kT;
    double params[statphy_rewl_num_vars];
    params[0] = beta;
    params[1] = l;

    double log_Z = -1e9;
    for (int i = 0; i < w->gwin.num_allbins; i++) {
      statphy_rewl_vars_t vars;
      statphy_rewl_vars_of_bin(&(w->gwin), i, vars);
      const double log_Z_E = w->lng[i] + action(vars, params);
      log_Z = (log_Z>log_Z_E?log_Z:log_Z_E)
            + log(1. + exp(-1.*fabs(log_Z-log_Z_E)));
    }
    double av_e = 0., av_e2 = 0.;
    double av_dw = 0., av_dw2 = 0.;
    double av_vx = 0., av_vx2 = 0.;
    for (int i = 0; i < w->gwin.num_allbins; i++) {
      statphy_rewl_vars_t vars;
      statphy_rewl_vars_of_bin(&(w->gwin), i, vars);
      const double wt = exp(w->lng[i]-log_Z+action(vars,params));
      const statphy_rewl_var_t Ndw = vars[0];
      const statphy_rewl_var_t Nvx = vars[1];
      const double dw = Ndw / (1. * L * L);
      const double vx = Nvx / (1. * L * L);
      const double e = dw + l * vx;
      const double dw2 = dw * dw;
      const double vx2 = vx * vx;
      const double e2 = e * e;
      av_e  += e  * wt;
      av_e2 += e2 * wt;
      av_dw  += dw  * wt;
      av_dw2 += dw2 * wt;
      av_vx  += vx  * wt;
      av_vx2 += vx2 * wt;
    }
    double avc_e2 = 0., avc_e3 = 0., avc_e4 = 0.;
    double avc_dw2 = 0., avc_dw3 = 0., avc_dw4 = 0.;
    double avc_vx2 = 0., avc_vx3 = 0., avc_vx4 = 0.;
    for (int i = 0; i < w->gwin.num_allbins; i++) {
      statphy_rewl_vars_t vars;
      statphy_rewl_vars_of_bin(&(w->gwin), i, vars);
      const double wt = exp(w->lng[i]-log_Z+action(vars,params));
      const statphy_rewl_var_t Ndw = vars[0];
      const statphy_rewl_var_t Nvx = vars[1];
      const double dw = Ndw / (1. * L * L);
      const double vx = Nvx / (1. * L * L);
      const double cdw = dw - av_dw;
      const double cvx = vx - av_vx;
      const double e = dw + l * vx;
      const double ce = e - av_e;
      avc_dw2  += (cdw * cdw) * wt;
      avc_dw3  += (cdw * cdw) * (cdw * wt);
      avc_dw4  += (cdw * cdw) * (cdw * cdw) * wt;
      avc_vx2  += (cvx * cvx) * wt;
      avc_vx3  += (cvx * cvx) * (cvx * wt);
      avc_vx4  += (cvx * cvx) * (cvx * cvx) * wt;
      avc_e2  += (ce * ce) * wt;
      avc_e3  += (ce * ce) * (ce * wt);
      avc_e4  += (ce * ce) * (ce * ce) * wt;
    }
    fp = fopen(datafile.c_str(),"a");
    fprintf(fp, "%e\t", l);
    fprintf(fp, "%e\t", kT);
    fprintf(fp, "%e\t", av_e);
    fprintf(fp, "%e\t", (L * L) * (beta * beta) * (av_e2 - av_e * av_e));
    fprintf(fp, "%e\t", avc_e3 * (L * L * L) * (beta * beta * beta));
    fprintf(fp, "%e\t", avc_e4 / (avc_e2 * avc_e2)) ;
    fprintf(fp, "%e\t", av_dw);
    fprintf(fp, "%e\t", (L * L) * (beta * beta)*(av_dw2 - av_dw * av_dw));
    fprintf(fp, "%e\t", avc_dw3 * (L * L * L)*(beta*beta*beta));
    fprintf(fp, "%e\t", avc_dw4 / (avc_dw2 * avc_dw2)) ;
    fprintf(fp, "%e\t", av_vx);
    fprintf(fp, "%e\t", (L * L) * (beta*beta)*(av_vx2 - av_vx * av_vx));
    fprintf(fp, "%e\t", avc_vx3 * (L * L * L)*(beta*beta*beta));
    fprintf(fp, "%e\t", avc_vx4 / (avc_vx2 * avc_vx2)) ;
    fprintf(fp, "\n");
    fclose(fp);

  } // ends kT loop
  } // ends l loop
  return;
}

int
main(int argc, char** argv)
{
  int dxDW           = 1;
  int dxVX           = 2;
  int L           = 1;
  int flag_read   = 1;
  double overlap  = 1.;
  double cutoff   = 1e-3;
  int use_runtime_partition = 0;
  for (int i = 1; i < argc; i++) {
    const std::string p = argv[i];
    if (p == std::string("-dxDW"))      dxDW = std::stoi(argv[i+1]);
    if (p == std::string("-dxVX"))      dxVX = std::stoi(argv[i+1]);
    if (p == std::string("-L"))       L = std::stoi(argv[i+1]);
    if (p == std::string("-overlap")) overlap = std::stod(argv[i+1]);
    if (p == std::string("-cutoff"))  cutoff = std::stod(argv[i+1]);
    if (p == std::string("-runtime")) use_runtime_partition = 1;
    if (p == std::string("-write"))   flag_read = 0;
    if (p == std::string("-read"))    flag_read = 1;
  }
  statphy_rewl_win_t vd;
  const int N     = L * L;
  vd.min[0]       = 0;    
  vd.max[0]       = 2 * N; 
  vd.dx[0]        = dxDW;
  vd.overlap[0]   = overlap; 
  vd.initial[0]   = 0;
  vd.min[1]       = 0;    
  vd.max[1]       = 1 * N; 
  vd.dx[1]        = dxVX;
  vd.overlap[1]   = 1.00; 
  vd.initial[1]   = 0;
  vd.use_runtime_partition = use_runtime_partition;
  vd.cutoff_fraction = cutoff;
  statphy_rewl* w = statphy_rewl_alloc(vd);
  if (flag_read == 0) {
    write(w, argc, argv);
  } else {
    read(w, argc, argv);
  }
  statphy_rewl_free(w);
  return 0;
}
