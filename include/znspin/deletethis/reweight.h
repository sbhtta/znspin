// Copyright (C) 2015 Soumyadeep Bhattacharya
// The MIT License (see LICENSE file for details).

/**
@file reweight.h
@brief collects data series for multiple variables
*/

#ifndef QTOOLS_REWEIGHT_H
#define QTOOLS_REWEIGHT_H

#include <array>
#include <vector>
#include <cassert>
#include <cmath>
#include <algorithm>
#include <qtools/stats.h>
#include <qtools/error.h>

namespace qtools {

/**
@defgroup qtools_reweight Data Reweighting
@ingroup qtools
@brief collects data series for multiple variables and
reweights them to other couplings
*/

/**
@addtogroup qtools_reweight
*/
/// @{

template<std::size_t NP>
using reweight_params = std::array<double,NP>;

template<std::size_t NV>
using reweight_vars = std::array<double,NV>;

/**
@brief collects data series for multiple variables
@tparam NP Number of parameters
@details Sample Usage:
@code{.cpp}
#include <qtools/reweight.h>

int main()
{
    constexpr const int NP = 2;
    constexpr const int NV = 4;
    int nSM = 1000;
    int num_vertices = 16;
    std::vector< qtools::reweight_params<NP> > param_points;
    param_points.push_back({2.00, 0.00});
    param_points.push_back({1.00, 0.00});
    param_points.push_back({0.70, 0.00});

    // gather data:
    qtools::reweight<2> hw(param_points, nSM, 100, 100, num_vertices);
    for (int p = 0; p < hw.num_parameter_points(); p++) {
        const auto params = hw.parameters(p);
        hw.open_parameter_point(p);
        for (int t = 0; t < nSM; t++) {
            // update ...
            qtools::reweight_params<NP> pvars;
            pvars[0] = -0.5;
            pvars[1] = 1.0;
            hw.add_data(pvars);
        }
        hw.close_parameter_point();
    }

    // reweight:
    auto varfunc =  [&](const qtools::reweight_params<NP> pvars) {
                        qtools::reweight_vars<NV> vars;
                        vars[0] = pvars[0];
                        vars[1] = pvars[0] * pvars[0];
                        vars[2] = pvars[1];
                        vars[3] = pvars[1] * pvars[1];
                        return vars;
                    };    
    constexpr const int nbins = 5;
    for (double kT = 0.60; kT < 1.80; kT += 0.10) {
        qtools::reweight_params<NP> params = {0};
        params[0] = 1.0 / kT;
        params[1] = 0.0;
        const auto data = hw.data_reweight<NV>(params, nbins, varfunc);
        std::cout << data[1] - data[0] * data[0] << std::endl;
    }
}
@endcode
*/
template<std::size_t NP>
class reweight
{
    using params_t = reweight_params<NP>;

    public:
    reweight(const std::vector<params_t>& param_points,
                const std::size_t series_max_size,
                const std::size_t lag_max,
                const std::size_t maxlagratio,
                const std::size_t num_vertices)
    {
        // populate parameter points list:
        f_parameter_points.resize(0);
        for (auto& plist : param_points) {
            f_parameter_points.push_back(plist);
        }
        // reserve space for data series:
        f_data.resize(num_parameter_points());
        for (auto& data_list : f_data) {
            data_list.reserve(series_max_size);
        }
        // resize autocorrelator list:
        f_autocorrelators.resize(NP);
        for (auto& autocorrelator_i : f_autocorrelators) {
            autocorrelator_i.resize(lag_max);
        }
        f_exptaus.resize(num_parameter_points());
        f_lag_max = lag_max;
        f_maxlagratio = maxlagratio;
        f_num_vertices = num_vertices;
        // resize free energies:
        exp_minus_fi.resize(num_parameter_points());
        f_free_energies_calculated = false;
    }

    /**
    @brief returns a reference to the number of values 
    for a given coupling
    */
    std::size_t
    num_parameter_points() const
    { return f_parameter_points.size(); }

    /**
    @brief returns the value at the given index in the 
    list of values for a given coupling
    */ 
    std::array<double,NP>
    parameters(const std::size_t n) const
    { return f_parameter_points.at(n); }

    /**
    @brief sets the current parameter point
    */
    void
    open_parameter_point(const std::size_t n)
    {
        assert(n < num_parameter_points());
        if (f_parameter_point_closed != true) {
            close_parameter_point();
        } 
        f_current_parameter_point = n;
        for (auto& autocorrelator_i : f_autocorrelators) {
            autocorrelator_i.resize(f_lag_max);
        }
        return; 
    }

    /**
    @brief closes the current parameter point
    */
    void
    close_parameter_point()
    {
        params_t taus;
        double max_tau = 0.0;
        for (std::size_t i = 0; i < NP; i++) {
            const double taui = tau(i);
            if (taui > max_tau) {
                max_tau = taui;
            }
        }
        f_exptaus[f_current_parameter_point] = 1 + 2 * max_tau;
        f_parameter_point_closed == true;
        return; 
    }

    /**
    @brief adds data to series
    */
    void
    add_data(const params_t value)
    {
        f_data[f_current_parameter_point].push_back(value); 
        if (f_lag_max > 1) {
            for (std::size_t a = 0; a < NP; a++) {
                f_autocorrelators[a].add_data(value[a]);
            }
        }
        return;
    }

	/**
	@brief returns a reference to the autocorrelator for
    the time series recorded for a given variable
    @param Index of the variable.
	*/
	autocorrelator&
	A(const std::size_t ivar)
	{ return f_autocorrelators[ivar]; }

	/**
	@brief returns the integrated autocorrelation time for a given variable
    @param var Index of the variable.
	@param count_to_tau_ratio Ratio between the maximum lag of
	the autocorrelation function and the estimated autocorrelation
	time.
	*/
	double 
	tau(const std::size_t ivar)
	{ return f_autocorrelators[ivar].tau(f_maxlagratio); }
    
    /**
    @brief returns the value at the given index in the 
    list of values for a given coupling
    */ 
    int
    g(const std::size_t n) const
    {
        if (f_lag_max > 1) { 
            return (f_exptaus.at(n) > 1 ? f_exptaus.at(n) : 1);
        } else {
            return 1;
        }
    }

    /**
    @brief returns the value at the given index in the 
    list of values for a given coupling
    */ 
    int
    size_series(const std::size_t n) const
    { return f_data.at(n).size(); }

    /**
    @brief returns the value at the given index in the 
    list of values for a given coupling
    */ 
    double
    exponent(const params_t params, 
             const std::size_t m, 
             const std::size_t t) const
    {
        double sum = 0.0;
        const auto values = f_data.at(m).at(t);
        for (std::size_t i = 0; i < NP; i++) { 
            sum += -params[i] * f_num_vertices * values[i];
        }
        return sum;
    }

		/**
    @brief returns the value at the given index in the 
    list of values for a given coupling
    */ 
    double
    exponent(const params_t params1,
						 const params_t params2, 
             const std::size_t m, 
             const std::size_t t) const
    {
        double sum = 0.0;
        const auto values = f_data.at(m).at(t);
        for (std::size_t i = 0; i < NP; i++) { 
            sum += -(params1[i] - params2[i]) * f_num_vertices * values[i];
        }
        return sum;
    }

    /**
    @brief calculate free energies
    */
    std::size_t
    calculate_free_energies()
    {
        std::vector<double> prev_exp_minus_fi(exp_minus_fi.size());
        // initialize values:
        for (std::size_t i = 0; i < num_parameter_points(); i++) {
            exp_minus_fi[i] = 1.;
            prev_exp_minus_fi[i] = exp_minus_fi[i];
        }
				int count = 0;
    double max_err = 100.;
    do {
        max_err = 0.;
        for (int i = 0; i < num_parameter_points(); i++) {
            exp_minus_fi[i] = 0.;
            #pragma omp parallel
            {
                double local_exp_minus_fi = 0.;
                #pragma omp for
                for (int a = 0; a < size_series(i); a++) {
                    double log_denom = -1.0e9;
                    for (int j = 0; j < num_parameter_points(); j++) {
                        const double tmp = exponent(parameters(j),parameters(i), i, a);
                        const double log_denom_j = tmp 
                                    + std::log(size_series(j)/prev_exp_minus_fi[j]);
                        log_denom = std::max(log_denom,log_denom_j)
                             + std::log(1. + std::exp(-1.*std::abs(log_denom-log_denom_j)));
                    } // ends j
                    local_exp_minus_fi += std::exp(-1. * log_denom);
                } // ends a
                #pragma omp critical
                {
                    exp_minus_fi[i] += local_exp_minus_fi;
                }
            } // ends omp parallel
        } // ends i
        for (int i = 1; i < num_parameter_points(); i++) {
            exp_minus_fi[i] = exp_minus_fi[i]/exp_minus_fi[0];
						double i_err = std::abs(exp_minus_fi[i]/prev_exp_minus_fi[i] - 1.);
            if (i_err > max_err) max_err = i_err;
            prev_exp_minus_fi[i] = exp_minus_fi[i];
        }
        exp_minus_fi[0] = 1.;
        prev_exp_minus_fi[0] = 1.;
        count++;
    } while ((max_err > 1e-3) && (count < 1e4));
       f_free_energies_calculated = true;
        return count;
    }

    /**
    @brief calculate free energies
    */
    double
    exp_minus_f(const params_t new_params)
    {
		double new_value = 0.;
		for (int i = 0; i < num_parameter_points(); i++) {
            #pragma omp parallel
            {
                double local_new_value = 0.;
                #pragma omp for
                for (int a = 0; a < size_series(i); a++) {
                    double log_denom = -1.0e9;
                    for (int j = 0; j < num_parameter_points(); j++) {
                        const double tmp = exponent(parameters(j),new_params, i, a);
                        const double log_denom_j = tmp 
                                    + std::log(size_series(j)/exp_minus_fi[j]);
                        log_denom = std::max(log_denom,log_denom_j)
                             + std::log(1. + std::exp(-1.*std::abs(log_denom-log_denom_j)));
                    } // ends j
                    local_new_value += std::exp(-1. * log_denom);
                } // ends a
                #pragma omp critical
                {
                    new_value += local_new_value;
                }
            } // ends omp parallel
        } // ends i
        return new_value;
    }

    template<std::size_t NV, typename VarFncT>
    std::array<double,NV>    
    data_stats(const params_t new_params, 
                  const  int m,
                  const int num_bins,
                  VarFncT&& varfunc)
    {
        using vars_t = reweight_vars<NV>;
        // check calculate_free_energies:
        assert(m < num_bins);
        if (f_free_energies_calculated != true) {
            calculate_free_energies();
        }
        const double exp_minus_fn = exp_minus_f(new_params);
        f_exp_minus_fn = exp_minus_fn;
        // calculate bin sizes: 
        std::vector<int> size_block(num_parameter_points(), 0);
        std::vector<int> size_fit_series(num_parameter_points(), 0);
        for (int i = 0; i < num_parameter_points(); i++) {
            size_block[i] = size_series(i) / num_bins;
            size_fit_series[i] = size_block[i] * num_bins;
        }
        // calculate partial means for each variable:
            std::array<double,NV> new_value;
            for (std::size_t n = 0; n < NV; n++) {
                new_value[n] = 0.0;
            }
			for (int i = 0; i < num_parameter_points(); i++) {
            const int size_reduced_series_i 
                    = size_fit_series[i] - size_block[i];
                const int size_fit_series_i = size_fit_series[i];
            #pragma omp parallel
            {
                std::array<double, NV> local_new_value;
                for (std::size_t n = 0; n < NV; n++) {
                    local_new_value[n] = 0.0;
                }
                #pragma omp for
                for (int a = 0; a < size_fit_series_i; a++) {
                    if ((num_bins < 2) || ((a / size_block[i]) != m)) {
                    double log_denom = -1.0e9;
                    for (int j = 0; j < num_parameter_points(); j++) {
                        const double tmp = exponent(parameters(j),new_params, i, a);
                        const double log_denom_j = tmp 
                                    + std::log((size_fit_series[j]-size_block[j])*(exp_minus_fn/exp_minus_fi[j]));
                        log_denom = std::max(log_denom,log_denom_j)
                             + std::log(1. + std::exp(-1.*std::abs(log_denom-log_denom_j)));
                    } // ends j
					const auto pvars = f_data[i][a];
                    const auto vars = varfunc(pvars);
					const double w = std::exp(-1. * log_denom);
                    for (std::size_t n = 0; n < NV; n++) {
                        local_new_value[n] += vars[n] * w;
                    }
                    }
                } // ends a
                #pragma omp critical
                {
                    for (std::size_t n = 0; n < NV; n++) {
                        new_value[n] += local_new_value[n];
                    }    
                }
            } // ends omp parallel
        } // ends i
        return new_value;    
    }

    template<std::size_t NV, typename VarFncT>
    std::array<double,NV>    
    data_stats(const std::size_t i,
        const int m,
        const int num_bins,
             VarFncT&& varfunc)
    {
        using vars_t = reweight_vars<NV>;
        // calculate bin sizes: 
        assert(m < num_bins);
        std::vector<int> size_block(num_parameter_points(), 0);
        std::vector<int> size_fit_series(num_parameter_points(), 0);
        for (int i = 0; i < num_parameter_points(); i++) {
            size_block[i] = size_series(i) / num_bins;
            size_fit_series[i] = size_block[i] * num_bins;
        }
        // calculate partial means for each variable:
        std::array<double,NV> partial_mean;
        std::array<double, NV> new_value;
            for (std::size_t n = 0; n < NV; n++) {
                new_value[n] = 0.0;
            }
            const int size_reduced_series_i 
                    = size_fit_series[i] - size_block[i];
                const int size_fit_series_i = size_fit_series[i];
            #pragma omp parallel
            {
                std::array<double, NV> local_new_value;
                for (std::size_t n = 0; n < NV; n++) {
                    local_new_value[n] = 0.0;
                }
                #pragma omp for
            for (int a = 0; a < size_fit_series_i; a++) {
                    if ((num_bins < 2) || ((a / size_block[i]) != m)) {
								const auto pvars = f_data[i][a];
                const auto vars = varfunc(pvars);
                        for (std::size_t n = 0; n < NV; n++) {
                            local_new_value[n] += vars[n];
                        }
                }
            } // ends a
                #pragma omp critical
                {
                    for (std::size_t n = 0; n < NV; n++) {
                        new_value[n] += local_new_value[n];
                    }    
                }
            } // ends omp parallel
            for (std::size_t n = 0; n < NV; n++) {
                const double num = size_fit_series[i] - size_block[i];
                partial_mean[n] = new_value[n] / num;
            }
     
        return partial_mean;    
    }

    double
    Z() const
    { return f_exp_minus_fn; }

    const params_t&
    unitZ_parameters() const
    { return f_parameter_points[0]; }

    int f_num_vertices;
    std::vector<params_t> f_parameter_points;
    ///< @brief stores the parameter values for each point
    ///  in the parameter space
    std::vector< std::vector<params_t> > f_data;
    ///< @brief stores the parameter values for each point
    ///  in the parameter space
	int f_lag_max = 0;
	///< @brief stores the maximum lag for calculating autocorrelation
	std::vector<autocorrelator> f_autocorrelators;
	///< @brief stores the autocorrelators for each variable
    std::size_t f_current_parameter_point = 0;
    bool f_parameter_point_closed = true;
    int f_maxlagratio;
	std::vector<int> f_exptaus;
    std::vector<double> exp_minus_fi;
    bool f_free_energies_calculated = false;
    double f_exp_minus_fn = 1;
    
};
/// @}

} // ends namespace qtools

#endif // QTOOLS_REWEIGHT_H
