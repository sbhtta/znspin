#ifndef ZNSPIN_CCL_BONDWISE_H
#define ZNSPIN_CCL_BONDWISE_H

#include <array>
#include <vector>
#include <cassert>
#include <algorithm>
#include <iostream>

#include <znspin/model.h>

namespace znspin {

template <lattice_name LatNm>
class ccl_bondwise_domain_walls;

template <std::size_t N, lattice_name LatNm>
class ccl_bondwise_vortex_strings;

template<lattice_name LatNm>
class ccl_bondwise
{
  public:
  // lattice constants:
  static constexpr const int D            = dimension(LatNm);
  static constexpr const int NumOutBonds  = num_outgoing_bonds(LatNm);

  // constructor:
  ccl_bondwise(const std::array<int,D>& axis_length)
  : length_(axis_length)
  : num_bonds_(num_vertices_in_lattice(LatNm, init_length) * NumOutBonds)
  { label_bond_.resize(num_bonds()); }

  // capacity:
  int num_bonds() const
  { return size(); }
  int size() const
  { return num_bonds(); }

  // label domain walls:
  template<int ox0, int ox1, int ox2 = 0, int ox3 = 0, typename DefDWFn = int>
	void label_domain_walls(const spinconfig<LatNm>& s, const DefDWFn& defdw)
  {
    // set boundary condition along each axis:
    if (D > 0) openx_[0] = ox0;
    if (D > 1) openx_[1] = ox1;
    if (D > 2) openx_[2] = ox2;
    if (D > 3) openx_[3] = ox3;

    // clear label flags:
		label_histogram_filled_     = false;
		axis_face_has_label_filled_ = false;
		largest_component_found_    = false;

    // initialize label values of bonds with their indices:
    for (int i = 0; i < size(); i++) {
      label_bond_[i] = i;
    }

    if (LatNm == lattice_name::square) {
      for_2D(x, y, length_[0], length_[1],
        // vertex neighbor map:
        // i7 i8 i9
        // i4 i0 i6
        // i1 i2 i3
        const auto i7  = iw(x-1,y+1); const auto i8  = iw(x+0,y+1); const auto i9  = iw(x+1,y+1);
        const auto i4  = iw(x-1,y+0); const auto i0  = iw(x+0,y+0); const auto i6  = iw(x+1,y+0);
                                      const auto i2  = iw(x+0,y-1); const auto i3  = iw(x+1,y-1);

        // copy spin states: 
        const auto s7  = s[i7 ]; const auto s8  = s[i8 ]; const auto s9  = s[i9 ];
        const auto s4  = s[i4 ]; const auto s0  = s[i0 ]; const auto s6  = s[i6 ];
                                 const auto s2  = s[i2 ]; const auto s3  = s[i3 ];

        // label +x bond :
        if (defdw(s0, s6, 0)) {
          if (valbend<  +0,-ox1>(x,y)) if (defdw(s2, s3, 0)) merge_roots(bond_index(0, i0), bond_index(0, i2));
          if (valbend<-ox0,-ox1>(x,y)) if (defdw(s2, s0, 1)) merge_roots(bond_index(0, i0), bond_index(1, i2));
          if (valbend<+ox0,-ox1>(x,y)) if (defdw(s3, s6, 1)) merge_roots(bond_index(0, i0), bond_index(1, i3));
          if (valbend<  +0,+ox1>(x,y)) if (defdw(s8, s9, 0)) merge_roots(bond_index(0, i0), bond_index(0, i8));
          if (valbend<-ox0,+ox1>(x,y)) if (defdw(s0, s8, 1)) merge_roots(bond_index(0, i0), bond_index(1, i0));
          if (valbend<+ox0,+ox1>(x,y)) if (defdw(s6, s9, 1)) merge_roots(bond_index(0, i0), bond_index(1, i6));
        }
        // label +y bond :
        if (defdw(s0, s8, 1)) {
          if (valbend<-ox0,  +0>(x,y)) if (defdw(s4, s7, 1)) merge_roots(bond_index(1, i0), bond_index(1, i4));
          if (valbend<-ox0,-ox1>(x,y)) if (defdw(s4, s0, 0)) merge_roots(bond_index(1, i0), bond_index(0, i4));
          if (valbend<-ox0,+ox1>(x,y)) if (defdw(s7, s8, 0)) merge_roots(bond_index(1, i0), bond_index(0, i7));
          if (valbend<+ox0,  +0>(x,y)) if (defdw(s6, s9, 1)) merge_roots(bond_index(1, i0), bond_index(1, i6));
          if (valbend<+ox0,-ox1>(x,y)) if (defdw(s0, s6, 0)) merge_roots(bond_index(1, i0), bond_index(0, i0));
          if (valbend<+ox0,+ox1>(x,y)) if (defdw(s8, s9, 0)) merge_roots(bond_index(1, i0), bond_index(0, i8));
        }
      );
    } else if (LatNm == lattice_name::cube) {
      for_3D(x, y, z, length_[0], length_[1], length_[2],
        // vertex neighbor map:
        // i7 i8 i9   i16 i17 i18   i25 i26 i27
        // i4 i5 i6   i13 i0  i15   i22 i23 i24
        // i1 i2 i3   i10 i11 i12   i19 i20 i21
                                          const auto i26 = iw(x+0,y+1,z+1);
        const auto i22 = iw(x-1,y+0,z+1); const auto i23 = iw(x+0,y+0,z+1); const auto i24 = iw(x+1,y+0,z+1);
                                          const auto i20 = iw(x+0,y-1,z+1);

        const auto i16 = iw(x-1,y+1,z+0); const auto i17 = iw(x+0,y+1,z+0); const auto i18 = iw(x+1,y+1,z+0);
        const auto i13 = iw(x-1,y+0,z+0); const auto i0  = iw(x+0,y+0,z+0); const auto i15 = iw(x+1,y+0,z+0);
                                          const auto i11 = iw(x+0,y-1,z+0); const auto i12 = iw(x+1,y-1,z+0);

                                          const auto i8  = iw(x+0,y+1,z-1);
                                          const auto i5  = iw(x+0,y+0,z-1); const auto i6  = iw(x+1,y+0,z-1);
        // copy spin values:
                                 const auto s26 = s[i26];
        const auto s22 = s[i22]; const auto s23 = s[i23]; const auto s24 = s[i24];
                                 const auto s20 = s[i20];

        const auto s16 = s[i16]; const auto s17 = s[i17]; const auto s18 = s[i18];
        const auto s13 = s[i13]; const auto s0  = s[i0 ]; const auto s15 = s[i15];
                                 const auto s11 = s[i11]; const auto s12 = s[i12];

                                 const auto s8  = s[i8 ]; 
                                 const auto s5  = s[i5 ]; const auto s6  = s[i6 ];
        // label +x bond :
        // i7 i8 i9   i16 i17 i18   i25 i26 i27
        // i4 i5 i6   i13 i0  i15   i22 i23 i24
        // i1 i2 i3   i10 i11 i12   i19 i20 i21
        if (defdw(s0, s15, 0)) {
          if (valbend<-ox0,  +0,  +0>(x,y,z)) if (defdw(s11,s12, 0)) merge_roots(bond_index(0, i0), bond_index(0, i11));
          if (valbend<-ox0,-ox1,  +0>(x,y,z)) if (defdw(s11,s0 , 1)) merge_roots(bond_index(0, i0), bond_index(1, i11));
          if (valbend<+ox0,+ox1,  +0>(x,y,z)) if (defdw(s15,s18, 1)) merge_roots(bond_index(0, i0), bond_index(1, i15));
          if (valbend<  +0,+ox1,  +0>(x,y,z)) if (defdw(s17,s18, 0)) merge_roots(bond_index(0, i0), bond_index(0, i17));
          if (valbend<-ox0,+ox1,  +0>(x,y,z)) if (defdw(s0, s17, 1)) merge_roots(bond_index(0, i0), bond_index(1, i0 ));
          if (valbend<+ox0,-ox1,  +0>(x,y,z)) if (defdw(s12,s15, 1)) merge_roots(bond_index(0, i0), bond_index(1, i12));
          if (valbend<  +0,  +0,-ox2>(x,y,z)) if (defdw(s5, s6 , 0)) merge_roots(bond_index(0, i0), bond_index(0, i5 ));
          if (valbend<-ox0,  +0,-ox2>(x,y,z)) if (defdw(s5, s0 , 2)) merge_roots(bond_index(0, i0), bond_index(2, i5 ));
          if (valbend<+ox0,  +0,-ox2>(x,y,z)) if (defdw(s6 ,s15, 2)) merge_roots(bond_index(0, i0), bond_index(2, i6 ));
          if (valbend<  +0,  +0,+ox2>(x,y,z)) if (defdw(s23,s24, 0)) merge_roots(bond_index(0, i0), bond_index(0, i23));
          if (valbend<-ox0,  +0,+ox2>(x,y,z)) if (defdw(s0, s23, 2)) merge_roots(bond_index(0, i0), bond_index(2, i0 ));
          if (valbend<+ox0,  +0,+ox2>(x,y,z)) if (defdw(s15,s24, 2)) merge_roots(bond_index(0, i0), bond_index(2, i15));
        }
        // label +y bond :
        if (defdw(s0, s17, 1)) {
          if (valbend<-ox0,  +0,  +0>(x,y,z)) if (defdw(s13,s16, 1)) merge_roots(bond_index(1, i0), bond_index(1, i13));
          if (valbend<-ox0,-ox1,  +0>(x,y,z)) if (defdw(s13, s0, 0)) merge_roots(bond_index(1, i0), bond_index(0, i13));
          if (valbend<-ox0,+ox1,  +0>(x,y,z)) if (defdw(s16,s17, 0)) merge_roots(bond_index(1, i0), bond_index(0, i16));
          if (valbend<+ox0,  +0,  +0>(x,y,z)) if (defdw(s15,s18, 1)) merge_roots(bond_index(1, i0), bond_index(1, i15));
          if (valbend<+ox0,-ox1,  +0>(x,y,z)) if (defdw(s0, s15, 0)) merge_roots(bond_index(1, i0), bond_index(0, i0 ));
          if (valbend<+ox0,+ox1,  +0>(x,y,z)) if (defdw(s17,s18, 0)) merge_roots(bond_index(1, i0), bond_index(0, i17));
          if (valbend<  +0,  +0,-ox2>(x,y,z)) if (defdw(s5 ,s8 , 1)) merge_roots(bond_index(1, i0), bond_index(1, i5 ));
          if (valbend<  +0,-ox1,-ox2>(x,y,z)) if (defdw(s5, s0 , 2)) merge_roots(bond_index(1, i0), bond_index(2, i5 ));
          if (valbend<  +0,+ox1,-ox2>(x,y,z)) if (defdw(s8 ,s17, 2)) merge_roots(bond_index(1, i0), bond_index(2, i8 ));
          if (valbend<  +0,  +0,+ox2>(x,y,z)) if (defdw(s23,s26, 1)) merge_roots(bond_index(1, i0), bond_index(1, i23));
          if (valbend<  +0,-ox1,+ox2>(x,y,z)) if (defdw(s0, s23, 2)) merge_roots(bond_index(1, i0), bond_index(2, i0 ));
          if (valbend<  +0,+ox1,+ox2>(x,y,z)) if (defdw(s17,s26, 2)) merge_roots(bond_index(1, i0), bond_index(2, i17));
        }
        // label +z bond :
        if (defdw(s0 , s23, 2)) {
         if (valbend<-ox0,  +0,  +0>(x,y,z)) if (defdw(s13, s22, 2)) merge_roots(bond_index(2, i0), bond_index(2, i13));
         if (valbend<-ox0,  +0,-ox2>(x,y,z)) if (defdw(s13, s0 , 0)) merge_roots(bond_index(2, i0), bond_index(0, i13));
         if (valbend<-ox0,  +0,+ox2>(x,y,z)) if (defdw(s22, s23, 0)) merge_roots(bond_index(2, i0), bond_index(0, i22));
         if (valbend<+ox0,  +0,  +0>(x,y,z)) if (defdw(s15, s24, 2)) merge_roots(bond_index(2, i0), bond_index(2, i15));
         if (valbend<+ox0,  +0,-ox2>(x,y,z)) if (defdw(s0 , s15, 0)) merge_roots(bond_index(2, i0), bond_index(0, i0 ));
         if (valbend<+ox0,  +0,+ox2>(x,y,z)) if (defdw(s23, s24, 0)) merge_roots(bond_index(2, i0), bond_index(0, i23));
         if (valbend<  +0,-ox1,  +0>(x,y,z)) if (defdw(s11, s20, 2)) merge_roots(bond_index(2, i0), bond_index(2, i11));
         if (valbend<  +0,-ox1,-ox2>(x,y,z)) if (defdw(s11, s0 , 1)) merge_roots(bond_index(2, i0), bond_index(1, i11));
         if (valbend<  +0,-ox1,+ox2>(x,y,z)) if (defdw(s20, s23, 1)) merge_roots(bond_index(2, i0), bond_index(1, i20));
         if (valbend<  +0,+ox1,  +0>(x,y,z)) if (defdw(s17, s26, 2)) merge_roots(bond_index(2, i0), bond_index(2, i17));
         if (valbend<  +0,+ox1,-ox2>(x,y,z)) if (defdw(s0 , s17, 1)) merge_roots(bond_index(2, i0), bond_index(1, i0 ));
         if (valbend<  +0,+ox1,+ox2>(x,y,z)) if (defdw(s23, s26, 1)) merge_roots(bond_index(2, i0), bond_index(1, i23));
        }
      );
		} else if (LatNm == lattice_name::hypercube) {
      for_4D(x, y, z, t, length_[0], length_[1], length_[2], length_[3],
        // vertex neighbor map:
        // i61 i62 i63  i70 i71 i72  i79 i80 i81
        // i58 i59 i60  i67 i68 i69  i76 i77 i78
        // i55 i56 i57  i64 i65 i66  i73 i74 i75
        //
        // i34 i35 i36  i43 i44 i45  i52 i53 i54
        // i31 i32 i33  i40 i0  i42  i49 i50 i51
        // i28 i29 i30  i37 i38 i39  i46 i47 i48
        //
        // i7  i8  i9   i16 i17 i18  i25 i26 i27
        // i4  i5  i6   i13 i14 i15  i22 i23 i24
        // i1  i2  i3   i10 i11 i12  i19 i20 i21
                                              const auto i14 = iw(x+0,y+0,z+0,t-1); const auto i15 = iw(x+1,y+0,z+0,t-1);
                                              const auto i17 = iw(x+0,y+1,z+0,t-1);
                                              const auto i23 = iw(x+0,y+0,z+1,t-1);
                                              const auto i32 = iw(x+0,y+0,z-1,t+0); const auto i33 = iw(x+1,y+0,z-1,t+0);
                                              const auto i35 = iw(x+0,y+1,z-1,t+0);
                                              const auto i38 = iw(x+0,y-1,z+0,t+0); const auto i39 = iw(x+1,y-1,z+0,t+0);
        const auto i40 = iw(x-1,y+0,z+0,t+0); const auto i0  = iw(x+0,y+0,z+0,t+0); const auto i42 = iw(x+1,y+0,z+0,t+0);
        const auto i43 = iw(x-1,y+1,z+0,t+0); const auto i44 = iw(x+0,y+1,z+0,t+0); const auto i45 = iw(x+1,y+1,z+0,t+0);
                                              const auto i47 = iw(x+0,y-1,z+1,t+0);
        const auto i49 = iw(x-1,y+0,z+1,t+0); const auto i50 = iw(x+0,y+0,z+1,t+0); const auto i51 = iw(x+1,y+0,z+1,t+0);
                                              const auto i53 = iw(x+0,y+1,z+1,t+0);
                                              const auto i59 = iw(x+0,y+0,z-1,t+1);
                                              const auto i65 = iw(x+0,y-1,z+0,t+1);
        const auto i67 = iw(x-1,y+0,z+0,t+1); const auto i68 = iw(x+0,y+0,z+0,t+1); const auto i69 = iw(x+1,y+0,z+0,t+1);
                                              const auto i71 = iw(x+0,y+1,z+0,t+1);
                                              const auto i77 = iw(x+0,y+0,z+1,t+1);
                                 const auto s14 = s[i14]; const auto s15 = s[i15];
                                 const auto s17 = s[i17];
                                 const auto s23 = s[i23];
                                 const auto s32 = s[i32]; const auto s33 = s[i33];
                                 const auto s35 = s[i35];
                                 const auto s38 = s[i38]; const auto s39 = s[i39];
        const auto s40 = s[i40]; const auto s0  = s[i0 ]; const auto s42 = s[i42];
        const auto s43 = s[i43]; const auto s44 = s[i44]; const auto s45 = s[i45];
                                 const auto s47 = s[i47];
        const auto s49 = s[i49]; const auto s50 = s[i50]; const auto s51 = s[i51];
                                 const auto s53 = s[i53]; 
                                 const auto s59 = s[i59];
                                 const auto s65 = s[i65];
        const auto s67 = s[i67]; const auto s68 = s[i68]; const auto s69 = s[i69];
                                 const auto s71 = s[i71];
                                 const auto s77 = s[i77];
        // label +x bond :
        if (defdw(s0, s42, 0)) {
          if (valbend<-ox0,  +0,  +0,  +0>(x,y,z,t)) if (defdw(s38,s39, 0)) merge_roots(bond_index(0,i0), bond_index(0,i38));
          if (valbend<-ox0,-ox1,  +0,  +0>(x,y,z,t)) if (defdw(s38,s0 , 1)) merge_roots(bond_index(0,i0), bond_index(1,i38));
          if (valbend<+ox0,-ox1,  +0,  +0>(x,y,z,t)) if (defdw(s39,s42, 1)) merge_roots(bond_index(0,i0), bond_index(1,i39));
          if (valbend<  +0,+ox1,  +0,  +0>(x,y,z,t)) if (defdw(s44,s45, 0)) merge_roots(bond_index(0,i0), bond_index(0,i44));
          if (valbend<-ox0,+ox1,  +0,  +0>(x,y,z,t)) if (defdw(s0, s44, 1)) merge_roots(bond_index(0,i0), bond_index(1,i0 ));
          if (valbend<+ox0,+ox1,  +0,  +0>(x,y,z,t)) if (defdw(s42,s45, 1)) merge_roots(bond_index(0,i0), bond_index(1,i42));
          if (valbend<  +0,  +0,-ox2,  +0>(x,y,z,t)) if (defdw(s32,s33, 0)) merge_roots(bond_index(0,i0), bond_index(0,i32));
          if (valbend<-ox0,  +0,-ox2,  +0>(x,y,z,t)) if (defdw(s32,s0 , 2)) merge_roots(bond_index(0,i0), bond_index(2,i32));
          if (valbend<+ox0,  +0,-ox2,  +0>(x,y,z,t)) if (defdw(s33,s42, 2)) merge_roots(bond_index(0,i0), bond_index(2,i33));
          if (valbend<  +0,  +0,+ox2,  +0>(x,y,z,t)) if (defdw(s50,s51, 0)) merge_roots(bond_index(0,i0), bond_index(0,i50));
          if (valbend<-ox0,  +0,+ox2,  +0>(x,y,z,t)) if (defdw(s0 ,s50, 2)) merge_roots(bond_index(0,i0), bond_index(2,i0 ));
          if (valbend<+ox0,  +0,+ox2,  +0>(x,y,z,t)) if (defdw(s42,s51, 2)) merge_roots(bond_index(0,i0), bond_index(2,i42));
          if (valbend<  +0,  +0,  +0,-ox3>(x,y,z,t)) if (defdw(s14,s15, 0)) merge_roots(bond_index(0,i0), bond_index(0,i14));
          if (valbend<-ox0,  +0,  +0,-ox3>(x,y,z,t)) if (defdw(s14,s0 , 3)) merge_roots(bond_index(0,i0), bond_index(3,i14));
          if (valbend<+ox0,  +0,  +0,-ox3>(x,y,z,t)) if (defdw(s15,s42, 3)) merge_roots(bond_index(0,i0), bond_index(3,i15));
          if (valbend<-ox0,  +0,  +0,+ox3>(x,y,z,t)) if (defdw(s68,s69, 0)) merge_roots(bond_index(0,i0), bond_index(0,i68));
          if (valbend<-ox0,  +0,  +0,+ox3>(x,y,z,t)) if (defdw(s0, s68, 3)) merge_roots(bond_index(0,i0), bond_index(3,i0 ));
          if (valbend<+ox0,  +0,  +0,+ox3>(x,y,z,t)) if (defdw(s42,s69, 3)) merge_roots(bond_index(0,i0), bond_index(3,i42));
        }
        // label +y bond :
        if (defdw(s0, s44, 1)) {
          if (valbend<-ox0,  +0,  +0,  +0>(x,y,z,t)) if (defdw(s40,s43, 1)) merge_roots(bond_index(1,i0), bond_index(1,i40));
          if (valbend<-ox0,-ox1,  +0,  +0>(x,y,z,t)) if (defdw(s40,s0 , 0)) merge_roots(bond_index(1,i0), bond_index(0,i40));
          if (valbend<-ox0,+ox1,  +0,  +0>(x,y,z,t)) if (defdw(s43,s44, 0)) merge_roots(bond_index(1,i0), bond_index(0,i43));
          if (valbend<+ox0,  +0,  +0,  +0>(x,y,z,t)) if (defdw(s42,s45, 1)) merge_roots(bond_index(1,i0), bond_index(1,i42));
          if (valbend<+ox0,-ox1,  +0,  +0>(x,y,z,t)) if (defdw(s0 ,s42, 0)) merge_roots(bond_index(1,i0), bond_index(0,i0 ));
          if (valbend<+ox0,+ox1,  +0,  +0>(x,y,z,t)) if (defdw(s44,s45, 0)) merge_roots(bond_index(1,i0), bond_index(0,i44));
          if (valbend<  +0,  +0,-ox2,  +0>(x,y,z,t)) if (defdw(s32,s35, 1)) merge_roots(bond_index(1,i0), bond_index(1,i32));
          if (valbend<  +0,-ox1,-ox2,  +0>(x,y,z,t)) if (defdw(s32,s0 , 2)) merge_roots(bond_index(1,i0), bond_index(2,i32));
          if (valbend<+ox0,  +0,-ox2,  +0>(x,y,z,t)) if (defdw(s35,s44, 2)) merge_roots(bond_index(1,i0), bond_index(2,i35));
          if (valbend<  +0,  +0,+ox2,  +0>(x,y,z,t)) if (defdw(s50,s53, 1)) merge_roots(bond_index(1,i0), bond_index(1,i50));
          if (valbend<  +0,-ox1,+ox2,  +0>(x,y,z,t)) if (defdw(s0 ,s50, 2)) merge_roots(bond_index(1,i0), bond_index(2,i0 ));
          if (valbend<  +0,+ox1,+ox2,  +0>(x,y,z,t)) if (defdw(s44,s53, 2)) merge_roots(bond_index(1,i0), bond_index(2,i44));
          if (valbend<  +0,  +0,  +0,-ox3>(x,y,z,t)) if (defdw(s14,s17, 1)) merge_roots(bond_index(1,i0), bond_index(1,i14));
          if (valbend<  +0,-ox1,  +0,-ox3>(x,y,z,t)) if (defdw(s14,s0 , 3)) merge_roots(bond_index(1,i0), bond_index(3,i14));
          if (valbend<  +0,+ox1,  +0,-ox3>(x,y,z,t)) if (defdw(s17,s44, 3)) merge_roots(bond_index(1,i0), bond_index(3,i17));
          if (valbend<  +0,  +0,  +0,+ox3>(x,y,z,t)) if (defdw(s68,s71, 1)) merge_roots(bond_index(1,i0), bond_index(1,i68));
          if (valbend<  +0,-ox1,  +0,+ox3>(x,y,z,t)) if (defdw(s0 ,s68, 3)) merge_roots(bond_index(1,i0), bond_index(3,i0 ));
          if (valbend<  +0,+ox1,  +0,+ox3>(x,y,z,t)) if (defdw(s44,s71, 3)) merge_roots(bond_index(1,i0), bond_index(3,i44));
        }
        // label +z bond :
        if (defdw(s0 , s50, 2)) {
          if (valbend<-ox0,  +0,  +0,  +0>(x,y,z,t)) if (defdw(s40, s49, 2)) merge_roots(bond_index(2,i0), bond_index(2,i40));
          if (valbend<-ox0,  +0,-ox2,  +0>(x,y,z,t)) if (defdw(s40, s0 , 0)) merge_roots(bond_index(2,i0), bond_index(0,i40));
          if (valbend<-ox0,  +0,+ox2,  +0>(x,y,z,t)) if (defdw(s49, s50, 0)) merge_roots(bond_index(2,i0), bond_index(0,i49));
          if (valbend<+ox0,  +0,  +0,  +0>(x,y,z,t)) if (defdw(s42, s51, 2)) merge_roots(bond_index(2,i0), bond_index(2,i42));
          if (valbend<+ox0,  +0,-ox2,  +0>(x,y,z,t)) if (defdw(s0 , s42, 0)) merge_roots(bond_index(2,i0), bond_index(0,i0 ));
          if (valbend<+ox0,  +0,+ox2,  +0>(x,y,z,t)) if (defdw(s50, s51, 0)) merge_roots(bond_index(2,i0), bond_index(0,i50));
          if (valbend<  +0,-ox1,  +0,  +0>(x,y,z,t)) if (defdw(s38, s47, 2)) merge_roots(bond_index(2,i0), bond_index(2,i38));
          if (valbend<  +0,-ox1,-ox2,  +0>(x,y,z,t)) if (defdw(s38, s0 , 1)) merge_roots(bond_index(2,i0), bond_index(1,i38));
          if (valbend<  +0,-ox1,+ox2,  +0>(x,y,z,t)) if (defdw(s47, s50, 1)) merge_roots(bond_index(2,i0), bond_index(1,i47));
          if (valbend<  +0,+ox1,  +0,  +0>(x,y,z,t)) if (defdw(s44, s53, 2)) merge_roots(bond_index(2,i0), bond_index(2,i44));
          if (valbend<  +0,+ox1,-ox2,  +0>(x,y,z,t)) if (defdw(s0 , s44, 1)) merge_roots(bond_index(2,i0), bond_index(1,i0 ));
          if (valbend<  +0,+ox1,+ox2,  +0>(x,y,z,t)) if (defdw(s50, s53, 1)) merge_roots(bond_index(2,i0), bond_index(1,i50));
          if (valbend<  +0,  +0,  +0,-ox3>(x,y,z,t)) if (defdw(s14, s23, 2)) merge_roots(bond_index(2,i0), bond_index(2,i14));
          if (valbend<  +0,  +0,-ox2,-ox3>(x,y,z,t)) if (defdw(s14, s0 , 3)) merge_roots(bond_index(2,i0), bond_index(3,i14));
          if (valbend<  +0,  +0,+ox2,-ox3>(x,y,z,t)) if (defdw(s23, s50, 3)) merge_roots(bond_index(2,i0), bond_index(3,i23));
          if (valbend<  +0,  +0,  +0,+ox3>(x,y,z,t)) if (defdw(s68, s77, 2)) merge_roots(bond_index(2,i0), bond_index(2,i68));
          if (valbend<  +0,  +0,-ox2,+ox3>(x,y,z,t)) if (defdw(s0 , s68, 3)) merge_roots(bond_index(2,i0), bond_index(3,i0 ));
          if (valbend<  +0,  +0,+ox2,+ox3>(x,y,z,t)) if (defdw(s50, s77, 3)) merge_roots(bond_index(2,i0), bond_index(3,i50));
        }
        // label +t bond :
        if (defdw(s0 , s68, 3)) {
          if (valbend<-ox0,  +0,  +0,  +0>(x,y,z,t)) if (defdw(s40, s67, 3)) merge_roots(bond_index(3,i0), bond_index(3,i40));
          if (valbend<-ox0,  +0,  +0,-ox3>(x,y,z,t)) if (defdw(s40, s0 , 0)) merge_roots(bond_index(3,i0), bond_index(0,i40));
          if (valbend<-ox0,  +0,  +0,+ox3>(x,y,z,t)) if (defdw(s67, s68, 0)) merge_roots(bond_index(3,i0), bond_index(0,i67));
          if (valbend<+ox0,  +0,  +0,  +0>(x,y,z,t)) if (defdw(s42, s69, 3)) merge_roots(bond_index(3,i0), bond_index(3,i42));
          if (valbend<+ox0,  +0,  +0,-ox3>(x,y,z,t)) if (defdw(s0 , s42, 0)) merge_roots(bond_index(3,i0), bond_index(0,i0 ));
          if (valbend<+ox0,  +0,  +0,+ox3>(x,y,z,t)) if (defdw(s68, s69, 0)) merge_roots(bond_index(3,i0), bond_index(0,i68));
          if (valbend<  +0,-ox1,  +0,  +0>(x,y,z,t)) if (defdw(s38, s65, 3)) merge_roots(bond_index(3,i0), bond_index(3,i38));
          if (valbend<  +0,-ox1,  +0,-ox3>(x,y,z,t)) if (defdw(s38, s0 , 1)) merge_roots(bond_index(3,i0), bond_index(1,i38));
          if (valbend<  +0,-ox1,  +0,+ox3>(x,y,z,t)) if (defdw(s65, s68, 1)) merge_roots(bond_index(3,i0), bond_index(1,i65));
          if (valbend<  +0,+ox1,  +0,  +0>(x,y,z,t)) if (defdw(s44, s71, 3)) merge_roots(bond_index(3,i0), bond_index(3,i44));
          if (valbend<  +0,+ox1,  +0,-ox3>(x,y,z,t)) if (defdw(s0 , s44, 1)) merge_roots(bond_index(3,i0), bond_index(1,i0 ));
          if (valbend<  +0,+ox1,  +0,+ox3>(x,y,z,t)) if (defdw(s68, s69, 1)) merge_roots(bond_index(3,i0), bond_index(1,i68));
          if (valbend<  +0,  +0,-ox2,  +0>(x,y,z,t)) if (defdw(s32, s59, 3)) merge_roots(bond_index(3,i0), bond_index(3,i32));
          if (valbend<  +0,  +0,-ox2,-ox3>(x,y,z,t)) if (defdw(s32, s0 , 2)) merge_roots(bond_index(3,i0), bond_index(2,i32));
          if (valbend<  +0,  +0,-ox2,+ox3>(x,y,z,t)) if (defdw(s59, s68, 2)) merge_roots(bond_index(3,i0), bond_index(2,i59));
          if (valbend<  +0,  +0,-ox2,  +0>(x,y,z,t)) if (defdw(s50, s77, 3)) merge_roots(bond_index(3,i0), bond_index(3,i50));
          if (valbend<  +0,  +0,-ox2,-ox3>(x,y,z,t)) if (defdw(s0 , s50, 2)) merge_roots(bond_index(3,i0), bond_index(2,i0 ));
          if (valbend<  +0,  +0,-ox2,+ox3>(x,y,z,t)) if (defdw(s68, s77, 2)) merge_roots(bond_index(3,i0), bond_index(2,i68));
        }
      );
		} else {
      assert(false);
    } // lattice choice

    // finalize labels:
    for (int i = 0; i < size(); i++) {
      label_bond_[i] = find_root(i);
    }
    return;
	}

  // label vortex strings:
  template <std::size_t N, int ox0, int ox1, int ox2 = 0, int ox3 = 0>
	void label_vortex_strings(const spinconfig<LatNm>& s)
  {
    // set boundary condition along each axis:
    if (D > 0) openx_[0]        = ox0;
    if (D > 1) openx_[1]        = ox1;
    if (D > 2) openx_[2]        = ox2;
    if (D > 3) openx_[3]        = ox3;

    // clear label flags:
		label_histogram_filled_     = false;
		axis_face_has_label_filled_ = false;
		largest_component_found_    = false;

    // initialize label values of bonds with their indices:
    for (int i = 0; i < size(); i++) {
      label_bond_[i] = i;
    }

    if (LatNm == lattice_name::cube) {
      for_3D(x, y, z, length_[0], length_[1], length_[2],
        // make local copy of s[i], its neighbor spins
        // and its next nearest neighbor spins :
        // i7 i8 i9   i16 i17 i18   i25 i26 i27
        // i4 i5 i6   i13 i0  i15   i22 i23 i24
        // i1 i2 i3   i10 i11 i12   i19 i20 i21
        const auto sa = s.wrap(x+0,y+0,z+0);
        const auto sb = s.wrap(x+1,y+0,z+0);
        const auto sc = s.wrap(x+0,y+1,z+0);
        const auto sd = s.wrap(x+1,y+1,z+0);
        const auto se = s.wrap(x+0,y+0,z+1);
        const auto sf = s.wrap(x+1,y+0,z+1);
        const auto sg = s.wrap(x+0,y+1,z+1);
        const auto sh = s.wrap(x+1,y+1,z+1);
        const auto wn015 = winding_number<N>(sb,sd,sh,sf);
        const auto wn013 = winding_number<N>(sa,sc,sg,se);
        const auto wn017 = winding_number<N>(sh,sd,sc,sg);
        const auto wn011 = winding_number<N>(se,sa,sb,sf);
        const auto wn023 = winding_number<N>(se,sf,sh,sg);
        const auto wn05  = winding_number<N>(sb,sd,sc,sa);
        const auto i0  = iw(x+0,y+0,z+0);
        const auto i5  = iw(x+0,y+0,z-1);
        const auto i11 = iw(x+0,y-1,z+0);
        const auto i13 = iw(x-1,y+0,z+0);
        // label +x bond :
        if (valbend< 'b',  +0,  +0>(x,y,z) && (wn015 != 0) && (wn013 != 0)) merge_roots(bond_index(0,i0 ), bond_index(0,i13));
        if (valbend<-ox0,+ox1,  +0>(x,y,z) && (wn015 != 0) && (wn017 != 0)) merge_roots(bond_index(0,i0 ), bond_index(1,i0 ));
        if (valbend<-ox0,-ox1,  +0>(x,y,z) && (wn015 != 0) && (wn011 != 0)) merge_roots(bond_index(0,i0 ), bond_index(1,i11));
        if (valbend<-ox0,  +0,+ox2>(x,y,z) && (wn015 != 0) && (wn023 != 0)) merge_roots(bond_index(0,i0 ), bond_index(2,i0 ));
        if (valbend<-ox0,  +0,-ox2>(x,y,z) && (wn015 != 0) && (wn05  != 0)) merge_roots(bond_index(0,i0 ), bond_index(2,i5 ));
        // label -x edge :
        if (valbend<+ox0,+ox1,  +0>(x,y,z) && (wn013 != 0) && (wn017 != 0)) merge_roots(bond_index(0,i13), bond_index(1,i0 ));
        if (valbend<+ox0,-ox1,  +0>(x,y,z) && (wn013 != 0) && (wn011 != 0)) merge_roots(bond_index(0,i13), bond_index(1,i11));
        if (valbend<+ox0,  +0,+ox2>(x,y,z) && (wn013 != 0) && (wn023 != 0)) merge_roots(bond_index(0,i13), bond_index(2,i0 ));
        if (valbend<+ox0,  +0,-ox2>(x,y,z) && (wn013 != 0) && (wn05  != 0)) merge_roots(bond_index(0,i13), bond_index(2,i5 ));
        // label +y edge :
        if (valbend<  +0, 'b',  +0>(x,y,z) && (wn017 != 0) && (wn011 != 0)) merge_roots(bond_index(1,i0 ), bond_index(1,i11));
        if (valbend<  +0,-ox1,+ox2>(x,y,z) && (wn017 != 0) && (wn023 != 0)) merge_roots(bond_index(1,i0 ), bond_index(2,i0 ));
        if (valbend<  +0,-ox1,-ox2>(x,y,z) && (wn017 != 0) && (wn05  != 0)) merge_roots(bond_index(1,i0 ), bond_index(2,i5 ));
        // label -y edge :
        if (valbend<  +0,+ox1,+ox2>(x,y,z) && (wn011 != 0) && (wn023 != 0)) merge_roots(bond_index(1,i11), bond_index(2,i0 ));
        if (valbend<  +0,-ox1,+ox2>(x,y,z) && (wn011 != 0) && (wn05  != 0)) merge_roots(bond_index(1,i11), bond_index(2,i5 ));
        // label +z edge :
        if (valbend<  +0,  +0, 'b'>(x,y,z) && (wn023 != 0) && (wn05  != 0)) merge_roots(bond_index(2,i0 ), bond_index(2,i5 ));
      );
    } // lattice choice
 
    // finalize labels:
    for (int i = 0; i < size(); i++) {
      label_bond_[i] = find_root(i);
    }
		return;
	}

  bool is_spanning()
  {
    if (axis_face_has_label_filled_ != true) { fill_axis_face_has_label(); }
    bool spanning = false;
    for (int label = 0; label < (int)label_bond_.size(); label++) {
      for (int axis = 0; axis < (int)D; axis++) {
        if (openx_[axis] == true) {
          if ((axis_face_has_label(0, axis, label) == 1) &&
              (axis_face_has_label(1, axis, label) == 1)) {
            spanning = true;
          }
        }
      }
    }
    return spanning;
  }

	void accumulate_component_sizes_in_distribution()
	{
		if (size_dist_.size() < (label_bond_.size() + 1)) {
    	size_dist_.resize(label_bond_.size() + 1);
      for (int i = 0; i < (int)size_dist_.size(); i++) {
        size_dist_[i] = 0;
      }
		}
		if (label_histogram_filled_ == false) {
			fill_label_histogram();
		}
    for (int i = 0; i < (int)label_histogram_.size(); i++) {
      const auto size_i = label_histogram_[i];
			// as no discrimination between valid and invalid
			// elements are made during the labelling stage,
			// invalid elements form components of size one on
			// their own, so we will bin only those components
      // which have two or more elements: 
      if (size_i > 1) {
        size_dist_[size_i] += 1;
      }
    }
		// remove size of the largest component
    const auto largest_size = largest_component_size();
    if (largest_size > 1) {
        size_dist_[largest_size] -= 1;
    }
    accumulate_count_ += 1;
  }

	int largest_component_size()
	{	
		if (largest_component_found_ == false) {
			find_largest_component();
		}
		return largest_component_size_; 
	}

	double largest_component_fraction()
	{ 
		if (largest_component_found_ == false) {
			find_largest_component();
		}
		return largest_component_size_ / (1. * label_bond_.size()); 
	}

	int largest_component_label()
	{ 
		if (largest_component_found_ == false) {
			find_largest_component();
		}
		return largest_component_label_; 
	}

  double size_distribution(const int i) const
  {
    assert(size_dist_[i] > 0);
    return size_dist_[i] / (1. * accumulate_count_);
  }

  double average_component_size() const
  {
    long double num = 0., dem = 0.;
    int count = 0;
    for (int s = 0; s < (int)size_dist_.size(); s++) {
      const auto ns = size_dist_[s];
      const long double tmp = (s * ns) / (1. * size_dist_.size());
      num += (s * tmp - num) / (long double)(count++ + 1.);
      dem += (    tmp - dem) / (long double)(count++ + 1.);
      assert(ns >= 0);
      assert(tmp >= 0);
    }
    assert(num >= 0);
    assert(dem >= 0);
    return num / dem;
  }

  inline int
  length(const int i) const noexcept
  {
    assert((0 <= i) && (i < (int)D));
    return length_[i];
  }

  inline const std::array<int,D>&
  length() const noexcept
  { return length_; }

  inline int
  label_bond(const int bond, const int vertex_index) const
  { return label_bond_[bond_index(bond, vertex_index)]; }

  inline int&
  label_bond(const int bond, const int vertex_index)
  { return label_bond_[bond_index(bond, vertex_index)]; }

  inline int
  label_bond(const int bond,
             const int x,
             const int y,
             const int z = 0,
             const int t = 0) const
  { return label_bond_[bond_index(bond, x, y, z, t)]; }

  inline int&
  label_bond(const int bond,
             const int x,
             const int y,
             const int z = 0,
             const int t = 0)
  { return label_bond_[bond_index(bond, x, y, z, t)]; }

  void clear()
  {
    // clear flags:
		label_histogram_filled_     = false;
		axis_face_has_label_filled_ = false;
		largest_component_found_    = false;

    // clear values:
    accumulate_count_           = 0;
		largest_component_size_     = 0;
		largest_component_label_    = 0;

    // clear arrays:
    std::fill(label_bond_.begin(),label_bond_.end(),0);
    std::fill(axis_face_has_label_.begin(),axis_face_has_label_.end(),0);
    std::fill(label_histogram_.begin(), label_histogram_.end(), 0);
    std::fill(size_dist_.begin(), size_dist_.end(), 0);

    return;
  }

  ccl_bondwise_domain_walls<LatNm>
  domain_walls(const int threshold = 2)
  {
    if (label_histogram_filled_ == false) { fill_label_histogram(); }
    return ccl_bondwise_domain_walls<LatNm>(*this, threshold);
  }

  template <std::size_t N>
  ccl_bondwise_vortex_strings<N,LatNm>
  vortex_strings(const int threshold = 2)
  {
    if (label_histogram_filled_ == false) { fill_label_histogram(); }
    return ccl_bondwise_vortex_strings<N,LatNm>(*this, threshold);
  }

  inline int
  iw(const int x,
     const int y,
     const int z = 0,
     const int t = 0) const
  {
    switch(D) {
    case 2 : return qtools::flatten_wrap_coords(x,y,length_[0],length_[1]);
    case 3 : return qtools::flatten_wrap_coords(x,y,z,length_[0],length_[1],length_[2]);
    case 4 : return qtools::flatten_wrap_coords(x,y,z,t,length_[0],length_[1],length_[2],length_[3]);
    default: return -1;
    }
  }

  inline int find_root(int i) const
  {
    while (i != label_bond_[i]) { i = label_bond_[i]; }
    return i;
  }

  inline void merge_roots(const int index_i, const int index_j)
  {
    const int rooti = find_root(index_i);
    const int rootj = find_root(index_j);
    if (rootj < rooti)     {label_bond_[rooti] = rootj;}
    else                   {label_bond_[rootj] = rooti;}
    return;
  }

	void fill_label_histogram()
	{
		if (label_histogram_.size() < label_bond_.size()) {
    	label_histogram_.resize(label_bond_.size());
		}
      for (int i = 0; i < (int)label_histogram_.size(); i++) {
        label_histogram_[i] = 0;
      }
      for (int i = 0; i < (int)label_bond_.size(); i++) {
        const auto root_label = label_bond_[i];
        label_histogram_[root_label] += 1;
      }
		label_histogram_filled_ = true;
		return;
	}

	void find_largest_component()
	{
		if (label_histogram_filled_ == false) {
			fill_label_histogram();
		}
		largest_component_size_ = 0;
		largest_component_label_ = 0;
      int local_largest_component_size   = 0;
      int local_largest_component_label  = 0;
      for (int i = 0; i < (int)label_histogram_.size(); i++) {
        const auto size_i = label_histogram_[i];
        if (size_i > local_largest_component_size) {
          local_largest_component_size  = size_i;
					local_largest_component_label = i;
        }
      }
        if (local_largest_component_size > largest_component_size_) {
          largest_component_size_ = local_largest_component_size;
					largest_component_label_ = local_largest_component_label;
        }
		largest_component_found_ = true;
		return;
	}

  inline int
  axis_face_has_label(const int face,
                      const int axis,
                      const int label) const
  { return axis_face_has_label_[face + 2 * (axis + D * label)]; }

  inline int&
  axis_face_has_label(const int face,
                      const int axis,
                      const int label)
  { return axis_face_has_label_[face + 2 * (axis + D * label)]; }

  inline int
  axis_face_has_label(const int face,
                      const int axis,
                      const int bond,
                      const int x,
                      const int y,
                      const int z = 0,
                      const int t = 0) const
  { return axis_face_has_label_[face + 2 * (axis + D * label_bond(bond, x, y, z, t))]; }

  inline int&
  axis_face_has_label(const int face,
                      const int axis,
                      const int bond,
                      const int x,
                      const int y,
                      const int z = 0,
                      const int t = 0)
  { return axis_face_has_label_[face + 2 * (axis + D * label_bond(bond, x, y, z, t))]; }

  inline int
  bond_index(const int bond,
             const int vertex_index) const
  { return bond + NumOutBonds * vertex_index; }

  inline int
  bond_index(const int bond,
             const int x,
             const int y,
             const int z = 0,
             const int t = 0) const
  {
    switch(D) {
    case 2 : return bond + NumOutBonds * (x + length_[0] * y);
    case 3 : return bond + NumOutBonds * (x + length_[0] *
             (y + length_[1] * z));
    case 4 : return bond + NumOutBonds * (x + length_[0] *
             (y + length_[1] * (z + length_[2] * t)));
    default: return -1;
    }
  }

  void fill_axis_face_has_label()
  {
		axis_face_has_label_filled_ = true;
    if (axis_face_has_label_.size() != (2 * D * label_bond_.size())) {
      axis_face_has_label_.resize(2 * D * label_bond_.size());
    }
      for (int i = 0; i < (int)axis_face_has_label_.size(); i++) {
        axis_face_has_label_[i] = 0;
      }
	    if (2 == dimension(LatNm)) {
      	for_2D(x, y, length_[0], length_[1],
          if (x == 0) {
            for (int bond = 0; bond < (int)NumOutBonds; bond++) {
              axis_face_has_label(0, 0, bond, x, y) = 1;
            }
          }
          if (x == length_[0] - 1) {
            for (int bond = 0; bond < (int)NumOutBonds; bond++) {
              axis_face_has_label(1, 0, bond, x, y) = 1;
            }
          }
          if (y == 0) {
            for (int bond = 0; bond < (int)NumOutBonds; bond++) {
              axis_face_has_label(0, 1, bond, x, y) = 1;
            }
          }
          if (y == length_[1] - 1) {
            for (int bond = 0; bond < (int)NumOutBonds; bond++) {
              axis_face_has_label(1, 1, bond, x, y) = 1;
            }
          }
        );
      } else if (3 == dimension(LatNm)) {
      	for_3D(x, y, z, length_[0], length_[1], length_[2],
          if (x == 0) {
            for (int bond = 0; bond < (int)NumOutBonds; bond++) {
              axis_face_has_label(0, 0, bond, x, y, z) = 1;
            }
          }
          if (x == length_[0] - 1) {
            for (int bond = 0; bond < (int)NumOutBonds; bond++) {
              axis_face_has_label(1, 0, bond, x, y, z) = 1;
            }
          }
          if (y == 0) {
            for (int bond = 0; bond < (int)NumOutBonds; bond++) {
              axis_face_has_label(0, 1, bond, x, y, z) = 1;
            }
          }
          if (y == length_[1] - 1) {
            for (int bond = 0; bond < (int)NumOutBonds; bond++) {
              axis_face_has_label(1, 1, bond, x, y, z) = 1;
            }
          }
          if (z == 0) {
            for (int bond = 0; bond < (int)NumOutBonds; bond++) {
              axis_face_has_label(0, 2, bond, x, y, z) = 1;
            }
          }
          if (z == length_[2] - 1) {
            for (int bond = 0; bond < (int)NumOutBonds; bond++) {
              axis_face_has_label(1, 2, bond, x, y, z) = 1;
            }
          }
        );
      } else if (4 == dimension(LatNm)) {
      	for_4D(x, y, z, t, length_[0], length_[1], length_[2], length_[3],
          if (x == 0) {
            for (int bond = 0; bond < (int)NumOutBonds; bond++) {
              axis_face_has_label(0, 0, bond, x, y, z, t) = 1;
            }
          }
          if (x == length_[0] - 1) {
            for (int bond = 0; bond < (int)NumOutBonds; bond++) {
              axis_face_has_label(1, 0, bond, x, y, z, t) = 1;
            }
          }
          if (y == 0) {
            for (int bond = 0; bond < (int)NumOutBonds; bond++) {
              axis_face_has_label(0, 1, bond, x, y, z, t) = 1;
            }
          }
          if (y == length_[1] - 1) {
            for (int bond = 0; bond < (int)NumOutBonds; bond++) {
              axis_face_has_label(1, 1, bond, x, y, z, t) = 1;
            }
          }
          if (z == 0) {
            for (int bond = 0; bond < (int)NumOutBonds; bond++) {
              axis_face_has_label(0, 2, bond, x, y, z, t) = 1;
            }
          }
          if (z == length_[2] - 1) {
            for (int bond = 0; bond < (int)NumOutBonds; bond++) {
              axis_face_has_label(1, 2, bond, x, y, z, t) = 1;
            }
          }
          if (t == 0) {
            for (int bond = 0; bond < (int)NumOutBonds; bond++) {
              axis_face_has_label(0, 3, bond, x, y, z, t) = 1;
            }
          }
          if (t == length_[3] - 1) {
            for (int bond = 0; bond < (int)NumOutBonds; bond++) {
              axis_face_has_label(1, 3, bond, x, y, z, t) = 1;
            }
          }
        );
      }
    return;
  }

  template <int dx, int dy, int dz = 0, int dt = 0>
  bool valbend(const int x, const int y, const int z = 0, const int t = 0) const
  {
    if (D > 0) if (dx == -1) if (!(x > 0               )) return false;
    if (D > 0) if (dx =='b') if (!(x > 0               )) return false;
    if (D > 0) if (dx == +1) if (!(x < (length_[0] - 1))) return false;
    if (D > 0) if (dx =='b') if (!(x < (length_[0] - 1))) return false;
    if (D > 1) if (dy == -1) if (!(y > 0               )) return false;
    if (D > 1) if (dy =='b') if (!(y > 0               )) return false;
    if (D > 1) if (dy == +1) if (!(y < (length_[1] - 1))) return false;
    if (D > 1) if (dy =='b') if (!(y < (length_[1] - 1))) return false;
    if (D > 2) if (dz == -1) if (!(z > 0               )) return false;
    if (D > 2) if (dz =='b') if (!(z > 0               )) return false;
    if (D > 2) if (dz == +1) if (!(z < (length_[2] - 1))) return false;
    if (D > 2) if (dz =='b') if (!(z < (length_[2] - 1))) return false;
    if (D > 3) if (dt == -1) if (!(t > 0               )) return false;
    if (D > 3) if (dt =='b') if (!(t > 0               )) return false;
    if (D > 3) if (dt == +1) if (!(t < (length_[3] - 1))) return false;
    if (D > 3) if (dt =='b') if (!(t < (length_[3] - 1))) return false;
    return true; 
  }

  // flags used in labelling:
	bool 	label_histogram_filled_     = 0;
	bool  axis_face_has_label_filled_ = 0;
	bool 	largest_component_found_    = 0;

  // common values used in labelling:
	int 	largest_component_size_     = 0;
	int 	largest_component_label_    = 0;
  int 	accumulate_count_           = 0;

  // length along each axis:
	const std::array<int, D>    length_{{0}};

  // openess of boundary condition along each axis:
	std::array<int, D>          openx_{{0}};

  // arrays used in labelling:
  std::vector<int>            label_bond_         {{0}};
  std::vector<int>            axis_face_has_label_{{0}};
  std::vector<int>            label_histogram_    {{0}};
  std::vector<long long int>  size_dist_          {{0}};

};

template <lattice_name LatNm>
class ccl_bondwise_domain_walls
{
  public:
  const ccl_bondwise<LatNm>& ccl_;
  const int threshold_;

  ccl_bondwise_domain_walls(const ccl_bondwise<LatNm>& ccl, const int threshold)
  : ccl_(ccl), threshold_(threshold)
  {;}

  friend std::ostream&
  operator<<(std::ostream& output, const ccl_bondwise_domain_walls<LatNm> sp)
  {
    assert(sp.ccl_.label_histogram_filled_ == true);
    if (LatNm == znspin::lattice_name::square) {
      for_2D(x, y, sp.ccl_.length(0), sp.ccl_.length(1),
        if (sp.ccl_.label_histogram_[sp.ccl_.label_bond(0,x,y)] >= sp.threshold_) {
          output << x+0.5 << "\t" << y-0.5 << "\t";
          output << x+0.5 << "\t" << y+0.5 << "\t";
          output << sp.ccl_.label_histogram_[sp.ccl_.label_bond(0,x,y)] << std::endl;
        }
        if (sp.ccl_.label_histogram_[sp.ccl_.label_bond(1,x,y)] >= sp.threshold_) {
          output << x-0.5 << "\t" << y+0.5 << "\t";
          output << x+0.5 << "\t" << y+0.5 << "\t";
          output << sp.ccl_.label_histogram_[sp.ccl_.label_bond(1,x,y)] << std::endl;
        }
      );
    } else if (LatNm == znspin::lattice_name::cube) {
      for_3D(x, y, z, sp.ccl_.length(0), sp.ccl_.length(1), sp.ccl_.length(2),
        if (sp.ccl_.label_histogram_[sp.ccl_.label_bond(0,x,y)] >= sp.threshold_) {
          output << x+0.5 << "\t" << y-0.5 << "\t" << z-0.5 << "\t";
          output << x+0.5 << "\t" << y+0.5 << "\t" << z-0.5 << "\t";
          output << x+0.5 << "\t" << y-0.5 << "\t" << z+0.5 << "\t";
          output << x+0.5 << "\t" << y+0.5 << "\t" << z+0.5 << "\t";
          output << sp.ccl_.label_histogram_[sp.ccl_.label_bond(0,x,y)] << std::endl;
        }
        if (sp.ccl_.label_histogram_[sp.ccl_.label_bond(1,x,y)] >= sp.threshold_) {
          output << x-0.5 << "\t" << y+0.5 << "\t" << z-0.5 << "\t";
          output << x+0.5 << "\t" << y+0.5 << "\t" << z-0.5 << "\t";
          output << x-0.5 << "\t" << y+0.5 << "\t" << z+0.5 << "\t";
          output << x+0.5 << "\t" << y+0.5 << "\t" << z+0.5 << "\t";
          output << sp.ccl_.label_histogram_[sp.ccl_.label_bond(1,x,y)] << std::endl;
        }
        if (sp.ccl_.label_histogram_[sp.ccl_.label_bond(2,x,y)] >= sp.threshold_) {
          output << x-0.5 << "\t" << y-0.5 << "\t" << z+0.5 << "\t";
          output << x+0.5 << "\t" << y-0.5 << "\t" << z+0.5 << "\t";
          output << x-0.5 << "\t" << y+0.5 << "\t" << z+0.5 << "\t";
          output << x+0.5 << "\t" << y+0.5 << "\t" << z+0.5 << "\t";
          output << sp.ccl_.label_histogram_[sp.ccl_.label_bond(2,x,y)] << std::endl;
        }
      );
    }
    return output;
  }
};

template <std::size_t N, lattice_name LatNm>
class ccl_bondwise_vortex_strings
{
  public:
  const ccl_bondwise<LatNm>& ccl_;
  const int threshold_;

  ccl_bondwise_vortex_strings(const ccl_bondwise<LatNm>& ccl, const int threshold)
  : ccl_(ccl), threshold_(threshold)
  {;}

  friend std::ostream&
  operator<<(std::ostream& output, const ccl_bondwise_vortex_strings<N,LatNm> sp)
  {
    assert(sp.ccl_.label_histogram_filled_ == true);
    if ((N > 2) && (dimension(LatNm) == 3)) {
      for_3D(x, y, z, sp.ccl_.length(0), sp.ccl_.length(1), sp.ccl_.length(2),
        if (sp.ccl_.label_histogram_[sp.ccl_.label_bond(0,x,y)] >= sp.threshold_) {
          output << x+0.5 << "\t" << y+0.5 << "\t" << z+0.5 << "\t";
          output << x+1.5 << "\t" << y+0.5 << "\t" << z+0.5 << "\t";
          output << sp.ccl_.label_histogram_[sp.ccl_.label_bond(0,x,y)] << std::endl;
        }
        if (sp.ccl_.label_histogram_[sp.ccl_.label_bond(1,x,y)] >= sp.threshold_) {
          output << x+0.5 << "\t" << y+0.5 << "\t" << z+0.5 << "\t";
          output << x+0.5 << "\t" << y+1.5 << "\t" << z+0.5 << "\t";
          output << sp.ccl_.label_histogram_[sp.ccl_.label_bond(1,x,y)] << std::endl;
        }
        if (sp.ccl_.label_histogram_[sp.ccl_.label_bond(2,x,y)] >= sp.threshold_) {
          output << x+0.5 << "\t" << y+0.5 << "\t" << z+0.5 << "\t";
          output << x+0.5 << "\t" << y+0.5 << "\t" << z+1.5 << "\t";
          output << sp.ccl_.label_histogram_[sp.ccl_.label_bond(2,x,y)] << std::endl;
        }
      );
    }
    return output;
  }
};

} // namespace znspin

#endif // ends ZNSPIN_CCL_BONDWISE_H
