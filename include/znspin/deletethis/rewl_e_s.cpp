/*
  Density of states for 3 state Potts model with vortex suppression.
  g++ -std=c++11 -Wall -O3 -fopenmp rewl_e_n1_n2.cpp -o rewl_e_n1_n2
  
  write dos:
  ./rewl_e_n1_n2 -write -L 8 -runtime -overlap 0.75 -mulf 4 -minf 1e-6 -t 1000 -x 100 -h 0.8
  read dos:
  ./rewl_e_n1_n2 -read -L 8 -minl -6.0 -maxl 6.0 -incl 0.2 -minkT 0.0 -maxkT 3.0 -inckT 0.1  

*/

#include <iostream>
#include <fstream>
#include <string>
#include <omp.h>

#define USE_RNG_GSL
#define VERBOSE
#define statphy_rewl_num_vars   2
#define statphy_rewl_var_t      int
#define statphy_rewl_max_bins   1e7
#include "rewl.h"

// macro for iteration over a square grid with
// four sublattice decomposition:
#define decompose_four(x, y, L, stuff) do { \
  for (y = 0; y < L; y+= 2) {               \
    for (x = 0; x < L; x+=2) {              \
      {stuff;}                              \
    }                                       \
  }                                         \
  for (y = 0; y < L; y+= 2) {               \
    for (x = 1; x < L; x+=2) {              \
      {stuff;}                              \
    }                                       \
  }                                         \
  for (y = 1; y < L; y+= 2) {               \
    for (x = 0; x < L; x+=2) {              \
      {stuff;}                              \
    }                                       \
  }                                         \
  for (y = 1; y < L; y+= 2) {               \
    for (x = 1; x < L; x+=2) {              \
      {stuff;}                              \
    }                                       \
  }                                         \
} while(0)

// winding number for three state spins:
constexpr inline int
wn3(const int s0, const int s1,
    const int s2, const int s3)
{ return (((s0 != s1) + (s1 != s2) 
         + (s2 != s3) + (s3 != s0)) == 3); }

// holds system parameters:
typedef struct
{
  double    l;
  int       L;
  int*      s;
  int       N0;
  int       N1;
  int       N2;
}
sys_params;

void
func(statphy_rewl_win_t* win, statphy_rewl_wlk_t* wlk, 
     const statphy_rewl_var_t* var, void* params)
{
  const double l= ((sys_params*)params)->l;
  const int l2  = 2 * l;
  const int L   = ((sys_params*)params)->L;
  int*      s   = ((sys_params*)params)->s;
  int       N0  = ((sys_params*)params)->N0;
  int       N1  = ((sys_params*)params)->N1;
  int       N2  = ((sys_params*)params)->N2;
  statphy_rewl_rng_t* rng = wlk->rng;
  int x, y;
  decompose_four(x, y, L,
    // s7 s8 s9
    // s4 s0 s6
    // s1 s2 s3
    const int s1 = s[(x==0?L-1:x-1) + L * (y==0?L-1:y-1)];
    const int s2 = s[ x             + L * (y==0?L-1:y-1)];
    const int s3 = s[(x==L-1?0:x+1) + L * (y==0?L-1:y-1)];
    const int s4 = s[(x==0?L-1:x-1) + L *  y            ];
    const int s0 = s[ x             + L *  y            ];
    const int s6 = s[(x==L-1?0:x+1) + L *  y            ];
    const int s7 = s[(x==0?L-1:x-1) + L * (y==L-1?0:y+1)];
    const int s8 = s[ x             + L * (y==L-1?0:y+1)];
    const int s9 = s[(x==L-1?0:x+1) + L * (y==L-1?0:y+1)];
    const int flip_right = (statphy_rewl_rng_uniform(rng) > 0.5);
    const int sn = (s0 == 0 ? (flip_right ? 1 : 2) :
                    s0 == 1 ? (flip_right ? 2 : 0) :
                              (flip_right ? 0 : 1));
    const int dNdw =  ((sn != s6) + (sn != s8) + (sn != s4) + (sn != s2))
                    - ((s0 != s6) + (s0 != s8) + (s0 != s4) + (s0 != s2));
    const int dNvx =  ( wn3(sn, s6, s9, s8) + wn3(sn, s8, s7, s4)
                      + wn3(sn, s4, s1, s2) + wn3(sn, s2, s3, s6))
                    - ( wn3(s0, s6, s9, s8) + wn3(s0, s8, s7, s4)
                      + wn3(s0, s4, s1, s2) + wn3(s0, s2, s3, s6));
    const int dN0  =  (sn == 0) - (s0 == 0);
    const int dN1  =  (sn == 1) - (s0 == 1);
    const int dN2  =  (sn == 2) - (s0 == 2);
    const int new_N0 = N0 + dN0;
    const int new_N1 = N1 + dN1;
    const int new_N2 = N2 + dN2;
    const int S = (new_N0 > new_N1 ? (new_N0 > new_N2 ? new_N0 : new_N2) 
                                   : (new_N1 > new_N2 ? new_N1 : new_N2));
    statphy_rewl_vars_t new_var;
    new_var[0] = var[0] + dNdw + l2 * (dNvx/2);
    new_var[1] = S;
    const int accept_flip = statphy_rewl_accept(win, wlk, new_var);
    if (accept_flip) {
      s[x + L * y]  = sn;
      N0 = new_N0;
      N1 = new_N1;
      N2 = new_N2;
    }
    if (accept_flip == 2) {
      return;
    }
  );
  ((sys_params*)params)->N0 = N0;
  ((sys_params*)params)->N1 = N1;
  ((sys_params*)params)->N2 = N2;
  return;
}

double
action(const statphy_rewl_var_t* vars, const double* params)
{
  // -beta*E
  return -params[0] * vars[0];
}

std::string
dos_filename(std::string& fprex, const double l, const int L)
{
  std::string filename = fprex
                         + "_l_" + std::to_string(L)
                         + "_L_" + std::to_string(L)
                         + "_dos.dat";
  return filename;
}

std::string
data_filename(std::string& fprex, const double l, const int L)
{
  std::string filename;
    filename = fprex
             + "_l_" + std::to_string(l)
             + "_L_" + std::to_string(L)
             + "_data.dat";
  return filename;
}

void
write(statphy_rewl* w, int argc, char** argv)
{
  std::string fprex = argv[0];
  double  l    = 0;
  int     L    = 1;
  double  mulf = 4;
  double  minf = 0.9;
  int     t    = 1;
  int     x    = 1;
  double  h    = 0.0;
  for (int i = 1; i < argc; i++) {
    const std::string p = argv[i];
    if (p == std::string("-l"))     l     = std::stod(argv[i+1]);
    if (p == std::string("-L"))     L     = std::stoi(argv[i+1]);
    if (p == std::string("-mulf"))  mulf  = std::stod(argv[i+1]);
    if (p == std::string("-minf"))  minf  = std::stod(argv[i+1]);
    if (p == std::string("-t"))     t     = std::stoi(argv[i+1]);
    if (p == std::string("-x"))     x     = std::stoi(argv[i+1]);
    if (p == std::string("-h"))     h     = std::stod(argv[i+1]);
  }
  double start_time = omp_get_wtime();
  #pragma omp parallel
  {
    int* s = (int*)malloc(L * L * sizeof(int));
    sys_params params;
    params.l    = l;
    params.L    = L;
    params.s    = s;
    params.N0   = L*L;
    params.N1   = 0;
    params.N2   = 0;
    auto filename = dos_filename(fprex, l, L);
    statphy_rewl_calculate_dos(w, func, &params, mulf, t, x, h, 
                               minf, filename.c_str());
    free(s);
  }
  double end_time = omp_get_wtime();
  fprintf(stderr, "time taken = %e s\n", end_time - start_time);
  return;
}

inline bool
file_exists(const std::string& name) {
    if (FILE *file = fopen(name.c_str(), "r")) {
        fclose(file);
        return true;
    } else {
        return false;
    }
}

void
read(statphy_rewl* w, int argc, char** argv)
{
  std::string fprex = argv[0];
  double  l         = 0;
  int     L         = 1;
  double  minkT     = 0.0;
  double  maxkT     = 1000.0;
  double  inckT     = 10000.0;
  for (int i = 1; i < argc; i++) {
    const std::string p = argv[i];
    if (p == std::string("-l"))     l     = std::stod(argv[i+1]);
    if (p == std::string("-L"))     L     = std::stoi(argv[i+1]);
    if (p == std::string("-minkT")) minkT = std::stod(argv[i+1]);
    if (p == std::string("-maxkT")) maxkT = std::stod(argv[i+1]);
    if (p == std::string("-inckT")) inckT = std::stod(argv[i+1]);
  }
  
  {
    auto filename = dos_filename(fprex, l, L);
    if (file_exists(filename)) {
      statphy_rewl_read_dos(w, filename.c_str());
    }
  }

  std::string datafile = data_filename(fprex, l, L);
  FILE* fp;
  fp = fopen(datafile.c_str(),"w");
  fclose(fp);
  for (double kT = minkT; kT < maxkT; kT += inckT) {
    const double beta = 1./kT;
    double params[statphy_rewl_num_vars];
    params[0] = beta;

    double log_Z = -1e9;
    for (int i = 0; i < w->gwin.num_allbins; i++) {
      statphy_rewl_vars_t vars;
      statphy_rewl_vars_of_bin(&(w->gwin), i, vars);
      const double log_Z_E = w->lng[i] + action(vars, params);
      log_Z = (log_Z>log_Z_E?log_Z:log_Z_E)
            + log(1. + exp(-1.*fabs(log_Z-log_Z_E)));
    }
    double av_e = 0., av_e2 = 0.;
    double av_s = 0., av_s2 = 0., av_s4 = 0.;
    for (int i = 0; i < w->gwin.num_allbins; i++) {
      statphy_rewl_vars_t vars;
      statphy_rewl_vars_of_bin(&(w->gwin), i, vars);
      const double wt = exp(w->lng[i]-log_Z+action(vars,params));
      const statphy_rewl_var_t E = vars[0];
      const statphy_rewl_var_t S = vars[1];
      const double e = E / (1. * L * L);
      const double s = S / (1. * L * L);
      const double e2 = e * e;
      const double s2 = s * s;
      const double s4 = (s * s) * (s * s);
      av_e  += e  * wt;
      av_e2 += e2 * wt;
      av_s  += s  * wt;
      av_s2 += s2 * wt;
      av_s4 += s4 * wt;
    }
    double avc_e2 = 0., avc_e3 = 0., avc_e4 = 0.;
    for (int i = 0; i < w->gwin.num_allbins; i++) {
      statphy_rewl_vars_t vars;
      statphy_rewl_vars_of_bin(&(w->gwin), i, vars);
      const double wt = exp(w->lng[i]-log_Z+action(vars,params));
      const statphy_rewl_var_t E = vars[0];
      const double e = E / (1. * L * L);
      const double ce = e - av_e;
      avc_e2  += (ce * ce) * wt;
      avc_e3  += (ce * ce) * (ce * wt);
      avc_e4  += (ce * ce) * (ce * ce) * wt;
    }
    fp = fopen(datafile.c_str(),"a");
    fprintf(fp, "%e\t", l);
    fprintf(fp, "%e\t", kT);
    fprintf(fp, "%e\t", av_e);
    fprintf(fp, "%e\t", (L * L) * (beta * beta) * (av_e2 - av_e * av_e));
    fprintf(fp, "%e\t", avc_e3 * (L * L * L) * (beta * beta * beta));
    fprintf(fp, "%e\t", 1. - avc_e4 / (3. * avc_e2 * avc_e2)) ;
    fprintf(fp, "%e\t", av_s);
    fprintf(fp, "%e\t", (L * L) * (beta) * (av_s2 - av_s * av_s));
    fprintf(fp, "%e\t", 1. - av_s4 / (2. * av_s2 * av_s2)) ;
    fprintf(fp, "\n");
    fclose(fp);

  } // ends kT loop
  return;
}

int
main(int argc, char** argv)
{
  int dxE          = 1;
  int dxS          = 1;
  double l         = 0;
  int L           = 1;
  int flag_read   = 1;
  double overlap  = 1.;
  double cutoff   = 1e-3;
  int use_runtime_partition = 0;
  for (int i = 1; i < argc; i++) {
    const std::string p = argv[i];
    if (p == std::string("-dxE"))      dxE = std::stoi(argv[i+1]);
    if (p == std::string("-dxS"))      dxS = std::stoi(argv[i+1]);
    if (p == std::string("-l"))       l = std::stod(argv[i+1]);
    if (p == std::string("-L"))       L = std::stoi(argv[i+1]);
    if (p == std::string("-overlap")) overlap = std::stod(argv[i+1]);
    if (p == std::string("-cutoff"))  cutoff = std::stod(argv[i+1]);
    if (p == std::string("-runtime")) use_runtime_partition = 1;
    if (p == std::string("-write"))   flag_read = 0;
    if (p == std::string("-read"))    flag_read = 1;
  }
  statphy_rewl_win_t vd;
  const int N     = L * L;
  vd.min[0]       = (l < 0 ? l * N : 0);    
  vd.max[0]       = (l < 0 ? 2 * N : 2 * N + l * N); 
  vd.dx[0]        = dxE;
  vd.overlap[0]   = overlap; 
  vd.initial[0]   = 0;
  vd.min[1]       = 0;    
  vd.max[1]       = 1 * N; 
  vd.dx[1]        = dxS;
  vd.overlap[1]   = 1.00; 
  vd.initial[1]   = N;
  vd.use_runtime_partition = use_runtime_partition;
  vd.cutoff_fraction = cutoff;
  statphy_rewl* w = statphy_rewl_alloc(vd);
  if (flag_read == 0) {
    write(w, argc, argv);
  } else {
    read(w, argc, argv);
  }
  statphy_rewl_free(w);
  return 0;
}
