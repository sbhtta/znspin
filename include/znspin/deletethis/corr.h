#ifndef ZNSPIN_CORR_H
#define ZNSPIN_CORR_H

#include <vector>
#include <cmath>
#include <complex>
#include <cassert>
#include <qspin/setup.h>
#include <qtools/lattice.h>

namespace qspin {

template<std::size_t Dm>
struct correlator
{

  correlator()
  {
    static_assert(Dm == 2, "yet to write correlator for D != 2.");
    fft_vector  = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) *  1);
    fft_store   = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) *  1);
    fft_fwd_p   = fftw_plan_dft_2d(1, 1, fft_vector, fft_vector, FFTW_FORWARD, FFTW_ESTIMATE);
    fft_bck_p   = fftw_plan_dft_2d(1, 1, fft_store, fft_vector, FFTW_BACKWARD, FFTW_ESTIMATE);
  }

  ~correlator()
  { destroy(); }

  void destroy()
  {
    fftw_destroy_plan(fft_fwd_p);
    fftw_destroy_plan(fft_bck_p);
    fftw_free(fft_store);
    fftw_free(fft_vector);
  }

  void
  setup(const std::array<int,Dm>& lx)
  {
    destroy();
    config_count = 0;
    for (std::size_t i = 0; i < Dm; i++) {
      f_length[i] = lx[i];
    }
    fft_vector  = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) *  lx[0] * lx[1]);
    fft_store   = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) *  lx[0] * lx[1]);
    fft_fwd_p   = fftw_plan_dft_2d(lx[0], lx[1], fft_vector, fft_vector, FFTW_FORWARD, FFTW_ESTIMATE);
    fft_bck_p   = fftw_plan_dft_2d(lx[0], lx[1], fft_store, fft_vector, FFTW_BACKWARD, FFTW_ESTIMATE);
    #pragma omp parallel for
    for (std::size_t i = 0; i < lx[0] * lx[1]; i++) {
      fft_store[i][0] = 0;
      fft_store[i][1] = 0;
    }
  }

  std::size_t size() const
  { return f_length[0] / 2; }

  double
  operator[](const std::size_t r) const noexcept
  { return fft_vector[r][0] / fft_vector[0][0]; }

  template<typename FN0, typename FN1>
  void process(FN0&& func0, FN1&& func1)
  {
    const auto lx = f_length;
    // set complex variables:
    #pragma omp parallel for
    for (std::size_t i = 0; i < lx[0] * lx[1]; i++) {
      fft_vector[i][0] = func0(i);
      fft_vector[i][1] = func1(i);
    }
    // perform fourier transform:
    fftw_execute(fft_fwd_p);
    // create a series with the power of
    // the fourier elements:
    // F_new[k] = |F[k]|^2;
    #pragma omp parallel for
    for (std::size_t i = 0; i < lx[0] * lx[1]; i++) {
      const auto tmp0 = fft_vector[i][0];
      const auto tmp1 = fft_vector[i][1];
      const auto magn = tmp0 * tmp0 + tmp1 * tmp1;
      fft_store[i][0] += (magn - fft_store[i][0]) / (config_count + 1.0);
      //fft_store[i][1] += 0;
    }
    config_count += 1;
  }

  void
  finalize()
  {
    // subtract DC component:
    fft_store[0][0] = 0;
    fft_store[0][1] = 0;
    // perform inverse fourier transform:
    fftw_execute(fft_bck_p);
  }

  fftw_complex *fft_vector;
  fftw_complex *fft_store;
  fftw_plan fft_fwd_p;
  fftw_plan fft_bck_p;
  std::array<int,Dm> f_length;
  int config_count;

};
/// @}

/**
@addtogroup qspin_locobs
*/
/// @{
/**
@brief returns degree m correlations at half system size
@note Uses OpenMP parallel region.
@tparam Q Number of spin states.
@tparam LN Name of the lattice.
@tparam EF Status of external field.
@tparam VS Status of vortex suppression.
@param H Hamiltonian of the model.
@param s Array containing spin states.
*/
template<std::size_t 						Q,
				 qtools::lattice_name 	LN,  
				 external_field 				EF,
				 vortex_suppression 		VS>
std::array<double,Q/2+1>
Cmtheta(const hamiltonian<Q, LN, EF, VS>& H,
        const std::vector<int>& s)
{
	std::array<double,Q/2+1> cmtheta = {0}; 
    std::array<int,Q*Q> num_spins = {0};
	#pragma omp parallel
	{
        std::array<int,Q*Q> local_num_spins = {0};
		const auto lx = H.length();
		if (LN == qtools::lattice_name::square) {
	  	omp_for_each_vertex_2D(x, y, lx[0], lx[1],
				const auto s0 = s[qtools::index_coords(x,y,lx[0],lx[1])];
				const auto sr = s[qtools::index_coords(x+lx[0]/2,y+lx[1]/2,lx[0],lx[1])];
				num_spins[s0 + Q * sr] += 1;
			);
		} else if (LN == qtools::lattice_name::cube) {
	  	omp_for_each_vertex_3D(x, y, z, lx[0], lx[1], lx[2],
				const auto s0 = s[qtools::index_coords(x,y,z,lx[0],lx[1],lx[2])];
				const auto sr = s[qtools::index_coords(x+lx[0]/2,y+lx[1]/2,z+lx[2]/2,lx[0],lx[1],lx[2])];
				num_spins[s0 + Q * sr] += 1;
			);	
		} else if (LN == qtools::lattice_name::hypercube) {
	  	omp_for_each_vertex_3D(x, y, z, t, lx[0], lx[1], lx[2], lx[3],
				const auto s0 = s[qtools::index_coords(x,y,z,lx[0],lx[1],lx[2],lx[3])];
				const auto sr = s[qtools::index_coords(x+lx[0]/2,y+lx[1]/2,z+lx[2]/2,t+lx[3]/2,lx[0],lx[1],lx[2],lx[3])];
				num_spins[s0 + Q * sr] += 1;
			);	
		}
		// bin to fraction of spins in each state:
		#pragma omp critical
		{
			for (int si = 0; si < Q; si++) {
				for (int sj = 0; sj < Q; sj++) {
                    num_spins[si + Q * sj] += local_num_spins[si + Q * sj];
                }
			}
		}
	}
			for (int si = 0; si < Q; si++) {
				for (int sj = 0; sj < Q; sj++) {
					const double tmp = std::cos(2 * M_PI * (si - sj) / Q);
                    for (int m = 1; m <= (int)(Q/2); m++) {
                        const auto mfac = (
                                            m == 1 ? tmp :
                                            m == 2 ? 2 * (tmp * tmp) - 1.0:
                                            std::cos(m * M_PI * (si - sj)/Q)
                                            );
                        cmtheta[m] += mfac * num_spins[si + Q * sj]
                                        / (1.0 * H.num_vertices());  
				    }
                }
             }
   cmtheta[0] = 1.0; 
	return cmtheta;
}


} // ends namespace qspin

#endif // ends QSPIN_CORR_H
