#ifndef ZNSPIN_H
#define ZNSPIN_H

#include <znspin/model.h>
#include <znspin/update.h>
#include <znspin/observables.h>
#include <znspin/utils.h>

#endif // ZNSPIN_H
