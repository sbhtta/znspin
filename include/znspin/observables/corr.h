#ifndef ZNSPIN_OBSERVABLES_CORR_H
#define ZNSPIN_OBSERVABLES_CORR_H

#include <array>
#include <vector>
#include <cassert>
#include <algorithm>
#include <iostream>
#include <qtools/lattice.h>

namespace znspin {

template <std::size_t N, qtools::lattice::name LatNm>
class twopoint_statebin
{
  public:
  static constexpr const int D = qtools::lattice::info<LatNm>::dimension;

  twopoint_statebin(const std::array<int,D>& init_length)
  : length_(circumscribing_cube(init_length))
  {
    count_ = 0;
    bin_.resize(N * N * length_[0]/2);
    std::fill(bin_.begin(),bin_.end(),0);
    assert(bin_.size() < 1e6);
  }

  void add_data(const spinconfig<N,LatNm>& s)
  {
    count_ += s.num_vertices();
    if (LatNm == qtools::lattice::name::square) {
      qtools::lattice::info<qtools::lattice::name::square>::for_each_vertex(s.length(),
        [&](const int x, const int y) {
          const auto s0 = s(x,y);
          for (int r = 0; r < size(); r++) {
            const auto sr = s.wrap(x+r,y+r);
            bin_[s0 + N * (sr + N * r)] += 1;
          }
        }
      );
    } else if (LatNm == qtools::lattice::name::cube) {
      qtools::lattice::info<qtools::lattice::name::cube>::for_each_vertex(s.length(),
        [&](const int x, const int y, const int z) {
          const auto s0 = s(x,y,z);
          for (int r = 0; r < size(); r++) {
            const auto sr = s.wrap(x+r,y+r,z+r);
            bin_[s0 + N * (sr + N * r)] += 1;
          }
        }
      );
    } else if (LatNm == qtools::lattice::name::hypercube) {
      qtools::lattice::info<qtools::lattice::name::hypercube>::for_each_vertex(s.length(),
        [&](const int x, const int y, const int z, const int t) {
          const auto s0 = s(x,y,z,t);
          for (int r = 0; r < size(); r++) {
            const auto sr = s.wrap(x+r,y+r,z+r,t+r);
            bin_[s0 + N * (sr + N * r)] += 1;
          }
        }
      );
    }
    return;
  }

  int size() const
  { return length_[0]/2; }

  double distance(const int r) const
  {
    assert((0 <= r)  && (r  < size()));
    return r * std::sqrt(D);
  }

  double operator()(const int r, const int s0, const int sr) const
  {
    assert((0 <= r)  && (r  < size()));
    assert((0 <= s0) && (s0 < (int)N));
    assert((0 <= sr) && (sr < (int)N));
    assert(0 <= bin_[s0 + N * (sr + N * r)]);
    return bin_[s0 + N * (sr + N * r)] / (1. * count_); 
  }

  void clear()
  {
    count_ = 0;
    std::fill(bin_.begin(),bin_.end(),0);
    return;
  }

  private:
  std::array<int,D>
  circumscribing_cube(const std::array<int,D> Lx) const
  {
    std::array<int,D> result;
    int smallest_length = Lx[0];
    for (int i = 0; i < D; i++) {
      if (Lx[i] < smallest_length) {
        smallest_length = Lx[i];
      }
    }
    std::fill(result.begin(),result.end(),smallest_length);
    return result;
  }

  long int                count_ = 0;
  const std::array<int,D> length_;
  std::vector<long int>   bin_;
};


template <qtools::lattice::name LatNm>
class correlator
{
  public:
  static constexpr const int D = qtools::lattice::info<LatNm>::dimension;

  correlator(const std::array<int,D>& init_length)
  : length_(circumscribing_cube(init_length))
  {
    count_ = 0;
    corr_.resize(length_[0]/2);
    std::fill(corr_.begin(),corr_.end(),0);
  }

  template <std::size_t N, typename CorrFn>
  void add_data(const spinconfig<N,LatNm>& s, const CorrFn& corrfunc)
  {
    count_ += s.num_vertices();
    if (LatNm == qtools::lattice::name::square) {
      qtools::lattice::info<qtools::lattice::name::square>::for_each_vertex(s.length(),
        [&](const int x, const int y) {
          const auto s0 = s(x,y);
          for (int r = 0; r < (int)corr_.size(); r++) {
            const auto sr = s.wrap(x+r,y+r);
            corr_[r] += corrfunc(s0,sr);
          }
        }
      );
    } else if (LatNm == qtools::lattice::name::cube) {
      qtools::lattice::info<qtools::lattice::name::cube>::for_each_vertex(s.length(),
        [&](const int x, const int y, const int z) {
          const auto s0 = s(x,y,z);
          for (int r = 0; r < (int)corr_.size(); r++) {
            const auto sr = s.wrap(x+r,y+r,z+r);
            corr_[r] += corrfunc(s0,sr);
          }
        }
      );
    } else if (LatNm == qtools::lattice::name::hypercube) {
      qtools::lattice::info<qtools::lattice::name::hypercube>::for_each_vertex(s.length(),
        [&](const int x, const int y, const int z, const int t) {
          const auto s0 = s(x,y,z,t);
          for (int r = 0; r < (int)corr_.size(); r++) {
            const auto sr = s.wrap(x+r,y+r,z+r,t+r);
            corr_[r] += corrfunc(s0,sr);
          }
        }
      );
    }
    return;
  }

  int size() const
  { return corr_.size(); }

  double distance(const int r) const
  { return r * std::sqrt(D); }

  double operator()(const int r) const
  {
    assert((0 <= r) && (r < (int)corr_.size()));
    return corr_[r] / (1. * count_); 
  }

  void clear()
  {
    count_ = 0;
    std::fill(corr_.begin(),corr_.end(),0);
    return;
  }

  friend std::ostream&
  operator<<(std::ostream& output, const correlator<LatNm>& C)
  {
    for (int i = 0; i < (int)C.size(); i++) {
      output << C.distance(i) << "\t" << C(i) << std::endl;
    }
    return output;
  }

  private:
  std::array<int,D>
  circumscribing_cube(const std::array<int,D> Lx) const
  {
    std::array<int,D> result;
    int smallest_length = Lx[0];
    for (int i = 0; i < D; i++) {
      if (Lx[i] < smallest_length) {
        smallest_length = Lx[i];
      }
    }
    std::fill(result.begin(),result.end(),smallest_length);
    return result;
  }

  long int                count_ = 0;
  const std::array<int,D> length_;
  std::vector<double>     corr_;
};

} // namespace znspin

#endif // ZNSPIN_OBSERVABLES_CORR_H
