#ifndef ZNSPIN_OBSERVABLES_STATE_H
#define ZNSPIN_OBSERVABLES_STATE_H

#include <array>
#include <algorithm>
#include <complex>
#include <znspin/model.h>

namespace znspin {

template<std::size_t N, qtools::lattice::name LatNm>
std::array<double,N>
state_fraction(const spinconfig<N,LatNm>& s)
{
	std::array<int,N> num = {{0}};
  for (int i = 0; i < s.num_vertices(); i++) {
    num[s[i]] += 1;
  }
	std::array<double,N> frac = {{0}}; 
  for (int i = 0; i < (int)N; i++) {
    frac[i] = num[i] / (1. * s.num_vertices());
  }
	return frac;
}

template <std::size_t N>
inline std::array<int,N>
largest_states(const std::array<double,N>& sfrac)
{
  std::array<int,N> tmp = {{0}};
  for (int i = 0; i < (int)N; i++) { tmp[i] = i; }
  std::sort(tmp.begin(), tmp.end(),
    [&](const int a, const int b) { return sfrac[a] > sfrac[b]; });
  return tmp;
}

template<std::size_t N>
inline double
order_parameter_Sn(const std::array<double,N>& sfrac)
{ return (sfrac[largest_states(sfrac)[0]] - 1/(1. * N)) * N / (N - 1.); }

template<std::size_t N>
inline std::complex<double>
order_parameter_Zn(const std::array<double,N>& sfrac)
{ 
	double mx = 0., my = 0.;
	for (int s = 0; s < (int)N; s++) {
		mx += std::cos(2. * M_PI * s / (1. * N)) * sfrac[s];
		my += std::sin(2. * M_PI * s / (1. * N)) * sfrac[s];
	}
	std::complex<double> m(mx,my);
	return m;
}

template<std::size_t N>
inline std::complex<double>
order_parameter_Z3(const std::array<double,N>& sfrac)
{
  std::array<double,3> z3sfrac = {{0,0,0}};
  for (int i = 0; i < static_cast<int>(N); i += 3) {
    z3sfrac[0] += sfrac[i+0];
    z3sfrac[1] += sfrac[i+1];
    z3sfrac[2] += sfrac[i+2];
  }
	return order_parameter_Zn(z3sfrac);
}

template<std::size_t N>
inline std::array<double,2>
ashkin_teller_sigma_fraction(const std::array<double,N>& sfrac)
{ 
  assert(N == 4);
  // map for (s = sigma tau) : 0 = --, 1 = +-, 2 = ++, 3 = -+
  std::array<double,2> sigma = {{0,0}};
  sigma[0] = sfrac[0] + sfrac[3]; // sigma -ve
  sigma[1] = sfrac[1] + sfrac[2]; // sigma +ve
	return sigma;
}

template<std::size_t N>
inline std::array<double,2>
ashkin_teller_tau_fraction(const std::array<double,N>& sfrac)
{ 
  assert(N == 4);
  // map for (s = sigma tau) : 0 = --, 1 = +-, 2 = ++, 3 = -+
  std::array<double,2> tau = {{0,0}};
  tau[0] = sfrac[0] + sfrac[1]; // tau -ve
  tau[1] = sfrac[2] + sfrac[3]; // tau +ve
	return tau;
}

template<std::size_t N>
inline std::array<double,2>
ashkin_teller_sigmatau_fraction(const std::array<double,N>& sfrac)
{ 
  assert(N == 4);
  // map for (s = sigma tau) : 0 = --, 1 = +-, 2 = ++, 3 = -+
  std::array<double,2> sigmatau = {{0,0}};
  sigmatau[0] = sfrac[1] + sfrac[3]; // sigmatau -ve
  sigmatau[1] = sfrac[0] + sfrac[2]; // sigmatau +ve
	return sigmatau;
}

template<std::size_t		      N,
				 qtools::lattice::name 	LatNm,  
				 external_field 		  ExtFld,
				 vortex_suppression   VxSupp,
				 vortex_suppression   Z3VxSupp>
double
hEsi_per_spin(const hamiltonian<N,LatNm,ExtFld,VxSupp,Z3VxSupp>& H,
              const std::array<double,N> sfrac)
{
	double e = 0.0;
	for (int si = 0; si < (int)N; si++) {
	  e += H.h() * H.Esi(si) * sfrac[si];
	}
	return e;
}


} // namespace znspin

#endif // ZNSPIN_OBSERVABLES_STATE_H
