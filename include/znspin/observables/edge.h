#ifndef ZNSPIN_OBSERVABLES_EDGE_H
#define ZNSPIN_OBSERVABLES_EDGE_H

#include <array>
#include <cmath>
#include <complex>
#include <znspin/model.h>

namespace znspin {

template<std::size_t  	      N,
				 qtools::lattice::name 	LatNm>
std::array<std::array<std::array<double,
           qtools::lattice::info<LatNm>::max_num_outgoing_edges_per_vertex>,N>,N>
edge_fraction(const spinconfig<N,LatNm>& s)
{
	constexpr const auto NumOutEdges
    = qtools::lattice::info<LatNm>::max_num_outgoing_edges_per_vertex;

	std::array<int, N * N * NumOutEdges> count = {{0}};
  if (LatNm == qtools::lattice::name::square) {
    qtools::lattice::info<qtools::lattice::name::square>::for_each_vertex(s.length(),
      [&](const int x, const int y) {
        const auto s0 = s.wrap(x+0,y+0);
        const auto s1 = s.wrap(x+1,y+0);
        const auto s2 = s.wrap(x+0,y+1);
        count[qtools::flatten_coords<3>({{N,N,NumOutEdges}},s0,s1,0)] += 1;
        count[qtools::flatten_coords<3>({{N,N,NumOutEdges}},s0,s2,1)] += 1;
      }
    );
  } else if (LatNm == qtools::lattice::name::cube) {
    qtools::lattice::info<qtools::lattice::name::cube>::for_each_vertex(s.length(),
      [&](const int x, const int y, const int z) {
        const auto s0 = s.wrap(x+0,y+0,z+0);
        const auto s1 = s.wrap(x+1,y+0,z+0);
        const auto s2 = s.wrap(x+0,y+1,z+0);
        const auto s3 = s.wrap(x+0,y+0,z+1);
        count[qtools::flatten_coords<3>({{N,N,NumOutEdges}},s0,s1,0)] += 1;
        count[qtools::flatten_coords<3>({{N,N,NumOutEdges}},s0,s2,1)] += 1;
        count[qtools::flatten_coords<3>({{N,N,NumOutEdges}},s0,s3,2)] += 1;
      }
    );
  } else if (LatNm == qtools::lattice::name::hypercube) {
    qtools::lattice::info<qtools::lattice::name::hypercube>::for_each_vertex(s.length(),
      [&](const int x, const int y, const int z, const int t) {
        const auto s0 = s.wrap(x+0,y+0,z+0,t+0);
        const auto s1 = s.wrap(x+1,y+0,z+0,t+0);
        const auto s2 = s.wrap(x+0,y+1,z+0,t+0);
        const auto s3 = s.wrap(x+0,y+0,z+1,t+0);
        const auto s4 = s.wrap(x+0,y+0,z+0,t+1);
        count[qtools::flatten_coords<3>({{N,N,NumOutEdges}},s0,s1,0)] += 1;
        count[qtools::flatten_coords<3>({{N,N,NumOutEdges}},s0,s2,1)] += 1;
        count[qtools::flatten_coords<3>({{N,N,NumOutEdges}},s0,s3,2)] += 1;
        count[qtools::flatten_coords<3>({{N,N,NumOutEdges}},s0,s4,3)] += 1;
      }
    );
  }
  std::array<std::array<std::array<double, NumOutEdges>,N>,N> frac;
  for (int b = 0; b < NumOutEdges; b++) {
    for (int sj = 0; sj < (int)N; sj++) {
      for (int si = 0; si < (int)N; si++) {
        frac[si][sj][b] = count[qtools::flatten_coords<3>({{N,N,NumOutEdges}},si,sj,b)] / (1. * s.num_edges());
      }
    }
  }
	return frac;
}

template<std::size_t		      N,
				 qtools::lattice::name 	LatNm,  
				 external_field 		  ExtFld,
				 vortex_suppression   VxSupp,
				 vortex_suppression   Z3VxSupp>
double
Esisj_per_spin(const hamiltonian<N,LatNm,ExtFld,VxSupp,Z3VxSupp>& H,
                const std::array<std::array<std::array<double, qtools::lattice::info<LatNm>::max_num_outgoing_edges_per_vertex>,N>,N> edgfrac)
{
	constexpr const auto NumOutEdges = qtools::lattice::info<LatNm>::max_num_outgoing_edges_per_vertex;
	double e = 0.0;
	for (int b = 0; b < NumOutEdges; b++) {
		for (int si = 0; si < (int)N; si++) {
			for (int sj = 0; sj < (int)N; sj++) {
				e += H.Esisj(si, sj, b) * edgfrac[si][sj][b];
			}
		}
	}
	return e * NumOutEdges;
}

template<std::size_t		    N,
				 std::size_t 	      NumOutEdges,  
				 typename           DefDWFn>
double
domain_wall_fraction(const std::array<std::array<std::array<double, NumOutEdges>,N>,N> edgfrac, const DefDWFn& defdw)
{
	double rho = 0.0;
	for (int b = 0; b < (int)NumOutEdges; b++) {
		for (int si = 0; si < (int)N; si++) {
			for (int sj = 0; sj < (int)N; sj++) {
				rho += defdw(si,sj,b) * edgfrac[si][sj][b];
			}
		}
	}
	return rho;
}

template<std::size_t N, std::size_t NumOutEdges>  
double
cossisj_axis0(const std::array<std::array<std::array<double, NumOutEdges>,N>,N> edgfrac)
{
	double sum = 0.0;
	for (int si = 0; si < (int)N; si++) {
	  for (int sj = 0; sj < (int)N; sj++) {
		  sum += std::cos(state_angle<N>(si) - state_angle<N>(sj)) * edgfrac[si][sj][0];
		}
	}
	return sum * NumOutEdges;
}

template<std::size_t N, std::size_t NumOutEdges>  
double
sinsisj_axis0(const std::array<std::array<std::array<double, NumOutEdges>,N>,N> edgfrac)
{
	double sum = 0.0;
	for (int si = 0; si < (int)N; si++) {
	  for (int sj = 0; sj < (int)N; sj++) {
		  sum += std::sin(state_angle<N>(si) - state_angle<N>(sj)) * edgfrac[si][sj][0];
		}
	}
	return sum * NumOutEdges;
}

} // namespace znspin

#endif // ZNSPIN_OBSERVABLES_EDGE_H
