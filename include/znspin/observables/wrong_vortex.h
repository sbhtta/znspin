#ifndef ZNSPIN_OBS_WRONG_VORTEX_H
#define ZNSPIN_OBS_WRONG_VORTEX_H

#include <vector>
#include <cmath>
#include <complex>
#include <znspin/model.h>

namespace znspin {

template<std::size_t N, qtools::lattice::name LatNm>
double
wrong_vortex_fraction(const spinconfig<N,LatNm>& s)
{
  if (N <= 2) { return 0; }

	int count = 0;
  if (LatNm == qtools::lattice::name::square) {
    qtools::lattice::info<qtools::lattice::name::square>::for_each_vertex(s.length(),
      [&](const int x, const int y) {
        const auto s0 = s.wrap(x+0,y+0);
        // x,y plane:
        const auto s1 = s.wrap(x+1,y+0);
        const auto s2 = s.wrap(x+1,y+1);
        const auto s3 = s.wrap(x+0,y+1);
        const auto wn = wrong_winding_number<N>(s0,s1,s2,s3);
        count += (wn != 0);
      }
    );
  } else if (LatNm == qtools::lattice::name::cube) {
    qtools::lattice::info<qtools::lattice::name::cube>::for_each_vertex(s.length(),
      [&](const int x, const int y, const int z) {
        const auto s0  = s.wrap(x+0,y+0,z+0);
        // x,y plane:
        const auto s1  = s.wrap(x+1,y+0,z+0);
        const auto s2  = s.wrap(x+1,y+1,z+0);
        const auto s3  = s.wrap(x+0,y+1,z+0);
        // x,z plane:
        const auto s4  = s.wrap(x+1,y+0,z+0);
        const auto s5  = s.wrap(x+1,y+0,z+1);
        const auto s6  = s.wrap(x+0,y+0,z+1);
        /// y,z plane:
        const auto s7  = s.wrap(x+0,y+1,z+0);
        const auto s8  = s.wrap(x+0,y+1,z+1);
        const auto s9  = s.wrap(x+0,y+0,z+1);
        const auto wn_xy = wrong_winding_number<N>(s0,s1,s2,s3);
        const auto wn_xz = wrong_winding_number<N>(s0,s4,s5,s6);
        const auto wn_yz = wrong_winding_number<N>(s0,s7,s8,s9);
        count += (wn_xy != 0);
        count += (wn_xz != 0);
        count += (wn_yz != 0);
      }
    );
  } else if (LatNm == qtools::lattice::name::hypercube) {
    qtools::lattice::info<qtools::lattice::name::hypercube>::for_each_vertex(s.length(),
      [&](const int x, const int y, const int z, const int t) {
        const auto s0  = s.wrap(x+0,y+0,z+0,t+0);
        // x,y plane:
        const auto s1  = s.wrap(x+1,y+0,z+0,t+0);
        const auto s2  = s.wrap(x+1,y+1,z+0,t+0);
        const auto s3  = s.wrap(x+0,y+1,z+0,t+0);
        // x,z plane:
        const auto s4  = s.wrap(x+1,y+0,z+0,t+0);
        const auto s5  = s.wrap(x+1,y+0,z+1,t+0);
        const auto s6  = s.wrap(x+0,y+0,z+1,t+0);
        // x,t plane:
        const auto s7  = s.wrap(x+1,y+0,z+0,t+0);
        const auto s8  = s.wrap(x+1,y+0,z+0,t+1);
        const auto s9  = s.wrap(x+0,y+0,z+0,t+1);
        /// y,z plane:
        const auto s10 = s.wrap(x+0,y+1,z+0,t+0);
        const auto s11 = s.wrap(x+0,y+1,z+1,t+0);
        const auto s12 = s.wrap(x+0,y+0,z+1,t+0);
        /// y,t plane:
        const auto s13 = s.wrap(x+0,y+1,z+0,t+0);
        const auto s14 = s.wrap(x+0,y+1,z+0,t+1);
        const auto s15 = s.wrap(x+0,y+0,z+0,t+1);
        /// z,t plane:
        const auto s16 = s.wrap(x+0,y+0,z+1,t+0);
        const auto s17 = s.wrap(x+0,y+0,z+1,t+1);
        const auto s18 = s.wrap(x+0,y+0,z+0,t+1);
        const auto wn_xy = wrong_winding_number<N>(s0,s1,s2,s3);
        const auto wn_xz = wrong_winding_number<N>(s0,s4,s5,s6);
        const auto wn_xt = wrong_winding_number<N>(s0,s7,s8,s9);
        const auto wn_yz = wrong_winding_number<N>(s0,s10,s11,s12);
        const auto wn_yt = wrong_winding_number<N>(s0,s13,s14,s15);
        const auto wn_zt = wrong_winding_number<N>(s0,s16,s17,s18);
        count += (wn_xy != 0);
        count += (wn_xz != 0);
        count += (wn_xt != 0);
        count += (wn_yz != 0);
        count += (wn_yt != 0);
        count += (wn_zt != 0);
      }
    );
  }
  const int D = qtools::lattice::info<LatNm>::dimension;
  return (count / ((D*(D-1)/2.) * s.num_vertices()));
}

template<std::size_t N, qtools::lattice::name LatNm>
std::array<double,2>
sublattice_wrong_vortex_fraction(const spinconfig<N,LatNm>& s)
{
  assert(LatNm == qtools::lattice::name::square);
	std::array<int,2> count = {{0}};
  qtools::lattice::info<qtools::lattice::name::square>::for_each_vertex(s.length(),
    [&](const int x, const int y) {
      const auto s0 = s.wrap(x+0,y+0);
      // x,y plane:
      const auto s1 = s.wrap(x+1,y+0);
      const auto s2 = s.wrap(x+1,y+1);
      const auto s3 = s.wrap(x+0,y+1);
      const int sublattice = 2 * ((x + y) & 1) - 1;
      const auto wn = wrong_winding_number<N>(s0,s1,s2,s3);
      const int mvx = wn * sublattice;
      count[0] += (mvx > 0);
      count[1] += (mvx < 0);
    }
  );
	std::array<double,2> frac = {{0}}; 
  for (int i = 0; i < 2; i++) {
    frac[i] = count[i] / (1. * s.num_vertices());
  }
	return frac;
}

template<std::size_t N, qtools::lattice::name LatNm>
std::array<std::array<double,2>,3>
sublattice3d_wrong_vortex_fraction(const spinconfig<N,LatNm>& s)
{
  assert(LatNm == qtools::lattice::name::cube);
	std::array<int,6> count = {{0}};
  qtools::lattice::info<qtools::lattice::name::cube>::for_each_vertex(s.length(),
    [&](const int x, const int y, const int z) {
      const auto s0 = s.wrap(x+0,y+0,z+0);
      // y,z plane:
      {
        const auto s1 = s.wrap(x,y+1,z+0);
        const auto s2 = s.wrap(x,y+1,z+1);
        const auto s3 = s.wrap(x,y+0,z+1);
        const int sublattice = 2 * ((y + z) & 1) - 1;
        const auto wn = wrong_winding_number<N>(s0,s1,s2,s3);
        const int mvx = wn * sublattice;
        count[0] += (mvx > 0);
        count[1] += (mvx < 0);
      }
      // z,x plane:
      {
        const auto s1 = s.wrap(x+0,y,z+1);
        const auto s2 = s.wrap(x+1,y,z+1);
        const auto s3 = s.wrap(x+1,y,z+0);
        const int sublattice = 2 * ((z + x) & 1) - 1;
        const auto wn = wrong_winding_number<N>(s0,s1,s2,s3);
        const int mvx = wn * sublattice;
        count[2] += (mvx > 0);
        count[3] += (mvx < 0);
      }
      // x,y plane:
      {
        const auto s1 = s.wrap(x+1,y+0,z);
        const auto s2 = s.wrap(x+1,y+1,z);
        const auto s3 = s.wrap(x+0,y+1,z);
        const int sublattice = 2 * ((x + y) & 1) - 1;
        const auto wn = wrong_winding_number<N>(s0,s1,s2,s3);
        const int mvx = wn * sublattice;
        count[4] += (mvx > 0);
        count[5] += (mvx < 0);
      }
    }
  );
	std::array<std::array<double,2>,3> frac; 
  frac[0][0] = count[0] / (1. * s.num_vertices());
  frac[0][1] = count[1] / (1. * s.num_vertices());
  frac[1][0] = count[2] / (1. * s.num_vertices());
  frac[1][1] = count[3] / (1. * s.num_vertices());
  frac[2][0] = count[4] / (1. * s.num_vertices());
  frac[2][1] = count[5] / (1. * s.num_vertices());
	return frac;
}

} // namespace znspin

#endif // ZNSPIN_OBS_WRONG_VORTEX_H
