#ifndef ZNSPIN_UPDATE_H
#define ZNSPIN_UPDATE_H

#include <znspin/update/metropolis.h>
#include <znspin/update/wrong_winding_number_metropolis.h>
#include <znspin/update/effectivez3_state_metropolis.h>
#include <znspin/update/sublattice_state_metropolis.h>
#include <znspin/update/sublattice_effectivez3_state_metropolis.h>
#include <znspin/update/wolff.h>
#include <znspin/update/ucmsw.h>

#endif // ZNSPIN_UPDATE_H
