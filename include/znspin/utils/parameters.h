#ifndef ZNSPIN_UTILS_PARAMETERS_H
#define ZNSPIN_UTILS_PARAMETERS_H

#include <string>
#include <qtools/argument.h>

namespace znspin {

class parameters
{
  public:
  parameters(const int argc, char** argv)
  {
    minLx     = qtools::fetch_argument_int(   argc, argv, "-minLx",    0);
    maxLx     = qtools::fetch_argument_int(   argc, argv, "-maxLx",    10000);
    incLx     = qtools::fetch_argument_int(   argc, argv, "-incLx",    1000000);
    packLx    = qtools::find_argument(argc, argv, "-packLx");
    minkT     = qtools::fetch_argument_double(argc, argv, "-minkT",    0);
    maxkT     = qtools::fetch_argument_double(argc, argv, "-maxkT",    2000);
    inckT     = qtools::fetch_argument_double(argc, argv, "-inckT",    100000);
    packkT    = qtools::find_argument(argc, argv, "-packkT");
    minh      = qtools::fetch_argument_double(argc, argv, "-minh",    0);
    maxh      = qtools::fetch_argument_double(argc, argv, "-maxh",    2000);
    inch      = qtools::fetch_argument_double(argc, argv, "-inch",    100000);
    packh     = qtools::find_argument(argc, argv, "-packh");
    minlm     = qtools::fetch_argument_double(argc, argv, "-minlm",    0);
    maxlm     = qtools::fetch_argument_double(argc, argv, "-maxlm",    2000);
    inclm     = qtools::fetch_argument_double(argc, argv, "-inclm",    100000);
    packlm    = qtools::find_argument(argc, argv, "-packlm3");
    minlm3     = qtools::fetch_argument_double(argc, argv, "-minlm3",    0);
    maxlm3     = qtools::fetch_argument_double(argc, argv, "-maxlm3",    2000);
    inclm3     = qtools::fetch_argument_double(argc, argv, "-inclm3",    100000);
    packlm3    = qtools::find_argument(argc, argv, "-packlm3");

    rEQ       = qtools::fetch_argument_double(argc, argv, "-rEQ",      0.1);
    stride    = qtools::fetch_argument_int(   argc, argv, "-stride",   1);
    nSM       = qtools::fetch_argument_int(   argc, argv, "-nSM",      0);
    nWN       = qtools::fetch_argument_int(   argc, argv, "-nWN",      10);
    maxlag    = qtools::fetch_argument_int(   argc, argv, "-maxlag",   0);
    uncorrpts = qtools::fetch_argument_int(   argc, argv, "-uncorrpts",0);

    proc      = qtools::fetch_argument_int(   argc, argv, "-proc",     0);
    minproc   = qtools::fetch_argument_int(   argc, argv, "-minproc",  0);
    maxproc   = qtools::fetch_argument_int(   argc, argv, "-maxproc",  0);
    incproc   = qtools::fetch_argument_int(   argc, argv, "-incproc",  1);
    seed      = qtools::fetch_argument_int(   argc, argv, "-seed",     555);

    gentpictures = qtools::find_argument(argc, argv, "-gentpictures");
    gentmxmydist = qtools::find_argument(argc, argv, "-gentmxmydist");
    avgpmxmydist = qtools::find_argument(argc, argv, "-avgpmxmydist");
    gentspincorr = qtools::find_argument(argc, argv, "-gentspincorr");
    avgpspincorr = qtools::find_argument(argc, argv, "-avgpspincorr");
    gentdwvxdist = qtools::find_argument(argc, argv, "-gentdwvxdist");
    avgpdwvxdist = qtools::find_argument(argc, argv, "-avgpdwvxdist");
    packdwvxdist = qtools::find_argument(argc, argv, "-packdwvxdist");
    gentobsvlist = qtools::find_argument(argc, argv, "-gentobsvlist");
    packobsvlist = qtools::find_argument(argc, argv, "-packobsvlist");
    avgpobsvcorr = qtools::find_argument(argc, argv, "-avgpobsvcorr");
    packobsvcorr = qtools::find_argument(argc, argv, "-packobsvcorr");
    obsdw01dw02  = qtools::find_argument(argc, argv, "-obsdw01dw02" );
    obsdw12dw03  = qtools::find_argument(argc, argv, "-obsdw12dw03" );
    obsdw0 = qtools::find_argument(argc, argv, "-obsdw0" );
    obsdw1 = qtools::find_argument(argc, argv, "-obsdw1" );
    obsdw2 = qtools::find_argument(argc, argv, "-obsdw2" );
    obsdw3 = qtools::find_argument(argc, argv, "-obsdw3" );
    obsdwl0p1dwl0m1  = qtools::find_argument(argc, argv, "-obsdwl0p1dwl0m1" );
    obsdwp1m1dwl0p2  = qtools::find_argument(argc, argv, "-obsdwp1m1dwl0p2" );
    obsdwpercolation = qtools::find_argument(argc, argv, "-obsdwpercolation" );
    obsvxpercolation = qtools::find_argument(argc, argv, "-obsvxpercolation" );
    obsvxsublattice = qtools::find_argument(argc, argv, "-obsvxsublattice" );
    obsz3vxpercolation = qtools::find_argument(argc, argv, "-obsz3vxpercolation" );
    obsz3vxsublattice = qtools::find_argument(argc, argv, "-obsz3vxsublattice" );
    wrongwindingnumber = qtools::find_argument(argc, argv, "-wrongwindingnumber" );

    fliprandom         = qtools::find_argument(argc, argv, "-fliprandom");
    initrandom         = qtools::find_argument(argc, argv, "-initrandom");
    inittwostaterandom = qtools::find_argument(argc, argv, "-inittwostaterandom");
    initcheckerboard   = qtools::find_argument(argc, argv, "-initcheckerboard");
    initvxavxlattice   = qtools::find_argument(argc, argv, "-initvxavxlattice");
    nmetropolis        = qtools::fetch_argument_int(argc, argv, "-nmetropolis", 1);
    nwolffmulticluster = qtools::fetch_argument_int(argc, argv, "-nwolffmulticluster", 0);
    nucmswmulticluster = qtools::fetch_argument_int(argc, argv, "-nucmswmulticluster", 0);

    sublatticestate    = qtools::find_argument(argc, argv, "-sublatticestate");
    effectivez3state   = qtools::find_argument(argc, argv, "-effectivez3state");

    name      = argv[0];
    if (sublatticestate) {
      name = name + std::string("_sublatticestate");
    }
  
    std::size_t dirpos = name.find("./");
    if (dirpos != std::string::npos) {
      name = name.substr(dirpos+2);
    }
    nEQ       = rEQ * nSM * stride;
    procseed  = (proc + 1) * seed;
  }

  std::string ppointstring_Lx_kT(const int Lx, const double kT) const
  { return std::string("kT_" + std::to_string(kT) + "_Lx_" + std::to_string(Lx)); }

  std::string ppointname_Lx_kT(const int Lx, const double kT) const
  { return name + "_" + ppointstring_Lx_kT(Lx, kT); }

  std::string ppackstring_Lx_kT(const int Lx, const double kT) const
  {
    if (packkT && packLx) {
      return std::string("kT_packed_Lx_packed");
    } else if (!packkT && packLx) {
      return std::string("kT_" + std::to_string(kT) + "_Lx_packed");
    } else if (packkT && !packLx) {
      return std::string("kT_packed_Lx_" + std::to_string(Lx));
    } else if (!packkT && !packLx) {
      return std::string("kT_" + std::to_string(kT) + "_Lx_" + std::to_string(Lx));
    } else {
      assert(false);
      return std::string("invalid_pack");
    }
  }

  std::string ppackname_Lx_kT(const int Lx, const double kT) const
  { return name + "_" + ppackstring_Lx_kT(Lx, kT); }

  std::string ppointstring_Lx_kT_h(const int Lx, const double kT, const double h) const
  { return std::string("h_" + std::to_string(h) + "_kT_" + std::to_string(kT) + "_Lx_" + std::to_string(Lx)); }

  std::string ppointname_Lx_kT_h(const int Lx, const double kT, const double h) const
  { return name + "_" + ppointstring_Lx_kT_h(Lx, kT, h); }

  std::string ppackstring_Lx_kT_h(const int Lx, const double kT, const double h) const
  {
    if (packh && packkT && packLx) {
      return std::string("h_packed_kT_packed_Lx_packed");
    } else if (!packh && packkT && packLx) {
      return std::string("h_" + std::to_string(h) + "_kT_packed_Lx_packed");
    } else if (packh && !packkT && packLx) {
      return std::string("h_packed_kT_" + std::to_string(kT) + "_Lx_packed");
    } else if (!packh && !packkT && packLx) {
      return std::string( "h_" + std::to_string(h) + "_kT_" + std::to_string(kT) + "_Lx_packed");
    } else if (packh && packkT && !packLx) {
      return std::string("h_packed_kT_packed_Lx_" + std::to_string(Lx));
    } else if ((!packh) && (packkT) && (!packLx)) {
      return std::string("h_" + std::to_string(h) + "_kT_packed_Lx_" + std::to_string(Lx));
    } else if (packh && !packkT && !packLx) {
      return std::string("h_packed_kT_" + std::to_string(kT) + "_Lx_" + std::to_string(Lx));
    } else if (!packh && !packkT && !packLx) {
      return std::string("h_" + std::to_string(h) + "_kT_" + std::to_string(kT) + "_Lx_" + std::to_string(Lx));
    } else {
      assert(false);
      return std::string("invalid_pack");
    }
  }

  std::string ppackname_Lx_kT_h(const int Lx, const double kT, const double h) const
  { return name + "_" + ppackstring_Lx_kT_h(Lx, kT, h); }

  std::string ppointstring_Lx_kT_lm(const int Lx, const double kT, const double lm) const
  { return std::string("lm_" + std::to_string(lm) + "_kT_" + std::to_string(kT) + "_Lx_" + std::to_string(Lx)); }

  std::string ppointname_Lx_kT_lm(const int Lx, const double kT, const double lm) const
  { return name + "_" + ppointstring_Lx_kT_lm(Lx, kT, lm); }

  std::string ppackstring_Lx_kT_lm(const int Lx, const double kT, const double lm) const
  {
    if (packlm && packkT && packLx) {
      return std::string("lm_packed_kT_packed_Lx_packed");
    } else if (!packlm && packkT && packLx) {
      return std::string("lm_" + std::to_string(lm) + "_kT_packed_Lx_packed");
    } else if (packlm && !packkT && packLx) {
      return std::string("lm_packed_kT_" + std::to_string(kT) + "_Lx_packed");
    } else if (!packlm && !packkT && packLx) {
      return std::string( "lm_" + std::to_string(lm) + "_kT_" + std::to_string(kT) + "_Lx_packed");
    } else if (packlm && packkT && !packLx) {
      return std::string("lm_packed_kT_packed_Lx_" + std::to_string(Lx));
    } else if ((!packlm) && (packkT) && (!packLx)) {
      return std::string("lm_" + std::to_string(lm) + "_kT_packed_Lx_" + std::to_string(Lx));
    } else if (packlm && !packkT && !packLx) {
      return std::string("lm_packed_kT_" + std::to_string(kT) + "_Lx_" + std::to_string(Lx));
    } else if (!packlm && !packkT && !packLx) {
      return std::string("lm_" + std::to_string(lm) + "_kT_" + std::to_string(kT) + "_Lx_" + std::to_string(Lx));
    } else {
      assert(false);
      return std::string("invalid_pack");
    }
  }

  std::string ppackname_Lx_kT_lm(const int Lx, const double kT, const double lm) const
  { return name + "_" + ppackstring_Lx_kT_lm(Lx, kT, lm); }

  std::string name;

  int     minLx;
  int     maxLx;
  int     incLx;
  int     packLx;

  double  minkT;
  double  maxkT;
  double  inckT;
  int     packkT;

  double  minh;
  double  maxh;
  double  inch;
  int     packh;

  double  minlm;
  double  maxlm;
  double  inclm;
  int     packlm;

  double  minlm3;
  double  maxlm3;
  double  inclm3;
  int     packlm3;

  int     fliprandom;
  int     initrandom;
  int     inittwostaterandom;
  int     initcheckerboard;
  int     initvxavxlattice;
  int     nmetropolis;
  int     nwolffmulticluster;
  int     nucmswmulticluster;

  int     effectivez3state;
  int     sublatticestate;

  int     stride;
  int     nSM;
  int     nWN;
  double  rEQ;
  int     nEQ;

  int     maxlag;
  int     uncorrpts;

  int     proc;
  int     minproc;
  int     maxproc;
  int     incproc;
  int     seed;
  int     procseed;

  int     gentpictures;
  int     gentmxmydist;
  int     avgpmxmydist;
  int     gentspincorr;
  int     avgpspincorr;
  int     gentdwvxdist;
  int     avgpdwvxdist;
  int     packdwvxdist;
  int     gentobsvlist;
  int     packobsvlist;
  int     avgpobsvcorr;
  int     packobsvcorr;
  int     obsdwpercolation;
  int     obsdw01dw02;
  int     obsdw12dw03;
  int     obsdw0;
  int     obsdw1;
  int     obsdw2;
  int     obsdw3;
  int     obsdwl0p1dwl0m1;
  int     obsdwp1m1dwl0p2;
  int     obsvxpercolation;
  int     obsvxsublattice;
  int     obsz3vxpercolation;
  int     obsz3vxsublattice;
  int     wrongwindingnumber;
};

} // namespace znspin

#endif // ZNSPIN_UTILS_PARAMETERS_H
