#ifndef ZNSPIN_UTILS_MXMYDIST_H
#define ZNSPIN_UTILS_MXMYDIST_H

#include <iostream>
#include <fstream>
#include <array>
#include <string>
#include <qtools/rng.h>
#include <qtools/histogram.h>
#include <znspin/model.h>
#include <znspin/update.h>
#include <znspin/observables/state.h>
#include <znspin/utils/mixupdate.h>

namespace znspin {

template<std::size_t N, qtools::lattice::name LatNm>
void
fill_mxmydist(const spinconfig<N,LatNm>& s,
              qtools::histogram<2>& h)
{
  const auto sfrac  = state_fraction(s);
  const auto znmag  = order_parameter_Zn(sfrac);
  const auto modm   = std::abs(znmag);
  const auto phi    = std::arg(znmag);
  const auto dphi   = static_cast<double>(2. * M_PI / (1. * N));
  const auto wphi   = std::fmod(phi + 2. * M_PI, dphi);
  for (int si = 0; si < (int)N; si++) {
    for (int sign : {-1,+1}) {
      const double mx = modm * std::cos(sign * wphi + dphi * si);
      const double my = modm * std::sin(sign * wphi + dphi * si);
      h.add_data(mx, my);
    }
  }
  return;
}

template <std::size_t         N,
          qtools::lattice::name LatNm,
          external_field      ExtFld,
          vortex_suppression  VxSupp,
          vortex_suppression  Z3VxSupp>
void gentmxmydist(const parameters& params,
                  const hamiltonian<N, LatNm, ExtFld, VxSupp, Z3VxSupp>& H,
                  const std::array<int,qtools::lattice::info<LatNm>::dimension>& length,
                  const std::string ppointname)
{
  auto rng = qtools::rng_vector(1, params.procseed);
  auto s   = spinconfig<N,LatNm>(length);
  auto h   = qtools::histogram<2>({{-1,+1,0.01}},{{-1,+1,0.01}});


  s.fill(0);
  if (params.initrandom) {
    for (int i = 0; i < s.num_vertices(); i++) {
      s[i] = qtools::rng_uniform(rng[0]) * N;
    }
  } else if (params.inittwostaterandom) {
    for (int i = 0; i < s.num_vertices(); i++) {
      s[i] = qtools::rng_uniform(rng[0]) * 2;
    }
  } else if (params.initcheckerboard) {
    s.initialize_checkerboard();
  } else if (params.initvxavxlattice) {
    s.initialize_vxavxlattice();
  }
  for (int t = 0; t < params.nEQ; t++) {
    znspin::mixupdate(params, H, s, rng);
  }
  const int NEff = (N&1) ? 2*N : N;
  if (params.sublatticestate) {
    auto snew = spinconfig<NEff,LatNm>(s.length());
    for (int t = 0; t < params.nSM; t++) {
      for (int i = 0; i < params.stride; i++) {
        znspin::mixupdate(params, H, s, rng);
      }
      s.copy_sublattice_states_to(snew);
      fill_mxmydist(snew, h);
    }
  } else {
    for (int t = 0; t < params.nSM; t++) {
      for (int i = 0; i < params.stride; i++) {
        znspin::mixupdate(params, H, s, rng);
      }
      fill_mxmydist(s, h);
    }
  }
  std::string filename = ppointname + "_gentmxmydist_nSM_" + std::to_string(params.nSM)
                       + "_stride_" + std::to_string(params.stride)
                       + "_proc_" + std::to_string(params.proc) + ".dat";
  std::ofstream outfile(filename);
  outfile << std::scientific << h;
  return;
}

template <std::size_t         N,
          qtools::lattice::name LatNm,
          external_field      ExtFld,
          vortex_suppression  VxSupp,
          vortex_suppression  Z3VxSupp>
void avgpmxmydist(const parameters& params,
                  const hamiltonian<N, LatNm, ExtFld, VxSupp, Z3VxSupp>& H,
                  const std::array<int,qtools::lattice::info<LatNm>::dimension>& length,
                  const std::string ppointname)
{
  auto h = qtools::histogram<2>({{-1,+1,0.01}},{{-1,+1,0.01}});
  int file_count = 0;
  for (int proc = params.minproc; proc <= params.maxproc; proc += params.incproc) {
    std::string filename = ppointname + "_gentmxmydist_nSM_" + std::to_string(params.nSM)
                         + "_stride_" + std::to_string(params.stride)
                         + "_proc_" + std::to_string(proc) + ".dat";
    std::ifstream infile(filename);
    if (infile.is_open()) {
      double mx, my, value;
      while (infile >> mx >> my >> value) {
        h.add_value_data(value, mx, my);
      }
      infile.close();
      file_count++;
    }
  }
  if (file_count > 0) {
    std::string filename = ppointname + "_avgpmxmydist_nSM_" + std::to_string(params.nSM * file_count)
                         + "_stride_" + std::to_string(params.stride) + ".dat";
    std::ofstream outfile(filename);
    outfile << std::scientific << h;
  }
  return;
}

} // namespace znspin

#endif // ZNSPIN_UTILS_MXMYDIST_H
