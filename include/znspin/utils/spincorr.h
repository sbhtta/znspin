#ifndef ZNSPIN_UTILS_SPINCORR_H
#define ZNSPIN_UTILS_SPINCORR_H

#include <iostream>
#include <fstream>
#include <array>
#include <string>
#include <cmath>
#include <qtools/rng.h>
#include <qtools/statistics.h>
#include <znspin/model.h>
#include <znspin/update.h>
#include <znspin/observables/corr.h>
#include <znspin/utils/parameters.h>
#include <znspin/utils/mixupdate.h>

namespace znspin {

template <std::size_t         N,
          qtools::lattice::name LatNm,
          external_field      ExtFld,
          vortex_suppression  VxSupp,
          vortex_suppression  Z3VxSupp>
void gentspincorr(const parameters& params,
                  const hamiltonian<N, LatNm, ExtFld, VxSupp, Z3VxSupp>& H,
                  const std::array<int,qtools::lattice::info<LatNm>::dimension>& length,
                  const std::string ppointname)
{
  auto rng = qtools::rng_vector(1, params.procseed);
  auto s   = spinconfig<N,LatNm>(length);
  auto C   = correlator<LatNm>(length);

  s.fill(0);
  if (params.initrandom) {
    for (int i = 0; i < s.num_vertices(); i++) {
      s[i] = qtools::rng_uniform(rng[0]) * N;
    }
  } else if (params.inittwostaterandom) {
    for (int i = 0; i < s.num_vertices(); i++) {
      s[i] = qtools::rng_uniform(rng[0]) * 2;
    }
  } else if (params.initcheckerboard) {
    s.initialize_checkerboard();
  } else if (params.initvxavxlattice) {
    s.initialize_vxavxlattice();
  }
  for (int t = 0; t < params.nEQ; t++) {
    znspin::mixupdate(params, H, s, rng);
  }

  if (params.sublatticestate) {
    const int NEff = (N&1) ? 2*N : N;
    auto snew = spinconfig<NEff,LatNm>(s.length());
    std::array<std::array<double, NEff>,NEff> cos_state;
    for (int sj = 0; sj < (int)NEff; sj++) {
      for (int si = 0; si < (int)NEff; si++) {
        cos_state[si][sj] = std::cos(state_angle<NEff>(si) - state_angle<NEff>(sj));
      }
    }
    for (int t = 0; t < params.nSM; t++) {
      for (int i = 0; i < params.stride; i++) {
        znspin::mixupdate(params, H, s, rng);
      }
      s.copy_sublattice_states_to(snew);
      C.add_data(snew, [&](const int si, const int sj){ return cos_state[si][sj]; });
    }
  } else {
    std::array<std::array<double, N>,N> cos_state;
    for (int sj = 0; sj < (int)N; sj++) {
      for (int si = 0; si < (int)N; si++) {
        cos_state[si][sj] = std::cos(state_angle<N>(si) - state_angle<N>(sj));
      }
    }
    for (int t = 0; t < params.nSM; t++) {
      for (int i = 0; i < params.stride; i++) {
        znspin::mixupdate(params, H, s, rng);
      }
      C.add_data(s, [&](const int si, const int sj){ return cos_state[si][sj]; });
    }
  }

  std::string filename = ppointname + "_gentspincorr_nSM_" + std::to_string(params.nSM)
                       + "_stride_" + std::to_string(params.stride)
                       + "_proc_" + std::to_string(params.proc) + ".dat";
  std::ofstream outfile(filename);
  outfile << std::scientific << C;
  return;
}

template <std::size_t         N,
          qtools::lattice::name LatNm,
          external_field      ExtFld,
          vortex_suppression  VxSupp,
          vortex_suppression  Z3VxSupp>
void avgpspincorr(const parameters& params,
                  const hamiltonian<N, LatNm, ExtFld, VxSupp, Z3VxSupp>& H,
                  const std::array<int,qtools::lattice::info<LatNm>::dimension>& length,
                  const std::string ppointname)
{
  bool first_file_found = false;
  std::vector<double> list_dist;  
  for (int proc = params.minproc; proc <= params.maxproc; proc += params.incproc) {
    std::string filename = ppointname + "_gentspincorr_nSM_" + std::to_string(params.nSM)
                         + "_stride_" + std::to_string(params.stride)
                         + "_proc_" + std::to_string(proc) + ".dat";
    std::ifstream infile(filename);
    if (infile.is_open()) {
      first_file_found  = true;
      double dist, value;
      while (infile >> dist >> value) {
        list_dist.push_back(dist);
      }
      infile.close();
    }
    if (first_file_found) break;
  }
  if (list_dist.size() > 0) {
    std::vector< std::vector<double> > C(list_dist.size());  
    for (int proc = params.minproc; proc <= params.maxproc; proc += params.incproc) {
      std::string filename = ppointname + "_gentspincorr_nSM_" + std::to_string(params.nSM)
                           + "_stride_" + std::to_string(params.stride)
                           + "_proc_" + std::to_string(proc) + ".dat";
      std::ifstream infile(filename);
      if (infile.is_open()) {
        double dist, value;
        int count = 0;
        while (infile >> dist >> value) {
          C[count++].push_back(value);
        }
        infile.close();
        assert(count == (int)list_dist.size());
      }
    }
    std::string filename = ppointname + "_avgpspincorr_nSM_" + std::to_string(params.nSM * C[0].size())
                         + "_stride_" + std::to_string(params.stride) + ".dat";
    std::ofstream outfile(filename);
    for (int i = 0; i < (int)list_dist.size(); i++) {
      outfile << std::scientific;
      outfile << list_dist[i] << "\t";
      outfile << qtools::jackknife_mean_error(C[i]) << "\t";
      outfile << std::endl;
    }
  }
  return;
}

} // namespace znspin

#endif // ZNSPIN_UTILS_SPINCORR_H
