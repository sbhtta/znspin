#ifndef ZNSPIN_UTILS_MIXUPDATE_H
#define ZNSPIN_UTILS_MIXUPDATE_H

#include <qtools/rng.h>
#include <znspin/model.h>
#include <znspin/update.h>
#include <znspin/utils/parameters.h>

namespace znspin {

template <std::size_t         N,
          qtools::lattice::name LatNm,
          external_field      ExtFld,
          vortex_suppression  VxSupp,
          vortex_suppression  Z3VxSupp>
void mixupdate(const parameters& params,
               const hamiltonian<N, LatNm, ExtFld, VxSupp, Z3VxSupp>& H,
                     spinconfig<N, LatNm>& s,
                     qtools::rng_vector& rng)
{
  if (VxSupp == vortex_suppression::off && Z3VxSupp == vortex_suppression::off) {
    for (int t = 0; t < params.nwolffmulticluster; t++) {
      znspin::wolff_multicluster_update(H, s, rng);
    }
    for (int t = 0; t < params.nucmswmulticluster; t++) {
      znspin::ucmsw_multicluster_update(H, s, rng);
    }
    for (int t = 0; t < params.nmetropolis; t++) {
      if (params.sublatticestate) {
        znspin::sublattice_state_metropolis_update(H, s, rng, params.fliprandom);
      } else {
        znspin::metropolis_update(H, s, rng, params.fliprandom);
      }
    }
  } else {
    for (int t = 0; t < params.nmetropolis; t++) {
      if (Z3VxSupp == vortex_suppression::off) {
        if (params.sublatticestate) {
          znspin::sublattice_state_metropolis_update(H, s, rng, params.fliprandom);
        } else {
          if (params.wrongwindingnumber) {
            znspin::wrong_winding_number_metropolis_update(H, s, rng, params.fliprandom);
          } else {
            znspin::metropolis_update(H, s, rng, params.fliprandom);
          }
        }
      } else {
        if (params.sublatticestate) {
          znspin::sublattice_effectivez3_state_metropolis_update(H, s, rng, params.fliprandom);
        } else {
          znspin::effectivez3_state_metropolis_update(H, s, rng, params.fliprandom);
        }
      }
    }
  }
  return;
}

} // namespace znspin

#endif // ZNSPIN_UTILS_MIXUPDATE_H
