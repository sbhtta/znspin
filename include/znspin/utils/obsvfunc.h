#ifndef ZNSPIN_UTILS_OBSVFUNC_H
#define ZNSPIN_UTILS_OBSVFUNC_H

#include <iostream>
#include <fstream>
#include <array>
#include <string>
#include <qtools/rng.h>
#include <qtools/databook.h>
#include <znspin/model.h>
#include <znspin/update.h>
#include <znspin/observables.h>
#include <znspin/utils/parameters.h>
#include <znspin/utils/mixupdate.h>

namespace znspin {

enum class gentobsv
{
  avgH, esisj,
  modm, mr,
  cosNphi, cos2Nphi,
  rho_dw,   spn_dw,   lrg_dw,
  rho_dw01, spn_dw01, lrg_dw01,
  rho_dw02, spn_dw02, lrg_dw02,
  rho_dw12, spn_dw12, lrg_dw12,
  rho_dw03, spn_dw03, lrg_dw03,
  rho_vx,   spn_vx,   lrg_vx,
  modm_vxsub,
  modsigma, modtau, modsigmatau,
  cossisj_axis0, sinsqsisj_axis0,
  rho_dw0, spn_dw0, lrg_dw0,
  rho_dw1, spn_dw1, lrg_dw1,
  rho_dw2, spn_dw2, lrg_dw2,
  rho_dw3, spn_dw3, lrg_dw3,
  rho_dwl0p1, spn_dwl0p1, lrg_dwl0p1,
  rho_dwl0m1, spn_dwl0m1, lrg_dwl0m1,
  rho_dwp1m1, spn_dwp1m1, lrg_dwp1m1,
  rho_dwl0p2, spn_dwl0p2, lrg_dwl0p2,
  z3modm, z3mr,
  z3cosNphi, z3cos2Nphi,
  z3rho_vx,   z3spn_vx,   z3lrg_vx,
  z3modm_vxsub,
  all,
};

enum class packobsv
{
  avgH , cv_avgH , M3_avgH , Ue_avgH,       // 4
  esisj, cv_esisj, M3_esisj, Ue_esisj,      // 12
  helicity_modulus,                         // 20

  modm , chi_modm,           U_modm  ,      // 22
  mr   , chi_mr  ,           U_mr    ,      // 28
  cosNphi, cos2Nphi, oneminuscos2Nphiby2,   // 34
  modsigma, modtau, modsigmatau,            // 40

  rho_dw  , spn_dw  , lrg_dw  ,             // 46
  rho_dw01, spn_dw01, lrg_dw01,             // 52
  rho_dw02, spn_dw02, lrg_dw02,             // 58
  rho_dw12, spn_dw12, lrg_dw12,             // 64
  rho_dw03, spn_dw03, lrg_dw03,             // 70

  rho_dw0, spn_dw0, lrg_dw0,                // 76
  rho_dw1, spn_dw1, lrg_dw1,                // 82
  rho_dw2, spn_dw2, lrg_dw2,                // 88
  rho_dw3, spn_dw3, lrg_dw3,                // 94

  rho_dwl0p1, spn_dwl0p1, lrg_dwl0p1,       // 100
  rho_dwl0m1, spn_dwl0m1, lrg_dwl0m1,       // 106
  rho_dwp1m1, spn_dwp1m1, lrg_dwp1m1,       // 112
  rho_dwl0p2, spn_dwl0p2, lrg_dwl0p2,       // 118

  rho_vx  , spn_vx  , lrg_vx  ,             // 124
  modm_vxsub, chi_modm_vxsub, U_modm_vxsub, // 130

  z3modm , z3chi_modm,           z3U_modm  ,      // 136
  z3mr   , z3chi_mr  ,           z3U_mr    ,      // 142
  z3cosNphi, z3cos2Nphi, z3oneminuscos2Nphiby2,   // 148

  z3rho_vx  , z3spn_vx  , z3lrg_vx  ,             // 154
  z3modm_vxsub, z3chi_modm_vxsub, z3U_modm_vxsub, // 160

  all,
};

template <std::size_t           HN,
          qtools::lattice::name LatNm,
          external_field        ExtFld,
          vortex_suppression    VxSupp,
          vortex_suppression    Z3VxSupp,
          std::size_t           N,
          std::size_t           Z3N>
void gentobsvlist_main(const parameters& params,
                       const hamiltonian<HN, LatNm, ExtFld, VxSupp, Z3VxSupp>& H,
                       spinconfig<HN,LatNm>& hams,
                       spinconfig<N,LatNm>& s,
                       spinconfig<Z3N,LatNm>& z3s,
                       qtools::databook<double,static_cast<int>(gentobsv::all),gentobsv>& db)
{
  const auto lm       = H.lambda();
  const auto lm3      = H.lambda3();

  const auto rho_vx   = (params.wrongwindingnumber ? wrong_vortex_fraction(s) : vortex_fraction(s));
  const auto z3rho_vx = (params.effectivez3state ? vortex_fraction(z3s) : 0);

  db.add_data(gentobsv::rho_vx,   rho_vx);
  if (params.obsvxsublattice && (N > 2)) {
    if (qtools::lattice::info<LatNm>::dimension == 2) {
      const auto rho_vxsub = (params.wrongwindingnumber ? sublattice_wrong_vortex_fraction(s) : sublattice_vortex_fraction(s));
      db.add_data(gentobsv::modm_vxsub, std::abs(order_parameter_Zn(rho_vxsub)));
    } else if (qtools::lattice::info<LatNm>::dimension == 3) {
      const auto rho_vxsub = sublattice3d_vortex_fraction(s);
      const auto modm_vxsub0 = std::abs(order_parameter_Zn(rho_vxsub[0]));
      const auto modm_vxsub1 = std::abs(order_parameter_Zn(rho_vxsub[1]));
      const auto modm_vxsub2 = std::abs(order_parameter_Zn(rho_vxsub[2]));
      const auto modm_vxsub  = std::sqrt(1/3.)*std::sqrt(modm_vxsub0*modm_vxsub0 + modm_vxsub1*modm_vxsub1 + modm_vxsub2*modm_vxsub2);
      db.add_data(gentobsv::modm_vxsub, modm_vxsub);
    }
  }

  db.add_data(gentobsv::z3rho_vx,   z3rho_vx);
  if (params.effectivez3state && params.obsz3vxsublattice && (N > 2)) {
    if (qtools::lattice::info<LatNm>::dimension == 2) {
      const auto z3rho_vxsub = sublattice_vortex_fraction(z3s);
      db.add_data(gentobsv::z3modm_vxsub, std::abs(order_parameter_Zn(z3rho_vxsub)));
    } else if (qtools::lattice::info<LatNm>::dimension == 3) {
      const auto z3rho_vxsub   = sublattice3d_vortex_fraction(z3s);
      const auto z3modm_vxsub0 = std::abs(order_parameter_Zn(z3rho_vxsub[0]));
      const auto z3modm_vxsub1 = std::abs(order_parameter_Zn(z3rho_vxsub[1]));
      const auto z3modm_vxsub2 = std::abs(order_parameter_Zn(z3rho_vxsub[2]));
      const auto z3modm_vxsub  = std::sqrt(1/3.)*std::sqrt(z3modm_vxsub0*z3modm_vxsub0 + z3modm_vxsub1*z3modm_vxsub1 + z3modm_vxsub2*z3modm_vxsub2);
      db.add_data(gentobsv::z3modm_vxsub, z3modm_vxsub);
    }
  }


  double avgH = 0;
  if (VxSupp != vortex_suppression::off) {
    avgH += lm * rho_vx;
  }
  if (Z3VxSupp != vortex_suppression::off) {
    avgH += lm3 * z3rho_vx;
  }
  const auto hambfrac = edge_fraction(hams);
  const auto esisj    = Esisj_per_spin(H, hambfrac);
  const auto bfrac    = edge_fraction(s); 
  if (ExtFld == external_field::on) {
    const auto hamsfrac = state_fraction(hams);
    const auto hesi     = hEsi_per_spin(H, hamsfrac);
    avgH += hesi;
  }
  db.add_data(gentobsv::avgH, avgH + esisj);
  db.add_data(gentobsv::esisj, esisj);
  const auto val_cossisj_axis0 = cossisj_axis0(bfrac);
  const auto val_sinsisj_axis0 = sinsisj_axis0(bfrac);
  db.add_data(gentobsv::cossisj_axis0,    val_cossisj_axis0);
  db.add_data(gentobsv::sinsqsisj_axis0,  val_sinsisj_axis0 * val_sinsisj_axis0);

  const auto sfrac    = state_fraction(s);
  const auto srank    = largest_states(sfrac);
  const auto znmag    = order_parameter_Zn(sfrac);
  db.add_data(gentobsv::modm,     std::abs(znmag));
  db.add_data(gentobsv::mr,       std::abs(znmag) * std::cos(N * std::arg(znmag)));
  db.add_data(gentobsv::cosNphi,  std::cos(N * std::arg(znmag)));
  db.add_data(gentobsv::cos2Nphi, std::cos(2 * N * std::arg(znmag)));

  const auto z3mag    = order_parameter_Z3(sfrac);
  db.add_data(gentobsv::z3modm,     std::abs(z3mag));
  db.add_data(gentobsv::z3mr,       std::abs(z3mag) * std::cos(3 * std::arg(z3mag)));
  db.add_data(gentobsv::z3cosNphi,  std::cos(3 * std::arg(z3mag)));
  db.add_data(gentobsv::z3cos2Nphi, std::cos(2 * 3 * std::arg(z3mag)));

  if (N == 4) {
    const auto modsigma     = std::abs(order_parameter_Zn(ashkin_teller_sigma_fraction(sfrac)));
    const auto modtau       = std::abs(order_parameter_Zn(ashkin_teller_tau_fraction(sfrac)));
    const auto modsigmatau  = std::abs(order_parameter_Zn(ashkin_teller_sigmatau_fraction(sfrac)));
    db.add_data(gentobsv::modsigma,     (modsigma > modtau ? modsigma : modtau));
    db.add_data(gentobsv::modtau,       (modsigma > modtau ? modtau : modsigma));
    db.add_data(gentobsv::modsigmatau,  modsigmatau);
  }

  const auto defdw   = [](const int si, const int sj, const int b)
    { return (si != sj); };
  const auto defdw01 = [srank](const int si, const int sj, const int b)
    { return ((si == srank[0]) && (sj == srank[1])) ||
             ((si == srank[1]) && (sj == srank[0])); };
  const auto defdw02 = [srank](const int si, const int sj, const int b)
    { return ((si == srank[0]) && (sj == srank[2])) ||
             ((si == srank[2]) && (sj == srank[0])); };
  const auto defdw12 = [srank](const int si, const int sj, const int b)
    { return ((si == srank[1]) && (sj == srank[2])) ||
             ((si == srank[2]) && (sj == srank[1])); };
  const auto defdw03 = [srank](const int si, const int sj, const int b)
    { return ((si == srank[0]) && (sj == srank[3])) ||
             ((si == srank[3]) && (sj == srank[0])); };

  const auto defdw0 = [srank](const int si, const int sj, const int b)
    { return std::abs(state_difference<N>(si-sj)) == 0; };
  const auto defdw1 = [srank](const int si, const int sj, const int b)
    { return std::abs(state_difference<N>(si-sj)) == 1; };
  const auto defdw2 = [srank](const int si, const int sj, const int b)
    { return std::abs(state_difference<N>(si-sj)) == 2; };
  const auto defdw3 = [srank](const int si, const int sj, const int b)
    { return std::abs(state_difference<N>(si-sj)) == 3; };

  const int tp1 = ((srank[0] + 1 + static_cast<int>(N)) % static_cast<int>(N));
  const int tm1 = ((srank[0] - 1 + static_cast<int>(N)) % static_cast<int>(N));
  const int tp2 = ((srank[0] + 2 + static_cast<int>(N)) % static_cast<int>(N));
  const int tm2 = ((srank[0] - 2 + static_cast<int>(N)) % static_cast<int>(N));
  const auto sl0 = srank[0];
  const auto sp1 = (sfrac[tp1] >  sfrac[tm1] ? tp1 : tm1);
  const auto sm1 = (sfrac[tp1] <= sfrac[tm1] ? tp1 : tm1);
  const auto sp2 = (sfrac[tp2] >  sfrac[tm2] ? tp2 : tm2);
  const auto defdwl0p1 = [sl0,sp1](const int si, const int sj, const int b)
    { return ((si == sl0) && (sj == sp1)) ||
             ((si == sp1) && (sj == sl0)); };
  const auto defdwl0m1 = [sl0,sm1](const int si, const int sj, const int b)
    { return ((si == sl0) && (sj == sm1)) ||
             ((si == sm1) && (sj == sl0)); };
  const auto defdwp1m1 = [sp1,sm1](const int si, const int sj, const int b)
    { return ((si == sp1) && (sj == sm1)) ||
             ((si == sm1) && (sj == sp1)); };
  const auto defdwl0p2 = [sl0,sp2](const int si, const int sj, const int b)
    { return ((si == sl0) && (sj == sp2)) ||
             ((si == sp2) && (sj == sl0)); };
  const auto rho_dw   = domain_wall_fraction(bfrac, defdw);
  const auto rho_dw01 = domain_wall_fraction(bfrac, defdw01);
  const auto rho_dw02 = domain_wall_fraction(bfrac, defdw02);
  const auto rho_dw12 = domain_wall_fraction(bfrac, defdw12);
  const auto rho_dw03 = domain_wall_fraction(bfrac, defdw03);

  const auto rho_dw0 = domain_wall_fraction(bfrac, defdw0);
  const auto rho_dw1 = domain_wall_fraction(bfrac, defdw1);
  const auto rho_dw2 = domain_wall_fraction(bfrac, defdw2);
  const auto rho_dw3 = domain_wall_fraction(bfrac, defdw3);

  const auto rho_dwl0p1 = domain_wall_fraction(bfrac, defdwl0p1);
  const auto rho_dwl0m1 = domain_wall_fraction(bfrac, defdwl0m1);
  const auto rho_dwp1m1 = domain_wall_fraction(bfrac, defdwp1m1);
  const auto rho_dwl0p2 = domain_wall_fraction(bfrac, defdwl0p2);

  db.add_data(gentobsv::rho_dw,   rho_dw);
  db.add_data(gentobsv::rho_dw01, rho_dw01);
  db.add_data(gentobsv::rho_dw02, rho_dw02);
  db.add_data(gentobsv::rho_dw12, rho_dw12);
  db.add_data(gentobsv::rho_dw03, rho_dw03);

  db.add_data(gentobsv::rho_dw0, rho_dw0);
  db.add_data(gentobsv::rho_dw1, rho_dw1);
  db.add_data(gentobsv::rho_dw2, rho_dw2);
  db.add_data(gentobsv::rho_dw3, rho_dw3);

  db.add_data(gentobsv::rho_dwl0p1, rho_dwl0p1);
  db.add_data(gentobsv::rho_dwl0m1, rho_dwl0m1);
  db.add_data(gentobsv::rho_dwp1m1, rho_dwp1m1);
  db.add_data(gentobsv::rho_dwl0p2, rho_dwl0p2);

  if (params.obsdwpercolation) {
    s.template label_domain_walls<true,true,true,true>(defdw);
    db.add_data(gentobsv::spn_dw, s.domain_walls_are_spanning());
    s.template label_domain_walls<false,false,false,false>(defdw);
    db.add_data(gentobsv::lrg_dw, s.largest_domain_wall_fraction());
  }

  if (params.obsvxpercolation) {
    if (params.wrongwindingnumber) {
      s.template label_wrong_vortex_strings<true,true,true,true>();
    } else {
      s.template label_vortex_strings<true,true,true,true>();
    }
    db.add_data(gentobsv::spn_vx, s.vortex_strings_are_spanning());
    if (params.wrongwindingnumber) {
      s.template label_wrong_vortex_strings<false,false,false,false>();
    } else {
      s.template label_vortex_strings<false,false,false,false>();
    }
    db.add_data(gentobsv::lrg_vx, s.largest_vortex_string_fraction());
  }

  if (params.obsz3vxpercolation) {
    s.template label_z3vortex_strings<true,true,true,true>();
    db.add_data(gentobsv::spn_vx, s.vortex_strings_are_spanning());
    s.template label_z3vortex_strings<false,false,false,false>();
    db.add_data(gentobsv::lrg_vx, s.largest_vortex_string_fraction());
  }

  if (params.obsdwpercolation) {
    if (params.obsdw01dw02) {
      s.template label_domain_walls<true,true,true,true>(defdw01);
      db.add_data(gentobsv::spn_dw01, s.domain_walls_are_spanning());
      s.template label_domain_walls<false,false,false,false>(defdw01);
      db.add_data(gentobsv::lrg_dw01, s.largest_domain_wall_fraction());
      s.template label_domain_walls<true,true,true,true>(defdw02);
      db.add_data(gentobsv::spn_dw02, s.domain_walls_are_spanning());
      s.template label_domain_walls<false,false,false,false>(defdw02);
      db.add_data(gentobsv::lrg_dw02, s.largest_domain_wall_fraction());
    }
    if (params.obsdw12dw03) {
      s.template label_domain_walls<true,true,true,true>(defdw12);
      db.add_data(gentobsv::spn_dw12, s.domain_walls_are_spanning());
      s.template label_domain_walls<false,false,false,false>(defdw12);
      db.add_data(gentobsv::lrg_dw12, s.largest_domain_wall_fraction());
      s.template label_domain_walls<true,true,true,true>(defdw03);
      db.add_data(gentobsv::spn_dw03, s.domain_walls_are_spanning());
      s.template label_domain_walls<false,false,false,false>(defdw03);
      db.add_data(gentobsv::lrg_dw03, s.largest_domain_wall_fraction());
    }
    if (params.obsdw0) {
      s.template label_domain_walls<true,true,true,true>(defdw0);
      db.add_data(gentobsv::spn_dw0, s.domain_walls_are_spanning());
      s.template label_domain_walls<false,false,false,false>(defdw0);
      db.add_data(gentobsv::lrg_dw0, s.largest_domain_wall_fraction());
    }
    if (params.obsdw1) {
      s.template label_domain_walls<true,true,true,true>(defdw1);
      db.add_data(gentobsv::spn_dw1, s.domain_walls_are_spanning());
      s.template label_domain_walls<false,false,false,false>(defdw1);
      db.add_data(gentobsv::lrg_dw1, s.largest_domain_wall_fraction());
    }
    if (params.obsdw2) {
      s.template label_domain_walls<true,true,true,true>(defdw2);
      db.add_data(gentobsv::spn_dw2, s.domain_walls_are_spanning());
      s.template label_domain_walls<false,false,false,false>(defdw2);
      db.add_data(gentobsv::lrg_dw2, s.largest_domain_wall_fraction());
    }
    if (params.obsdw3) {
      s.template label_domain_walls<true,true,true,true>(defdw3);
      db.add_data(gentobsv::spn_dw3, s.domain_walls_are_spanning());
      s.template label_domain_walls<false,false,false,false>(defdw3);
      db.add_data(gentobsv::lrg_dw3, s.largest_domain_wall_fraction());
    }
    if (params.obsdwl0p1dwl0m1) {
      s.template label_domain_walls<true,true,true,true>(defdwl0p1);
      db.add_data(gentobsv::spn_dwl0p1, s.domain_walls_are_spanning());
      s.template label_domain_walls<false,false,false,false>(defdwl0p1);
      db.add_data(gentobsv::lrg_dwl0p1, s.largest_domain_wall_fraction());
      s.template label_domain_walls<true,true,true,true>(defdwl0m1);
      db.add_data(gentobsv::spn_dwl0m1, s.domain_walls_are_spanning());
      s.template label_domain_walls<false,false,false,false>(defdwl0m1);
      db.add_data(gentobsv::lrg_dwl0m1, s.largest_domain_wall_fraction());
    }
    if (params.obsdwp1m1dwl0p2) {
      s.template label_domain_walls<true,true,true,true>(defdwp1m1);
      db.add_data(gentobsv::spn_dwp1m1, s.domain_walls_are_spanning());
      s.template label_domain_walls<false,false,false,false>(defdwp1m1);
      db.add_data(gentobsv::lrg_dwp1m1, s.largest_domain_wall_fraction());
      s.template label_domain_walls<true,true,true,true>(defdwl0p2);
      db.add_data(gentobsv::spn_dwl0p2, s.domain_walls_are_spanning());
      s.template label_domain_walls<false,false,false,false>(defdwl0p2);
      db.add_data(gentobsv::lrg_dwl0p2, s.largest_domain_wall_fraction());
    }
  }

  return;
}

template <std::size_t           N,
          qtools::lattice::name LatNm,
          external_field        ExtFld,
          vortex_suppression    VxSupp,
          vortex_suppression    Z3VxSupp>
void gentobsvlist(const parameters& params,
                  const hamiltonian<N, LatNm, ExtFld, VxSupp, Z3VxSupp>& H,
                  const std::array<int,qtools::lattice::info<LatNm>::dimension>& length,
                  const std::string ppointname)
{
  auto rng = qtools::rng_vector(1, params.procseed);
  auto s   = spinconfig<N,LatNm>(length);
  auto db  = qtools::databook<double,static_cast<int>(gentobsv::all),gentobsv>(params.nSM);

  s.fill(0);
  if (params.initrandom) {
    for (int i = 0; i < s.num_vertices(); i++) {
      s[i] = qtools::rng_uniform(rng[0]) * N;
    }
  } else if (params.inittwostaterandom) {
    for (int i = 0; i < s.num_vertices(); i++) {
      s[i] = qtools::rng_uniform(rng[0]) * 2;
    }
  } else if (params.initcheckerboard) {
    s.initialize_checkerboard();
  } else if (params.initvxavxlattice) {
    s.initialize_vxavxlattice();
  }
  for (int t = 0; t < params.nEQ; t++) {
    mixupdate(params, H, s, rng);
  }

  if (params.sublatticestate) {
    const int NEff = (N&1) ? 2*N : N;
    auto subs = spinconfig<NEff,LatNm>(s.length());
    for (int t = 0; t < params.nSM; t++) {
      for (int i = 0; i < params.stride; i++) {
        znspin::mixupdate(params, H, s, rng);
      }
      s.copy_sublattice_states_to(subs);
      gentobsvlist_main(params, H, s, subs, subs, db);
    }
  } else if (params.effectivez3state) {
    auto z3s = spinconfig<3,LatNm>(s.length());
    for (int t = 0; t < params.nSM; t++) {
      for (int i = 0; i < params.stride; i++) {
        znspin::mixupdate(params, H, s, rng);
      }
      s.copy_effectivez3_states_to(z3s);
      gentobsvlist_main(params, H, s, s, z3s, db);
    }
  } else {
    for (int t = 0; t < params.nSM; t++) {
      for (int i = 0; i < params.stride; i++) {
        znspin::mixupdate(params, H, s, rng);
      }
      gentobsvlist_main(params, H, s, s, s, db);
    }
  }

  std::string filename = ppointname + "_gentobsvlist_nSM_" + std::to_string(params.nSM)
                         + "_stride_" + std::to_string(params.stride)
                         + "_proc_" + std::to_string(params.proc) + ".dat";
  std::ofstream outfile(filename);
  outfile << std::scientific << db;
  return;
}

template <std::size_t         N,
          qtools::lattice::name LatNm,
          external_field      ExtFld,
          vortex_suppression  VxSupp,
          vortex_suppression  Z3VxSupp,
          typename            AppparamsFn>
void packobsvlist(const parameters& params,
                  const hamiltonian<N, LatNm, ExtFld, VxSupp, Z3VxSupp>& H,
                  const std::array<int,qtools::lattice::info<LatNm>::dimension>& length,
                  const std::string ppointname,
                  const std::string ppackname,
                  const AppparamsFn& appparams)
{
  int num_files = 0;
  for (int proc = params.minproc; proc <= params.maxproc; proc += params.incproc) {
    std::string filename = ppointname + "_gentobsvlist_nSM_" + std::to_string(params.nSM)
                         + "_stride_" + std::to_string(params.stride)
                         + "_proc_" + std::to_string(proc) + ".dat";
    std::ifstream infile(filename);
    if (infile.is_open()) {
      infile.close();
      num_files++;
    }
  }

  const int num_points = params.nSM * num_files;
  auto dbgentobsv = qtools::databook<double,static_cast<int>(gentobsv::all), gentobsv>(num_points, params.nWN);
  auto dbpackobsv = qtools::databook<double,static_cast<int>(packobsv::all), packobsv>(params.nWN);
  for (int proc = params.minproc; proc <= params.maxproc; proc += params.incproc) {
    std::string filename = ppointname + "_gentobsvlist_nSM_" + std::to_string(params.nSM)
                         + "_stride_" + std::to_string(params.stride)
                         + "_proc_" + std::to_string(proc) + ".dat";
    std::ifstream infile(filename);
    if (infile.is_open()) {
      infile >> dbgentobsv;
      infile.close();
    }
  }

  const int nv = qtools::lattice::info<LatNm>::num_vertices(length);
  const double kT = H.kT();
  for (int w = 0; w < params.nWN; w++) {
    dbpackobsv.add_data(packobsv::avgH,       qtools::mean(dbgentobsv.data(gentobsv::avgH,  w)));
    dbpackobsv.add_data(packobsv::cv_avgH,    qtools::variance(dbgentobsv.data(gentobsv::avgH,  w)) * nv / (kT*kT));
    dbpackobsv.add_data(packobsv::M3_avgH,    qtools::central_moment<3>(dbgentobsv.data(gentobsv::avgH,  w)) * (nv*nv*nv) / (kT*kT*kT));
    dbpackobsv.add_data(packobsv::Ue_avgH,    1. - qtools::kurtosis(dbgentobsv.data(gentobsv::avgH,  w)) / 3.);

    dbpackobsv.add_data(packobsv::esisj,      qtools::mean(dbgentobsv.data(gentobsv::esisj, w)));
    dbpackobsv.add_data(packobsv::cv_esisj,   qtools::variance(dbgentobsv.data(gentobsv::esisj, w)) * nv / (kT*kT));
    dbpackobsv.add_data(packobsv::M3_esisj,   qtools::central_moment<3>(dbgentobsv.data(gentobsv::esisj,  w)) * (nv*nv*nv) / (kT*kT*kT));
    dbpackobsv.add_data(packobsv::Ue_esisj,   1. - qtools::kurtosis(dbgentobsv.data(gentobsv::esisj,  w)) / 3.);

    dbpackobsv.add_data(packobsv::helicity_modulus,  qtools::mean(dbgentobsv.data(gentobsv::cossisj_axis0, w))
                                                   - qtools::mean(dbgentobsv.data(gentobsv::sinsqsisj_axis0, w)) * nv / kT);

    dbpackobsv.add_data(packobsv::modm,       qtools::mean(dbgentobsv.data(gentobsv::modm, w)));
    dbpackobsv.add_data(packobsv::chi_modm,   qtools::variance(dbgentobsv.data(gentobsv::modm, w)) * nv / kT);
    dbpackobsv.add_data(packobsv::U_modm,     1. - qtools::raw_kurtosis(dbgentobsv.data(gentobsv::modm, w)) / 3.);

    dbpackobsv.add_data(packobsv::mr,         qtools::mean(dbgentobsv.data(gentobsv::mr, w)));
    dbpackobsv.add_data(packobsv::chi_mr,     qtools::variance(dbgentobsv.data(gentobsv::mr, w)) * nv / kT);
    dbpackobsv.add_data(packobsv::U_mr,       qtools::kurtosis(dbgentobsv.data(gentobsv::mr, w)));

    dbpackobsv.add_data(packobsv::cosNphi,   qtools::mean(dbgentobsv.data(gentobsv::cosNphi, w)));
    dbpackobsv.add_data(packobsv::cos2Nphi,  qtools::mean(dbgentobsv.data(gentobsv::cos2Nphi, w)));
    dbpackobsv.add_data(packobsv::oneminuscos2Nphiby2,  (1. - qtools::mean(dbgentobsv.data(gentobsv::cos2Nphi, w)))/2.);

    dbpackobsv.add_data(packobsv::modsigma,       qtools::mean(dbgentobsv.data(gentobsv::modsigma, w)));
    dbpackobsv.add_data(packobsv::modtau,       qtools::mean(dbgentobsv.data(gentobsv::modtau, w)));
    dbpackobsv.add_data(packobsv::modsigmatau,       qtools::mean(dbgentobsv.data(gentobsv::modsigmatau, w)));

    dbpackobsv.add_data(packobsv::rho_dw,     qtools::mean(dbgentobsv.data(gentobsv::rho_dw, w)));
    dbpackobsv.add_data(packobsv::spn_dw,     qtools::mean(dbgentobsv.data(gentobsv::spn_dw, w)));
    dbpackobsv.add_data(packobsv::lrg_dw,     qtools::mean(dbgentobsv.data(gentobsv::lrg_dw, w)));

    dbpackobsv.add_data(packobsv::rho_dw01,   qtools::mean(dbgentobsv.data(gentobsv::rho_dw01, w)));
    dbpackobsv.add_data(packobsv::spn_dw01,   qtools::mean(dbgentobsv.data(gentobsv::spn_dw01, w)));
    dbpackobsv.add_data(packobsv::lrg_dw01,   qtools::mean(dbgentobsv.data(gentobsv::lrg_dw01, w)));

    dbpackobsv.add_data(packobsv::rho_dw02,   qtools::mean(dbgentobsv.data(gentobsv::rho_dw02, w)));
    dbpackobsv.add_data(packobsv::spn_dw02,   qtools::mean(dbgentobsv.data(gentobsv::spn_dw02, w)));
    dbpackobsv.add_data(packobsv::lrg_dw02,   qtools::mean(dbgentobsv.data(gentobsv::lrg_dw02, w)));

    dbpackobsv.add_data(packobsv::rho_dw12,   qtools::mean(dbgentobsv.data(gentobsv::rho_dw12, w)));
    dbpackobsv.add_data(packobsv::spn_dw12,   qtools::mean(dbgentobsv.data(gentobsv::spn_dw12, w)));
    dbpackobsv.add_data(packobsv::lrg_dw12,   qtools::mean(dbgentobsv.data(gentobsv::lrg_dw12, w)));

    dbpackobsv.add_data(packobsv::rho_dw03,   qtools::mean(dbgentobsv.data(gentobsv::rho_dw03, w)));
    dbpackobsv.add_data(packobsv::spn_dw03,   qtools::mean(dbgentobsv.data(gentobsv::spn_dw03, w)));
    dbpackobsv.add_data(packobsv::lrg_dw03,   qtools::mean(dbgentobsv.data(gentobsv::lrg_dw03, w)));
 
    dbpackobsv.add_data(packobsv::rho_dw0,   qtools::mean(dbgentobsv.data(gentobsv::rho_dw0, w)));
    dbpackobsv.add_data(packobsv::spn_dw0,   qtools::mean(dbgentobsv.data(gentobsv::spn_dw0, w)));
    dbpackobsv.add_data(packobsv::lrg_dw0,   qtools::mean(dbgentobsv.data(gentobsv::lrg_dw0, w)));

    dbpackobsv.add_data(packobsv::rho_dw1,   qtools::mean(dbgentobsv.data(gentobsv::rho_dw1, w)));
    dbpackobsv.add_data(packobsv::spn_dw1,   qtools::mean(dbgentobsv.data(gentobsv::spn_dw1, w)));
    dbpackobsv.add_data(packobsv::lrg_dw1,   qtools::mean(dbgentobsv.data(gentobsv::lrg_dw1, w)));

    dbpackobsv.add_data(packobsv::rho_dw2,   qtools::mean(dbgentobsv.data(gentobsv::rho_dw2, w)));
    dbpackobsv.add_data(packobsv::spn_dw2,   qtools::mean(dbgentobsv.data(gentobsv::spn_dw2, w)));
    dbpackobsv.add_data(packobsv::lrg_dw2,   qtools::mean(dbgentobsv.data(gentobsv::lrg_dw2, w)));

    dbpackobsv.add_data(packobsv::rho_dw3,   qtools::mean(dbgentobsv.data(gentobsv::rho_dw3, w)));
    dbpackobsv.add_data(packobsv::spn_dw3,   qtools::mean(dbgentobsv.data(gentobsv::spn_dw3, w)));
    dbpackobsv.add_data(packobsv::lrg_dw3,   qtools::mean(dbgentobsv.data(gentobsv::lrg_dw3, w)));

    dbpackobsv.add_data(packobsv::rho_dwl0p1,   qtools::mean(dbgentobsv.data(gentobsv::rho_dwl0p1, w)));
    dbpackobsv.add_data(packobsv::spn_dwl0p1,   qtools::mean(dbgentobsv.data(gentobsv::spn_dwl0p1, w)));
    dbpackobsv.add_data(packobsv::lrg_dwl0p1,   qtools::mean(dbgentobsv.data(gentobsv::lrg_dwl0p1, w)));

    dbpackobsv.add_data(packobsv::rho_dwl0m1,   qtools::mean(dbgentobsv.data(gentobsv::rho_dwl0m1, w)));
    dbpackobsv.add_data(packobsv::spn_dwl0m1,   qtools::mean(dbgentobsv.data(gentobsv::spn_dwl0m1, w)));
    dbpackobsv.add_data(packobsv::lrg_dwl0m1,   qtools::mean(dbgentobsv.data(gentobsv::lrg_dwl0m1, w)));

    dbpackobsv.add_data(packobsv::rho_dwp1m1,   qtools::mean(dbgentobsv.data(gentobsv::rho_dwp1m1, w)));
    dbpackobsv.add_data(packobsv::spn_dwp1m1,   qtools::mean(dbgentobsv.data(gentobsv::spn_dwp1m1, w)));
    dbpackobsv.add_data(packobsv::lrg_dwp1m1,   qtools::mean(dbgentobsv.data(gentobsv::lrg_dwp1m1, w)));

    dbpackobsv.add_data(packobsv::rho_dwl0p2,   qtools::mean(dbgentobsv.data(gentobsv::rho_dwl0p2, w)));
    dbpackobsv.add_data(packobsv::spn_dwl0p2,   qtools::mean(dbgentobsv.data(gentobsv::spn_dwl0p2, w)));
    dbpackobsv.add_data(packobsv::lrg_dwl0p2,   qtools::mean(dbgentobsv.data(gentobsv::lrg_dwl0p2, w)));
 
    dbpackobsv.add_data(packobsv::rho_vx,     qtools::mean(dbgentobsv.data(gentobsv::rho_vx, w)));
    dbpackobsv.add_data(packobsv::spn_vx,     qtools::mean(dbgentobsv.data(gentobsv::spn_vx, w)));
    dbpackobsv.add_data(packobsv::lrg_vx,     qtools::mean(dbgentobsv.data(gentobsv::lrg_vx, w)));

    dbpackobsv.add_data(packobsv::modm_vxsub,       qtools::mean(dbgentobsv.data(gentobsv::modm_vxsub, w)));
    dbpackobsv.add_data(packobsv::chi_modm_vxsub,   qtools::variance(dbgentobsv.data(gentobsv::modm_vxsub, w)) * nv / kT);
    dbpackobsv.add_data(packobsv::U_modm_vxsub,     1. - qtools::raw_kurtosis(dbgentobsv.data(gentobsv::modm_vxsub, w)) / 2.);

    dbpackobsv.add_data(packobsv::z3modm,       qtools::mean(dbgentobsv.data(gentobsv::z3modm, w)));
    dbpackobsv.add_data(packobsv::z3chi_modm,   qtools::variance(dbgentobsv.data(gentobsv::z3modm, w)) * nv / kT);
    dbpackobsv.add_data(packobsv::z3U_modm,     1. - qtools::raw_kurtosis(dbgentobsv.data(gentobsv::z3modm, w)) / 3.);

    dbpackobsv.add_data(packobsv::z3mr,         qtools::mean(dbgentobsv.data(gentobsv::z3mr, w)));
    dbpackobsv.add_data(packobsv::z3chi_mr,     qtools::variance(dbgentobsv.data(gentobsv::z3mr, w)) * nv / kT);
    dbpackobsv.add_data(packobsv::z3U_mr,       qtools::kurtosis(dbgentobsv.data(gentobsv::z3mr, w)));

    dbpackobsv.add_data(packobsv::z3cosNphi,   qtools::mean(dbgentobsv.data(gentobsv::z3cosNphi, w)));
    dbpackobsv.add_data(packobsv::z3cos2Nphi,  qtools::mean(dbgentobsv.data(gentobsv::z3cos2Nphi, w)));
    dbpackobsv.add_data(packobsv::z3oneminuscos2Nphiby2,  (1. - qtools::mean(dbgentobsv.data(gentobsv::z3cos2Nphi, w)))/2.);

    dbpackobsv.add_data(packobsv::z3rho_vx,     qtools::mean(dbgentobsv.data(gentobsv::z3rho_vx, w)));
    dbpackobsv.add_data(packobsv::z3spn_vx,     qtools::mean(dbgentobsv.data(gentobsv::z3spn_vx, w)));
    dbpackobsv.add_data(packobsv::z3lrg_vx,     qtools::mean(dbgentobsv.data(gentobsv::z3lrg_vx, w)));

  } 

  if (num_files > 0) {
    std::string filename = ppackname + "_packobsvlist_nSM_" + std::to_string(params.nSM * num_files)
                         + "_stride_" + std::to_string(params.stride) + ".dat";
    std::ofstream outfile(filename, std::ios::app);
    outfile << std::scientific;
    appparams(outfile);
    for (int iobs = 0; iobs < static_cast<int>(packobsv::all); iobs++) {
      outfile << qtools::jackknife_mean_error(dbpackobsv.data(iobs)) << "\t";
    }
    outfile << std::endl;
  }
  return;
}

template <std::size_t         N,
          qtools::lattice::name LatNm,
          external_field      ExtFld,
          vortex_suppression  VxSupp,
          vortex_suppression  Z3VxSupp>
void avgpobsvcorr(const parameters& params,
                  const hamiltonian<N, LatNm, ExtFld, VxSupp, Z3VxSupp>& H,
                  const std::array<int,qtools::lattice::info<LatNm>::dimension>& length,
                  const std::string ppointname)
{
  int num_files = 0;
  for (int proc = params.minproc; proc <= params.maxproc; proc += params.incproc) {
    std::string filename = ppointname + "_gentobsvlist_nSM_" + std::to_string(params.nSM)
                         + "_stride_" + std::to_string(params.stride)
                         + "_proc_" + std::to_string(proc) + ".dat";
    std::ifstream infile(filename);
    if (infile.is_open()) {
      infile.close();
      num_files++;
    }
  }

  const int num_points = params.nSM * num_files;
  auto dbgentobsv = qtools::databook<double,static_cast<int>(gentobsv::all), gentobsv>(num_points);
  for (int proc = params.minproc; proc <= params.maxproc; proc += params.incproc) {
    std::string filename = ppointname + "_gentobsvlist_nSM_" + std::to_string(params.nSM)
                         + "_stride_" + std::to_string(params.stride)
                         + "_proc_" + std::to_string(proc) + ".dat";
    std::ifstream infile(filename);
    if (infile.is_open()) {
      infile >> dbgentobsv;
      infile.close();
    }
  }

  std::string filename = ppointname + "_avgpobsvcorr_nSM_" + std::to_string(params.nSM * num_files)
                       + "_stride_" + std::to_string(params.stride) + ".dat";
  std::ofstream outfile(filename);
  outfile << std::scientific;
  for (int lag = 0; lag < dbgentobsv.autocorrelation(0).maxlag(); lag++) {
    for (int iobs = 0; iobs < static_cast<int>(gentobsv::all); iobs++) {
      outfile << dbgentobsv.autocorrelation(iobs)(lag) << "\t";
    }
    outfile << std::endl;
  }
  return;
}

template <std::size_t         N,
          qtools::lattice::name LatNm,
          external_field      ExtFld,
          vortex_suppression  VxSupp,
          vortex_suppression  Z3VxSupp,
          typename            AppparamsFn>
void packobsvcorr(const parameters& params,
                  const hamiltonian<N, LatNm, ExtFld, VxSupp, Z3VxSupp>& H,
                  const std::array<int,qtools::lattice::info<LatNm>::dimension>& length,
                  const std::string ppointname,
                  const std::string ppackname,
                  const AppparamsFn& appparams)
{
  int num_files = 0;
  for (int proc = params.minproc; proc <= params.maxproc; proc += params.incproc) {
    std::string filename = ppointname + "_gentobsvlist_nSM_" + std::to_string(params.nSM)
                         + "_stride_" + std::to_string(params.stride)
                         + "_proc_" + std::to_string(proc) + ".dat";
    std::ifstream infile(filename);
    if (infile.is_open()) {
      infile.close();
      num_files++;
    }
  }

  const int num_points = params.nSM * num_files;
  auto dbgentobsv = qtools::databook<double,static_cast<int>(gentobsv::all), gentobsv>(num_points);
  for (int proc = params.minproc; proc <= params.maxproc; proc += params.incproc) {
    std::string filename = ppointname + "_gentobsvlist_nSM_" + std::to_string(params.nSM)
                         + "_stride_" + std::to_string(params.stride)
                         + "_proc_" + std::to_string(proc) + ".dat";
    std::ifstream infile(filename);
    if (infile.is_open()) {
      infile >> dbgentobsv;
      infile.close();
    }
  }

  std::string filename = ppackname + "_packobsvcorr_nSM_" + std::to_string(params.nSM * num_files)
                       + "_stride_" + std::to_string(params.stride) + ".dat";
  std::ofstream outfile(filename, std::ios::app);
  outfile << std::scientific;
  appparams(outfile);
  for (int iobs = 0; iobs < static_cast<int>(gentobsv::all); iobs++) {
    outfile << dbgentobsv.autocorrelation(iobs).tau(params.uncorrpts) << "\t";
  }
  outfile << std::endl;
  return;
}

} // namespace znspin

#endif // ZNSPIN_UTILS_OBSVFUNC_H
