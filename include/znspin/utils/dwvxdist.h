#ifndef ZNSPIN_UTILS_DWVXDIST_H
#define ZNSPIN_UTILS_DWVXDIST_H

#include <iostream>
#include <fstream>
#include <array>
#include <vector>
#include <string>
#include <qtools/rng.h>
#include <qtools/statistics.h>
#include <znspin/model.h>
#include <znspin/update.h>
#include <znspin/utils/parameters.h>
#include <znspin/utils/mixupdate.h>

namespace znspin {

template <std::size_t           NOld,
          qtools::lattice::name LatNm,
          external_field        ExtFld,
          vortex_suppression    VxSupp,
          vortex_suppression    Z3VxSupp,
          std::size_t           N>
void
fill_dwvxdist(const parameters& params,
              const hamiltonian<NOld, LatNm, ExtFld, VxSupp, Z3VxSupp>& H,
              spinconfig<N,LatNm>& s,
              std::vector<long long int>& dw,
              std::vector<long long int>& vx,
              std::vector<long long int>& z3vx,
              std::vector<long long int>& dw01,
              std::vector<long long int>& dw02,
              std::vector<long long int>& dw12,
              std::vector<long long int>& dw03,
              std::vector<long long int>& dwl0p1,
              std::vector<long long int>& dwl0m1,
              std::vector<long long int>& dwp1m1,
              std::vector<long long int>& dwl0p2)
{
  const bool GentDW = (N > 1);
  const bool GentVX = ((N > 2) && (qtools::lattice::info<LatNm>::dimension == 3));
  const auto sfrac  = state_fraction(s);
  const auto srank  = largest_states(sfrac);
  const auto defdw  = [](const int si, const int sj, const int b)
    { return (si != sj); };
  const auto defdw01 = [srank](const int si, const int sj, const int b)
    { return ((si == srank[0]) && (sj == srank[1])) ||
             ((si == srank[1]) && (sj == srank[0])); };
  const auto defdw02 = [srank](const int si, const int sj, const int b)
    { return ((si == srank[0]) && (sj == srank[2])) ||
             ((si == srank[2]) && (sj == srank[0])); };
  const auto defdw12 = [srank](const int si, const int sj, const int b)
    { return ((si == srank[1]) && (sj == srank[2])) ||
             ((si == srank[2]) && (sj == srank[1])); };
  const auto defdw03 = [srank](const int si, const int sj, const int b)
    { return ((si == srank[0]) && (sj == srank[3])) ||
             ((si == srank[3]) && (sj == srank[0])); };

  const int tp1 = ((srank[0] + 1 + static_cast<int>(N)) % static_cast<int>(N));
  const int tm1 = ((srank[0] - 1 + static_cast<int>(N)) % static_cast<int>(N));
  const int tp2 = ((srank[0] + 2 + static_cast<int>(N)) % static_cast<int>(N));
  const int tm2 = ((srank[0] - 2 + static_cast<int>(N)) % static_cast<int>(N));
  const auto sl0 = srank[0];
  const auto sp1 = (sfrac[tp1] >  sfrac[tm1] ? tp1 : tm1);
  const auto sm1 = (sfrac[tp1] <= sfrac[tm1] ? tp1 : tm1);
  const auto sp2 = (sfrac[tp2] >  sfrac[tm2] ? tp2 : tm2);
  const auto defdwl0p1 = [sl0,sp1](const int si, const int sj, const int b)
    { return ((si == sl0) && (sj == sp1)) ||
             ((si == sp1) && (sj == sl0)); };
  const auto defdwl0m1 = [sl0,sm1](const int si, const int sj, const int b)
    { return ((si == sl0) && (sj == sm1)) ||
             ((si == sm1) && (sj == sl0)); };
  const auto defdwp1m1 = [sp1,sm1](const int si, const int sj, const int b)
    { return ((si == sp1) && (sj == sm1)) ||
             ((si == sm1) && (sj == sp1)); };
  const auto defdwl0p2 = [sl0,sp2](const int si, const int sj, const int b)
    { return ((si == sl0) && (sj == sp2)) ||
             ((si == sp2) && (sj == sl0)); };
  if (GentDW && params.obsdwpercolation) {
    s.template label_domain_walls<false,false,false,false>(defdw);
    s.bin_all_domain_wall_sizes_to_distribution(dw);
    const auto tmp = s.largest_domain_wall_size();
    if (tmp > 1) dw[tmp] -= 1;
  }
  if (GentVX && params.obsvxpercolation) {
    if (params.wrongwindingnumber) {
      s.template label_vortex_strings<false,false,false,false>();
    } else {
      s.template label_wrong_vortex_strings<false,false,false,false>();
    }
    s.bin_all_vortex_string_sizes_to_distribution(vx);
    const auto tmp = s.largest_vortex_string_size();
    if (tmp > 1) vx[tmp] -= 1;
  }
  if (GentVX && params.obsz3vxpercolation) {
    s.template label_z3vortex_strings<false,false,false,false>();
    s.bin_all_vortex_string_sizes_to_distribution(z3vx);
    const auto tmp = s.largest_vortex_string_size();
    if (tmp > 1) z3vx[tmp] -= 1;
  }

  if (params.obsdw01dw02 && params.obsdwpercolation) {
    s.template label_domain_walls<false,false,false,false>(defdw01);
    s.bin_all_domain_wall_sizes_to_distribution(dw01);
    const auto tmp01 = s.largest_domain_wall_size();
    if (tmp01 > 1) dw01[tmp01] -= 1;
    s.template label_domain_walls<false,false,false,false>(defdw02);
    s.bin_all_domain_wall_sizes_to_distribution(dw02);
    const auto tmp02 = s.largest_domain_wall_size();
    if (tmp02 > 1) dw02[tmp02] -= 1;
  }
  if (params.obsdw12dw03 && params.obsdwpercolation) {
    s.template label_domain_walls<false,false,false,false>(defdw12);
    s.bin_all_domain_wall_sizes_to_distribution(dw12);
    const auto tmp12 = s.largest_domain_wall_size();
    if (tmp12 > 1) dw12[tmp12] -= 1;
    s.template label_domain_walls<false,false,false,false>(defdw03);
    s.bin_all_domain_wall_sizes_to_distribution(dw03);
    const auto tmp03 = s.largest_domain_wall_size();
    if (tmp03 > 1) dw03[tmp03] -= 1;
  }
  if (params.obsdwl0p1dwl0m1 && params.obsdwpercolation) {
    s.template label_domain_walls<false,false,false,false>(defdwl0p1);
    s.bin_all_domain_wall_sizes_to_distribution(dwl0p1);
    const auto tmpl0p1 = s.largest_domain_wall_size();
    if (tmpl0p1 > 1) dwl0p1[tmpl0p1] -= 1;
    s.template label_domain_walls<false,false,false,false>(defdwl0m1);
    s.bin_all_domain_wall_sizes_to_distribution(dwl0m1);
    const auto tmpl0m1 = s.largest_domain_wall_size();
    if (tmpl0m1 > 1) dwl0m1[tmpl0m1] -= 1;
  }
  if (params.obsdwp1m1dwl0p2 && params.obsdwpercolation) {
    s.template label_domain_walls<false,false,false,false>(defdwp1m1);
    s.bin_all_domain_wall_sizes_to_distribution(dwp1m1);
    const auto tmpp1m1 = s.largest_domain_wall_size();
    if (tmpp1m1 > 1) dwp1m1[tmpp1m1] -= 1;
    s.template label_domain_walls<false,false,false,false>(defdwl0p2);
    s.bin_all_domain_wall_sizes_to_distribution(dwl0p2);
    const auto tmpl0p2 = s.largest_domain_wall_size();
    if (tmpl0p2 > 1) dwl0p2[tmpl0p2] -= 1;
  }
  return;
}

template <std::size_t           N,
          qtools::lattice::name LatNm,
          external_field        ExtFld,
          vortex_suppression    VxSupp,
          vortex_suppression    Z3VxSupp>
void gentdwvxdist(const parameters& params,
                  const hamiltonian<N, LatNm, ExtFld, VxSupp, Z3VxSupp>& H,
                  const std::array<int,qtools::lattice::info<LatNm>::dimension>& length,
                  const std::string ppointname)
{
  const bool GentDW = (N > 1);
  const bool GentVX = ((N > 2) && (qtools::lattice::info<LatNm>::dimension == 3));
  auto rng  = qtools::rng_vector(1, params.procseed);
  auto s    = spinconfig<N,LatNm>(length);
  auto dw   = std::vector<long long int>((params.obsdwpercolation ? s.num_edges() + 1 : 0), 0);
  auto vx   = std::vector<long long int>((params.obsvxpercolation ? s.num_edges() + 1 : 0), 0);
  auto z3vx = std::vector<long long int>((params.obsz3vxpercolation ? s.num_edges() + 1 : 0), 0);

  auto dw01  = std::vector<long long int>((params.obsdw01dw02 ? s.num_edges()+1 : 0),0);
  auto dw02  = std::vector<long long int>((params.obsdw01dw02 ? s.num_edges()+1 : 0),0);
  auto dw12  = std::vector<long long int>((params.obsdw12dw03 ? s.num_edges()+1 : 0),0);
  auto dw03  = std::vector<long long int>((params.obsdw12dw03 ? s.num_edges()+1 : 0),0);

  auto dwl0p1  = std::vector<long long int>((params.obsdwl0p1dwl0m1 ? s.num_edges()+1 : 0),0);
  auto dwl0m1  = std::vector<long long int>((params.obsdwl0p1dwl0m1 ? s.num_edges()+1 : 0),0);
  auto dwp1m1  = std::vector<long long int>((params.obsdwp1m1dwl0p2 ? s.num_edges()+1 : 0),0);
  auto dwl0p2  = std::vector<long long int>((params.obsdwp1m1dwl0p2 ? s.num_edges()+1 : 0),0);

  s.fill(0);
  if (params.initrandom) {
    for (int i = 0; i < s.num_vertices(); i++) {
      s[i] = qtools::rng_uniform(rng[0]) * N;
    }
  } else if (params.inittwostaterandom) {
    for (int i = 0; i < s.num_vertices(); i++) {
      s[i] = qtools::rng_uniform(rng[0]) * 2;
    }
  } else if (params.initcheckerboard) {
    s.initialize_checkerboard();
  } else if (params.initvxavxlattice) {
    s.initialize_vxavxlattice();
  }
  for (int t = 0; t < params.nEQ; t++) {
    znspin::mixupdate(params, H, s, rng);
  }

  const int NEff = (N&1) ? 2*N : N;
  if (params.sublatticestate) {
    auto snew = spinconfig<NEff,LatNm>(s.length());
    for (int t = 0; t < params.nSM; t++) {
      for (int i = 0; i < params.stride; i++) {
        znspin::mixupdate(params, H, s, rng);
      }
      s.copy_sublattice_states_to(snew);
      fill_dwvxdist(params, H, snew, dw, vx, z3vx, dw01, dw02, dw12, dw03, dwl0p1, dwl0m1, dwp1m1, dwl0p2);
    }
  } else {
    for (int t = 0; t < params.nSM; t++) {
      for (int i = 0; i < params.stride; i++) {
        znspin::mixupdate(params, H, s, rng);
      }
      fill_dwvxdist(params, H, s, dw, vx, z3vx, dw01, dw02, dw12, dw03, dwl0p1, dwl0m1, dwp1m1, dwl0p2);
    }
  }

  if (GentDW && params.obsdwpercolation) {
    std::string filename = ppointname + "_gentdwvxdist_nSM_" + std::to_string(params.nSM)
                         + "_stride_" + std::to_string(params.stride)
                         + "_proc_" + std::to_string(params.proc) + "_dw.dat";
    std::ofstream outfile(filename);
    for (std::size_t i = 0; i < dw.size(); i++) {
      outfile << i << "\t" << dw[i] << std::endl;
    }
  }
  if (GentVX && params.obsvxpercolation) {
    std::string filename = ppointname + "_gentdwvxdist_nSM_" + std::to_string(params.nSM)
                         + "_stride_" + std::to_string(params.stride)
                         + "_proc_" + std::to_string(params.proc) + "_vx.dat";
    std::ofstream outfile(filename);
    for (std::size_t i = 0; i < vx.size(); i++) {
      outfile << i << "\t" << vx[i] << std::endl;
    }
  }
  if (GentVX && params.obsz3vxpercolation) {
    std::string filename = ppointname + "_gentdwvxdist_nSM_" + std::to_string(params.nSM)
                         + "_stride_" + std::to_string(params.stride)
                         + "_proc_" + std::to_string(params.proc) + "_z3vx.dat";
    std::ofstream outfile(filename);
    for (std::size_t i = 0; i < z3vx.size(); i++) {
      outfile << i << "\t" << z3vx[i] << std::endl;
    }
  }
  if (params.obsdw01dw02 && params.obsdwpercolation) {
    std::string filename = ppointname + "_gentdwvxdist_nSM_" + std::to_string(params.nSM)
                         + "_stride_" + std::to_string(params.stride)
                         + "_proc_" + std::to_string(params.proc) + "_dw01.dat";
    std::ofstream outfile(filename);
    for (std::size_t i = 0; i < dw01.size(); i++) {
      outfile << i << "\t" << dw01[i] << std::endl;
    }
  }
  if (params.obsdw01dw02 && params.obsdwpercolation) {
    std::string filename = ppointname + "_gentdwvxdist_nSM_" + std::to_string(params.nSM)
                         + "_stride_" + std::to_string(params.stride)
                         + "_proc_" + std::to_string(params.proc) + "_dw02.dat";
    std::ofstream outfile(filename);
    for (std::size_t i = 0; i < dw02.size(); i++) {
      outfile << i << "\t" << dw02[i] << std::endl;
    }
  }
  if (params.obsdw12dw03 && params.obsdwpercolation) {
    std::string filename = ppointname + "_gentdwvxdist_nSM_" + std::to_string(params.nSM)
                         + "_stride_" + std::to_string(params.stride)
                         + "_proc_" + std::to_string(params.proc) + "_dw12.dat";
    std::ofstream outfile(filename);
    for (std::size_t i = 0; i < dw12.size(); i++) {
      outfile << i << "\t" << dw12[i] << std::endl;
    }
  }
  if (params.obsdwl0p1dwl0m1 && params.obsdwpercolation) {
    std::string filename = ppointname + "_gentdwvxdist_nSM_" + std::to_string(params.nSM)
                         + "_stride_" + std::to_string(params.stride)
                         + "_proc_" + std::to_string(params.proc) + "_dwl0p1.dat";
    std::ofstream outfile(filename);
    for (std::size_t i = 0; i < dwl0p1.size(); i++) {
      outfile << i << "\t" << dwl0p1[i] << std::endl;
    }
  }
  if (params.obsdwl0p1dwl0m1 && params.obsdwpercolation) {
    std::string filename = ppointname + "_gentdwvxdist_nSM_" + std::to_string(params.nSM)
                         + "_stride_" + std::to_string(params.stride)
                         + "_proc_" + std::to_string(params.proc) + "_dwl0m1.dat";
    std::ofstream outfile(filename);
    for (std::size_t i = 0; i < dwl0m1.size(); i++) {
      outfile << i << "\t" << dwl0m1[i] << std::endl;
    }
  }
  if (params.obsdwp1m1dwl0p2 && params.obsdwpercolation) {
    std::string filename = ppointname + "_gentdwvxdist_nSM_" + std::to_string(params.nSM)
                         + "_stride_" + std::to_string(params.stride)
                         + "_proc_" + std::to_string(params.proc) + "_dwp1m1.dat";
    std::ofstream outfile(filename);
    for (std::size_t i = 0; i < dwp1m1.size(); i++) {
      outfile << i << "\t" << dwp1m1[i] << std::endl;
    }
  }
  if (params.obsdwp1m1dwl0p2 && params.obsdwpercolation) {
    std::string filename = ppointname + "_gentdwvxdist_nSM_" + std::to_string(params.nSM)
                         + "_stride_" + std::to_string(params.stride)
                         + "_proc_" + std::to_string(params.proc) + "_dwl0p2.dat";
    std::ofstream outfile(filename);
    for (std::size_t i = 0; i < dwl0p2.size(); i++) {
      outfile << i << "\t" << dwl0p2[i] << std::endl;
    }
  }
  return;
}

template <std::size_t         N,
          qtools::lattice::name LatNm,
          external_field      ExtFld,
          vortex_suppression  VxSupp,
          vortex_suppression  Z3VxSupp>
void avgpdwvxdist(const parameters& params,
                  const hamiltonian<N, LatNm, ExtFld, VxSupp, Z3VxSupp>& H,
                  const std::array<int,qtools::lattice::info<LatNm>::dimension>& length,
                  const std::string ppointname)
{
  std::vector< std::vector<long long int> > dw(qtools::lattice::info<LatNm>::num_edges(length));
  std::vector< std::vector<long long int> > vx(qtools::lattice::info<LatNm>::num_edges(length));
  for (int proc = params.minproc; proc <= params.maxproc; proc += params.incproc) {
    std::string filename = ppointname + "_gentdwvxdist_nSM_" + std::to_string(params.nSM)
                         + "_stride_" + std::to_string(params.stride)
                         + "_proc_" + std::to_string(proc) + ".dat";
    std::ifstream infile(filename);
    if (infile.is_open()) {
      long long int dist, dwsizecount, vxsizecount;
      int count = 0;
      while (infile >> dist >> dwsizecount >> vxsizecount) {
        dw[count].push_back(dwsizecount);
        vx[count].push_back(vxsizecount);
        assert(dist == count);
        count++;
      }
      infile.close();
      assert(count == (qtools::lattice::info<LatNm>::num_edges(length) + 1));
    }
  }
  if (dw[0].size() > 0) {
    std::string filename = ppointname + "_avgpdwvxdist_nSM_" + std::to_string(params.nSM * dw[0].size())
                         + "_stride_" + std::to_string(params.stride) + ".dat";
    std::ofstream outfile(filename);
    for (int i = 0; i < (int)dw.size(); i++) {
      outfile << std::scientific;
      outfile << i << "\t";
      outfile << qtools::jackknife_mean_error(dw[i]) << "\t";
      outfile << qtools::jackknife_mean_error(vx[i]) << "\t";
      outfile << std::endl;
    }
  }
  return;
}

template <std::size_t         N,
          qtools::lattice::name LatNm,
          external_field      ExtFld,
          vortex_suppression  VxSupp,
          vortex_suppression  Z3VxSupp,
          typename            AppparamsFn>
void packdwvxdist_main(const parameters& params,
                  const hamiltonian<N, LatNm, ExtFld, VxSupp, Z3VxSupp>& H,
                  const std::array<int,qtools::lattice::info<LatNm>::dimension>& length,
                  const std::string ppointname,
                  const std::string ppackname,
                  const AppparamsFn& appparams,
                  std::string suffix)
{
  {
    std::vector<double> avgsize;
    for (int proc = params.minproc; proc <= params.maxproc; proc += params.incproc) {
      std::string filename = ppointname + "_gentdwvxdist_nSM_" + std::to_string(params.nSM)
                           + "_stride_" + std::to_string(params.stride)
                           + "_proc_" + std::to_string(proc) + "_" + suffix + ".dat";
      std::ifstream infile(filename);
      if (infile.is_open()) {
        std::vector<long long int> sizehist(qtools::lattice::info<LatNm>::num_edges(length) + 1,0);
        int size;
        long long int count;
        while (infile >> size >> count) {
          sizehist[size] = count;
        }
        infile.close();
        avgsize.push_back(qtools::average_component_size(sizehist));
      }
    }
    if (avgsize.size() > 0) {
      std::string filename = ppackname + "_packdwvxdist_nSM_" + std::to_string(params.nSM)
                           + "_stride_" + std::to_string(params.stride) + "_" + suffix + ".dat";
      std::ofstream outfile(filename, std::ios::app);
      outfile << std::scientific;
      appparams(outfile);
      outfile << qtools::jackknife_mean_error(avgsize) << "\t";
      outfile << std::endl;
    }
  }
  return;
}

template <std::size_t         N,
          qtools::lattice::name LatNm,
          external_field      ExtFld,
          vortex_suppression  VxSupp,
          vortex_suppression  Z3VxSupp,
          typename            AppparamsFn>
void packdwvxdist(const parameters& params,
                  const hamiltonian<N, LatNm, ExtFld, VxSupp, Z3VxSupp>& H,
                  const std::array<int,qtools::lattice::info<LatNm>::dimension>& length,
                  const std::string ppointname,
                  const std::string ppackname,
                  const AppparamsFn& appparams)
{
  if (params.obsdwpercolation) {
    packdwvxdist_main(params, H, length, ppointname, ppackname, appparams, "dw");
  }
  if (params.obsvxpercolation) {
    packdwvxdist_main(params, H, length, ppointname, ppackname, appparams, "vx");
  }
  if (params.obsz3vxpercolation) {
    packdwvxdist_main(params, H, length, ppointname, ppackname, appparams, "z3vx");
  }
  if (params.obsdw01dw02 && params.obsdwpercolation) {
    packdwvxdist_main(params, H, length, ppointname, ppackname, appparams, "dw01");
    packdwvxdist_main(params, H, length, ppointname, ppackname, appparams, "dw02");
  }
  if (params.obsdw12dw03 && params.obsdwpercolation) {
    packdwvxdist_main(params, H, length, ppointname, ppackname, appparams, "dw12");
    packdwvxdist_main(params, H, length, ppointname, ppackname, appparams, "dw03");
  }
  if (params.obsdwl0p1dwl0m1 && params.obsdwpercolation) {
    packdwvxdist_main(params, H, length, ppointname, ppackname, appparams, "dwl0p1");
    packdwvxdist_main(params, H, length, ppointname, ppackname, appparams, "dwl0m1");
  }
  if (params.obsdwp1m1dwl0p2 && params.obsdwpercolation) {
    packdwvxdist_main(params, H, length, ppointname, ppackname, appparams, "dwp1m1");
    packdwvxdist_main(params, H, length, ppointname, ppackname, appparams, "dwl0p2");
  }
  return;
}

} // namespace znspin

#endif // ZNSPIN_UTILS_DWVXDIST_H
