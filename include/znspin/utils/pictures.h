#ifndef ZNSPIN_UTILS_PICTURES_H
#define ZNSPIN_UTILS_PICTURES_H

#include <iostream>
#include <fstream>
#include <array>
#include <string>
#include <qtools/rng.h>
#include <znspin/model.h>
#include <znspin/update.h>
#include <znspin/utils/parameters.h>
#include <znspin/utils/mixupdate.h>

namespace znspin {

template <std::size_t           NOld,
          qtools::lattice::name LatNm,
          external_field        ExtFld,
          vortex_suppression    VxSupp,
          vortex_suppression    Z3VxSupp,
          std::size_t           N>
void
gentpictures_main(const parameters& params,
                  const hamiltonian<NOld, LatNm, ExtFld, VxSupp, Z3VxSupp>& H,
                  spinconfig<N,LatNm>& s,
                  const std::string ppointname,
                  const int t,
                  const std::string suffix)
{
  const auto sfrac  = state_fraction(s);
  if (N > 1) {
    std::ofstream outfile(ppointname + "_gentpictures_spins_" + std::to_string(t) + suffix + ".dat");
    outfile << s.spins();
  }
  if (N > 1) {
    const auto defdw = [](const int si, const int sj, const int bond)
                       { return (si != sj); };

    const auto srank = largest_states(sfrac);
    const auto defdw01 = [srank](const int si, const int sj, const int b)
      { return ((si == srank[0]) && (sj == srank[1])) ||
               ((si == srank[1]) && (sj == srank[0])); };
    const auto defdw02 = [srank](const int si, const int sj, const int b)
      { return ((si == srank[0]) && (sj == srank[2])) ||
               ((si == srank[2]) && (sj == srank[0])); };
    const auto defdw12 = [srank](const int si, const int sj, const int b)
      { return ((si == srank[1]) && (sj == srank[2])) ||
               ((si == srank[2]) && (sj == srank[1])); };
    const auto defdw03 = [srank](const int si, const int sj, const int b)
      { return ((si == srank[0]) && (sj == srank[3])) ||
               ((si == srank[3]) && (sj == srank[0])); };

    const auto defdw0 = [srank](const int si, const int sj, const int b)
      { return (std::abs(state_difference<N>(si-sj)) == 0); };
    const auto defdw1 = [srank](const int si, const int sj, const int b)
      { return (std::abs(state_difference<N>(si-sj)) == 1); };
    const auto defdw2 = [srank](const int si, const int sj, const int b)
      { return (std::abs(state_difference<N>(si-sj)) == 2); };
    const auto defdw3 = [srank](const int si, const int sj, const int b)
      { return (std::abs(state_difference<N>(si-sj)) == 3); };

    const int tp1 = ((srank[0] + 1 + static_cast<int>(N)) % static_cast<int>(N));
    const int tm1 = ((srank[0] - 1 + static_cast<int>(N)) % static_cast<int>(N));
    const int tp2 = ((srank[0] + 2 + static_cast<int>(N)) % static_cast<int>(N));
    const int tm2 = ((srank[0] - 2 + static_cast<int>(N)) % static_cast<int>(N));
    const auto sl0 = srank[0];
    const auto sp1 = (sfrac[tp1] >  sfrac[tm1] ? tp1 : tm1);
    const auto sm1 = (sfrac[tp1] <= sfrac[tm1] ? tp1 : tm1);
    const auto sp2 = (sfrac[tp2] >  sfrac[tm2] ? tp2 : tm2);
    const auto defdwl0p1 = [sl0,sp1](const int si, const int sj, const int b)
      { return ((si == sl0) && (sj == sp1)) ||
               ((si == sp1) && (sj == sl0)); };
    const auto defdwl0m1 = [sl0,sm1](const int si, const int sj, const int b)
      { return ((si == sl0) && (sj == sm1)) ||
               ((si == sm1) && (sj == sl0)); };
    const auto defdwp1m1 = [sp1,sm1](const int si, const int sj, const int b)
      { return ((si == sp1) && (sj == sm1)) ||
               ((si == sm1) && (sj == sp1)); };
    const auto defdwl0p2 = [sl0,sp2](const int si, const int sj, const int b)
      { return ((si == sl0) && (sj == sp2)) ||
               ((si == sp2) && (sj == sl0)); };
    s.template label_domain_walls<false,false,false>(defdw);
    std::ofstream outfile(ppointname + "_gentpictures_dw_" + std::to_string(t) + suffix + ".dat");
    outfile << s.domain_walls(defdw);

    if (params.obsdw0) {
      s.template label_domain_walls<false,false,false>(defdw0);
      std::ofstream outfile(ppointname + "_gentpictures_dw0_" + std::to_string(t) + suffix + ".dat");
      outfile << s.domain_walls(defdw0);
    }
    if (params.obsdw1) {
      s.template label_domain_walls<false,false,false>(defdw1);
      std::ofstream outfile(ppointname + "_gentpictures_dw1_" + std::to_string(t) + suffix + ".dat");
      outfile << s.domain_walls(defdw1);
    }
    if (params.obsdw2) {
      s.template label_domain_walls<false,false,false>(defdw2);
      std::ofstream outfile(ppointname + "_gentpictures_dw2_" + std::to_string(t) + suffix + ".dat");
      outfile << s.domain_walls(defdw2);
    }
    if (params.obsdw3) {
      s.template label_domain_walls<false,false,false>(defdw3);
      std::ofstream outfile(ppointname + "_gentpictures_dw3_" + std::to_string(t) + suffix + ".dat");
      outfile << s.domain_walls(defdw3);
    }

    if (params.obsdw01dw02) {
      s.template label_domain_walls<false,false,false>(defdw01);
      std::ofstream outfile01(ppointname + "_gentpictures_dw01_" + std::to_string(t) + suffix + ".dat");
      outfile01 << s.domain_walls(defdw01);
      s.template label_domain_walls<false,false,false>(defdw02);
      std::ofstream outfile02(ppointname + "_gentpictures_dw02_" + std::to_string(t) + suffix + ".dat");
      outfile02 << s.domain_walls(defdw02);
    }

    if (params.obsdw12dw03) {
      s.template label_domain_walls<false,false,false>(defdw12);
      std::ofstream outfile12(ppointname + "_gentpictures_dw12_" + std::to_string(t) + suffix + ".dat");
      outfile12 << s.domain_walls(defdw12);
      s.template label_domain_walls<false,false,false>(defdw03);
      std::ofstream outfile03(ppointname + "_gentpictures_dw03_" + std::to_string(t) + suffix + ".dat");
      outfile03 << s.domain_walls(defdw03);
    }
    if (params.obsdwl0p1dwl0m1) {
      s.template label_domain_walls<false,false,false>(defdwl0p1);
      std::ofstream outfile01(ppointname + "_gentpictures_dwl0p1_" + std::to_string(t) + suffix + ".dat");
      outfile01 << s.domain_walls(defdwl0p1);
      s.template label_domain_walls<false,false,false>(defdwl0m1);
      std::ofstream outfile02(ppointname + "_gentpictures_dwl0m1_" + std::to_string(t) + suffix + ".dat");
      outfile02 << s.domain_walls(defdwl0m1);
    }
    if (params.obsdwp1m1dwl0p2) {
      s.template label_domain_walls<false,false,false>(defdwp1m1);
      std::ofstream outfile01(ppointname + "_gentpictures_dwp1m1_" + std::to_string(t) + suffix + ".dat");
      outfile01 << s.domain_walls(defdwp1m1);
      s.template label_domain_walls<false,false,false>(defdwl0p2);
      std::ofstream outfile02(ppointname + "_gentpictures_dwl0p2_" + std::to_string(t) + suffix + ".dat");
      outfile02 << s.domain_walls(defdwl0p2);
    }

  }
  if (N > 2) { 
    if (qtools::lattice::info<LatNm>::dimension == 2) {
      {
        std::ofstream outfile(ppointname + "_gentpictures_vx_" + std::to_string(t) + suffix + ".dat");
        if (params.wrongwindingnumber) {  
          outfile << s.template wrong_vortices();
        } else {
          outfile << s.template vortices();
        }
      }
      if (params.effectivez3state) {
        std::ofstream outfile(ppointname + "_gentpictures_vx_" + std::to_string(t) + suffix + "_z3.dat");
        outfile << s.template z3vortices(); 
      }
    } else if (qtools::lattice::info<LatNm>::dimension == 3) {
      {
        if (params.wrongwindingnumber) {
          s.template label_vortex_strings<false,false,false>();
          std::ofstream outfile(ppointname + "_gentpictures_vx_" + std::to_string(t) + suffix + ".dat");
          outfile << s.template vortices();
        } else {
          s.template label_wrong_vortex_strings<false,false,false>();
          std::ofstream outfile(ppointname + "_gentpictures_vx_" + std::to_string(t) + suffix + ".dat");
          outfile << s.template wrong_vortices();
        }
      }
      if (params.effectivez3state) {
        s.template label_z3vortex_strings<false,false,false>();
        std::ofstream outfile(ppointname + "_gentpictures_vx_" + std::to_string(t) + suffix + "_z3.dat");
        outfile << s.template z3vortices();
      }
    }
  }
  return;
}

template <std::size_t         N,
          qtools::lattice::name LatNm,
          external_field      ExtFld,
          vortex_suppression  VxSupp,
          vortex_suppression  Z3VxSupp>
void gentpictures(const parameters& params,
                  const hamiltonian<N, LatNm, ExtFld, VxSupp, Z3VxSupp>& H,
                  const std::array<int,qtools::lattice::info<LatNm>::dimension>& length,
                  const std::string ppointname)
{
  const int NEff = (N&1) ? 2*N : N;
  auto rng = qtools::rng_vector(1, params.procseed);
  auto s   = spinconfig<N,LatNm>(length);

  s.fill(0);
  if (params.initrandom) {
    for (int i = 0; i < s.num_vertices(); i++) {
      s[i] = qtools::rng_uniform(rng[0]) * N;
    }
  } else if (params.inittwostaterandom) {
    for (int i = 0; i < s.num_vertices(); i++) {
      s[i] = qtools::rng_uniform(rng[0]) * 2;
    }
  } else if (params.initcheckerboard) {
    s.initialize_checkerboard();
  } else if (params.initvxavxlattice) {
    s.initialize_vxavxlattice();
  }
  for (int t = 0; t < params.nEQ; t++) {
    znspin::mixupdate(params, H, s, rng);
  }

  if (params.sublatticestate) {
    auto snew= spinconfig<NEff,LatNm>(length);
    for (int t = 0; t < params.stride; t++) {
      for (int i = 0; i < params.nSM; i++) {
        znspin::mixupdate(params, H, s, rng);
      }
      s.copy_sublattice_states_to(snew);
      gentpictures_main(params, H, s, ppointname, t, std::string(""));
      gentpictures_main(params, H, snew, ppointname, t, std::string("_sub"));
    }
  } else {
    for (int t = 0; t < params.stride; t++) {
      for (int i = 0; i < params.nSM; i++) {
        znspin::mixupdate(params, H, s, rng);
      }
      gentpictures_main(params, H, s, ppointname, t, std::string(""));
    }
  }
  return;
}

} // namespace znspin

#endif // ZNSPIN_UTILS_PICTURES_H
