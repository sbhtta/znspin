BeginPackage["ZnSpinGraphics`"];

ZnSpinGraphicsCombineBulk3DSlice2D::usage =
  "ZnSpinGraphicsCombineBulk3DSlice2D[bulk3D_,slice2D_,axis_,coord_]
   returns a given 2D graphics overlaid on a given 3D graphics at a
   coordinate along an axis."

Begin["`Private`"]
  ZnSpinGraphicsCombineBulk3DSlice2D[bulk3D_,slice2D_,axis_,coord_,opts:OptionsPattern[]]:=
    Module[{mx,my},
      frame = {}; boxframe = {}; texturecoords = {};
      If[axis == 1,
        ymin = (PlotRange /. slice[[2]])[[1]][[1]];
        ymax = (PlotRange /. slice[[2]])[[1]][[2]];
        zmin = (PlotRange /. slice[[2]])[[2]][[1]];
        zmax = (PlotRange /. slice[[2]])[[2]][[2]];
        frame = {{coord, ymin, zmin}, {coord, ymin, zmax}, {coord, ymax, zmax}, {coord, ymax, zmin}};
        boxframe = {{mincoord, maxcoord}, {ymin, ymax}, {zmin, zmax}};
        texturecoords = {{0, 0}, {0, 1}, {1, 1}, {1, 0}};
      ];
      If[axis == 2,
        xmin = (PlotRange /. slice[[2]])[[1]][[1]];
        xmax = (PlotRange /. slice[[2]])[[1]][[2]];
        zmin = (PlotRange /. slice[[2]])[[2]][[1]];
        zmax = (PlotRange /. slice[[2]])[[2]][[2]];
        frame = {{xmin, coord, zmin}, {ymax, coord, zmin}, {xmax, coord, zmax}, {ymin, coord, zmax}};
        boxframe = {{xmin, xmax}, {mincoord, maxcoord}, {zmin, zmax}};
        texturecoords = {{0, 0}, {1, 0}, {1, 1}, {0, 1}};
      ];
      If[axis == 3,
        xmin = (PlotRange /. slice[[2]])[[1]][[1]];
        xmax = (PlotRange /. slice[[2]])[[1]][[2]];
        ymin = (PlotRange /. slice[[2]])[[2]][[1]];
        ymax = (PlotRange /. slice[[2]])[[2]][[2]];
        frame = {{xmin, ymin, coord}, {xmin, ymax, coord}, {xmax, ymax, coord}, {xmax, ymin, coord}};
        boxframe = {{xmin, xmax}, {ymin, ymax}, {mincoord, maxcoord}};
        texturecoords = {{0, 0}, {0, 1}, {1, 1}, {1, 0}};
      ];
      slice2Din3D = Graphics3D[{EdgeForm[], Texture[slice2D],
                    Polygon[frame, VertexTextureCoordinates -> texturecoords]}, Boxed -> False];
      Graphics3D[{bulk3D[[1]], slice2Din3D[[1]]}, Evaluate[FilterRules[{opts}, Options[Graphics]]]]
    ];
End[]

ZnSpinGraphicsPotential::usage =
  "ZnSpinGraphicsPotential[n,a,b,hn,h2n] returns a 3D plot of the
   potential for a Zn continuum theory in the order parameter space."

Begin["`Private`"]
  ZnSpinGraphicsPotential[n_,a_,b_,hn_,h2n_,thickness_:0.01,z_:-0.9,xmin_:1.4,opts:OptionsPattern[]]:=
    Module[{mx,my},
      modm[mx_,my_] := Sqrt[mx^2+my^2];
      phi[mx_,my_]  := ArcTan[mx,my];
      f[mx_,my_]    := a*modm[mx,my]^2 + b*modm[mx,my]^4 + hn*Cos[n*phi[mx,my]] + h2n*Cos[2*n*phi[mx,my]];
      fp[m_,t_]     := a*m^2 + b*m^4 + hn*Cos[n t] + h2n*Cos[2 n t];
      fminval        = FindMinValue[{f[mx,my], mx^2+my^2<=1},{mx,my}];
      p1             = RegionPlot[f[mx,my] < (fminval+thickness) && mx^2+my^2 <= 1,
                        {mx,-1,1}, {my,-1,1},
                        BoundaryStyle -> None,
                        PlotStyle     -> Gray,
                        FrameStyle    -> Directive[Black,Thick,20],
                        FrameLabel    -> {Style[Subscript["m", "x"], FontFamily->"Times"],
                                          Style[Subscript["m", "y"], FontFamily->"Times"]}
                       ];
      p2             = RevolutionPlot3D[fp[m,t],
                        {m,0,1}, {t,0,2\[Pi]},
                        Axes -> False, Boxed -> False,
                        Evaluate[FilterRules[{opts}, Options[Graphics]]]
                       ];
      p3             = Graphics3D[{EdgeForm[], Texture[p1],
                         Polygon[{{-xmin,-xmin,z}, {1,-xmin,z}, {1,1,z}, {-xmin,1,z}},
                          VertexTextureCoordinates->{{0,0},{1,0},{1,1},{0,1}}
                         ]}
                       ];
      Graphics3D[{p2[[1]], p3[[1]]}, Lighting->"Neutral", BoxRatios->{1,1,-z}, Boxed->False,
                 Evaluate[FilterRules[{opts}, Options[Graphics]]]]
    ];
End[]

ZnSpinGraphicsSliceSpins3Dto2D::usage =
  "ZnSpinGraphicsSliceSpins3Dto2D[file,axis,coord] reads the configuration of
   spins on a three dimensional grid from a given file and returns a two dimensional
   slice of spins on the face at the given coordinate along the given axis."

Begin["`Private`"]
  ZnSpinGraphicsSliceSpins3Dto2D[file_,axis_,coord_] :=
    Module[{list = {}, data = {}},
      data = Partition[Flatten[If[FileExistsQ[file],Import[file],{}]], 4];
      DeleteCases[Table[If[data[[i]][[axis]] == coord, Drop[data[[i]],{axis}], {}], {i, Length[data]}],{}]
    ];
End[]

ZnSpinGraphicsSliceDomainWalls3Dto2D::usage =
  "ZnSpinGraphicsSliceDomainWalls3Dto2D[file,axis,coord] reads the configuration of
   domain walls on a three dimensional grid from a given file and returns a two dimensional
   slice of domain walls on the face at the given coordinate along the given axis."

Begin["`Private`"]
  ZnSpinGraphicsSliceDomainWalls3Dto2D[file_,axis_,coord_] :=
    Module[{list = {}, data = {}},
      data = Partition[Flatten[If[FileExistsQ[file],Import[file],{}]], 13];
      WallCutsSlice[dwcorners_] := (Abs[(dwcorners[[1]][[axis]] - dwcorners[[2]][[axis]])] + 
                                    Abs[(dwcorners[[2]][[axis]] - dwcorners[[3]][[axis]])] + 
                                    Abs[(dwcorners[[3]][[axis]] - dwcorners[[4]][[axis]])] + 
                                    Abs[(dwcorners[[4]][[axis]] - dwcorners[[1]][[axis]])] != 0) &&
                                    ((dwcorners[[1]][[axis]] + dwcorners[[2]][[axis]] + 
                                      dwcorners[[3]][[axis]] + dwcorners[[4]][[axis]])/4 == coord);
      ProjectWall[dwcorners_, componentsize_] := If[dwcorners[[1]][[axis]] == dwcorners[[2]][[axis]], 
                                                  Flatten[{Drop[dwcorners[[1]], {axis}], Drop[dwcorners[[2]], {axis}], componentsize}], 
                                                  Flatten[{Drop[dwcorners[[1]], {axis}], Drop[dwcorners[[4]], {axis}], componentsize}]];
      DeleteCases[Table[If[WallCutsSlice[Partition[Most[data[[i]]],3]],
                           ProjectWall[Partition[Most[data[[i]]],3],data[[i]][[13]]],
                           {}], {i, Length[data]}],{}]
    ];
End[]

ZnSpinGraphicsSliceVortices3Dto2D::usage =
  "ZnSpinGraphicsSliceVortices3Dto2D[file,axis,coord] reads the configuration of
   vortex strings on a three dimensional grid from a given file and returns a two dimensional
   slice of vortices on the face at the given coordinate along the given axis."

Begin["`Private`"]
  ZnSpinGraphicsSliceVortices3Dto2D[file_,axis_,coord_] :=
    Module[{list = {}, data = {}},
      data = Partition[Flatten[If[FileExistsQ[file],Import[file],{}]], 8];
      StringCutsSlice[vxends_] := (Abs[(vxends[[1]][[axis]] - vxends[[2]][[axis]])] != 0) &&
                                  ((vxends[[1]][[axis]] + vxends[[2]][[axis]])/2 == coord);
      ProjectString[vxends_, wn_] := Flatten[{Drop[vxends[[1]], {axis}], wn}];
      DeleteCases[Table[If[StringCutsSlice[Partition[Drop[data[[i]],-2],3]],
                           ProjectString[Partition[Drop[data[[i]],-2],3],data[[i]][[7]]],
                           {}], {i, Length[data]}],{}]
    ];
End[]

ZnSpinGraphicsSpins::usage =
  "ZnSpinGraphicsSpins[file,n,[hue]] reads the configuration of
   n-state spins on a two dimensional grid from a given file and
   creates a Graphics object containing the spins. Hue of the spins can
   be changed by providing a custom hue, e.g. Hue[0.2]."

Begin["`Private`"]
  ZnSpinGraphicsSpins[file_,n_,hue0_:Hue[0],opts:OptionsPattern[]] :=
    Module[{s = If[Length[hue0] > 1, hue0[[2]], 1],
            b = If[Length[hue0] > 2, hue0[[3]], 1],
            a = If[Length[hue0] > 3, hue0[[4]], 1],
            list = {}, data = {}},
      data = Partition[Flatten[If[FileExistsQ[file],Import[file],{}]], 3];
      Geom[i_]      := Rectangle[Most[data[[i]]] - 0.5];
      SpinState[i_] := Last[data[[i]]];
      SpinHue[i_]   := Mod[If[Length[hue0] > 0, hue0[[1]], 0] + SpinState[i] / n, 1];
      list = DeleteCases[Table[{EdgeForm[None], Hue[SpinHue[i], s, b, a], Geom[i]}, {i, Length[data]}], {}];
      Graphics[list,Evaluate[FilterRules[{opts}, Options[Graphics]]]]
    ];
End[]

ZnSpinGraphicsSpinsCustomRGBs::usage =
  "ZnSpinGraphicsSpinsCustomRGBs[file,n,rgbs] reads the configuration of
   n-state spins on a two dimensional grid from a given file and
   creates a Graphics object containing the spins. Hue of the spins can
   be changed by providing a list of RGB colors, e.g. {{255,32,1},{128,0,0}}."

Begin["`Private`"]
  ZnSpinGraphicsSpinsCustomRGBs[file_,n_,rgbs_,opts:OptionsPattern[]] :=
    Module[{list = {}, data = {}},
      data = Partition[Flatten[If[FileExistsQ[file],Import[file],{}]], 3];
      Geom[i_]      := Rectangle[Most[data[[i]]] - 0.5];
      SpinState[i_] := Last[data[[i]]];
      SpinHue[i_]   := ColorConvert[RGBColor[rgbs[[SpinState[i]+1]][[1]]/255,rgbs[[SpinState[i]+1]][[2]]/255,rgbs[[SpinState[i]+1]][[3]]/255], "HSB"];
      list = DeleteCases[Table[{EdgeForm[None], SpinHue[i], Geom[i]}, {i, Length[data]}], {}];
      Graphics[list,Evaluate[FilterRules[{opts}, Options[Graphics]]]]
    ];
End[]

ZnSpinGraphicsSpins3D::usage =
  "ZnSpinGraphicsSpins3D[file,n,[hue]] reads the configuration of
   n-state spins on a three dimensional grid from a given file and
   creates a Graphics3D object containing the spins. Hue of the spins can
   be changed by providing a custom hue, e.g. Hue[0.2]."

Begin["`Private`"]
  ZnSpinGraphicsSpins3D[file_,n_,hue0_:Hue[0],opts:OptionsPattern[]] :=
    Module[{s = If[Length[hue0] > 1, hue0[[2]], 1],
            b = If[Length[hue0] > 2, hue0[[3]], 1],
            a = If[Length[hue0] > 3, hue0[[4]], 1],
            list = {}, data = {}},
      data = Partition[Flatten[If[FileExistsQ[file],Import[file],{}]], 4];
      Geom[i_]      := Cuboid[Most[data[[i]]] - 0.5];
      SpinState[i_] := Last[data[[i]]];
      SpinHue[i_]   := Mod[If[Length[hue0] > 0, hue0[[1]], 0] + SpinState[i] / n, 1];
      list = DeleteCases[Table[{EdgeForm[None], Hue[SpinHue[i], s, b, a], Geom[i]}, {i, Length[data]}], {}];
      Graphics3D[list,Evaluate[FilterRules[{opts}, Options[Graphics3D]]]]
    ];
End[]

ZnSpinGraphicsSpins3DCustomRGBs::usage =
  "ZnSpinGraphicsSpins3DCustomRGBs[file,n,rgbs] reads the configuration of
   n-state spins on a three dimensional grid from a given file and
   creates a Graphics3D object containing the spins. Hue of the spins can
   be changed by providing a list of rgb colors, e.g. {{255,128,0},{128,0,0}}."

Begin["`Private`"]
  ZnSpinGraphicsSpins3DCustomRGBs[file_,n_,rgbs_,opts:OptionsPattern[]] :=
    Module[{list = {}, data = {}},
      data = Partition[Flatten[If[FileExistsQ[file],Import[file],{}]], 4];
      Geom[i_]      := Cuboid[Most[data[[i]]] - 0.5];
      SpinState[i_] := Last[data[[i]]];
      SpinHue[i_]   := ColorConvert[RGBColor[rgbs[[SpinState[i]+1]][[1]]/255,rgbs[[SpinState[i]+1]][[2]]/255,rgbs[[SpinState[i]+1]][[3]]/255], "HSB"];
      list = DeleteCases[Table[{EdgeForm[None], SpinHue[i], Geom[i]}, {i, Length[data]}], {}];
      Graphics3D[list,Evaluate[FilterRules[{opts}, Options[Graphics3D]]]]
    ];
End[]

ZnSpinGraphicsDomainWalls::usage =
  "ZnSpinGraphicsDomainWalls[file,[all],[hue]] reads the configuration of
   domain walls on a two dimensional grid from a given file and creates a
   Graphics object containing all the walls. Only the largest wall can be
   chosen by setting the all argument to False. Hue of the walls can
   be changed by providing a custom hue, e.g. Hue[0.2]."

Begin["`Private`"]
  ZnSpinGraphicsDomainWalls[file_,all_:True,hue0_:Hue[0],opts:OptionsPattern[]] :=
    Module[{s = If[Length[hue0] > 1, hue0[[2]], 1],
            b = If[Length[hue0] > 2, hue0[[3]], 1],
            a = If[Length[hue0] > 3, hue0[[4]], 1],
            list = {}, data = {}, largestsize = 1},
      data = Partition[Flatten[If[FileExistsQ[file],Import[file],{}]], 5];
      For[i = 1, i <= Length[data], i++, If[Last[data[[i]]] > largestsize, largestsize = Last[data[[i]]];]];
      Geom[i_]          := Line[Partition[Most[data[[i]]], 2]];
      IsLargest[i_]     := (Last[data[[i]]] == largestsize);
      SizeFraction[i_]  := (Last[data[[i]]] / largestsize);
      IsPrintable[i_]   := ((all == True) || IsLargest[i]);
      ObjectHue[i_]     := If[Length[hue0] > 0, hue0[[1]], 0];
      list = DeleteCases[Table[If[IsPrintable[i], {EdgeForm[None], Hue[ObjectHue[i], s, b, a], Geom[i]}, {}], {i, Length[data]}], {}];
      Graphics[list,Evaluate[FilterRules[{opts}, Options[Graphics]]]]
    ];
End[]

ZnSpinGraphicsAllButLargestDomainWalls::usage =
  "ZnSpinGraphicsAllButLargestDomainWalls[file,[hue]] reads the configuration of
   domain walls on a two dimensional grid from a given file and creates a
   Graphics object containing all the walls except the largest one. Hue of the
   walls can be changed by providing a custom hue, e.g. Hue[0.2]."

Begin["`Private`"]
  ZnSpinGraphicsAllButLargestDomainWalls[file_,hue0_:Hue[0],opts:OptionsPattern[]] :=
    Module[{s = If[Length[hue0] > 1, hue0[[2]], 1],
            b = If[Length[hue0] > 2, hue0[[3]], 1],
            a = If[Length[hue0] > 3, hue0[[4]], 1],
            list = {}, data = {}, largestsize = 1},
      data = Partition[Flatten[If[FileExistsQ[file],Import[file],{}]], 5];
      For[i = 1, i <= Length[data], i++, If[Last[data[[i]]] > largestsize, largestsize = Last[data[[i]]];]];
      Geom[i_]          := Line[Partition[Most[data[[i]]], 2]];
      IsLargest[i_]     := (Last[data[[i]]] == largestsize);
      SizeFraction[i_]  := (Last[data[[i]]] / largestsize);
      IsPrintable[i_]   := (!IsLargest[i]);
      ObjectHue[i_]     := If[Length[hue0] > 0, hue0[[1]], 0];
      list = DeleteCases[Table[If[IsPrintable[i], {EdgeForm[None], Hue[ObjectHue[i], s, b, a], Geom[i]}, {}], {i, Length[data]}], {}];
      Graphics[list,Evaluate[FilterRules[{opts}, Options[Graphics]]]]
    ];
End[]

ZnSpinGraphicsLargestDomainWalls::usage =
  "ZnSpinGraphicsLargestDomainWalls[file,[hue]] reads the configuration of
   domain walls on a two dimensional grid from a given file and creates a
   Graphics object containing the largest wall. Hue of the wall can
   be changed by providing a custom hue, e.g. Hue[0.2]."

Begin["`Private`"]
  ZnSpinGraphicsLargestDomainWalls[file_,hue0_:Hue[0],opts:OptionsPattern[]] :=
    Module[{s = If[Length[hue0] > 1, hue0[[2]], 1],
            b = If[Length[hue0] > 2, hue0[[3]], 1],
            a = If[Length[hue0] > 3, hue0[[4]], 1],
            list = {}, data = {}, largestsize = 1},
      data = Partition[Flatten[If[FileExistsQ[file],Import[file],{}]], 5];
      For[i = 1, i <= Length[data], i++, If[Last[data[[i]]] > largestsize, largestsize = Last[data[[i]]];]];
      Geom[i_]          := Line[Partition[Most[data[[i]]], 2]];
      IsLargest[i_]     := (Last[data[[i]]] == largestsize);
      SizeFraction[i_]  := (Last[data[[i]]] / largestsize);
      IsPrintable[i_]   := (IsLargest[i]);
      ObjectHue[i_]     := If[Length[hue0] > 0, hue0[[1]], 0];
      list = DeleteCases[Table[If[IsPrintable[i], {EdgeForm[None], Hue[ObjectHue[i], s, b, a], Thick, Geom[i]}, {}], {i, Length[data]}], {}];
      Graphics[list,Evaluate[FilterRules[{opts}, Options[Graphics]]]]
    ];
End[]

ZnSpinGraphicsDomainWalls3D::usage =
  "ZnSpinGraphicsDomainWalls3D[file,[all],[hue]] reads the configuration of
   domain walls on a three dimensional grid from a given file and creates a
   Graphics object containing all the walls. Only the largest wall can be
   chosen by setting the all argument to False. Hue of the walls can
   be changed by providing a custom hue, e.g. Hue[0.2]."

Begin["`Private`"]
  ZnSpinGraphicsDomainWalls3D[file_,all_:True,hue0_:Hue[0],opts:OptionsPattern[]] :=
    Module[{s = If[Length[hue0] > 1, hue0[[2]], 1],
            b = If[Length[hue0] > 2, hue0[[3]], 1],
            a = If[Length[hue0] > 3, hue0[[4]], 1],
            list = {}, data = {}, largestsize = 1},
      data = Partition[Flatten[If[FileExistsQ[file],Import[file],{}]], 13];
      For[i = 1, i <= Length[data], i++, If[Last[data[[i]]] > largestsize, largestsize = Last[data[[i]]];]];
      Geom[i_]          := Polygon[Partition[Most[data[[i]]], 3]];
      IsLargest[i_]     := (Last[data[[i]]] == largestsize);
      SizeFraction[i_]  := (Last[data[[i]]] / largestsize);
      IsPrintable[i_]   := ((all == True) || IsLargest[i]);
      ObjectHue[i_]     := If[Length[hue0] > 0, hue0[[1]], 0];
      list = DeleteCases[Table[If[IsPrintable[i], {EdgeForm[None], Hue[ObjectHue[i], s, b, a], Geom[i]}, {}], {i, Length[data]}], {}];
      Graphics3D[list,Evaluate[FilterRules[{opts}, Options[Graphics3D]]]]
    ];
End[]

ZnSpinGraphicsAllButLargestDomainWalls3D::usage =
  "ZnSpinGraphicsAllButLargestDomainWalls3D[file,[hue]] reads the configuration of
   domain walls on a three dimensional grid from a given file and creates a
   Graphics object containing all the walls except the largest one. Hue of the walls can
   be changed by providing a custom hue, e.g. Hue[0.2]."

Begin["`Private`"]
  ZnSpinGraphicsAllButLargestDomainWalls3D[file_,hue0_:Hue[0],opts:OptionsPattern[]] :=
    Module[{s = If[Length[hue0] > 1, hue0[[2]], 1],
            b = If[Length[hue0] > 2, hue0[[3]], 1],
            a = If[Length[hue0] > 3, hue0[[4]], 1],
            list = {}, data = {}, largestsize = 1},
      data = Partition[Flatten[If[FileExistsQ[file],Import[file],{}]], 13];
      For[i = 1, i <= Length[data], i++, If[Last[data[[i]]] > largestsize, largestsize = Last[data[[i]]];]];
      Geom[i_]          := Polygon[Partition[Most[data[[i]]], 3]];
      IsLargest[i_]     := (Last[data[[i]]] == largestsize);
      SizeFraction[i_]  := (Last[data[[i]]] / largestsize);
      IsPrintable[i_]   := (!IsLargest[i]);
      ObjectHue[i_]     := If[Length[hue0] > 0, hue0[[1]], 0];
      list = DeleteCases[Table[If[IsPrintable[i], {EdgeForm[None], Hue[ObjectHue[i], s, b, a], Geom[i]}, {}], {i, Length[data]}], {}];
      Graphics3D[list,Evaluate[FilterRules[{opts}, Options[Graphics3D]]]]
    ];
End[]

ZnSpinGraphicsLargestDomainWalls3D::usage =
  "ZnSpinGraphicsLargestDomainWalls3D[file,[hue]] reads the configuration of
   domain walls on a three dimensional grid from a given file and creates a
   Graphics object containing the largest wall. Hue of the wall can
   be changed by providing a custom hue, e.g. Hue[0.2]."

Begin["`Private`"]
  ZnSpinGraphicsLargestDomainWalls3D[file_,hue0_:Hue[0],opts:OptionsPattern[]] :=
    Module[{s = If[Length[hue0] > 1, hue0[[2]], 1],
            b = If[Length[hue0] > 2, hue0[[3]], 1],
            a = If[Length[hue0] > 3, hue0[[4]], 1],
            list = {}, data = {}, largestsize = 1},
      data = Partition[Flatten[If[FileExistsQ[file],Import[file],{}]], 13];
      For[i = 1, i <= Length[data], i++, If[Last[data[[i]]] > largestsize, largestsize = Last[data[[i]]];]];
      Geom[i_]          := Polygon[Partition[Most[data[[i]]], 3]];
      IsLargest[i_]     := (Last[data[[i]]] == largestsize);
      SizeFraction[i_]  := (Last[data[[i]]] / largestsize);
      IsPrintable[i_]   := (IsLargest[i]);
      ObjectHue[i_]     := If[Length[hue0] > 0, hue0[[1]], 0];
      list = DeleteCases[Table[If[IsPrintable[i], {EdgeForm[None], Hue[ObjectHue[i], s, b, a], Geom[i]}, {}], {i, Length[data]}], {}];
      Graphics3D[list,Evaluate[FilterRules[{opts}, Options[Graphics3D]]]]
    ];
End[]


ZnSpinGraphicsVortices::usage =
  "ZnSpinGraphicsVortices[file,[hue],[hue1],[hue2],[r]] reads the configuration of
   vortices on a two dimensional grid from a given file and creates a
   Graphics object containing all the vortices. Hue of the vortices can
   be changed by providing a custom hue, e.g. Hue[0.2]. Antivortices can
   be provided a different hue by setting a suitable hue1. Radius of vortices
   and antivortices is given by r."

Begin["`Private`"]
  ZnSpinGraphicsVortices[file_,hue0_:Hue[0.5],hue1_:Hue[0],hue2_:Hue[0.3],r_:0.6,opts:OptionsPattern[]] :=
    Module[{s = If[Length[hue0] > 1, hue0[[2]], 1],
            b = If[Length[hue0] > 2, hue0[[3]], 1],
            a = If[Length[hue0] > 3, hue0[[4]], 1],
            list = {}, data = {}, largestsize = 1},
      data = Partition[Flatten[If[FileExistsQ[file],Import[file],{}]], 3];
      Geom[i_]          := Disk[Most[data[[i]]], r];
      IsAntiVortex[i_]  := (Last[data[[i]]] < 0);
      IsWn2Vortex[i_] := (Abs[Last[data[[i]]]] == 2);
      ObjectHue[i_]     := If[IsWn2Vortex[i], If[Length[hue2] > 0, hue2[[1]], 0.3],
                            If[IsAntiVortex[i], If[Length[hue1] > 0, hue1[[1]], 0.0], If[Length[hue0] > 0, hue0[[1]], 0.5]]];
      list = DeleteCases[Table[{EdgeForm[None], Hue[ObjectHue[i], s, b, a], Geom[i]}, {i, Length[data]}], {}];
      Graphics[list,Evaluate[FilterRules[{opts}, Options[Graphics]]]]
    ];
End[]

ZnSpinGraphicsVortices3D::usage =
  "ZnSpinGraphicsVortices3D[file,[all],[hue0],[hue1],[hue2],[r]] reads the configuration of
   vortex strings on a three dimensional grid from a given file and creates a
   Graphics object containing all the strings. Only the largest string can be
   chosen by setting the all argument to False. Hue of the strings can
   be changed by providing a custom hue, e.g. Hue[0.2]. String radius is given
   by r."

Begin["`Private`"]
  ZnSpinGraphicsVortices3D[file_,all_:True,hue0_:Hue[0.5],hue1_:Hue[0],hue2_:Hue[0.3],r_:0.1,opts:OptionsPattern[]] :=
    Module[{s = If[Length[hue0] > 1, hue0[[2]], 1],
            b = If[Length[hue0] > 2, hue0[[3]], 1],
            a = If[Length[hue0] > 3, hue0[[4]], 1],
            list = {}, data = {}, largestsize = 1},
      data = Partition[Flatten[If[FileExistsQ[file],Import[file],{}]], 8];
      For[i = 1, i <= Length[data], i++, If[Last[data[[i]]] > largestsize, largestsize = Last[data[[i]]];]];
      Geom[i_]          := Cylinder[Partition[Take[data[[i]],6], 3], r];
      IsLargest[i_]     := (Last[data[[i]]] == largestsize);
      SizeFraction[i_]  := (Last[data[[i]]] / largestsize);
      IsPrintable[i_]   := ((all == True) || IsLargest[i]);
      IsAntiVortex[i_]  := (data[[i]][[7]] < 0);
      IsWn2Vortex[i_] := (Abs[Last[data[[i]]]] == 2);
      ObjectHue[i_]     := If[IsWn2Vortex[i], If[Length[hue2] > 0, hue2[[1]], 0.3],
                            If[IsAntiVortex[i], If[Length[hue1] > 0, hue1[[1]], 0.0], If[Length[hue0] > 0, hue0[[1]], 0.5]]];
      list = DeleteCases[Table[If[IsPrintable[i], {EdgeForm[None], Hue[ObjectHue[i], s, b, a], Geom[i]}, {}], {i, Length[data]}], {}];
      Graphics3D[list,Evaluate[FilterRules[{opts}, Options[Graphics3D]]]]
    ];
End[]

ZnSpinGraphicsAllButLargestVortices3D::usage =
  "ZnSpinGraphicsAllButLargestVortices3D[file,[hue0],[hue1],[hue2],[r]] reads the configuration of
   vortex strings on a three dimensional grid from a given file and creates a
   Graphics object containing all strings except the largest one. 
   Hue of the strings can be changed by providing a custom hue, e.g. Hue[0.2]. String
   radius is given by r."

Begin["`Private`"]
  ZnSpinGraphicsAllButLargestVortices3D[file_,hue0_:Hue[0.5],hue1_:Hue[0],hue2_:Hue[0.3],r_:0.1,opts:OptionsPattern[]] :=
    Module[{s = If[Length[hue0] > 1, hue0[[2]], 1],
            b = If[Length[hue0] > 2, hue0[[3]], 1],
            a = If[Length[hue0] > 3, hue0[[4]], 1],
            list = {}, data = {}, largestsize = 1},
      data = Partition[Flatten[If[FileExistsQ[file],Import[file],{}]], 8];
      For[i = 1, i <= Length[data], i++, If[Last[data[[i]]] > largestsize, largestsize = Last[data[[i]]];]];
      Geom[i_]          := Cylinder[Partition[Take[data[[i]],6], 3], r];
      IsLargest[i_]     := (Last[data[[i]]] == largestsize);
      SizeFraction[i_]  := (Last[data[[i]]] / largestsize);
      IsPrintable[i_]   := (!IsLargest[i]);
      IsAntiVortex[i_]  := (data[[i]][[7]] < 0);
      IsWn2Vortex[i_] := (Abs[Last[data[[i]]]] == 2);
      ObjectHue[i_]     := If[IsWn2Vortex[i], If[Length[hue2] > 0, hue2[[1]], 0.3],
                            If[IsAntiVortex[i], If[Length[hue1] > 0, hue1[[1]], 0.0], If[Length[hue0] > 0, hue0[[1]], 0.5]]];
      list = DeleteCases[Table[If[IsPrintable[i], {EdgeForm[None], Hue[ObjectHue[i], s, b, a], Geom[i]}, {}], {i, Length[data]}], {}];
      Graphics3D[list,Evaluate[FilterRules[{opts}, Options[Graphics3D]]]]
    ];
End[]

ZnSpinGraphicsLargestVortices3D::usage =
  "ZnSpinGraphicsLargestVortices3D[file,[hue0],[hue1],[hue2],[r]] reads the configuration of
   vortex strings on a three dimensional grid from a given file and creates a
   Graphics object containing the largest string. Hue of the string can
   be changed by providing a custom hue, e.g. Hue[0.2]. String radius is given
   by r."

Begin["`Private`"]
  ZnSpinGraphicsLargestVortices3D[file_,hue0_:Hue[0.5],hue1_:Hue[0.0],hue2_:Hue[0.3],r_:0.1,opts:OptionsPattern[]] :=    
    Module[{s = If[Length[hue0] > 1, hue0[[2]], 1],
            b = If[Length[hue0] > 2, hue0[[3]], 1],
            a = If[Length[hue0] > 3, hue0[[4]], 1],
            list = {}, data = {}, largestsize = 1},
      data = Partition[Flatten[If[FileExistsQ[file],Import[file],{}]], 8];
      For[i = 1, i <= Length[data], i++, If[Last[data[[i]]] > largestsize, largestsize = Last[data[[i]]];]];
      Geom[i_]          := Cylinder[Partition[Take[data[[i]],6], 3], r];
      IsLargest[i_]     := (Last[data[[i]]] == largestsize);
      SizeFraction[i_]  := (Last[data[[i]]] / largestsize);
      IsPrintable[i_]   := (IsLargest[i]);
      IsAntiVortex[i_]  := (data[[i]][[7]] < 0);
      IsWn2Vortex[i_] := (Abs[Last[data[[i]]]] == 2);
      ObjectHue[i_]     := If[IsWn2Vortex[i], If[Length[hue2] > 0, hue2[[1]], 0.3],
                            If[IsAntiVortex[i], If[Length[hue1] > 0, hue1[[1]], 0.0], If[Length[hue0] > 0, hue0[[1]], 0.5]]];
      list = DeleteCases[Table[If[IsPrintable[i], {EdgeForm[None], Hue[ObjectHue[i], s, b, a], Geom[i]}, {}], {i, Length[data]}], {}];
      Graphics3D[list,Evaluate[FilterRules[{opts}, Options[Graphics3D]]]]
    ];
End[]

EndPackage[]
