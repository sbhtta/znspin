#!/usr/local/bin/MathematicaScript -script

SetDirectory["./"];
Get["ZnSpinGraphics.m"];

Dim             = 3;
Zn              = ToExpression[$ScriptCommandLine[[2]]];
Lx              = ToExpression[$ScriptCommandLine[[3]]];
SpinSat         = ToExpression[$ScriptCommandLine[[4]]];
fileout         = $ScriptCommandLine[[5]];
filespins       = $ScriptCommandLine[[6]];
filedw          = $ScriptCommandLine[[7]];
filevx          = $ScriptCommandLine[[8]];
imagelargestdw  = ZnSpinGraphicsLargestDomainWalls3D[filedw, Hue[0,0,0,0.25], PlotRange->Table[{-0.5,Lx-0.49},{Dim}], Lighting->"Neutral"];
imagelargestvx  = ZnSpinGraphicsLargestVortices3D[filevx, Hue[0.55], Hue[0.05], Hue[0.3], 0.15, PlotRange->Table[{-0.5,Lx-0.49},{Dim}],Lighting->"Neutral"];
imageslicespins = ZnSpinGraphicsSliceSpins3D[filespins, 3, Lx-1, Zn, Hue[0.1,SpinSat,1], PlotRange->Table[{-0.5,Lx-0.49},{Dim}],Lighting->"Neutral"];
composite       = Rasterize[Show[{imagelargestdw, imagelargestvx, imageslicespins}], ImageResolution->100];
Export[fileout, composite];
