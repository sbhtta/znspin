#!/usr/local/bin/MathematicaScript -script

SetDirectory["./"];
Get["ZnSpinGraphics.m"];

Lx            = ToExpression[$ScriptCommandLine[[2]]];
axis          = ToExpression[$ScriptCommandLine[[3]]];
coord         = ToExpression[$ScriptCommandLine[[4]]];
fileoutspins  = $ScriptCommandLine[[5]];
fileoutdw     = $ScriptCommandLine[[6]];
fileoutvx     = $ScriptCommandLine[[7]];
fileinspins   = $ScriptCommandLine[[8]];
fileindw      = $ScriptCommandLine[[9]];
fileinvx      = $ScriptCommandLine[[10]];

Export[fileoutspins, ZnSpinGraphicsSliceSpins3Dto2D[fileinspins, axis, coord]];
Export[fileoutdw, ZnSpinGraphicsSliceDomainWalls3Dto2D[fileindw, axis, coord]];
Export[fileoutvx, ZnSpinGraphicsSliceVortices3Dto2D[fileinvx, axis, coord]];
