#!/bin/bash

## usage:
## ./ZnSpinMxMyDist.sh oufile data.dat

gnuplot <<- EOF
  set cbrange [*:*]
  set palette defined ( \
    0 '#FFFFFF',\
    1 '#BCBDDC',\
    2 '#756BB1',\
    3 '#000000',\
    4 '#000000',\
    5 '#DE2D26',\
    6 '#FC8D59',\
    7 '#FF7F00' )

  unset colorbox
  unset cbtics
  set size square
  set xrange[-1:1]
  set yrange[-1:1]
  unset key
  unset xtics
  unset ytics
  unset border
  set tmargin 0
  set bmargin 0
  set rmargin 0
  set lmargin 0
  set terminal pngcairo size 256,256 enhanced font 'Verdana,10'
  set output "$1"
  plot "$2" u 1:2:3 w image
EOF
