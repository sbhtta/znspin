#!/usr/local/bin/MathematicaScript -script

SetDirectory["./"];
Get["ZnSpinGraphics.m"];

Dim       = 2;
Zn        = ToExpression[$ScriptCommandLine[[2]]];
Lx        = ToExpression[$ScriptCommandLine[[3]]];
SpinSat   = ToExpression[$ScriptCommandLine[[4]]];
fileout   = $ScriptCommandLine[[5]];
filespins = $ScriptCommandLine[[6]];
filedw    = $ScriptCommandLine[[7]];
filevx    = $ScriptCommandLine[[8]];
imagespins      = ZnSpinGraphicsSpins[filespins, Zn, Hue[0.1,SpinSat,1], PlotRange->Table[{-0.5,Lx-0.5},{Dim}]];
imagealldw      = ZnSpinGraphicsAllButLargestDomainWalls[filedw, Hue[0.5,0.3,0.5], PlotRange->Table[{-0.5,Lx-0.5},{Dim}]];
imagelargestdw  = ZnSpinGraphicsLargestDomainWalls[filedw, Hue[0,1,0], PlotRange->Table[{-0.5,Lx-0.5},{Dim}]];
imageallvx      = ZnSpinGraphicsVortices[filevx, Hue[0.55,1,0.8], Hue[0.05,1,0.8], Hue[0.30,1,0.8], 0.5, PlotRange->Table[{-0.5,Lx-0.5},{Dim}]];
composite       = Rasterize[Show[{imagespins, imagealldw, imagelargestdw, imageallvx}], ImageResolution->100];
Export[fileout, composite];
