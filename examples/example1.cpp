// order parameter of square lattice Ising model at different temperatures
#include <iostream>
#include <fstream>
#include <cmath>
#include <qtools/qtools.h>
#include <znspin/znspin.h>

int main()
{
  // setup one random number generator stream:
  auto rng = qtools::rng_vector(1, 12345);

  // setup Hamiltonian:
  const auto numstates = 2;
  const auto lattice   = qtools::lattice::name::square;
  const auto extfield  = znspin::external_field::off;
  const auto Esisj     = [&](const int si, const int sj, const int bond)->double
                         { return 2 * (si != sj); };
  const auto Esi       = [](const int si)->double
                         { return 0; };
  auto H = znspin::hamiltonian<numstates, lattice, extfield>(Esisj, Esi);

  // setup spin configuration:
  const int  lx     = 32;
  const int  dim    = qtools::lattice::info<lattice>::dimension;
  const auto length = qtools::uniform_array<int,dim>(lx);
  auto s = znspin::spinconfig<numstates,lattice>(length);

  // create databook:
  const int samples = 10000;
  auto db = qtools::databook<double,1>(samples);

  // create output file:
  std::ofstream outfile("example1.dat");

  for (double kT = 1.0; kT < 3.0; kT += 0.2) {
    s.fill(0);    // reset all spins to 0
    H.set_kT(kT); // update temperature

    // equilibriate:
    for (int t = 0; t < 0.1 * samples; t++) {
      znspin::metropolis_update(H, s, rng);
    }

    // record order parameter:
    db.clear();
    for (int t = 0; t < db.num_points(); t++) {
      znspin::metropolis_update(H, s, rng);
      const auto state_fraction = znspin::state_fraction(s);
      const auto magnetization  = state_fraction[1] - state_fraction[0];
      const auto orderparam     = std::abs(magnetization);
      db.add_data(orderparam);
    }  
    
    // append order parameter statistics to output file:
    outfile << kT << "\t";
    outfile << qtools::standard_mean_error(db.data());
    outfile << std::endl;
  }
  return 0;
}
