// domain wall spanning probability in the square lattice Ising model
#include <iostream>
#include <fstream>
#include <cmath>
#include <qtools/qtools.h>
#include <znspin/znspin.h>

const int N = 3;

inline int
z6state(const int s, const int x, const int y, const int z)
{
  if ((x+y+z) & 1) {
    return (2 * s + 3) % 6;
  } else {
    return 2 * s;
  }
}
int main(int argc, char** argv)
{
  // setup one random number generator stream:
  auto rng = qtools::rng_vector(1, 12345);

  // setup Hamiltonian:
  const int numstates = N;
  const auto lattice   = qtools::lattice::name::cube;
  const auto extfield  = znspin::external_field::off;
  const auto vxsupp    = znspin::vortex_suppression::off;
  const auto Esisj     = [&](const int si, const int sj, const int bond)->double
                         { 
                           switch(std::abs(znspin::state_difference<numstates>(si,sj))) {
                             case 0 : return 1;
                             case 1 : return 0;
                             case 2 : return 1e4;
                             default: return 1e4;
                           }
                         };
  const auto Esi       = [](const int si)->double
                         { return 0; };
  auto H = znspin::hamiltonian<numstates, lattice, extfield, vxsupp>(Esisj, Esi);

  // setup spin configuration:
  const int  lx     = qtools::fetch_argument_int(argc, argv, "-minLx", 32);
  const int  dim    = qtools::lattice::info<lattice>::dimension;
  const auto length = qtools::uniform_array<int,dim>(lx);
  auto s = znspin::spinconfig<numstates,lattice>(length);

    s.fill(0);    // reset all spins to 0
    qtools::lattice::info<qtools::lattice::name::cube>::for_each_vertex(s.length(),
        [&](const int x, const int y, const int z) {
          s.wrap(x+0,y+0,z+0) = (x+y+z)&1;
        }
      );
    H.set_kT(qtools::fetch_argument_double(argc, argv, "-minkT", 1.0)); // update temperature

    // equilibriate:
    for (int t = 0; t < 1000; t++) {
      znspin::metropolis_update(H, s, rng);
    }

    std::ofstream outfilespins("example11_spins.dat");
    outfilespins << s.spins();

    std::ofstream outfilez6("example11_z6spins.dat");
    qtools::lattice::info<qtools::lattice::name::cube>::for_each_vertex(s.length(),
        [&](const int x, const int y, const int z) {
          const auto s0 = z6state(s.wrap(x+0,y+0,z+0),x,y,z);
          outfilez6 << x << "\t" << y << "\t" << "\t" << z << "\t" << s0 << std::endl;
        }
      );
  return 0;
}
