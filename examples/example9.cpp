// domain wall spanning probability in the square lattice Ising model
#include <iostream>
#include <fstream>
#include <cmath>
#include <qtools/qtools.h>
#include <znspin/znspin.h>

const int N = 4;

const int k[N] = {0,2,1};

inline int
D(const int si, const int sj)
{
  const int dsisj = znspin::state_difference<N>(si,sj);
  return (dsisj > 0 ? +1 : -1) * k[std::abs(dsisj)];
}

int main(int argc, char** argv)
{
  // setup one random number generator stream:
  auto rng = qtools::rng_vector(1, 12345);

  // setup Hamiltonian:
  const int numstates = N;
  const auto lattice   = qtools::lattice::name::square;
  const auto extfield  = znspin::external_field::off;
  const auto vxsupp    = znspin::vortex_suppression::off;
  const auto Esisj     = [&](const int si, const int sj, const int bond)->double
                         { 
                           switch(std::abs(znspin::state_difference<numstates>(si,sj))) {
                             case 0 : return 0;
                             case 1 : return 0.2;
                             case 2 : return 1;
                             default: return 1e4;
                           }
                         };
  const auto Esi       = [](const int si)->double
                         { return 0; };
  auto H = znspin::hamiltonian<numstates, lattice, extfield, vxsupp>(Esisj, Esi);

  // setup spin configuration:
  const int  lx     = qtools::fetch_argument_int(argc, argv, "-minLx", 32);
  const int  dim    = qtools::lattice::info<lattice>::dimension;
  const auto length = qtools::uniform_array<int,dim>(lx);
  auto s = znspin::spinconfig<numstates,lattice>(length);

    s.fill(0);    // reset all spins to 0
    H.set_kT(qtools::fetch_argument_double(argc, argv, "-minkT", 1.0)); // update temperature

    // equilibriate:
    for (int t = 0; t < 1000; t++) {
      znspin::wolff_multicluster_update(H, s, rng);
    }
    const auto defdw = [](const int si, const int sj, const int b)
                       { return (si != sj); };
    s.template label_domain_walls<true,true,true,true>(defdw);

    std::ofstream outfilespins("example9_spins.dat");
    outfilespins << s.spins();

    std::ofstream outfiledw("example9_dw.dat");
    outfiledw << s.domain_walls(defdw);

    std::ofstream outfilevx("example9_vx.dat");
    qtools::lattice::info<qtools::lattice::name::square>::for_each_vertex(s.length(),
        [&](const int x, const int y) {
          const auto s0 = s.wrap(x+0,y+0);
          const auto s1 = s.wrap(x+1,y+0);
          const auto s2 = s.wrap(x+1,y+1);
          const auto s3 = s.wrap(x+0,y+1);
          const auto wn = D(s1,s0) + D(s2,s1) + D(s3,s2) + D(s0,s3);
          if (wn != 0) {
            outfilevx << x+0.5 << "\t" << y+0.5 << "\t" << wn << std::endl;
          }
        }
      );
  return 0;
}
