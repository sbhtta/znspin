#include <znspin/znspin.h>

int main()
{
  const int N = 6;

  for (int sD = 0; sD < N; sD++)
  for (int sC = 0; sC < N; sC++)
  for (int sB = 0; sB < N; sB++)
  for (int sA = 0; sA < N; sA++) {
    const int norml_wn = znspin::winding_number<N>(sA,sB,sC,sD);
    //const int wrong_wn = znspin::wrong_winding_number<N>(sA,sB,sC,sD);
    const int smart_wn = znspin::smart_winding_number<N>(sA,sB,sC,sD);

    if (norml_wn != smart_wn) {
      std::cout << sA << " " << sB << " " << sC << " " << sD << " " << norml_wn << " " << smart_wn << std::endl;
    }
  }
  return 0;
}
