#include <cmath>
#include <iostream>

const int N = 4;
const int k[N] = {0,2,1};

inline int
d(const int si, const int sj)
{
  if (si - sj > N) {
    return si - sj - N;
  } else if (si - sj < -N) {
    return si - sj + N;
  } else {
    return si - sj;
  }
}

inline int
D(const int si, const int sj)
{
  const int dsisj = d(si,sj);
  return (dsisj > 0 ? +1 : -1) * k[std::abs(d(si,sj))];
}

int main()
{
  int bin[8*N] = {0};
  for (int a = 0; a < N; a++)
  for (int b = 0; b < N; b++)
  for (int c = 0; c < N; c++)
  for (int d = 0; d < N; d++) {
    const int wn = D(b,a) + D(c,b) + D(d,c) + D(a,d);
    bin[4*N+wn] += 1;
  }
  for (int i = 0; i < 8*N; i++) {
    //std::cerr << i-4*N << " " << bin[i] << std::endl;
  }
  const int wn = D(2,0) + D(2,2) + D(2,2) + D(0,2);
  std::cerr << wn << std::endl; 
  return 0;
}
