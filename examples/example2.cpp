// scaling of order parameter, susceptibility, energy and specific heat
// of square lattice zero field Ising model at critical temperature
#include <iostream>
#include <fstream>
#include <cmath>
#include <qtools/qtools.h>
#include <znspin/znspin.h>

int main()
{
  // setup one random number generator stream:
  auto rng = qtools::rng_vector(1, 12345);

  // setup Hamiltonian:
  const auto numstates = 2;
  const auto lattice   = qtools::lattice::name::square;
  const auto extfield  = znspin::external_field::off;
  const auto Esisj     = [&](const int si, const int sj, const int bond)->double
                         { return 2 * (si != sj); };
  const auto Esi       = [](const int si)->double
                         { return 0; };
  const double Tc      = 2.269;
  auto H = znspin::hamiltonian<numstates, lattice, extfield>(Esisj, Esi, Tc);

  // create output file:
  std::ofstream outfile("example2.dat");

  for (int lx = 8; lx <= 256; lx *= 2) {
    // setup spin configuration:
    const int  dim    = qtools::lattice::info<lattice>::dimension;
    const auto length = qtools::uniform_array<int,dim>(lx);
    auto s = znspin::spinconfig<numstates,lattice>(length);
    s.fill(0);

    // equilibriate:
    for (int t = 0; t < 1000; t++) {
      znspin::metropolis_update(H, s, rng);
    }

    // record observables:
    const int samples = 10000;
    const int windows = 10;
    auto db = qtools::databook<double,2>(samples,windows);
    for (int t = 0; t < db.num_points(); t++) {
      znspin::metropolis_update(H, s, rng);
      const auto state_fraction = znspin::state_fraction(s);
      const auto magnetization  = state_fraction[1] - state_fraction[0];
      const auto orderparam     = std::abs(magnetization);
      const auto bfrac          = znspin::edge_fraction(s);
      const auto esisj          = znspin::energy_per_spin(H, bfrac);
      db.add_data(0,orderparam);
      db.add_data(1,esisj);
    }  
    
    // calculate observable statistics over windows:
    auto dbwin = qtools::databook<double,4>(windows);
    for (int win = 0; win < windows; win++) {
      const double mean_orderparam = qtools::mean(db.data(0,win));
      const double susceptibility  = qtools::variance(db.data(0,win))
                                       * s.num_vertices() / Tc;
      const double mean_energy     = qtools::mean(db.data(1,win));
      const double specific_heat   = qtools::variance(db.data(1,win))
                                       * s.num_vertices() / (Tc * Tc);
      dbwin.add_data(0, mean_orderparam);
      dbwin.add_data(1, susceptibility);
      dbwin.add_data(2, mean_energy);
      dbwin.add_data(3, specific_heat);
    }

    // append order parameter statistics to output file:
    outfile << std::scientific;
    outfile << lx << "\t";
    outfile << qtools::jackknife_mean_error(dbwin.data(0)) << "\t";
    outfile << qtools::jackknife_mean_error(dbwin.data(1)) << "\t";
    outfile << qtools::jackknife_mean_error(dbwin.data(2)) << "\t";
    outfile << qtools::jackknife_mean_error(dbwin.data(3)) << "\t";
    outfile << std::endl;
  }
  return 0;
}
