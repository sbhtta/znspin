// compare equilibrium behavior of the square lattice Ising model using
// Metropolis single-spin, Wolff multi-cluster and Swendsen-Wang multi-cluster
// alogrithms, order parameter curves obtained in the three cases should
// almost coincide, check using gnuplot:
// plot 'example3.dat' u 1:2:3 w yerrorbars
// repl 'example3.dat' u 1:2:3 w yerrorbars
// repl 'example3.dat' u 1:2:3 w yerrorbars
#include <iostream>
#include <fstream>
#include <cmath>
#include <qtools/qtools.h>
#include <znspin/znspin.h>

int main()
{
  // setup one random number generator stream:
  auto rng = qtools::rng_vector(1, 12345);

  // setup Hamiltonian:
  const auto numstates = 2;
  const auto lattice   = qtools::lattice::name::square;
  const auto extfield  = znspin::external_field::off;
  const auto Esisj     = [&](const int si, const int sj, const int bond)->double
                         { return 2 * (si != sj); };
  const auto Esi       = [](const int si)->double
                         { return 0; };
  auto H = znspin::hamiltonian<numstates, lattice, extfield>(Esisj, Esi);

  // setup spin configuration:
  const int  lx     = 32;
  const int  dim    = qtools::lattice::info<lattice>::dimension;
  const auto length = qtools::uniform_array<int,dim>(lx);
  auto s = znspin::spinconfig<numstates,lattice>(length);

  // create output file:
  std::ofstream outfile("example3.dat");

  for (double kT = 1.0; kT < 3.0; kT += 0.2) {
    H.set_kT(kT); // update temperature

    // create databook for three time series:
    const int samples = 10000;
    auto db = qtools::databook<double,3>(samples);

    // reset spins, equilibriate and measure order parameter for
    // Metropolis single-spin flip algorithm:
    s.fill(0);
    for (int t = 0; t < 0.1 * samples; t++) {
      znspin::metropolis_update(H, s, rng);
    }
    for (int t = 0; t < db.num_points(); t++) {
      znspin::metropolis_update(H, s, rng);
      const auto state_fraction = znspin::state_fraction(s);
      const auto magnetization  = state_fraction[1] - state_fraction[0];
      const auto orderparam     = std::abs(magnetization);
      db.add_data(0, orderparam);
    }  

    // reset spins, equilibriate and measure order parameter for
    // Wolff multi-cluster algorithm:
    s.fill(0);
    for (int t = 0; t < 0.1 * samples; t++) {
      znspin::wolff_multicluster_update(H, s, rng);
    }
    for (int t = 0; t < db.num_points(); t++) {
      znspin::wolff_multicluster_update(H, s, rng);
      const auto state_fraction = znspin::state_fraction(s);
      const auto magnetization  = state_fraction[1] - state_fraction[0];
      const auto orderparam     = std::abs(magnetization);
      db.add_data(1, orderparam);
    }  

    // reset spins, equilibriate and measure order parameter for
    // Swendsen-Wang multi-cluster algorithm:
    s.fill(0);
    for (int t = 0; t < 0.1 * samples; t++) {
      znspin::ucmsw_multicluster_update(H, s, rng);
    }
    for (int t = 0; t < db.num_points(); t++) {
      znspin::ucmsw_multicluster_update(H, s, rng);
      const auto state_fraction = znspin::state_fraction(s);
      const auto magnetization  = state_fraction[1] - state_fraction[0];
      const auto orderparam     = std::abs(magnetization);
      db.add_data(2, orderparam);
    }  
    
    // append order parameter statistics to output file:
    outfile << kT << "\t";
    outfile << qtools::jackknife_mean_error(db.data(0)) << "\t";
    outfile << qtools::jackknife_mean_error(db.data(1)) << "\t";
    outfile << qtools::jackknife_mean_error(db.data(2)) << "\t";
    outfile << std::endl;
  }
  return 0;
}
