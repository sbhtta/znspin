// order parameter distribution of a square lattice 3-state Potts model
// in the ordered phase and the disordered phase
// check result in gnuplot as:
// set size square
// plot 'example6_kT_0.800000.dat' u 1:2:3 w image
// plot 'example6_kT_1.300000.dat' u 1:2:3 w image
#include <iostream>
#include <fstream>
#include <cmath>
#include <complex>
#include <qtools/qtools.h>
#include <znspin/znspin.h>

int main()
{
  // setup one random number generator stream:
  auto rng = qtools::rng_vector(1, 12345);

  // setup Hamiltonian:
  const auto numstates = 3;
  const auto lattice   = qtools::lattice::name::square;
  const auto extfield  = znspin::external_field::off;
  const auto Esisj     = [&](const int si, const int sj, const int bond)->double
                         { return (si != sj); };
  const auto Esi       = [](const int si)->double
                         { return 0; };
  auto H = znspin::hamiltonian<numstates, lattice, extfield>(Esisj, Esi);

  // setup spin configuration:
  const int  lx     = 32;
  const int  dim    = qtools::lattice::info<lattice>::dimension;
  const auto length = qtools::uniform_array<int,dim>(lx);
  auto s = znspin::spinconfig<numstates,lattice>(length);

  for (double kT = 0.8; kT < 1.5; kT += 0.5) {
    // reset all spins:
    s.fill(0);

    // equilibriate:
    H.set_kT(kT);
    for (int t = 0; t < 1000; t++) {
      znspin::metropolis_update(H, s, rng);
    }

    // record correlation:
    const int samples = 10000;
    auto hist = qtools::histogram<2>({{-1,+1,0.01}},{{-1,+1,0.01}});
    for (int t = 0; t < samples; t++) {
      znspin::metropolis_update(H, s, rng);
      const auto state_fraction = znspin::state_fraction(s);
      const auto orderparam     = znspin::order_parameter_Zn(state_fraction);
      hist.add_data(orderparam.real(), orderparam.imag());
    }  
    
    // print distribution to file:
    std::ofstream outfile(std::string("example6_kT_") +
                          std::to_string(kT) +
                          std::string(".dat"));
    if (outfile.is_open()) {
      outfile << std::scientific;
      outfile << hist;
      outfile.close();
    }
  }
  return 0;
}
