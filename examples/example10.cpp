// domain wall spanning probability in the square lattice Ising model
#include <iostream>
#include <fstream>
#include <cmath>
#include <qtools/qtools.h>
#include <znspin/znspin.h>

const int N = 3;

inline int
z6state(const int s, const int x, const int y)
{
  if ((x+y) & 1) {
    return (2 * s + 3) % 6;
  } else {
    return 2 * s;
  }
}
int main(int argc, char** argv)
{
  // setup one random number generator stream:
  auto rng = qtools::rng_vector(1, 12345);

  // setup Hamiltonian:
  const int numstates = N;
  const auto lattice   = qtools::lattice::name::square;
  const auto extfield  = znspin::external_field::off;
  const auto vxsupp    = znspin::vortex_suppression::off;
  const auto Esisj     = [&](const int si, const int sj, const int bond)->double
                         { 
                           switch(std::abs(znspin::state_difference<numstates>(si,sj))) {
                             case 0 : return 1;
                             case 1 : return 0;
                             case 2 : return 1e4;
                             default: return 1e4;
                           }
                         };
  const auto Esi       = [](const int si)->double
                         { return 0; };
  auto H = znspin::hamiltonian<numstates, lattice, extfield, vxsupp>(Esisj, Esi);

  // setup spin configuration:
  const int  lx     = qtools::fetch_argument_int(argc, argv, "-minLx", 32);
  const int  dim    = qtools::lattice::info<lattice>::dimension;
  const auto length = qtools::uniform_array<int,dim>(lx);
  auto s = znspin::spinconfig<numstates,lattice>(length);

    s.fill(0);    // reset all spins to 0
    H.set_kT(qtools::fetch_argument_double(argc, argv, "-minkT", 1.0)); // update temperature

    const int samples = qtools::fetch_argument_int(argc, argv,"-nSM",1000);

    // equilibriate:
    for (int t = 0; t < 0.1 * samples; t++) {
      znspin::wolff_multicluster_update(H, s, rng);
    }

    auto h   = qtools::histogram<2>({{-1,+1,0.01}},{{-1,+1,0.01}});

  const double dphi  = 2. * M_PI / (1. * N);

  for (int t = 0; t < samples; t++) {
    for (int i = 0; i < 1; i++) {
      znspin::wolff_multicluster_update(H, s, rng);
    }
    std::array<int,6> num = {{0}};
    qtools::lattice::info<qtools::lattice::name::square>::for_each_vertex(s.length(),
        [&](const int x, const int y) {
          const auto s0n = z6state(s.wrap(x+0,y+0),x,y);
          num[s0n] += 1;
        }
      );
  std::array<double,6> sfrac = {{0}};
  for (int i = 0; i < 6; i++) {
    sfrac[i] = num[i] / (1. * s.num_vertices());
  }
    const auto znmag  = znspin::order_parameter_Zn(sfrac);
    const auto modm   = std::abs(znmag);
    const auto phi    = std::arg(znmag);
    const auto wphi   = std::fmod(phi + 2. * M_PI, dphi);
    for (int si = 0; si < (int)N; si++) {
      for (int sign : {-1,+1}) {
        const double mx = modm * std::cos(sign * wphi + dphi * si);
        const double my = modm * std::sin(sign * wphi + dphi * si);
        h.add_data(mx, my);
      }
    }
  }

  std::ofstream outfile("example10_mxmydist.dat");
  outfile << h;

    std::ofstream outfilespins("example10_spins.dat");
    outfilespins << s.spins();

    std::ofstream outfilevx("example10_vx.dat");
    std::ofstream outfilevxm("example10_vxm.dat");
    std::ofstream outfilez6("example10_z6spins.dat");
    qtools::lattice::info<qtools::lattice::name::square>::for_each_vertex(s.length(),
        [&](const int x, const int y) {
          const auto s0 = (s.wrap(x+0,y+0));
          const auto s1 = (s.wrap(x+1,y+0));
          const auto s2 = (s.wrap(x+1,y+1));
          const auto s3 = (s.wrap(x+0,y+1));
          const auto s0n = z6state(s.wrap(x+0,y+0),x,y);
          const auto s1n = z6state(s.wrap(x+1,y+0),x,y);
          const auto s2n = z6state(s.wrap(x+1,y+1),x,y);
          const auto s3n = z6state(s.wrap(x+0,y+1),x,y);
          const auto wn = znspin::winding_number<6>(s0n,s1n,s2n,s3n);
          const auto wnm = znspin::state_difference<N>(s1,s0) +
                           znspin::state_difference<N>(s2,s1) +
                           znspin::state_difference<N>(s3,s2) +
                           znspin::state_difference<N>(s0,s3);
          if ((wn == 0) != (wnm == 0)) {std::cerr << "ERROR!" << std::endl;}
          if (wn != 0) {
            outfilevx << x+0.5 << "\t" << y+0.5 << "\t" << wn << std::endl;
          }
          outfilez6 << x << "\t" << y << "\t" << s0n << std::endl;
        }
      );
  return 0;
}
