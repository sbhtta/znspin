#include <znspin/znspin.h>

int main(int argc, char** argv)
{
  const auto N          = 4;
  const auto LatNm      = qtools::lattice::name::square;
  const auto ExtFld     = znspin::external_field::off;
  const auto VxSupp     = znspin::vortex_suppression::on;
  const auto VxType     = znspin::vortex_type::standard;
  const auto Esisj      = [&](const int si, const int sj, const int bond)->double
                          {
                            //return (si != sj);
                            //return -std::cos(znspin::state_angle<N>(si) - znspin::state_angle<N>(sj));
                            switch(std::abs(znspin::state_difference<N,true>(si, sj))) {
                              case 0 : return 0.;
                              case 1 : return 2.;
                              case 2 : return 1.;
                              case 3 : return 1e4;
                              case 4 : return 1e4;
                              default: return 1e4;
                            }
                          };
  const auto Esi        = [](const int si)->double{ return 0.; };
  const auto H          = znspin::hamiltonian<N, LatNm, ExtFld, VxSupp, VxType>(Esisj, Esi);

  std::cout << "eff(0 - 2) : " << H.effective_state_difference<false>(0,2) << std::endl;
  std::cout << "eff(2 - 0) : " << H.effective_state_difference<false>(2,0) << std::endl;
  std::cout << H.effective_winding_number<znspin::vortex_type::nonchiral>(0,0,0,0) << std::endl;
  std::cout << H.effective_winding_number<znspin::vortex_type::nonchiral>(0,2,0,2) << std::endl;
  std::cout << H.effective_winding_number<znspin::vortex_type::standard>(0,1,1,1) << std::endl;
  return 0;
}
