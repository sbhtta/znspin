#include <znspin/znspin.h>

int main(int argc, char** argv)
{
  const znspin::parameters  params(argc, argv);

  for (double lm3 =  params.minlm3; lm3 <   params.maxlm3; lm3 +=  params.inclm3)
  for (int    Lx =  params.minLx; Lx <=  params.maxLx; Lx +=  params.incLx) {
  for (double kT =  params.minkT; kT <   params.maxkT; kT +=  params.inckT) {
  for (double lm =  params.minlm; lm <   params.maxlm; lm +=  params.inclm) {
    const auto N          = 6;
    const auto LatNm      = qtools::lattice::name::cube;
    const auto ExtFld     = znspin::external_field::off;
    const auto VxSupp     = znspin::vortex_suppression::on;
    const auto VxType     = znspin::vortex_type::standard;
    const auto Z3VxSupp     = znspin::vortex_suppression::on;
    const auto Z3VxType     = znspin::vortex_type::standard;
    const auto Esisj      = [&](const int si, const int sj, const int bond)->double
                            {
                              //return (si != sj);
                              //return -std::cos(znspin::state_angle<N>(si) - znspin::state_angle<N>(sj));
                              switch(std::abs(znspin::state_difference<N,true>(si, sj))) {
                               case 0 : return 0.;
                               case 1 : return 0.5;
                               case 2 : return 1.5;
                               case 3 : return 2.;
                               case 4 : return 1e4;
                               default: return 1e4;
                              }
                            };
    const auto Esi        = [](const int si)->double{ return 0.; };
    const auto H          = znspin::hamiltonian<N, LatNm, ExtFld, VxSupp, VxType, Z3VxSupp, Z3VxType>(Esisj, Esi, kT, 0., lm, lm3);
    const auto length     = qtools::uniform_array<int,qtools::lattice::info<LatNm>::dimension>(Lx);
    const auto ppointname = params.ppointname_Lx_kT_lm(Lx, kT, lm) + std::string("_lm3_") + std::to_string(lm3);
    const auto ppackname  = params.ppackname_Lx_kT_lm(Lx, kT, lm) + std::string("_lm3_") + std::to_string(lm3);
    const auto appparams  = [&](std::ostream& output)->std::ostream&
                            { output << lm << "\t" << kT << "\t" << Lx << "\t"; return output; };

    if ( params.gentpictures)  znspin::gentpictures(params, H, length, ppointname);

    if ( params.gentmxmydist)  znspin::gentmxmydist(params, H, length, ppointname);
    if ( params.avgpmxmydist)  znspin::avgpmxmydist(params, H, length, ppointname);

    if ( params.gentspincorr)  znspin::gentspincorr(params, H, length, ppointname);
    if ( params.avgpspincorr)  znspin::avgpspincorr(params, H, length, ppointname);

    if ( params.gentdwvxdist)  znspin::gentdwvxdist(params, H, length, ppointname);
    if ( params.avgpdwvxdist)  znspin::avgpdwvxdist(params, H, length, ppointname);
    if ( params.packdwvxdist)  znspin::packdwvxdist(params, H, length, ppointname, ppackname, appparams);
 
    if ( params.gentobsvlist)  znspin::gentobsvlist(params, H, length, ppointname);
    if ( params.packobsvlist)  znspin::packobsvlist(params, H, length, ppointname, ppackname, appparams);

    if ( params.avgpobsvcorr)  znspin::avgpobsvcorr(params, H, length, ppointname);
    if ( params.packobsvcorr)  znspin::packobsvcorr(params, H, length, ppointname, ppackname, appparams);

  }}}
  return 0;
}
