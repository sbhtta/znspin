// two-point correlations of square lattice zero field Ising model
// at critical temperature
#include <iostream>
#include <fstream>
#include <cmath>
#include <qtools/qtools.h>
#include <znspin/znspin.h>

int main()
{
  // setup one random number generator stream:
  auto rng = qtools::rng_vector(1, 12345);

  // setup Hamiltonian:
  const auto numstates = 2;
  const auto lattice   = qtools::lattice::name::square;
  const auto extfield  = znspin::external_field::off;
  const auto Esisj     = [&](const int si, const int sj, const int bond)->double
                         { return 2 * (si != sj); };
  const auto Esi       = [](const int si)->double
                         { return 0; };
  const double Tc      = 2.269;
  auto H = znspin::hamiltonian<numstates, lattice, extfield>(Esisj, Esi, Tc);

  for (int lx = 8; lx <= 256; lx *= 2) {
    // setup spin configuration:
    const int  dim    = qtools::lattice::info<lattice>::dimension;
    const auto length = qtools::uniform_array<int,dim>(lx);
    auto s = znspin::spinconfig<numstates,lattice>(length);
    s.fill(0);

    // equilibriate:
    for (int t = 0; t < 1000; t++) {
      znspin::metropolis_update(H, s, rng);
    }

    // record correlation:
    const int samples = 10000;
    auto B = znspin::twopoint_statebin<numstates,lattice>(length);
    auto C = znspin::correlator<lattice>(length);
    const auto CFunc = [](const int si, const int sj)
                       { return (2 * si - 1) * (2 * sj - 1); };
    for (int t = 0; t < samples; t++) {
      znspin::metropolis_update(H, s, rng);
      B.add_data(s);
      C.add_data(s, CFunc);
    }  

    // print two-point correlation file from two point state bin:
    {
      std::ofstream outfile(std::string("example5_L_") +
                            std::to_string(lx) +
                            std::string("_B.dat"));
      if (outfile.is_open()) {
        outfile << std::scientific;
        for (int i = 0; i < B.size(); i++) {
          double cvalue = 0.;
          for (int si = 0; si < numstates; si++) {
            for (int sj= 0; sj < numstates; sj++) {
              cvalue += B(i, si, sj) * CFunc(si, sj);
            }
          }
          outfile << B.distance(i) << "\t" << cvalue << std::endl;
        }
        outfile.close();
      }    
    }

    // print two-point correlation file from correlator:
    {
      std::ofstream outfile(std::string("example5_L_") +
                            std::to_string(lx) +
                            std::string("_C.dat"));
      if (outfile.is_open()) {
        outfile << std::scientific;
        outfile << C;
        outfile.close();
      }
    }
  }
  return 0;
}
