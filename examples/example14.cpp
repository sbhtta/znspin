#include <iostream>
#include <znspin/znspin.h>

int main()
{
  const int N    = 4;
  const int Neff = 4;
    const auto LatNm      = qtools::lattice::name::square;
    const auto ExtFld     = znspin::external_field::off;
    const auto VxSupp     = znspin::vortex_suppression::on;
    const auto Esisj      = [&](const int si, const int sj, const int bond)->double
                            {
                              //return -std::cos(znspin::state_angle<N>(si) - znspin::state_angle<N>(sj));
                              switch(std::abs(znspin::state_difference<N>(si, sj))) {
                              case 0 : return 1.;
                              case 1 : return 0.;
                              case 2 : return 2;
                              case 3 : return 3;
                              case 4 : return 1e4;
                              default: return 1e4;
                              }
                            };
    const auto Esi        = [](const int si)->double{ return 0.; };
    const auto H          = znspin::hamiltonian<N, LatNm, ExtFld, VxSupp>(Esisj, Esi);

  
  for (int sA = 0; sA < N; sA+=1) {
    for (int sB = 0; sB < N; sB+=1) {
      for (int sC = 0; sC < N; sC+=1) {
        for (int sD = 0; sD < N; sD+=1) {
          const int subsA = znspin::sublattice_state<N>(sA,0,0);
          const int subsB = znspin::sublattice_state<N>(sB,1,0);
          const int subsC = znspin::sublattice_state<N>(sC,1,1);
          const int subsD = znspin::sublattice_state<N>(sD,0,1);
          const int wnsubs = znspin::winding_number<Neff>(subsA,subsB,subsC,subsD);
          const int wneff  = H.effective_winding_number(sA,sB,sC,sD);
          if (wnsubs != wneff) {
            std::cout << sA << " " << sB << " " << sC << " " << sD << "\t" << wnsubs << "\t" << wneff << std::endl;
          }
        }
      }
    }
  }
  return 0;
}
