// domain wall spanning probability in the square lattice Ising model
#include <iostream>
#include <fstream>
#include <cmath>
#include <qtools/qtools.h>
#include <znspin/znspin.h>

int main()
{
  // setup one random number generator stream:
  auto rng = qtools::rng_vector(1, 12345);

  // setup Hamiltonian:
  const auto numstates = 2;
  const auto lattice   = qtools::lattice::name::square;
  const auto extfield  = znspin::external_field::off;
  const auto Esisj     = [&](const int si, const int sj, const int bond)->double
                         { return 2 * (si != sj); };
  const auto Esi       = [](const int si)->double
                         { return 0; };
  auto H = znspin::hamiltonian<numstates, lattice, extfield>(Esisj, Esi);

  // setup spin configuration:
  const int  lx     = 32;
  const int  dim    = qtools::lattice::info<lattice>::dimension;
  const auto length = qtools::uniform_array<int,dim>(lx);
  auto s = znspin::spinconfig<numstates,lattice>(length);

  // create databook:
  const int samples = 10000;
  auto db = qtools::databook<double,1>(samples);

  // create output file:
  std::ofstream outfile("example7.dat");

  for (double kT = 2.0; kT < 3.0; kT += 0.2) {
    s.fill(0);    // reset all spins to 0
    H.set_kT(kT); // update temperature

    // equilibriate:
    for (int t = 0; t < 0.1 * samples; t++) {
      znspin::metropolis_update(H, s, rng);
    }

    // record spanning time series:
    db.clear();
    for (int t = 0; t < db.num_points(); t++) {
      znspin::metropolis_update(H, s, rng);
      const auto defdw = [](const int si, const int sj, const int b)
                         { return (si != sj); };
      s.template label_domain_walls<true,true,true,true>(defdw);
      db.add_data(s.domain_walls_are_spanning());
    }  
    
    // append order parameter statistics to output file:
    outfile << kT << "\t";
    outfile << qtools::standard_mean_error(db.data());
    outfile << std::endl;
  }
  return 0;
}
