// two-point correlations of square lattice zero field Ising model
// at critical temperature
#include <iostream>
#include <fstream>
#include <cmath>
#include <qtools/qtools.h>
#include <znspin/znspin.h>

int main(int argc, char** argv)
{
  // setup one random number generator stream:
const znspin::parameters  params(argc, argv);
  auto rng = qtools::rng_vector(1, params.procseed);

  // setup Hamiltonian:
  const int  N         = 31;
  const auto numstates = N;
  const auto lattice   = qtools::lattice::name::square;
  const auto extfield  = znspin::external_field::off;
  const auto Esisj     = [&](const int si, const int sj, const int bond)->double
                         { return -std::cos(znspin::state_angle<N>(si) - znspin::state_angle<N>(sj)); };
  const auto Esi       = [](const int si)->double
                         { return 0; };
  auto H = znspin::hamiltonian<numstates, lattice, extfield, znspin::vortex_suppression::on>(Esisj, Esi);
  
  const int lx = 128;
    // setup spin configuration:
    const int  dim    = qtools::lattice::info<lattice>::dimension;
    const auto length = qtools::uniform_array<int,dim>(lx);
    auto s = znspin::spinconfig<numstates,lattice>(length);

    s.fill(0);
    for (int i = 0; i < s.num_vertices(); i++) {
      s[i] = qtools::rng_uniform(rng[0]) * N;
    }

    H.set_kT(3.0);
    H.set_lambda(-5.0);

    // equilibriate:
    for (int t = 0; t < 10000; t++) {
      znspin::metropolis_update(H, s, rng, true);
    }

    std::ofstream out("example13.dat");
    out << s.vortices();

    // record correlation:
    const auto sf = znspin::sublattice_vortex_fraction(s);
    //std::cerr << sf[0] << " " << sf[1] << std::endl;
  
  return 0;
}
